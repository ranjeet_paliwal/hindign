/* eslint-disable no-case-declarations */
/* eslint-disable no-use-before-define */

import { actionTypes } from '../../actions/photoshow/index';

function getMetaData(photoshowData) {
  // const keywords =
  //   photoshowData && photoshowData.keywords && Array.isArray(photoshowData.keywords)
  //     ? photoshowData.keywords.map(item => item.hl)
  //     : '';

  const noFollowMeta = {
    name: 'ROBOTS',
    content: 'NOINDEX, NOFOLLOW',
  };

  const feedMeta = (photoshowData && photoshowData.pwa_meta) || null;

  const metaTags = [
    {
      name: 'description',
      content: (feedMeta && feedMeta.desc) || '',
    },
    {
      name: 'keywords',
      content: (feedMeta && feedMeta.key) || '',
    },
    {
      name: 'news_keywords',
      content: (feedMeta && feedMeta.key) || '',
    },
    // Not in feed
    {
      property: 'article:published_time',
      content: feedMeta.publishtime || '',
    },
    // Not in feed

    {
      property: 'article:modified_time',
      content: feedMeta.modified || '',
    },
    // Not in feed

    {
      property: 'og:updated_time',
      content: feedMeta.modified || '',
    },
    // Not in feed

    {
      property: 'og:title',
      content: (photoshowData.it && photoshowData.it.hl) || '',
    },
    // Not in feed

    {
      property: 'og:description',
      content: feedMeta.desc || '',
    },

    {
      property: 'og:url',
      content: photoshowData.wu || '',
    },

    {
      property: 'og:image',
      content: feedMeta.ogimg || '',
    },
    {
      property: 'twitter:title',
      content: (photoshowData.it && photoshowData.it.hl) || '',
    },
    {
      property: 'twitter:description',
      content: feedMeta.desc || '',
    },
    {
      property: 'twitter:url',
      content: photoshowData.wu || '',
    },
  ];

  const metaLinks = [
    {
      rel: 'canonical',
      href: feedMeta.canonical || '',
    },
  ];
  // Not in feed
  if (feedMeta && feedMeta.noindex === 1) {
    metaTags.push(noFollowMeta);
  }

  // Not in feed
  if (photoshowData && feedMeta && feedMeta.amphtml && feedMeta.noindex != 1) {
    const ampMeta = {
      rel: 'amphtml',
      href: feedMeta.amphtml,
    };
    metaLinks.push(ampMeta);
  }

  // console.log('Metaaaa ', metaTags);

  return {
    // keywords, ( not in feed)
    metaTags,
    metaLinks,
  };
}

const initialState = {
  data: null,
  nextDataFetchError: false,
  isFetchingNextdata: false,
  isFetching: false,
  fetchError: false,
};

function photoshow(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_PHOTOSHOW_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
        fetchError: false,
      };

    case actionTypes.FETCH_PHOTOSHOW_SUCCESS:
      // console.log('PL Action, ', action);
      const data = action.payload;
      const { photoData, adsData, photoshowRightData, urlMsid } = data;
      const dynamicMeta = photoData ? getMetaData(photoData) : '';

      const layoutTheme =
        (photoData && photoData.pwa_meta && photoData.pwa_meta.templatelayout) || '';

      let photoDataArray =
        photoData && photoData.items && Array.isArray(photoData.items) ? photoData.items : [];

      // To slice array in case of layout = Layout1
      // and url on browser doesnt match first one in items.
      let shiftBy = 0;
      const firstpicUrl = (photoDataArray[0] && photoDataArray[0].wu) || null;
      if (layoutTheme === 'Layout1') {
        const elemIndex = photoDataArray.findIndex(pData => pData.id === urlMsid);
        if (elemIndex !== -1) {
          photoDataArray = photoDataArray.slice(elemIndex);
          shiftBy = elemIndex;
        }
      }

      photoDataArray = photoDataArray.map(pItem => ({
        ...pItem,
        ibeat: (photoData && photoData.pwa_meta && photoData.pwa_meta.ibeat) || {},
      }));

      const newPhotoData = { ...photoData, items: photoDataArray, shiftBy, firstpicUrl };

      const allData = {
        photoshowData: newPhotoData,
        photoshowRightData,
        combinedPhotoArray: photoDataArray,
        nextPhotoArticlesData: [],
        adsData,
        metaData: dynamicMeta,
        quicklinksData: null,
      };

      return { ...state, data: allData, isFetching: false, fetchError: false };

    case actionTypes.FETCH_PHOTOSHOW_FAILURE:
      return { ...state, data: null, isFetching: false, fetchError: true };

    case actionTypes.UPDATE_PHOTOSHOW_REQUEST:
      return { ...state, isFetchingNextdata: true };

    case actionTypes.UPDATE_PHOTOSHOW_SUCCESS: {
      const { nextPhotoArticlesData } = state.data;
      const nextArticleData = action.payload;
      // Add the new photos fetched into the array.
      let newPhotosArray =
        nextArticleData && nextArticleData.items && Array.isArray(nextArticleData.items)
          ? nextArticleData.items
          : [];

      newPhotosArray = newPhotosArray.map(pItem => ({
        ...pItem,
        ibeat:
          (nextArticleData && nextArticleData.pwa_meta && nextArticleData.pwa_meta.ibeat) || {},
      }));

      const newData = {
        ...state.data,
        nextPhotoArticlesData: [...nextPhotoArticlesData, nextArticleData],
        combinedPhotoArray: [...state.data.combinedPhotoArray, ...newPhotosArray],
      };

      return { ...state, data: newData, isFetchingNextdata: false };
    }

    case actionTypes.UPDATE_PHOTOSHOW_FAILURE:
      return { ...state, isFetchingNextdata: false, nextDataFetchError: true };

    default:
      return state;
  }
}

export default photoshow;
