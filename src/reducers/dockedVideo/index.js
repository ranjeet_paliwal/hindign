/* eslint-disable no-case-declarations */
import { PLAY_DOCKED_VIDEO, STOP_DOCKED_VIDEO } from '../../actions/video';

const initialState = {
  videoData: null,
  articleID: '',
  isPlaying: false,
  isDocked: false,
};
function dockedVideo(state = initialState, action) {
  switch (action.type) {
    case PLAY_DOCKED_VIDEO:
      return {
        ...state,
        videoData: action.payload.videoData,
        articleID: action.payload.articleID,
        isPlaying: true,
        isDocked: true,
      };

    case STOP_DOCKED_VIDEO:
      return { videoData: null, isPlaying: false, isDocked: false, articleID: '' };

    default:
      return state;
  }
}
export default dockedVideo;
