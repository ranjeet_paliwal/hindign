import {
  FETCH_GADGETLIST_REQUEST,
  FETCH_GADGETLIST_SUCCESS,
  FETCH_GADGETLIST_FAILURE,
  FETCH_NEXT_GADGETLIST_REQUEST,
  FETCH_NEXT_GADGETLIST_SUCCESS,
  FETCH_NEXT_GADGETLIST_FAILURE,
} from '../../actions/gadgetlist/gadgetlist';

function gadgetlist(
  state = {
    isFetching: false,
    error: false,
    value: [],
    head: {},
    seriesid: '',
    gadgetLatestData: null,
    gadgetAdsData: null,
    msid: '', // Usage: If a user click on multiple articlelist rapidly(from Nav) and if data from feed1(which user clicks at first) comes late as compared to feed3 then feed3 data should be shown
  },
  action,
) {
  switch (action.type) {
    case FETCH_GADGETLIST_REQUEST:
      state.item = [];
      return {
        ...state,
        gadgetLatestData: null,
        gadgetAdsData: null,
        isFetching: true,
        error: false,
      };
    case FETCH_GADGETLIST_SUCCESS:
      if (action.payload) {
        state.item[0] = action.payload.gadgetListData;
        state.category = action.category;
      }
      return {
        ...state,
        gadgetLatestData: action.payload.gadgetLatestData,
        gadgetAdsData: action.payload.gadgetAdsData,
        isFetching: false,
        error: false,
      };

    case FETCH_GADGETLIST_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true,
      };

    case FETCH_NEXT_GADGETLIST_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: false,
      };

    case FETCH_NEXT_GADGETLIST_SUCCESS:
      // && action.payload.gadgets && action.payload.gadgets.length > 0
      if (typeof state.item[0] !== 'undefined') {
        state.item[action.curpg - 1] = action.payload;
      }
      return {
        ...state,
        isFetching: false,
      };

    default:
      return state;
  }
}

export default gadgetlist;
