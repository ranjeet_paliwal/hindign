/* eslint-disable no-use-before-define */
import { actionTypes } from '../../../actions/comparision/Trends/index';

const initialState = {
  data: null,
  isFetching: false,
};
function getMetaData(pwaMeta) {
  const keywords = [];
  const title = pwaMeta.title;

  const metaTags = [
    {
      name: 'description',
      content: pwaMeta && pwaMeta.desc,
    },
    {
      name: 'keywords',
      content: (pwaMeta && pwaMeta.keywords) || '',
    },

    {
      property: 'og:title',
      content: (pwaMeta && pwaMeta.title) || '',
    },
    {
      property: 'og:description',
      content: pwaMeta && pwaMeta.desc,
    },
    {
      property: 'og:url',
      content: (pwaMeta && pwaMeta.canonical) || '',
    },
    {
      property: 'og:image',
      content: (pwaMeta && pwaMeta.ogimg) || '',
    },
    {
      property: 'twitter:title',
      content: (pwaMeta && pwaMeta.title) || '',
    },
    {
      property: 'twitter:description',
      content: pwaMeta && pwaMeta.desc,
    },
    {
      property: 'twitter:url',
      content: (pwaMeta && pwaMeta.canonical) || '',
    },
  ];

  const metaLinks = [
    {
      rel: 'canonical',
      href: (pwaMeta && pwaMeta.canonical) || '',
    },
  ];

  // console.log('Metaaaa ', metaTags);

  return { title, keywords, metaTags, metaLinks };
}

function trendsComparision(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_GADGET_TRENDS_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_GADGET_TRENDS_SUCCESS: {
      // console.log('AL Action, ', action);
      const data = action.payload;
      // const compareData = (data && data.compareData) || '';
      const pwaMeta = (data && data.compareData && data.compareData.pwa_meta) || '';
      const metaData = getMetaData(pwaMeta);
      const allData = {
        listData: data,
        metaData,
      };
      return { ...state, data: allData, isFetching: false };
    }

    case actionTypes.UPDATE_GADGET_TRENDS_SUCCESS: {
      const updatedData = { ...state };
      updatedData.data.listData.compareData = action.payload;
      return { ...state, updatedData, isFetching: false };
    }

    default:
      return state;
  }
}

export default trendsComparision;
