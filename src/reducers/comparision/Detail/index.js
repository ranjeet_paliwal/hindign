/* eslint-disable no-use-before-define */
import { actionTypes } from '../../../actions/comparision/Detail/index';

const initialState = {
  data: null,
  isFetching: false,
};

function getMetaData(pwaMeta) {
  const keywords = [];
  const title = pwaMeta.title;

  const noFollow = {
    name: 'robots',
    content: 'noindex, nofollow',
  };
  const metaTags = [
    {
      name: 'description',
      content: pwaMeta && pwaMeta.desc,
    },
    {
      name: 'keywords',
      content: (pwaMeta && pwaMeta.keywords) || '',
    },

    {
      property: 'og:title',
      content: (pwaMeta && pwaMeta.title) || '',
    },
    {
      property: 'og:description',
      content: pwaMeta && pwaMeta.desc,
    },
    {
      property: 'og:url',
      content: (pwaMeta && pwaMeta.canonical) || '',
    },
    {
      property: 'og:image',
      content: (pwaMeta && pwaMeta.ogimg) || '',
    },
    {
      property: 'twitter:title',
      content: (pwaMeta && pwaMeta.title) || '',
    },
    {
      property: 'twitter:description',
      content: pwaMeta && pwaMeta.desc,
    },
    {
      property: 'twitter:url',
      content: (pwaMeta && pwaMeta.canonical) || '',
    },
  ];

  if (pwaMeta && pwaMeta.noindex == 1) {
    metaTags.push(noFollow);
  }

  const metaLinks = [
    {
      rel: 'canonical',
      href: (pwaMeta && pwaMeta.canonical) || '',
    },
  ];

  // console.log('Metaaaa ', metaTags);

  return { title, keywords, metaTags, metaLinks };
}

function comparisionDetail(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_GADGET_DETAIL_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_GADGET_DETAIL_SUCCESS: {
      // console.log('AL Action, ', action);
      const data = { ...action.payload };

      const pwaMeta = (data && data.compareData && data.compareData.pwa_meta) || '';
      const metaData = getMetaData(pwaMeta);
      data.metaData = metaData;

      return { ...state, data, isFetching: false };
    }

    case actionTypes.ADD_GADGET_DETAIL_SUCCESS: {
      const updatedData = Object.assign({}, state);
      updatedData.data.compareData = action.payload;
      return { ...state, updatedData, isFetching: false };
    }

    case actionTypes.REMOVE_GADGET_DETAIL_SUCCESS: {
      const updatedData = Object.assign({}, state);
      updatedData.data.compareData = action.payload;
      return { ...state, updatedData, isFetching: false };
    }

    default:
      return state;
  }
}

export default comparisionDetail;
