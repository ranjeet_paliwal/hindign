/* eslint-disable no-case-declarations */
import { actionTypes } from '../../actions/videoshow/index';

function getMetaData(videoData) {
  const keywords =
    videoData && videoData.keywords && Array.isArray(videoData.keywords)
      ? videoData.keywords.map(item => item.hl)
      : '';

  const noFollowMeta = {
    name: 'ROBOTS',
    content: 'NOINDEX, NOFOLLOW',
  };

  const feedMeta = (videoData && videoData.pwa_meta) || null;

  const metaTags = [
    {
      name: 'description',
      content: videoData && videoData.pwa_meta && videoData.pwa_meta.desc,
    },
    {
      name: 'keywords',
      content: videoData && videoData.pwa_meta && videoData.pwa_meta.key,
    },
    {
      name: 'news_keywords',
      content: videoData && videoData.pwa_meta && videoData.pwa_meta.key,
    },
    {
      property: 'article:published_time',
      content: videoData.dlseo,
    },
    {
      property: 'article:modified_time',
      content: videoData.luseo,
    },
    {
      property: 'og:updated_time',
      content: videoData.luseo,
    },
    {
      property: 'og:title',
      content: videoData && videoData.items && videoData.items[0] && videoData.items[0].hl,
    },
    {
      property: 'og:description',
      content: videoData.pwa_meta.desc,
    },
    {
      property: 'og:url',
      content: videoData.wu,
    },
    {
      property: 'og:image',
      content: videoData.pwa_meta.ogimg,
    },
    {
      property: 'twitter:title',
      content: videoData && videoData.items && videoData.items[0] && videoData.items[0].hl,
    },
    {
      property: 'twitter:description',
      content: videoData.pwa_meta.desc,
    },
    {
      property: 'twitter:url',
      content: videoData.wu,
    },
  ];

  const metaLinks = [
    {
      rel: 'canonical',
      href: videoData.pwa_meta.canonical,
    },
  ];

  if (feedMeta && feedMeta.noindex == 1) {
    metaTags.push(noFollowMeta);
  }

  if (feedMeta && feedMeta.nextItem && feedMeta.nextItem.url) {
    metaLinks.push({
      rel: 'next',
      href: feedMeta.nextItem.url,
    });
  }
  if (feedMeta.prevItem && feedMeta.prevItem.url) {
    metaLinks.push({
      rel: 'prev',
      href: feedMeta.prevItem.url,
    });
  }

  if (
    videoData &&
    videoData.pwa_meta &&
    videoData.pwa_meta.amphtml &&
    videoData.pwa_meta.noindex != 1
  ) {
    const ampMeta = {
      rel: 'amphtml',
      href: videoData.pwa_meta.amphtml,
    };
    metaLinks.push(ampMeta);
  }

  return { keywords, metaTags, metaLinks };
}
const initialState = {
  data: null,
  isFetching: false,
  isLoadingNextVideo: false,
};

function videoshow(state = initialState, action) {
  switch (action.type) {
    case actionTypes.UPDATE_VIDEOSHOW_REQUEST:
      return {
        ...state,
        isLoadingNextVideo: true,
      };

    case actionTypes.UPDATE_VIDEOSHOW_SUCCESS:
      return {
        ...state,
        isLoadingNextVideo: false,
        data: { ...state.data, videoData: action.payload },
      };

    case actionTypes.UPDATE_VIDEOSHOW_FAILURE:
      return {
        ...state,
        isLoadingNextVideo: false,
        data: { ...state.data, videoData: action.payload },
      };

    case actionTypes.FETCH_VIDEOSHOW_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_VIDEOSHOW_SUCCESS:
      const data = action.payload;
      const { videoData, adsData } = data;

      const dynamicMeta = videoData ? getMetaData(videoData) : '';

      const allData = {
        videoData,
        adsData,
        metaData: dynamicMeta,
      };

      return { ...state, data: allData, isFetching: false };

    case actionTypes.FETCH_VIDEOSHOW_FAILURE:
      return { ...state, data: null, isFetching: false, isError: true };

    default:
      return state;
  }
}

export default videoshow;
