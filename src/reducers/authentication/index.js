import * as actionTypes from '../../actions/actions';

function authentication(
  state = {
    loggedIn: false,
    userData: null,
  },
  action,
) {
  switch (action.type) {
    case actionTypes.STORE_USER_DETAILS:
      return { ...state, loggedIn: true, userData: action.payload };

    case actionTypes.REMOVE_USER_DETAILS:
      return { ...state, loggedIn: false, userData: null };

    default:
      return state;
  }
}

export default authentication;
