/* eslint-disable no-use-before-define */

import { actionTypes } from '../../actions/photolist/index';

function getMetaData(photolistData) {
  const keywords =
    photolistData && photolistData.keywords && Array.isArray(photolistData.keywords)
      ? photolistData.keywords.map(item => item.hl)
      : '';

  const noFollowMeta = {
    name: 'ROBOTS',
    content: 'NOINDEX, NOFOLLOW',
  };

  const feedMeta = (photolistData && photolistData.pwa_meta) || null;

  const metaTags = [
    {
      name: 'description',
      content: photolistData && photolistData.pwa_meta && photolistData.pwa_meta.desc,
    },
    {
      name: 'keywords',
      content: photolistData && photolistData.pwa_meta && photolistData.pwa_meta.key,
    },
    {
      name: 'news_keywords',
      content: photolistData && photolistData.pwa_meta && photolistData.pwa_meta.key,
    },
    {
      property: 'article:published_time',
      content: photolistData.dlseo,
    },
    {
      property: 'article:modified_time',
      content: photolistData.luseo,
    },
    {
      property: 'og:updated_time',
      content: photolistData.luseo,
    },
    {
      property: 'og:title',
      content: photolistData.pwa_meta.title,
    },
    {
      property: 'og:description',
      content: photolistData.pwa_meta.desc,
    },
    {
      property: 'og:url',
      content: photolistData.wu,
    },
    {
      property: 'og:image',
      content: photolistData.pwa_meta.ogimg,
    },
    {
      property: 'twitter:title',
      content: photolistData.pwa_meta.title,
    },
    {
      property: 'twitter:description',
      content: photolistData.pwa_meta.desc,
    },
    {
      property: 'twitter:url',
      content: photolistData.wu,
    },
  ];

  const metaLinks = [
    {
      rel: 'canonical',
      href: photolistData.pwa_meta.canonical,
    },
  ];

  if (feedMeta && feedMeta.noindex == 1) {
    metaTags.push(noFollowMeta);
  }

  if (feedMeta && feedMeta.nextItem && feedMeta.nextItem.url) {
    metaLinks.push({
      rel: 'next',
      href: feedMeta.nextItem.url,
    });
  }
  if (feedMeta.prevItem && feedMeta.prevItem.url) {
    metaLinks.push({
      rel: 'prev',
      href: feedMeta.prevItem.url,
    });
  }

  if (
    photolistData &&
    photolistData.pwa_meta &&
    photolistData.pwa_meta.amphtml &&
    photolistData.pwa_meta.noindex != 1
  ) {
    const ampMeta = {
      rel: 'amphtml',
      href: photolistData.pwa_meta.amphtml,
    };
    metaLinks.push(ampMeta);
  }

  // console.log('Metaaaa ', metaTags);

  return { keywords, metaTags, metaLinks };
}

const initialState = {
  data: null,
  isFetching: false,
};

function photolist(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_PHOTOLIST_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_PHOTOLIST_SUCCESS: {
      // console.log('PL Action, ', action);
      const data = action.payload;
      const { photolistData, adsData, quicklinksData } = data;
      const dynamicMeta = photolistData ? getMetaData(photolistData) : '';

      const allData = {
        photolistData,
        adsData,
        metaData: dynamicMeta,
        quicklinksData,
      };

      return { ...state, data: allData, isFetching: false };
    }

    case actionTypes.UPDATE_PHOTOLIST_SUCCESS: {
      const updatedData = Object.assign({}, state);
      updatedData.data.photolistData = action.payload;
      return { ...state, updatedData, isFetching: false };
    }
    case actionTypes.FETCH_PHOTOLIST_FAILURE:
      return { ...state, data: null, isFetching: false };
    default:
      return state;
  }
}

export default photolist;
