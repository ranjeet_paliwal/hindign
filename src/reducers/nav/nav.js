/* eslint-disable no-case-declarations */
import * as actionTypes from '../../actions/actions';

const initialState = {
  isFetching: false,
  error: false,
  value: [],
  hoverData: {},
  fetchingPageHeader: false,
  pageHeaderData: null,
  pageHeaderFetchError: false,
};
function nav(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_REQUEST_HOVER:
      const hoverData = Object.assign({}, state.hoverData);

      hoverData[action.secName] = { isFetching: true, fetchError: false };
      return {
        ...state,
        hoverData,
      };

    case actionTypes.FETCH_SUCCESS_HOVER:
      const { data, secName } = action.payload;
      const hoverDataNew = { ...state.hoverData };
      hoverDataNew[secName].data = data;
      hoverDataNew[secName].isFetching = false;
      hoverDataNew[secName].fetchError = false;

      return {
        ...state,
        error: false,
        hoverData: hoverDataNew,
      };

    case actionTypes.FETCH_FAILURE_HOVER:
      const hoverDataTransformed = { ...state.hoverData };
      hoverDataTransformed[action.payload.secName].data = null;
      hoverDataTransformed[action.payload.secName].isFetching = false;
      hoverDataTransformed[action.payload.secName].fetchError = true;
      return {
        ...state,
        hoverData: hoverDataTransformed,
      };

    case actionTypes.UPDATE_NAV_SUCCESS: {
      // debugger;
      return { ...state, activeSection: action.payload, isFetching: false };
    }

    case actionTypes.FETCH_SUCCESS_NAV:
      return {
        ...state,
        isFetching: true,
        error: false,
        value: action.payload,
      };

    case actionTypes.FETCH_HEADER_DATA_REQUEST:
      return { ...state, isFetching: true, error: false };

    case actionTypes.FETCH_HEADER_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: false,
        headerData: action.payload[0],
      };

    case actionTypes.FETCH_HEADER_DATA_FAILURE:
      return { ...state, isFetching: false, error: true, headerData: action.payload };

    /*
       Page header data fetch actions
    */

    case actionTypes.FETCH_REQUEST_NEWS:
      return {
        ...state,
        fetchingPageHeader: true,
        pageHeaderData: null,
        pageHeaderFetchError: false,
      };

    case actionTypes.FETCH_SUCCESS_NEWS:
      return {
        ...state,
        fetchingPageHeader: false,
        pageHeaderData: action.payload.navData,
        pageHeaderFetchError: false,
      };

    case actionTypes.FETCH_FAILURE_NEWS:
      return {
        ...state,
        fetchingPageHeader: false,
        pageHeaderData: null,
        pageHeaderFetchError: true,
      };

    default:
      return state;
  }
}
export default nav;
