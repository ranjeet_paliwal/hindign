/* eslint-disable no-case-declarations */
import {
  FETCH_BREADCRUMBS_REQUEST,
  FETCH_BREADCRUMBS_SUCCESS,
  FETCH_BREADCRUMBS_FAILURE,
} from '../../actions/nav/breadcrumbs';

const initialState = {
  data: null,
  isFetching: false,
  fetchError: null,
};
function breadcrumbs(state = initialState, action) {
  switch (action.type) {
    case FETCH_BREADCRUMBS_REQUEST:
      return { ...state, data: null, isFetching: true, fetchError: null };

    case FETCH_BREADCRUMBS_SUCCESS:
      return { ...state, data: action.payload, isFetching: false, fetchError: null };

    case FETCH_BREADCRUMBS_FAILURE:
      return { ...state, data: null, isFetching: false, fetchError: action.payload };

    default:
      return state;
  }
}
export default breadcrumbs;
