/* eslint-disable no-use-before-define */
import { actionTypes } from '../../actions/reviewlist/index';

function getMetaData(articleData) {
  const keywords =
    articleData && articleData.keywords && Array.isArray(articleData.keywords)
      ? articleData.keywords.map(item => item.hl)
      : '';

  const noFollowMeta = {
    name: 'ROBOTS',
    content: 'NOINDEX, NOFOLLOW',
  };

  const feedMeta = (articleData && articleData.pwa_meta) || null;

  const metaTags = [
    {
      name: 'description',
      content: articleData && articleData.pwa_meta && articleData.pwa_meta.desc,
    },
    {
      name: 'keywords',
      content: articleData && articleData.pwa_meta && articleData.pwa_meta.key,
    },
    {
      name: 'news_keywords',
      content: articleData && articleData.pwa_meta && articleData.pwa_meta.key,
    },
    {
      property: 'article:published_time',
      content: articleData.dlseo,
    },
    {
      property: 'article:modified_time',
      content: articleData.luseo,
    },
    {
      property: 'og:updated_time',
      content: articleData.luseo,
    },
    {
      property: 'og:title',
      content: articleData.pwa_meta.title,
    },
    {
      property: 'og:description',
      content: articleData.pwa_meta.desc,
    },
    {
      property: 'og:url',
      content: articleData.wu,
    },
    {
      property: 'og:image',
      content: articleData.pwa_meta.ogimg,
    },
    {
      property: 'twitter:title',
      content: articleData.pwa_meta.title,
    },
    {
      property: 'twitter:description',
      content: articleData.pwa_meta.desc,
    },
    {
      property: 'twitter:url',
      content: articleData.wu,
    },
  ];

  const metaLinks = [
    {
      rel: 'canonical',
      href: articleData.pwa_meta.canonical,
    },
  ];

  if (feedMeta && feedMeta.noindex == 1) {
    metaTags.push(noFollowMeta);
  }

  if (feedMeta && feedMeta.nextItem && feedMeta.nextItem.url) {
    metaLinks.push({
      rel: 'next',
      href: feedMeta.nextItem.url,
    });
  }
  if (feedMeta.prevItem && feedMeta.prevItem.url) {
    metaLinks.push({
      rel: 'prev',
      href: feedMeta.prevItem.url,
    });
  }

  if (
    articleData &&
    articleData.pwa_meta &&
    articleData.pwa_meta.amphtml &&
    articleData.pwa_meta.noindex != 1
  ) {
    const ampMeta = {
      rel: 'amphtml',
      href: articleData.pwa_meta.amphtml,
    };
    metaLinks.push(ampMeta);
  }

  return { keywords, metaTags, metaLinks };
}

const initialState = {
  data: null,
  isFetching: false,
};

function reviewlist(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_REVIEWLIST_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_REVIEWLIST_SUCCESS: {
      const data = action.payload;
      const { articleData, articleRhsData, adsData, staticMeta, pageHeaderData } = data;
      const dynamicMeta = articleData ? getMetaData(articleData) : '';

      if (dynamicMeta && dynamicMeta.metaTags) {
        dynamicMeta.metaTags = dynamicMeta.metaTags.concat(staticMeta.meta);
      }
      if (dynamicMeta && dynamicMeta.metaLinks) {
        dynamicMeta.metaLinks = dynamicMeta.metaLinks.concat(staticMeta.links);
      }

      const allData = {
        articleData,
        articleRhsData,
        adsData,
        metaData: dynamicMeta,
        pageHeaderData,
      };

      return { ...state, data: allData, isFetching: false };
    }

    case actionTypes.UPDATE_REVIEWLIST_SUCCESS: {
      const updatedData = Object.assign({}, state);
      updatedData.data.articleData = action.payload;

      return { ...state, data: updatedData, isFetching: false };
    }

    case actionTypes.FETCH_REVIEWLIST_FAILURE:
      return { ...state, data: null, isFetching: false };
    default:
      return state;
  }
}

export default reviewlist;
