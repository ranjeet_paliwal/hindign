/* eslint-disable no-use-before-define */

import { actionTypes } from '../../actions/videolist/index';

function getMetaData(videolistData) {
  const keywords =
    videolistData && videolistData.keywords && Array.isArray(videolistData.keywords)
      ? videolistData.keywords.map(item => item.hl)
      : '';

  const noFollowMeta = {
    name: 'ROBOTS',
    content: 'NOINDEX, NOFOLLOW',
  };

  const feedMeta = (videolistData && videolistData.pwa_meta) || null;

  const metaTags = [
    {
      name: 'description',
      content: videolistData && videolistData.pwa_meta && videolistData.pwa_meta.desc,
    },
    {
      name: 'keywords',
      content: videolistData && videolistData.pwa_meta && videolistData.pwa_meta.key,
    },
    {
      name: 'news_keywords',
      content: videolistData && videolistData.pwa_meta && videolistData.pwa_meta.key,
    },
    {
      property: 'article:published_time',
      content: videolistData.dlseo,
    },
    {
      property: 'article:modified_time',
      content: videolistData.luseo,
    },
    {
      property: 'og:updated_time',
      content: videolistData.luseo,
    },
    {
      property: 'og:title',
      content: videolistData.pwa_meta.title,
    },
    {
      property: 'og:description',
      content: videolistData.pwa_meta.desc,
    },
    {
      property: 'og:url',
      content: videolistData.wu,
    },
    {
      property: 'og:image',
      content: videolistData.pwa_meta.ogimg,
    },
    {
      property: 'twitter:title',
      content: videolistData.pwa_meta.title,
    },
    {
      property: 'twitter:description',
      content: videolistData.pwa_meta.desc,
    },
    {
      property: 'twitter:url',
      content: videolistData.wu,
    },
  ];

  const metaLinks = [
    {
      rel: 'canonical',
      href: videolistData.pwa_meta.canonical,
    },
  ];

  if (feedMeta && feedMeta.noindex == 1) {
    metaTags.push(noFollowMeta);
  }

  if (feedMeta && feedMeta.nextItem && feedMeta.nextItem.url) {
    metaLinks.push({
      rel: 'next',
      href: feedMeta.nextItem.url,
    });
  }
  if (feedMeta.prevItem && feedMeta.prevItem.url) {
    metaLinks.push({
      rel: 'prev',
      href: feedMeta.prevItem.url,
    });
  }

  if (
    videolistData &&
    videolistData.pwa_meta &&
    videolistData.pwa_meta.amphtml &&
    videolistData.pwa_meta.noindex != 1
  ) {
    const ampMeta = {
      rel: 'amphtml',
      href: videolistData.pwa_meta.amphtml,
    };
    metaLinks.push(ampMeta);
  }

  // console.log('Metaaaa ', metaTags);

  return { keywords, metaTags, metaLinks };
}

const initialState = {
  data: null,
  isFetching: false,
};

function videolist(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_VIDEOLIST_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
      };

    case actionTypes.FETCH_VIDEOLIST_SUCCESS: {
      // console.log('PL Action, ', action);
      const data = action.payload;
      const { videolistData, adsData, quicklinksData } = data;
      const dynamicMeta = videolistData ? getMetaData(videolistData) : '';

      const allData = {
        videolistData,
        adsData,
        metaData: dynamicMeta,
        quicklinksData,
      };

      return { ...state, data: allData, isFetching: false };
    }

    case actionTypes.UPDATE_VIDEOLIST_SUCCESS: {
      const updatedData = Object.assign({}, state);
      updatedData.data.videolistData = action.payload;
      return { ...state, updatedData, isFetching: false };
    }
    case actionTypes.FETCH_VIDEOLIST_FAILURE:
      return { ...state, data: null, isFetching: false };
    default:
      return state;
  }
}

export default videolist;
