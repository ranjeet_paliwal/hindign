import { actionTypes } from '../../actions/gadgetshow/index';

function getMetaData(gadgetData) {
  const keywords =
    gadgetData && gadgetData.keywords && Array.isArray(gadgetData.keywords)
      ? gadgetData.keywords.map(item => item.hl)
      : '';

  const noFollowMeta = {
    name: 'ROBOTS',
    content: 'NOINDEX, NOFOLLOW',
  };

  const feedMeta = (gadgetData && gadgetData.pwa_meta) || null;

  const metaTags = [
    {
      name: 'description',
      content: gadgetData && gadgetData.pwa_meta && gadgetData.pwa_meta.desc,
    },
    {
      name: 'keywords',
      content: gadgetData && gadgetData.pwa_meta && gadgetData.pwa_meta.key,
    },
    {
      name: 'news_keywords',
      content: gadgetData && gadgetData.pwa_meta && gadgetData.pwa_meta.key,
    },
    {
      property: 'article:published_time',
      content: gadgetData.dlseo,
    },
    {
      property: 'article:modified_time',
      content: gadgetData.luseo,
    },
    {
      property: 'og:updated_time',
      content: gadgetData.luseo,
    },
    {
      property: 'og:title',
      content: gadgetData.pwa_meta.title,
    },
    {
      property: 'og:description',
      content: gadgetData.pwa_meta.desc,
    },
    {
      property: 'og:url',
      content: gadgetData.wu,
    },
    {
      property: 'og:image',
      content: gadgetData.pwa_meta.ogimg,
    },
    {
      property: 'twitter:title',
      content: gadgetData.pwa_meta.title,
    },
    {
      property: 'twitter:description',
      content: gadgetData.pwa_meta.desc,
    },
    {
      property: 'twitter:url',
      content: gadgetData.wu,
    },
  ];

  const metaLinks = [
    {
      rel: 'canonical',
      href: gadgetData.pwa_meta.canonical,
    },
  ];

  if (feedMeta && feedMeta.noindex == 1) {
    metaTags.push(noFollowMeta);
  }

  if (feedMeta && feedMeta.nextItem && feedMeta.nextItem.url) {
    metaLinks.push({
      rel: 'next',
      href: feedMeta.nextItem.url,
    });
  }
  if (feedMeta.prevItem && feedMeta.prevItem.url) {
    metaLinks.push({
      rel: 'prev',
      href: feedMeta.prevItem.url,
    });
  }

  if (
    gadgetData &&
    gadgetData.pwa_meta &&
    gadgetData.pwa_meta.amphtml &&
    gadgetData.pwa_meta.noindex != 1
  ) {
    const ampMeta = {
      rel: 'amphtml',
      href: gadgetData.pwa_meta.amphtml,
    };
    metaLinks.push(ampMeta);
  }

  // console.log('Metaaaa ', metaTags);

  return { keywords, metaTags, metaLinks };
}

function gadgetshow(
  state = {
    data: null,
    isFetching: false,
    isFetchingRelatedArticles: false,
    relatedGadgetsToFetch: [],
    relatedAdsData: [],
    relatedGadgetsData: [],
    relatedArticlesFetchError: false,
    relatedArticlesDataFetched: false,
  },
  action,
) {
  switch (action.type) {
    case actionTypes.FETCH_GADGETSHOW_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
        isFetchingRelatedArticles: false,
        relatedGadgetsToFetch: [],
        relatedGadgetsData: [],
        relatedAdsData: [],
        relatedArticlesFetchError: false,
        relatedArticlesDataFetched: false,
      };

    case actionTypes.FETCH_GADGETSHOW_SUCCESS: {
      const gadgetData = (action.payload && action.payload.gadgetData) || null;
      const relatedGadgetsToFetch =
        action.payload &&
        action.payload.gadgetData &&
        action.payload.gadgetData.relatedgadgets &&
        action.payload.gadgetData.relatedgadgets.gadget
          ? [].concat(action.payload.gadgetData.relatedgadgets.gadget).map(rData => rData.uname)
          : [];

      const dynamicMeta = gadgetData ? getMetaData(gadgetData) : '';

      return {
        ...state,
        data: action.payload,
        metaData: dynamicMeta,
        relatedGadgetsToFetch,
        isFetching: false,
      };
    }
    case actionTypes.FETCH_GADGETSHOW_FAILURE:
      return { ...state, data: null, isFetching: false };

    case actionTypes.FETCH_RELATED_GADGETS_REQUEST:
      return {
        ...state,
        isFetchingRelatedArticles: true,
        relatedArticlesFetchError: false,
        relatedArticlesDataFetched: false,
      };

    case actionTypes.FETCH_RELATED_GADGETS_SUCCESS:
      const relatedGadgetsToFetch = state.relatedGadgetsToFetch.slice(1);
      return {
        ...state,
        relatedGadgetsData: [...state.relatedGadgetsData, action.payload.gadgetData],
        relatedAdsData: [...state.relatedAdsData, action.payload.adsData],
        relatedGadgetsToFetch,
        isFetchingRelatedArticles: false,
        relatedArticlesDataFetched: true,
        relatedArticlesFetchError: false,
      };

    case actionTypes.FETCH_RELATED_GADGETS_FAILURE:
      return {
        ...state,
        isFetchingRelatedArticles: false,
        relatedArticlesDataFetched: false,
        relatedArticlesFetchError: true,
      };

    default:
      return state;
  }
}

export default gadgetshow;
