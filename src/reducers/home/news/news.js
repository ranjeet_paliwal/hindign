import * as actionTypes from '../../../actions/actions';

const initialState = {
  isFetching: false,
  didFetch: false,
  error: false,
  value: [],
};

function news(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_REQUEST_NEWS:
      return { ...state, isFetching: true, didFetch: false, error: false, value: [] };
    case actionTypes.FETCH_SUCCESS_NEWS:
      return {
        ...state,
        isFetching: false,
        didFetch: true,
        error: false,
        value: action.payload,
      };

    case actionTypes.FETCH_FAILURE_NEWS:
      return { ...state, isFetching: false, didFetch: false, error: true, value: [] };
    default:
      return state;
  }
}
export default news;
