import * as actionTypes from '../../../actions/actions';

const initialState = {
  isFetching: false,
  error: false,
  value: [],
};
function mostRead(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FETCH_SUCCESS_MOSTREAD:
      return {
        ...state,
        isFetching: true,
        error: false,
        value: action.payload,
      };
    default:
      return state;
  }
}
export default mostRead;
