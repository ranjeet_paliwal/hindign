/* eslint-disable no-case-declarations */
import * as actionTypes from '../../actions/actions';

const initialState = {
  isFetching: false,
  error: false,
  value: null,
};

function config(state = initialState, action) {
  // // console.log(action);
  switch (action.type) {
    case actionTypes.FETCH_SUCCESS_CONFIG:
      return {
        ...state,
        isFetching: true,
        error: false,
        value: action.payload,
      };

    default:
      return state;
  }
}
export default config;
