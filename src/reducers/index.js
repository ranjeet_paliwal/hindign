import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import home from './home/home';
import nav from './nav/nav';
import news from './home/news/news';
import mostread from './home/mostread/mostread';
import articleshow from './articleshow';
import gadgetshow from './gadgetshow';
import articlelist from './articlelist';
import reviewlist from './reviewlist';
import photolist from './photolist';
import photoshow from './photoshow';
import videolist from './videolist';
import videoshow from './videoshow';
import gadgetlist from './gadgetlist/gadgetlist';
import gadgetfiltertool from './gadgetfiltertool/gadgetfiltertool';

import authentication from './authentication';
// import config from './config/config';
import breadcrumbs from './nav/breadcrumbs';
// import dockedVideo from './dockedVideo';
import compareList from './comparision/List';
import compareDetail from './comparision/Detail';
import trendsComparision from './comparision/Trends';

const rootReducer = combineReducers({
  home,
  nav,
  news,
  mostread,
  authentication,
  articleshow,
  gadgetshow,
  articlelist,
  reviewlist,
  photolist,
  photoshow,
  videolist,
  videoshow,
  routing: routerReducer,
  breadcrumbs,
  compareList,
  compareDetail,
  gadgetlist,
  gadgetfiltertool,
  trendsComparision,
});

export default rootReducer;
