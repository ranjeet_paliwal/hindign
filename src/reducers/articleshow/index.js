import { actionTypes } from '../../actions/articleshow/index';

function articleshow(
  state = {
    data: null,
    isFetching: true,
    isFetchingRelatedArticles: false,
    relatedArticlesData: null,
    relatedArticlesFetchError: false,
    relatedArticlesDataFetched: false,
  },
  action,
) {
  switch (action.type) {
    case actionTypes.FETCH_ARTICLESHOW_REQUEST:
      return {
        ...state,
        data: null,
        isFetching: true,
        isFetchingRelatedArticles: false,
        relatedArticlesData: null,
        relatedArticlesFetchError: false,
        relatedArticlesDataFetched: false,
      };

    case actionTypes.FETCH_ARTICLESHOW_SUCCESS:
      return { ...state, data: action.payload, isFetching: false };

    case actionTypes.FETCH_ARTICLESHOW_FAILURE:
      return { ...state, data: null, isFetching: false };

    case actionTypes.FETCH_RELATED_ARTICLES_REQUEST:
      return {
        ...state,
        relatedArticlesData: null,
        isFetchingRelatedArticles: true,
        relatedArticlesFetchError: false,
        relatedArticlesDataFetched: false,
      };

    case actionTypes.FETCH_RELATED_ARTICLES_SUCCESS:
      return {
        ...state,
        relatedArticlesData: action.payload,
        isFetchingRelatedArticles: false,
        relatedArticlesDataFetched: true,
        relatedArticlesFetchError: false,
      };

    case actionTypes.FETCH_RELATED_ARTILES_FAILURE:
      return {
        ...state,
        relatedArticlesData: null,
        isFetchingRelatedArticles: false,
        relatedArticlesDataFetched: false,
        relatedArticlesFetchError: true,
      };

    default:
      return state;
  }
}

export default articleshow;
