import { loadJS } from '../../common-utility';

const JSIbeat = 'https://agi-static.indiatimes.com/cms-common/ibeat.min.js';

export const logIbeat = data => {
  if (typeof iBeatPgTrend !== 'undefined') {
    iBeatPgTrend.init(data);
  }
};

/**
 * initialize:
 * intializing iBeat method with a unique gaId
 * @param {object} options #required ibeathost, ibeatkey, wapsitename,
 */
export const initIbeat = data => {
  loadJS(JSIbeat, () => {
    // if (typeof iBeatPgTrend !== 'undefined') {
    //   iBeatPgTrend.init(data);
    // }
  });
};

export default { logIbeat, initIbeat };
