import { initComScore } from '../lib';
import { getCookie } from '../../common-utility';

const Config = require(`../../../common/${process.env.SITE}`);

// eslint-disable-next-line consistent-return
export const initialize = () => {
  if (typeof window === 'undefined') {
    return false;
  }

  if (typeof initComScore === 'function') {
    initComScore();
    // eslint-disable-next-line no-underscore-dangle
    window._comscore = [];
    window.objComScore = Config.Analytics.comScore.trackingData || {};
  }

  if (getCookie('optout') !== undefined) {
    if (getCookie('ckns_policy') !== undefined) {
      window.objComScore.cs_ucfr = 1;
    } else {
      window.objComScore.cs_ucfr = 0;
    }
  }

  // eslint-disable-next-line no-underscore-dangle
  window._comscore.push(window.objComScore);
};
export const invokeComScore = () => {
  if (window && window.COMSCORE) {
    window.COMSCORE.beacon(Config.Analytics.comScore.trackingData);
  }
};

export default { initialize, invokeComScore };
