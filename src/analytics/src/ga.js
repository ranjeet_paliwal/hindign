import { initGoogle } from '../lib';
import { getCookie } from '../../common-utility';

const Config = require(`../../../common/${process.env.SITE}`);
const labelWarn = '@times-analytics ';

export const event = (eventCategory, eventAction, eventLabel) => {
  // ga('send', 'event', [eventCategory]*, [eventAction]*, [eventLabel], [eventValue], [fieldsObject]);
  if (typeof window !== 'undefined' && typeof window.ga === 'function') {
    window.ga('send', 'event', eventCategory, eventAction, eventLabel);
  }
};

function configureGA() {
  if (typeof window === 'undefined') {
    return false;
  }
  if (!window.ga) {
    // eslint-disable-next-line no-console
    console.warn(`${labelWarn} ga not loaded`);
    return false;
  }
  window.ga('create', Config.Analytics.ga.trackingId, 'auto', { allowLinker: true });
  window.ga('require', 'displayfeatures');
  window.ga('require', 'linker');
  window.ga('linker:autoLink', Config.Analytics.ga.autoLink);
  if (typeof getCookie === 'function') {
    window.ga('set', 'userId', getCookie('ssoid'));
  }
  if (typeof getCookie === 'function' && getCookie('optout') !== undefined) {
    window.ga('set', 'anonymizeIp', true); // disable user IP
  }
  return true;
}

export const initialize = () => {
  if (typeof window !== 'undefined' && !window.ga) {
    initGoogle();
    configureGA();
  }
  return true;
};

export const pageview = (pathName, params) => {
  if (typeof window !== 'undefined') {
    //const searchPath = `/${document.location.search}`;
    const searchPath =
      document.location && document.location.search && document.location.search !== ''
        ? `/${document.location.search}`
        : '';
    const path =
      !pathName && typeof document !== 'undefined'
        ? document.location.pathname + searchPath
        : pathName;
    // window.ga('set', 'page', path); // update the tracker and supply the new page value, use for SPAs
    if (typeof ga == 'function') {
      if (params && params.cd4) {
        ga('set', 'dimension4', params.cd4);
      } else {
        ga('set', 'dimension4', '');
      }
    }
    window.ga('send', 'pageview', path);
  }
};

export default { initialize, pageview, event };
