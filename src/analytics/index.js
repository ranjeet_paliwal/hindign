import ga from './src/ga';
import comscore from './src/comScore';
import iBeat from './src/iBeat';

export const analyticsGA = ga;
export const AnalyticsComscore = comscore;
export const AnalyticsIBeat = iBeat;

export default { analyticsGA, AnalyticsComscore, AnalyticsIBeat };
