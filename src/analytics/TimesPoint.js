/* eslint-disable no-restricted-globals */
import { loadJS, getCookie } from '../common-utility';

const TIMESPOINTCONFIG = {
  tpconfig: {
    isEnableTimesPoints: true,
    pcode: 'nbt',
    scode: 'nbt',
    ARTICLE_READ: 'read',
    WRITE_COMMENT: 'cmnt',
    WATCHING_VIDEO: 'watch_video',
    FACEBOOK_SHARE: 'sh_fb',
    TWITTER_SHARE: 'sh_tw',
    VIEWING_PHOTO: 'view_photo',
    WEBSITE_VISIT: 'visit',
    COMMENT_UP_VOTE: 'agree',
    MOVIE_RATING: 'act4375385',
    WRITE_MOVIE_RATING: 'mv_rv',
    FOLLOW_BUTTON_AUTHOR: 'follower',
    FOLLOW_BUTTON_USER: 'act5159736',
    UPVOTE_AUTHOR_POINT: 'act1163006',
    COMMENT_OFFENCIVE: 'act1229049',
    COMMENT_DOWN_VOTE: 'act1344975',
    BROWSER_NOTIFICATION: 'act752838',
    OPEN_NEWSLETTER: 'opn_nltr',
    MOVIE_MASTI_NEWS: 'sub_mm',
    DAILY_NEWS: 'sub_dn',
    HASI_MAJAK_NEWS: 'sub_hn',
    VABISYA_FAL_NEWS: 'sub_bh',
    COMMENT_SUFFIX: '_Comment',
    FACEBOOK_SUFFIX: '_FB',
    TWITTER_SUFFIX: '_Twitter',
    WRITE_MOVIE_RATING_SUFFIX: '_MReview',
    MOVIE_RATING_SUFFIX: '_MRate',
    ARTICLE_READ_SUFFIX: '_Read',
    COMMENT_UP_VOTE_SUFFIX: '_Upvote',
    UP_VOTE_AUTHOR_SUFFIX: '_UpvoteAuthor',
    COMMENT_DOWN_VOTE_SUFFIX: '_Downvote',
    COMMENT_OFFENCIVE_SUFFIX: '_Offensive',
    WATCHING_VIDEO_SUFFIX: '_video',
    PAYTM_CASHBACK_CAMPAIGN: 'act4097935',
    PAYTM_CASHBACK_CAMPAIGN_SUFFIX: '_PAYTM',
  },
  TPAsyncData: {
    pcode: 'nbt',
    scode: 'nbt',
    aname: 'visit',
    platform: 'WEB',
    userId: '',
    email: '',
    ssoid: '', // ssoid
    uemail: '',
  },
  get(name) {
    return this.tpconfig[name];
  },
};

function getArticleIdFromUrl() {
  const URIPath = window.location.pathname;
  const idPath = URIPath.split('/').pop(-1);
  const entityId = idPath.split('.cms')[0];
  return entityId;
}

function getTxnSuffix(acode) {
  let suffix = '';
  switch (acode) {
    case TIMESPOINTCONFIG.get('WRITE_COMMENT'):
      suffix = TIMESPOINTCONFIG.get('COMMENT_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('FACEBOOK_SHARE'):
      suffix = TIMESPOINTCONFIG.get('FACEBOOK_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('TWITTER_SHARE'):
      suffix = TIMESPOINTCONFIG.get('TWITTER_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('WRITE_MOVIE_RATING'):
      suffix = TIMESPOINTCONFIG.get('WRITE_MOVIE_RATING_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('MOVIE_RATING'):
      suffix = TIMESPOINTCONFIG.get('MOVIE_RATING_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('ARTICLE_READ'):
      suffix = TIMESPOINTCONFIG.get('ARTICLE_READ_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('COMMENT_UP_VOTE'):
      suffix = TIMESPOINTCONFIG.get('COMMENT_UP_VOTE_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('UPVOTE_AUTHOR_POINT'):
      suffix = TIMESPOINTCONFIG.get('UP_VOTE_AUTHOR_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('COMMENT_DOWN_VOTE'):
      suffix = TIMESPOINTCONFIG.get('COMMENT_DOWN_VOTE_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('COMMENT_OFFENCIVE'):
      suffix = TIMESPOINTCONFIG.get('COMMENT_OFFENCIVE_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('WATCHING_VIDEO'):
      suffix = TIMESPOINTCONFIG.get('WATCHING_VIDEO_SUFFIX');
      break;
    case TIMESPOINTCONFIG.get('PAYTM_CASHBACK_CAMPAIGN'):
      suffix = TIMESPOINTCONFIG.get('PAYTM_CASHBACK_CAMPAIGN_SUFFIX');
      break;
    default:
      suffix = '';
  }

  return suffix;
}

function TPData(userData) {
  const url = (typeof location !== 'undefined' && location.href) || '';
  const userId = (userData && userData.data.ssoid) || '';
  const userEmail = (userData && userData.data.emailList) || '';
  return {
    host: TIMESPOINTCONFIG.tpconfig.pcode,
    channel: TIMESPOINTCONFIG.tpconfig.scode,
    URL: url,
    userId,
    oid: '',
    email: userEmail,
  };
}

function initTimesWidget(userData) {
  window.TPWidget.init({
    widgets: [
      {
        ele: 'widget-two',
        widgetType: 'tpwidget-two',
      },
      {
        ele: 'widget-head',
        widgetType: 'tpwidget-one',
      },
    ],

    userLoginInfo: TPData(userData),
  });
}

function sendActivityRequest(request) {
  const Img = new Image(1, 1);
  Img.src = `${process.env.WEBSITE_URL}/rewardsapi.cms${request}`;
}

export function initPostLoginAction(loggedIn, userData) {
  try {
    window.TPWidget.PostLoginActions(TPData(userData));

    initTimesWidget(TPData(userData));
  } catch (ex) {
    //
  }
}

export function initTimesPoint() {
  initTimesWidget();
}

function prepareLogData(obj) {
  let request = `?uemail=${encodeURI(obj.uemail)}`;
  request += `&uid=${encodeURI(obj.ssoid)}`;
  request += `&pcode=${encodeURI(obj.pcode)}`;
  request += `&scode=${encodeURI(obj.scode)}`;
  request += `&aname=${encodeURI(obj.aname)}`;
  request += `&platform=${encodeURI(obj.platform)}`;
  request += `&txnId=${encodeURI(obj.txnId)}`;
  sendActivityRequest(request);
}

function getTranscationId(acode, id) {
  const txnSuffix = getTxnSuffix(acode);
  return id + txnSuffix;
}

function setActivityLog(acode, id) {
  try {
    const txnId = getTranscationId(acode, id);
    if (getCookie('ssoid') != null) {
      const ssoId = getCookie('ssoid');
      const emailId = getCookie('MSCSAuthDetails').split('=')[1];
      const TPAsyncDataCopy = Object.assign({}, TIMESPOINTCONFIG.TPAsyncData); // clones TIMESPOINTCONFIG.TPAsyncData into  TPAsyncDataCopy
      TPAsyncDataCopy.aname = acode;
      TPAsyncDataCopy.txnId = txnId;
      TPAsyncDataCopy.ssoid = ssoId;
      TPAsyncDataCopy.uemail = emailId;
      prepareLogData(TPAsyncDataCopy);
    } else {
      window.TPWidget.addPreLoginActivity(acode);
    }
  } catch (ex) {
    // alert(ex);
  }
}

export function logPoints(activity, id) {
  const uniqueId = id || getArticleIdFromUrl();
  if (TIMESPOINTCONFIG.tpconfig.isEnableTimesPoints === true) {
    if (uniqueId) {
      setActivityLog(activity, uniqueId);
    } else {
    }
  }
}

const ConfigData = TIMESPOINTCONFIG;

export { ConfigData };
