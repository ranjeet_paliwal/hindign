/* eslint-disable prefer-template */
/* eslint-disable no-var */
/* eslint-disable indent */
/* eslint-disable no-unused-expressions */
/* eslint-disable wrap-iife */
/* eslint-disable func-names */

export function initGoogle() {
  (function(i, s, o, g, r, a, m) {
    i.GoogleAnalyticsObject = r;
    (i[r] =
      i[r] ||
      function() {
        (i[r].q = i[r].q || []).push(arguments);
      }),
      (i[r].l = 1 * new Date());
    (a = s.createElement(o)), (m = s.getElementsByTagName(o)[0]);
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m);
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

  // (function(w, d, s, l, i) {
  //   w[l] = w[l] || [];
  //   w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
  //   var f = d.getElementsByTagName(s)[0],
  //     j = d.createElement(s),
  //     dl = l != 'dataLayer' ? '&l=' + l : '';
  //   j.async = true;
  //   j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
  //   f.parentNode.insertBefore(j, f);
  // })(window, document, 'script', 'dataLayer', 'GTM-NS5K3GZ');
}

export function initComScore() {
  (function() {
    var s = document.createElement('script');
    var el = document.getElementsByTagName('script')[0];
    s.async = true;
    s.src =
      (document.location.protocol == 'https:' ? 'https://sb' : 'http://b') +
      '.scorecardresearch.com/beacon.js';
    s.rel = 'dns-prefetch';
    el.parentNode.insertBefore(s, el);
  })();
}
