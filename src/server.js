import React from 'react';
import { compose } from 'redux';
import { renderToString, renderToNodeStream, renderToStaticMarkup } from 'react-dom/server';
import { createMemoryHistory, match, RouterContext } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import { Provider } from 'react-redux';
import Helmet from 'react-helmet';
import { flushWebpackRequireWeakIds } from 'react-loadable';
import fs from 'fs';
import { CookiesProvider } from 'react-cookie';
import configureStore from './store/configureStore';
import routes, { NotFoundComponent } from './routes';

import Html from './containers/html/Html';

const errorHTML = `
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html >
   <head >
      <meta content="NOINDEX, NOFOLLOW" name="ROBOTS">
      <meta name="viewport" content="width=device-width, height=device-height,initial-scale=1.0,user-scalable=no">
      <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
      <meta http-equiv="content-language" content="en">
      <title>Page not found  | Navbharat Times</title>
      <meta name="description" content="Page not found">
      <link rel='stylesheet' type='text/css' href='/css/main.css'/>
   </head>
   <body>
        <header lang="">
          <div data-gtm-event="logo_click" class="np-logo" style="margin-left:15px;">
              <a href="/"><img src="https://opt.toiimg.com/recuperator/img/nbt/m-60120174/Hindi-News.jpg" alt="Hindi News" title=""></a>
          </div>        
        </header>
        <div id="c_wdt_articleshow_1" class="error-article-section">
          <div class="error-wrapper">
            <span class="heading-text">Sorry, page not found</span>
            <div class="sub-heading">
              It may have expired, or there could be a typo. 
              Maybe you can find what you need on our homepage.
            </div>
            <a class="return-btn" href="/">RETURN</a>
          </div>
        </div>
   </body>
</html>
`;

function logError(ex) {
  const today = new Date();
  const todayString = `${today.getDate()}-${today.getMonth() + 1}-${today.getFullYear()}`;
  fs.appendFileSync(`/var/log/node/lang-error-${todayString}.log`, ex.stack);
}

function parseCookie(cookie) {
  if (typeof cookie !== 'undefined' && cookie !== '') {
    const cookieArr = cookie.split(';');
    const cookieFinalArr = {};
    for (let i = 0; i < cookieArr.lenght; i++) {
      const c = cookieArr[i];
      const temp = c.split('=');
      if (typeof temp[0] !== 'undefined' && typeof temp[1] !== 'undefined') {
        cookieFinalArr[temp[0].trim()] = temp[1].trim();
      }
    }
    return cookieFinalArr;
  }
  return [];
}

function flatten(arr) {
  return [].concat(...arr);
}

function uniq(arr) {
  return [...new Set(arr)];
}

function isTruthy(val) {
  return !!val;
}

const flattenUniq = compose(
  uniq,
  flatten,
);

function fetchComponentData(renderProps, store) {
  const requests = renderProps.components.filter(isTruthy).map(component => {
    // Handle `connect`ed components.
    if (component.WrappedComponent) {
      component = component.WrappedComponent;
    }

    if (component.fetchData) {
      const { params, router, history } = renderProps;
      let { query } = renderProps;
      if (!query) {
        query = Object.assign({}, renderProps.location.query);
      }

      return (
        component
          .fetchData({
            dispatch: store.dispatch,
            query,
            params,
            history,
            router,
          })
          // Make sure promise always successfully resolves
          .catch(ex => {
            // console.log('Server Error:', ex);
          })
      );
    }
    return null;
  });
  return Promise.all(requests);
}

function isNotFound(renderProps) {
  // return false;
  return !renderProps || renderProps.components.some(component => component === NotFoundComponent);
}

function getJsByChunkName(name, { assetsByChunkName }) {
  let assets = assetsByChunkName[name];
  if (!Array.isArray(assets)) {
    assets = [assets];
  }
  return assets.find(asset => /\.js$/.test(asset));
}

function getJsByModuleIds(moduleIds, { modulesById, chunksById }) {
  const chunkIds = flatten(
    moduleIds.map(id => {
      const clientModule = modulesById[id];
      if (!clientModule) {
        throw new Error(`${id} not found in client stats`);
      }
      return clientModule.chunks;
    }),
  );
  return flattenUniq(
    chunkIds.map(id => {
      return chunksById[id].files
        .filter(file => /\.js$/.test(file))
        .filter(file => !/\.hot-update\.js$/.test(file));
    }),
  );
}

function checkSeoRedirection(urlFound, state, params) {
  let urlToCheck = `${process.env.BASE_URL}${urlFound}`;

  //const urlToCheck = `${urlFound}`;
  const { id } = params;
  const isHome = urlFound == '/tech/';
  const isArticleShow = urlFound.includes('/articleshow/');
  const isArticleList = urlFound.includes('/articlelist/');
  const isVideoList = urlFound.includes('/videolist/');
  // const isPhotoShow = urlFound.includes('/photoshow/');
  // const isVideoShow = urlFound.includes('/videoshow/');
  const isPhotoList = urlFound.includes('/photolist/') || urlFound.includes('/photoarticlelist/');

  // const dotCmsIndex = urlFound.lastIndexOf('.cms');

  let redirectUrl = '/404';
  let notFound = false;

  const areStringsSame = (str1, str2) => {
    if (typeof str1 === 'string' && typeof str2 === 'string') {
      return str1 == str2;
    }
    return false;
  };

  let dataUrl = '';

  switch (true) {
    case isHome:
      redirectUrl = '/tech';
      notFound = true;
      break;

    case isArticleList:
      dataUrl =
        state.articlelist.data &&
        state.articlelist.data.articleData &&
        state.articlelist.data.articleData.pwa_meta &&
        state.articlelist.data.articleData.pwa_meta.redirectUrl;

      if (dataUrl && !areStringsSame(dataUrl, urlToCheck)) {
        redirectUrl = state.articlelist.data.articleData.pwa_meta.redirectUrl;
        notFound = true;
      }

      break;

    case isArticleShow:
      dataUrl =
        state.articleshow.data &&
        state.articleshow.data.articleData &&
        state.articleshow.data.articleData.pwa_meta &&
        state.articleshow.data.articleData.pwa_meta.redirectUrl;
      const isNoIndex = !!(
        state.articleshow.data &&
        state.articleshow.data.articleData &&
        state.articleshow.data.articleData.pwa_meta &&
        state.articleshow.data.articleData.pwa_meta.noindex != 1
      );

      if (dataUrl && !areStringsSame(dataUrl, urlToCheck) && isNoIndex) {
        redirectUrl = state.articleshow.data.articleData.pwa_meta.redirectUrl;
        notFound = true;
      }

      break;

    case isVideoList:
      dataUrl =
        state.videolist.data &&
        state.videolist.data.videolistData &&
        state.videolist.data.videolistData.pwa_meta &&
        state.videolist.data.videolistData.pwa_meta.redirectUrl;
      break;

    default:
      const gadgetShowUrl =
        state.gadgetshow &&
        state.gadgetshow.data &&
        state.gadgetshow.data.gadgetData &&
        state.gadgetshow.data.gadgetData.pwa_meta &&
        state.gadgetshow.data.gadgetData.pwa_meta.redirectUrl;

      const compareDetailUrl =
        state.compareDetail &&
        state.compareDetail.data &&
        state.compareDetail.data.compareData &&
        state.compareDetail.data.compareData.pwa_meta &&
        state.compareDetail.data.compareData.pwa_meta.redirectUrl;

      if (gadgetShowUrl) {
        // dataUrl = gadgetShowUrl;
      } else if (compareDetailUrl) {
        // dataUrl = compareDetailUrl;
      }

      break;
  }

  // check for domain in redirectionUrl , if present remove it
  // dataUrl = dataUrl.indexOf('.com') > -1 ? dataUrl.split['.com'][1] : dataUrl;

  if (dataUrl && !areStringsSame(dataUrl, urlToCheck)) {
    redirectUrl = dataUrl;
    notFound = true;
  }

  return { redirectUrl, notFound };
}

function getCssByChunkName(name, { assetsByChunkName }) {
  let assets = assetsByChunkName[name];
  if (!Array.isArray(assets)) {
    assets = [assets];
  }
  return assets.find(asset => /\.css$/.test(asset));
}

function getJs(moduleIds, stats) {
  return [
    getJsByChunkName('bootstrap', stats),
    ...getJsByModuleIds(moduleIds, stats),
    getJsByChunkName('client', stats),
  ].filter(isTruthy);
}

function getCss(stats) {
  return [getCssByChunkName('client', stats)].filter(isTruthy);
}
function isReactNotFound(html, pagetype) {
  return (pagetype == 'VS' || pagetype == 'AS') &&
    html.indexOf('<div id="parentContainer"></div>') != -1
    ? true
    : false;
}

function render(renderProps, store, stats, res, req) {
  // Write the <head> and root <div>

  // Render the frontend React app
  const markup = renderToString(
    <CookiesProvider cookies={req.universalCookies}>
      <Provider store={store}>
        <RouterContext {...renderProps} />
      </Provider>
    </CookiesProvider>,
  );
  // Pipe that HTML to the response

  // When React is finished, clean up the dangling HTML tags

  /* const markup = renderToString(
    <CookiesProvider cookies={req.universalCookies}>
      <Provider store={store}>
        <RouterContext {...renderProps} />
      </Provider>
    </CookiesProvider>,
  ); */

  const head = Helmet.rewind();
  const moduleIds = flushWebpackRequireWeakIds();
  const js = getJs(moduleIds, stats);

  const httpPushCache = [];
  const css = getCss(stats);
  const faviconPath = '/icons/nbtfavicon.ico';
  httpPushCache.push(`<${process.env.ASSET_PATH}${css}>; rel=preload; as=style`);
  httpPushCache.push(`<${process.env.WEBSITE_URL}${faviconPath}>; rel=preload; as=image`);
  for (const j in js) {
    if (js[j].indexOf('bootstrap') > -1 || js[j].indexOf('client') > -1) {
      httpPushCache.push(`<${process.env.ASSET_PATH + js[j]}>; rel=preload; as=script`);
    }
  }
  const httpCache = httpPushCache.join(', ');
  if (process.env.NODE_ENV === 'production') {
    res.setHeader('Link', httpCache);
  }

  const initialState = store.getState();
  const version = stats.version;

  return renderToStaticMarkup(
    <Html
      js={js}
      css={css}
      html={markup}
      head={head}
      initialState={initialState}
      version={version}
      isOperaMini={stats.isOperaMini}
    />,
  );
}

/**
 * Express middleware to render HTML using react-router
 * @param  {object}     stats Webpack stats output
 * @return {function}   middleware function
 */
export default ({ clientStats }) => {
  // Build stats maps for quicker lookups.
  const modulesById = clientStats.modules.reduce((modules, mod) => {
    modules[mod.id] = mod;
    return modules;
  }, {});
  const chunksById = clientStats.chunks.reduce((chunks, chunk) => {
    chunks[chunk.id] = chunk;
    return chunks;
  }, {});
  const assetsByChunkName = clientStats.assetsByChunkName;

  /**
   * @param  {object}     req Express request object
   * @param  {object}     res Express response object
   * @return {undefined}  undefined
   */
  return (req, res, next) => {
    const initialState = {};
    const url = req.url;
    const memoryHistory = createMemoryHistory(url);
    const store = configureStore(initialState);
    const history = syncHistoryWithStore(memoryHistory, store);
    const cookie = parseCookie(req.headers.cookie);

    match(
      {
        history,
        routes,
        location: url,
      },
      (error, redirectLocation, renderProps) => {
        // // console.log("Inside server.js", error);
        if (error) {
          res.status(500).send(error.message);
        } else if (redirectLocation) {
          res.redirect(301, `${redirectLocation.pathname}${redirectLocation.search}`);
        } else {
          fetchComponentData(renderProps, store).then(data => {
            const { notFound, redirectUrl } = checkSeoRedirection(
              renderProps.location.pathname,
              store.getState(),
              renderProps.params,
            );
            // let redirectUrl = '/404';
            // let notFound = false;
            const isArticleShow = url.includes('/articleshow/');
            const isArticleList = url.includes('/articlelist/') || url.includes('/reviews');
            const isVideoList = url.includes('/videolist/');
            const isVideoShow = url.includes('/videoshow/');
            const isPhotoShow = url.includes('/photoshow/');
            const isPhotoList = url.includes('/photolist/') || url.includes('/photoarticlelist/');
            const isCompare = url.includes('/tech/compare-');
            const isGadgetshow =
              url.includes('/tech/mobile-phones') ||
              url.includes('/tech/laptops') ||
              url.includes('/tech/cameras') ||
              url.includes('/tech/tablets') ||
              url.includes('/tech/smartwatch') ||
              url.includes('/tech/fitnessband');
            let pageType = '';
            if (url.includes('/articleshow/') || url.includes('/videoshow/')) {
              if (url.includes('/articleshow/')) {
                pageType = 'AS';
              } else {
                pageType = 'VS';
              }
            }
            const cacheTime = () => {
              switch (true) {
                case isArticleList:
                case isPhotoList:
                  return 20 * 60 * 1000; // 20minute

                case isArticleShow:
                  return 6 * 60 * 60 * 1000; // 6hour

                case isVideoList:
                  return 30 * 60 * 1000; // 30minute

                case isPhotoShow:
                case isVideoShow:
                  return 2 * 60 * 60 * 1000; // 2hour
                case isCompare:
                case isGadgetshow:
                  return 1 * 60 * 60 * 1000; //  2hour
                default:
                  return 10 * 60 * 1000; // 10 minute
              }
            };

            let html;
            try {
              html = render(
                renderProps,
                store,
                {
                  modulesById,
                  chunksById,
                  assetsByChunkName,
                  cookie,
                  version: clientStats.hash,
                },
                res,
                req,
              );

              /* html.pipe(
                res,
                { end: 'false' },
              );
              html.on('end', () => {
                res.end('');
              });
              return null; */
            } catch (ex) {
              if (process.env.NODE_ENV !== 'development') {
                logError(ex);
                res.status(500).send(errorHTML);
                return;
              }
              return next(ex);
            }

            if (notFound) {
              res.redirect(301, redirectUrl);
            } else {
              // if (isArticleShow || isArticleList) {
              //   if (isArticleShow) {
              //     res.setHeader('refreshTime', 6 * 60 * 60 * 1000); // 6 Hours
              //   } else if (isArticleList) {
              //     res.setHeader('refreshTime', 20 * 60 * 1000); // 10 minutes
              //   }
              // }

              res.setHeader('refreshTime', cacheTime());

              res
                .status(isNotFound(renderProps) || isReactNotFound(html, pageType) ? 404 : 200)
                .send(`<!doctype html>${html}`);
            }
          });
        }
      },
    );
  };
};
