/* eslint-disable no-restricted-globals */
/* eslint-disable no-undef */
const DOMAIN_NAME = self.location.origin;

const HOME_HTML_CACHE = 'web-homepage-cache-html';
// const SITE_ID = '696089404';

if (workbox) {
  // console.log(`Yay! Workbox is loaded ?? at`, DOMAIN_NAME);
  workbox.clientsClaim();
  workbox.precaching.suppressWarnings();

  self.__precacheManifest.unshift({
    url: '/icons/nbtfavicon.ico',
  });

  workbox.precaching.precacheAndRoute(self.__precacheManifest || [], {
    directoryIndex: null,
    cleanUrls: false,
  });

  // workbox.routing.registerRoute(
  //   '/',
  //   workbox.strategies.staleWhileRevalidate({
  //     cacheName: HOME_HTML_CACHE,
  //     plugins: [
  //       new workbox.expiration.Plugin({
  //         maxEntries: 1,
  //         maxAgeSeconds: 3 * 60, // 3 minutes
  //       }),
  //     ],
  //   }),
  // );

  self.addEventListener('install', event => {
    // console.log('Inside Install');
    const urls = ['/'];
    const cacheName = HOME_HTML_CACHE; // workbox.core.cacheNames.runtime
    event.waitUntil(
      caches.open(cacheName).then(cache => {
        return cache
          .addAll(urls)
          .then(() => {})
          .catch(() => {
            self.skipWaiting();
          });
      }),
    );
    return event.waitUntil(self.skipWaiting());
  });

  self.addEventListener('activate', event => {
    event.waitUntil(self.clients.claim()); // Become available to all pages
  });

  workbox.routing.registerRoute(
    /https:\/\/static.langimg.com\/thumb/,
    workbox.strategies.cacheFirst({
      cacheName: 'dynamic-images',
      plugins: [
        new workbox.cacheableResponse.Plugin({
          statuses: [200],
        }),
        new workbox.expiration.Plugin({
          maxEntries: 20,
          maxAgeSeconds: 3 * 60, // 3 minute
        }),
      ],
    }),
    'GET',
  );

  // workbox.routing.registerRoute(
  //   /\/(.*)articleshow(.*)\//,
  //   workbox.strategies.cacheFirst({
  //     cacheName: 'articles-cache',
  //     plugins: [
  //       new workbox.cacheableResponse.Plugin({
  //         statuses: [200],
  //       }),
  //       new workbox.expiration.Plugin({
  //         maxEntries: 10,
  //         maxAgeSeconds: 3 * 60, // 3 minute
  //       }),
  //     ],
  //   }),
  //   'GET',
  // );

  workbox.routing.registerRoute(
    /\/(.*)articlelist(.*)\//,
    workbox.strategies.cacheFirst({
      cacheName: 'web-articlelist-cache',
      plugins: [
        new workbox.cacheableResponse.Plugin({
          statuses: [200],
        }),
        new workbox.expiration.Plugin({
          maxEntries: 10,
          maxAgeSeconds: 3 * 60, // 5 minute
        }),
      ],
    }),
    'GET',
  );

  workbox.routing.registerRoute(
    /https:\/\/rprodnbt1.indiatimes.com/,
    workbox.strategies.cacheFirst({
      cacheName: 'static-resources',
      plugins: [
        new workbox.cacheableResponse.Plugin({
          statuses: [200],
        }),
        new workbox.expiration.Plugin({
          maxEntries: 100,
          maxAgeSeconds: 2 * 24 * 60 * 60, // 2d
        }),
      ],
    }),
    'GET',
  );
} else {
  // console.log(`Boo! Workbox didn't load ??`);
}
