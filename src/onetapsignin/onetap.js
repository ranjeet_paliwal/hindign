/* eslint-disable operator-linebreak */
import fetchJsonP from 'fetch-jsonp';
import { getCookie, setCookie, loadJS } from '../common-utility';
import { storeUserDetails } from '../actions/authentication';
import { analyticsGA } from '../analytics';

// clear iframe timer on any error
const clearIframeCheckInterval = () => {
  clearInterval(window.iframeTimer);
};

const GAeventCategory = 'Login';
const GAeventAction = 'One Tap';

const storeUserData = dispatch => {
  if (typeof getCookie !== 'undefined') {
    const ssoId = getCookie('ssoid');
    fetchJsonP(`https://myt.indiatimes.com/mytimes/profile/info/v1/?ssoid=${ssoId}`)
      .then(response => {
        return response.json();
      })
      .then(response => {
        if (response.sso) {
          const userData = {
            data: {
              ssoid: response.uid,
              firstName: response.F_N,
              lastName: response.L_N,
              emailList: response.EMAIL,
              thumb: response.thumb,
            },
          };
          analyticsGA.event(GAeventCategory, GAeventAction, 'Success_google');
          dispatch(storeUserDetails(userData));
        }
      })
      .catch(() => {
        analyticsGA.event(GAeventCategory, GAeventAction, 'Fail_Google');
      });
  }
};

const validateTicket = (ticketId, dispatch) => {
  fetchJsonP(
    `https://socialappsintegrator.indiatimes.com/socialsite/v1validateTicket?channel=nbt&ticketId=${ticketId}`,
  )
    .then(response => {
      return response.json();
    })
    .then(response => {
      if (response.code == '200') {
        storeUserData(dispatch);
      }
    })
    .catch(() => {});
};

const processLogin = dispatch => {
  fetchJsonP(
    'https://jsso.indiatimes.com/sso/crossdomain/getTicket?channel=nbt&platform=web&version=v1',
  )
    .then(response => {
      return response.json();
    })
    .then(response => {
      if (response.ticketId) {
        validateTicket(response.ticketId, dispatch);
      }
    })
    .catch(() => {});
};

const bindEvents = (options, dispatch) => {
  window.onGoogleYoloLoad = googleyolo => {
    googleyolo.setTimeouts(5000);
    // The 'googleyolo' object is ready for use.
    const checkForIframe = function checkForIframe() {
      const iframes = document.querySelectorAll('iframe');
      // LNULL ISSUE POSSIBLE
      for (let i = 0; i < iframes.length; i++) {
        const iframeSrc = iframes[i].getAttribute('src');
        if (iframeSrc && iframeSrc.indexOf('smartlock') > 0) {
          // ga if iframe present in page , hidden or visible
          if (!options.isViewGaTracked) {
            // gaTracking(options.eventGAObj, 'View');
            options.isViewGaTracked = true;
          }
          if (!iframes[i].getAttribute('hidden')) {
            // ga if iframe popup in page , visible and clear the timer
            analyticsGA.event(GAeventCategory, GAeventAction, 'Popup_Final_View');
            clearIframeCheckInterval();
          }
        }
      }
    };
    window.iframeTimer = setInterval(checkForIframe, options.onetapIframeCheckTimer);

    const tthisConstants = options.constants;

    // Hint method/promise is used to initiate google login
    // supportedAuthMethods is list of types of logins
    // clientID  is where all analytics data will be sent (Should be created by TOI google account) done!!!

    const hintPromise = googleyolo.hint({
      supportedAuthMethods: ['https://accounts.google.com'],
      supportedIdTokenProviders: [
        {
          uri: 'https://accounts.google.com',
          clientId: tthisConstants.clientId,
        },
      ],
    });
    // The Hint promise resolves here

    hintPromise.then(
      credential => {
        analyticsGA.event(GAeventCategory, GAeventAction, 'Popup_Final_successmsg');

        if (credential.idToken) {
          tthisConstants.credential = credential;
          // Send the token to your auth backend.home
          // // console.log("Promise" + credential.idToken);
          const googleLongToken = credential.idToken;
          const channel = tthisConstants.channel;
          const urlVerifyToken = `${
            tthisConstants.jssoAPIGetURL
          }?token=${googleLongToken}&channel=${channel}`;
          if (tthisConstants.jssoAPIGetURL && tthisConstants.jssoAPIGetURL) {
            fetchJsonP(urlVerifyToken, { method: 'GET', mode: 'cors', credentials: 'include' })
              .then(response => {
                return response.json();
              })
              .then(() => {
                // // console.log("INSIDE FETCH JSSO google ticket", data);
                // gaTracking(options.eventGAObj, 'Success_google');
                processLogin(dispatch);
              })
              .catch(() => {});
          }
          // // console.log(tthisConstants.credential);
        }
      },
      error => {
        // // console.log('error');
        // // console.log(error);
        switch (error.type) {
          case 'userCanceled':
            // The user closed the hint selector. Depending on the desired UX,
            // request manual sign up or do nothing.
            analyticsGA.event(GAeventCategory, GAeventAction, 'Close');

            setCookie('gonetap', true, 15, '/', '', true); // expires in 15days.
            clearIframeCheckInterval();
            break;
          case 'noCredentialsAvailable':
            // No hint available for the session. Depending on the desired UX,
            // request manual sign up or do nothing.
            analyticsGA.event(GAeventCategory, GAeventAction, 'ErrorNoCredentialsAvailable');
            analyticsGA.event(GAeventCategory, GAeventAction, 'View');

            clearIframeCheckInterval();
            break;
          case 'requestFailed':
            // The request failed, most likely because of a timeout.
            // You can retry another time if necessary.

            // this.logGA('ErrorTimesout');
            analyticsGA.event(GAeventCategory, GAeventAction, 'ErrorTimesout');
            clearIframeCheckInterval();
            break;
          case 'operationCanceled':
            // The operation was programmatically canceled, do nothing.
            break;
          case 'illegalConcurrentRequest':
            // Another operation is pending, this one was aborted.
            break;
          case 'initializationError':
            // Failed to initialize. Refer to error.message for debugging.
            break;
          case 'configurationError':
            // Configuration error. Refer to error.message for debugging.
            break;
          default:
          // Unknown error, do nothing.
        }
      },
    );
  };
};

export const saveUserData = dispatch => {
  storeUserData(dispatch);
};

export const OneTapIntialize = (options, dispatch) => {
  if (getCookie('ssoid')) {
    storeUserData(dispatch);
  } else {
    const isChrome =
      navigator.userAgent.toLowerCase().match('crios') ||
      navigator.userAgent.toLowerCase().match('chrome');
    const isFirefox = navigator.userAgent.toLowerCase().match('firefox');
    const isSafari = navigator.userAgent.toLowerCase().match('safari');
    const nonSupportedBrowsers =
      navigator.userAgent.toLowerCase().match('kaios') ||
      navigator.userAgent.toLowerCase().match('presto') ||
      navigator.userAgent.toLowerCase().match('kaios') ||
      navigator.userAgent.toLowerCase().match('ucbrowser') ||
      navigator.userAgent.toLowerCase().match('ucmini');
    if (
      !getCookie('onetapCookie') &&
      (isChrome || isSafari || isFirefox) &&
      !nonSupportedBrowsers &&
      typeof window !== 'undefined'
    ) {
      // // console.log('load smart lock js');
      bindEvents(options, dispatch);
      // load js after non logged in user is on page for specified seconds
      setTimeout(loadJS(options.urlSmartLock), 10000); // smartlockJsLoadTimer
    }
  }
};

export default { OneTapIntialize, saveUserData };
