export const PLAY_DOCKED_VIDEO = 'PLAY_DOCKED_VIDEO';
export const STOP_DOCKED_VIDEO = 'STOP_DOCKED_VIDEO';

export const playDockedVideo = data => ({
  type: PLAY_DOCKED_VIDEO,
  payload: data,
});

export const stopDockedVideo = () => ({
  type: STOP_DOCKED_VIDEO,
});
