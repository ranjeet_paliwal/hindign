/* eslint-disable implicit-arrow-linebreak */
import fetch from '../../utils/fetch/fetch';
import { UPDATE_NAV_SUCCESS } from '../actions';

export const actionTypes = {
  FETCH_PHOTOSHOW_REQUEST: 'FETCH_PHOTOSHOW_REQUEST',
  FETCH_PHOTOSHOW_SUCCESS: 'FETCH_PHOTOSHOW_SUCCESS',
  FETCH_PHOTOSHOW_FAILURE: 'FETCH_PHOTOSHOW_FAILURE',
  UPDATE_PHOTOSHOW_REQUEST: 'UPDATE_PHOTOSHOW_REQUEST',
  UPDATE_PHOTOSHOW_SUCCESS: 'UPDATE_PHOTOSHOW_SUCCESS',
  UPDATE_PHOTOSHOW_FAILURE: 'UPDATE_PHOTOSHOW_FAILURE',
};

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_PHOTOSHOW_FAILURE,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_PHOTOSHOW_SUCCESS,
    payload: data,
  };
}

function updatePhotoShowDataSuccess(photoshowData) {
  return {
    type: actionTypes.UPDATE_PHOTOSHOW_SUCCESS,
    payload: photoshowData,
  };
}

function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}
function updatePhotoShowDataFailure(error) {
  return {
    type: actionTypes.UPDATE_PHOTOSHOW_FAILURE,
    payload: error,
  };
}

function fetchData({ params }) {
  //
  const { id } = params; // id is defined in routes.js path expression

  // Main  data for video show page.
  const photoShowData = fetch(
    `${process.env.API_ENDPOINT}/pwagn_photoshow/${id}.cms?feedtype=json`,
  ).then(psData => {
    const { subsec1 } = psData;

    if (subsec1) {
      const headerApi = fetch(
        `${process.env.API_ENDPOINT}/webgn_nav.cms?type=navigation&msid=${subsec1}&feedtype=sjson`,
      ).catch(() => null);

      const photoshowRightApi = fetch(
        `${process.env.API_ENDPOINT}/webgn_photoshowright/${subsec1}.cms?feedtype=sjson`,
      ).catch(() => null);

      return Promise.all([headerApi, photoshowRightApi]).then(
        data => {
          return { headerData: data[0], photoShowData: psData, photoshowRightData: data[1] };
        },
        error => {
          return { photoShowData: psData, headerData: null, photoshowRightData: null };
        },
      );
    }
    return { photoShowData: psData, headerData: null, photoshowRightData: null };
  });

  // Ads data
  const promiseAds = fetch(
    'https://navbharattimesfeeds.indiatimes.com/pwafeeds/webgn_ads.cms?feedtype=sjson&pagetype=photoshow',
  );

  // const headerData = fetch(
  //   `${process.env.API_ENDPOINT}/webgn_nav.cms?type=navigation&msid=${id}&feedtype=sjson`,
  // );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_PHOTOSHOW_REQUEST,
    });

    return Promise.all([photoShowData, promiseAds].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            photoData: data[0] ? data[0].photoShowData : null,
            urlMsid: id,
            headerData: data[0] ? data[0].headerData : null,
            adsData: data[1],
            photoshowRightData: data[0] ? data[0].photoshowRightData : null,
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function shouldFetchData(state, params) {
  const { photoshow } = state;

  if (
    photoshow &&
    photoshow.data &&
    photoshow.data.photoshowData &&
    photoshow.data.photoshowData.it &&
    photoshow.data.photoshowData.it.id === params.id
  ) {
    return false;
  }
  return true;
}

export function updatePhotoShowData(photoShowId) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_PHOTOSHOW_REQUEST,
    });

    fetch(`${process.env.API_ENDPOINT}/pwagn_photoshow/${photoShowId}.cms?feedtype=json`).then(
      photoshowData => dispatch(updatePhotoShowDataSuccess(photoshowData)),
      error => dispatch(updatePhotoShowDataFailure(error)),
    );
  };
}

export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}
export function fetchDataIfNeeded({ query, params, history }) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}
