/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable max-len */
import fetch from '../../utils/fetch/fetch';
import { objToQueryStr } from '../../common-utility';
import { UPDATE_NAV_SUCCESS } from '../actions';
import {
  FETCH_BREADCRUMBS_REQUEST,
  fetchBreadCrumbsSuccess,
  fetchBreadCrumbsFailure,
} from '../nav/breadcrumbs';

// import fetchNewsDataIfNeeded from '../home/news/news';

const Config = require(`../../../common/${process.env.SITE}`);

export const actionTypes = {
  FETCH_GADGETSHOW_REQUEST: 'FETCH_GADGETSHOW_REQUEST',
  FETCH_GADGETSHOW_SUCCESS: 'FETCH_GADGETSHOW_SUCCESS',
  FETCH_GADGETSHOW_FAILURE: 'FETCH_GADGETSHOW_FAILURE',
  FETCH_RELATED_GADGETS_REQUEST: 'FETCH_RELATED_GADGETS_REQUEST',
  FETCH_RELATED_GADGETS_SUCCESS: 'FETCH_RELATED_GADGETS_SUCCESS',
  FETCH_RELATED_GADGETS_FAILURE: 'FETCH_RELATED_GADGETS_FAILURE',
};

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_GADGETSHOW_FAILURE,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_GADGETSHOW_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}

export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}

function fetchData(params) {
  let productId = '';
  if (params.splat && Array.isArray(params.splat)) {
    productId = params.splat.join('-');
    productId = productId && productId.toLowerCase();
  }

  const gadgetShowFeed = `${
    process.env.API_ENDPOINT
  }/pwagn_productshow.cms?pagetype=gadgetshow&productid=${productId}&category=${
    Config.gadgetMapping[params.category]
  }&uri=/tech/${params.category}/${productId}&platform=desktop&feedtype=sjson`;
  // console.log('productId', productId);
  // get productshow data
  const gadgetData = fetch(gadgetShowFeed).then(gData => {
    const { article } = gData;
    // console.log('==article====', article);
    if (article) {
      // get ads data
      const adsData = fetch(
        `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=gadgetshow&msid=${
          article.msid
        }`,
      );
      const latestTrendingData = fetch(
        `${
          process.env.API_ENDPOINT
        }/webgn_combine.cms?tag=secdata&secid=66635263,66635275&count=5&feedtype=sjson`,
      );
      return Promise.all([adsData, latestTrendingData]).then(
        data => {
          return { gadgetData: gData, adsData: data[0], latestTrendingData: data[1] };
        },
        error => {
          return { gadgetData: gData, adsData: null, latestTrendingData: null };
        },
      );
    }
    return { gadgetData: gData, adsData: null, latestTrendingData: null };
  });

  // const gadgetData = fetch(
  //   `${
  //     process.env.WEBSITE_URL
  //   }/pwa_productshow.cms?pagetype=gadgetshow&productid=${productId}&feedtype=sjson`,
  // );

  // get ads data
  // const adsData = fetch(
  //   `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=gadgetshow&msid=67521926`,
  // );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GADGETSHOW_REQUEST,
    });

    return Promise.all([gadgetData].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            gadgetData: data[0].gadgetData,
            adsData: data[0].adsData,
            latestTrendingData: data[0].latestTrendingData,
          }),
        ),
      e => dispatch(fetchDataFailure(e)),
    );
  };
}

function fetchRelatedGadgetsSuccess(data) {
  return {
    type: actionTypes.FETCH_RELATED_GADGETS_SUCCESS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchRelatedGadgetsFailure(e) {
  return {
    type: actionTypes.FETCH_RELATED_GADGETS_FAILURE,
    payload: e.message,
  };
}

function shouldFetchData(state, params) {
  let productId = '';
  // let stateProductId = '';
  if (params.splat && Array.isArray(params.splat)) {
    productId = params.splat.join('-');
  }
  // console.log('state11', state, params);

  const uname =
    state.gadgetshow &&
    state.gadgetshow.data &&
    state.gadgetshow.data.gadgetData &&
    Array.isArray(state.gadgetshow.data.gadgetData.gadget) &&
    state.gadgetshow.data.gadgetData.gadget[0] &&
    state.gadgetshow.data.gadgetData.gadget[0].uname;

  if (state.gadgetshow && !state.gadgetshow.data) {
    return true;
  }
  if (uname && productId && uname.toLowerCase() != productId.toLocaleLowerCase()) {
    return true;
  }
  return false;
}

export default function fetchDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData(params));
    }
    return Promise.resolve([]);
  };
}

function fetchRelatedGadgetArticle(params) {
  // Left section data for all related GADGETS

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_RELATED_GADGETS_REQUEST,
    });

    // // const gadgetData = fetch(
    // //   `${process.env.WEBSITE_URL}/pwa_productshow.cms?pagetype=gadgetshow&productid=${
    // //     params.id
    // //   }&category=${Config.gadgetMapping[params.category]}&feedtype=sjson`,
    // );

    const gadgetData = fetch(
      `${
        process.env.API_ENDPOINT
      }/pwagn_productshow.cms?pagetype=gadgetshow&productid=${params.id &&
        params.id.toLowerCase()}&category=${Config.gadgetMapping[params.category]}&uri=/tech/${
        params.category
      }/${params.id && params.id.toLowerCase()}&platform=desktop&feedtype=sjson`,
    ).then(gData => {
      const { article } = gData;
      // console.log('==article====', article);
      if (article) {
        // get ads data
        const adsData = fetch(
          `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=gadgetshow&msid=${
            article.msid
          }`,
        );
        return Promise.all([adsData]).then(
          data => {
            return { gadgetData: gData, adsData: data[0] };
          },
          error => {
            return { gadgetData: gData, adsData: null };
          },
        );
      }
      return { gadgetData: gData, adsData: null };
    });

    return Promise.all([gadgetData]).then(
      data => dispatch(fetchRelatedGadgetsSuccess(data[0])),
      error => dispatch(fetchRelatedGadgetsFailure(error)),
    );
  };
}

export function fetchRelatedGadgetData(params) {
  return (dispatch, getState) => {
    return dispatch(fetchRelatedGadgetArticle(params));
  };
}
