import fetch from '../../../utils/fetch/fetch';
import * as actionTypes from '../../actions';

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_FAILURE_MOSTREAD,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_SUCCESS_MOSTREAD,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchData() {
  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_REQUEST_MOSTREAD,
    });

    return fetch(
      `${
        process.env.API_ENDPOINT
      }/webgn_combine.cms?type=article&tag=ibeatmostread,nbtroforchannel&days=0&count=10&feedtype=sjson`,
    ).then(data => dispatch(fetchDataSuccess(data)), error => dispatch(fetchDataFailure(error)));
  };
}

function shouldFetchData(state) {
  const mostread = state.mostread;
  return !(mostread.value.length || mostread.isFetching);
}

export function fetchDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState())) {
      return dispatch(fetchData(params));
    }

    return Promise.resolve([]);
  };
}
