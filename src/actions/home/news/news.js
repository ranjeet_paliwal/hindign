/* eslint-disable max-len */
import fetch from '../../../utils/fetch/fetch';
import * as actionTypes from '../../actions';

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_FAILURE_NEWS,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  // const pageHeaderData = {};
  // pageHeaderData.nav = data[2] ? data[2] : null;

  const updatedData = {
    headline: data[0] ? data[0] : [],
    pageMeta: data[1] ? data[1] : {},
    // navData: pageHeaderData,
    sectionlist: data[2] ? data[2] : {},
    sectionData: data[3] ? data[3] : {},
    PhotosData: data[4] ? data[4] : {},
    VideosData: data[5] ? data[5] : {},
  };

  return {
    type: actionTypes.FETCH_SUCCESS_NEWS,
    meta: {
      receivedAt: Date.now(),
    },
    payload: updatedData,
  };
}

function fetchData() {
  const headlineData = fetch(
    `${
      process.env.API_ENDPOINT
    }/pwagn_home/66357606.cms?type=headline&feedtype=sjson&platform=web&count=17`,
  );

  // meta data api
  const meta = fetch(`${process.env.API_ENDPOINT}/webgn_meta/66357606.cms?feedtype=sjson`);

  // Page navbar main links
  // const navBarMainLinks = fetch(
  //   `http://langdev9338.indiatimes.com/web_nav.cms?feedtype=sjson&type=navbar`,
  // ).catch(() => ({}));

  const sectionData = fetch(
    `${
      process.env.API_ENDPOINT
    }/webgn_combine.cms?type=article&tag=solrreview,latestnews,ibeatmostread,ibeatmostshared&count=15&feedtype=sjson`,
  );

  const sectionData1 = fetch(
    `${
      process.env.API_ENDPOINT
    }/webgn_combine.cms?tag=secdata&secid=66131052,68079558,67572099&count=6&feedtype=sjson`,
  );
  //FIXME: Confirm if hardcoding needs to be changed
  const PhotosData = fetch(
    `${
      process.env.API_ENDPOINT
    }/webgn_homelist.cms?type=photo&count=11&feedtype=sjson&secid=19829828&platform=webgn`,
  );

  //FIXME: Confirm if hardcoding needs to be changed
  const VideosData = fetch(
    `${
      process.env.API_ENDPOINT
    }/webgn_combine.cms?type=video&count=11&feedtype=sjson&secid=20104392&platform=webgn`,
  );

  /* Header data  homepage calls stops here */

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_REQUEST_NEWS,
    });

    return Promise.all(
      [headlineData, meta, sectionData, sectionData1, PhotosData, VideosData].map(p =>
        p.catch(e => e),
      ),
    )
      .then(data => dispatch(fetchDataSuccess(data)))
      .catch(error => dispatch(fetchDataFailure(error)));
  };
}

function shouldFetchData(state) {
  const news = state.news;
  return !(news.value.length || news.didFetch);
}

export default function fetchDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState())) {
      return dispatch(fetchData(params));
    }
    return Promise.resolve([]);
  };
}
