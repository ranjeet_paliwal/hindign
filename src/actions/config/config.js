/* eslint-disable import/prefer-default-export */
/* eslint-disable operator-linebreak
   eslint-disable prefer-const */
import fetch from '../../utils/fetch/fetch';
import * as actionTypes from '../actions';

/* Fetching Nav Data */
function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_FAILURE_CONFIG,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_SUCCESS_CONFIG,
    meta: {
      receivedAt: Date.now(),
    },
    payload: data,
  };
}

function fetchData(pagetype) {
  const apiAds = `${process.env.API_ENDPOINT}/webgn_ads.cms?pagetype=${pagetype}&feedtype=sjson`;
  const p1 = fetch(apiAds);
  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_REQUEST_CONFIG,
    });
    return Promise.resolve(p1).then(
      data => dispatch(fetchDataSuccess(data)),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function shouldFetchData(state) {
  return true;
}

export function getAds(pageType) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState())) {
      return dispatch(fetchData(pageType));
    }

    return Promise.resolve([]);
  };
}
