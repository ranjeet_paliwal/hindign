/* eslint-disable max-len */
import fetch from '../../utils/fetch/fetch';
import { objToQueryStr } from '../../common-utility';
import { UPDATE_NAV_SUCCESS } from '../actions';
import {
  FETCH_BREADCRUMBS_REQUEST,
  fetchBreadCrumbsSuccess,
  fetchBreadCrumbsFailure,
} from '../nav/breadcrumbs';

// import fetchNewsDataIfNeeded from '../home/news/news';

const Config = require(`../../../common/${process.env.SITE}`);

export const actionTypes = {
  FETCH_ARTICLESHOW_REQUEST: 'FETCH_ARTICLESHOW_REQUEST',
  FETCH_ARTICLESHOW_SUCCESS: 'FETCH_ARTICLESHOW_SUCCESS',
  FETCH_ARTICLESHOW_FAILURE: 'FETCH_ARTICLESHOW_FAILURE',
  FETCH_RELATED_ARTICLES_REQUEST: 'FETCH_RELATED_ARTICLES_REQUEST',
  FETCH_RELATED_ARTICLES_SUCCESS: 'FETCH_RELATED_ARTICLES_SUCCESS',
  FETCH_RELATED_ARTILES_FAILURE: 'FETCH_RELATED_ARTILES_FAILURE',
};

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_ARTICLESHOW_FAILURE,
    payload: error.message,
  };
}

function getMetaData(articleData) {
  const pgURL = articleData.wu;
  const keywords =
    articleData && articleData.keywords && Array.isArray(articleData.keywords)
      ? articleData.keywords.map(item => item.hl)
      : '';

  const noFollowMeta = {
    name: 'ROBOTS',
    content: 'NOINDEX, NOFOLLOW',
  };

  const metaTags = [
    {
      name: 'description',
      content: articleData && articleData.pwa_meta && articleData.pwa_meta.desc,
    },
    {
      name: 'keywords',
      content: (keywords && keywords.join(', ')) || '',
    },
    {
      name: 'news_keywords',
      content: keywords,
    },
    {
      property: 'article:published_time',
      content: articleData.dlseo,
    },
    {
      property: 'article:modified_time',
      content: articleData.luseo,
    },
    {
      property: 'og:updated_time',
      content: articleData.luseo,
    },
    {
      property: 'og:title',
      content: articleData.hl,
    },
    {
      property: 'og:description',
      content: articleData.pwa_meta.desc,
    },
    {
      property: 'og:url',
      content: pgURL,
    },
    {
      property: 'og:image',
      content: articleData.pwa_meta.ogimg,
    },
    {
      property: 'twitter:title',
      content: articleData.hl,
    },
    {
      property: 'twitter:description',
      content: articleData.pwa_meta.desc,
    },
    {
      property: 'twitter:url',
      content: pgURL,
    },
  ];

  if (articleData && articleData.pwa_meta && articleData.pwa_meta.noindex == 1) {
    metaTags.push(noFollowMeta);
  }

  const metaLinks = [
    {
      rel: 'canonical',
      href: articleData.pwa_meta.canonical,
    },
  ];

  if (
    articleData &&
    articleData.pwa_meta &&
    articleData.pwa_meta.amphtml &&
    articleData.pwa_meta.noindex != 1
  ) {
    const ampMeta = {
      rel: 'amphtml',
      href: articleData.pwa_meta.amphtml,
    };
    metaLinks.push(ampMeta);
  }

  return { keywords, metaTags, metaLinks };
}

function fetchDataSuccess(data) {
  if (data) {
    const { articleData, commentsData, topCommentsData, adsData, staticMeta } = data;
    const dynamicMeta = articleData ? getMetaData(articleData) : '';
    dynamicMeta.metaTags = dynamicMeta.metaTags.concat(staticMeta.meta ? staticMeta.meta : []);
    dynamicMeta.metaLinks = dynamicMeta.metaLinks.concat(staticMeta.links ? staticMeta.links : []);

    const allData = {
      articleData,
      commentsData,
      topCommentsData,
      adsData,
      metaData: dynamicMeta,
    };

    return {
      type: actionTypes.FETCH_ARTICLESHOW_SUCCESS,
      meta: {
        receivedAt: Date.now(),
      },
      payload: allData,
    };
  }

  return '';
}

function fetchRelatedArticlesSuccess(data, dataLength, ids) {
  const articlesLeftData = data.slice(0, dataLength);
  const topCommentsData = data.slice(dataLength, dataLength * 2);
  const commentsData = data.slice(dataLength * 2, dataLength * 3);
  const adsData = data.slice(dataLength * 3);

  let relatedArticlesData = articlesLeftData.map((a, i) => ({
    id: ids[i],
    articleData: a,
    topCommentsData: topCommentsData[i],
    commentsData: commentsData[i],
    adsData: adsData[i],
  }));

  // Filter all values with non empty or null articleData
  // const filteredIds = relatedArticlesData
  //   .filter(relatedArtData => !relatedArtData.articleData)
  //   .map(data => data.id);

  relatedArticlesData = relatedArticlesData.filter(relatedArtData => relatedArtData.articleData);

  return {
    type: actionTypes.FETCH_RELATED_ARTICLES_SUCCESS,
    payload: relatedArticlesData,
  };
}

function fetchRelatedArticlesFailure(e) {
  return {
    type: actionTypes.FETCH_ARTICLESHOW_FAILURE,
    payload: e,
  };
}

function fetchData(params) {
  // Main article data (LHS)
  const articleData = fetch(
    `${process.env.API_BASEPOINT}/webgn_articleshow/${params.id}.cms?feedtype=sjson&version=v9`,
  );

  // Comments data for article
  const commentsParams = Object.assign({}, Config.Comments.allComments, { msid: params.id });

  const articleComments = fetch(
    `${process.env.API_ENDPOINT}/commentsdata.cms?${objToQueryStr(commentsParams)}`,
  );

  // Top comments data

  const topCommentsParams = Object.assign({}, Config.Comments.topComments, { msid: params.id });

  const topComments = fetch(
    `${process.env.API_ENDPOINT}/commentsdata.cms?${objToQueryStr(topCommentsParams)}`,
  );

  // Ads data for articles
  const articleAds = fetch(
    `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=articleshow&msid=${
      params.id
    }`,
  )
    .then(response => {
      response.articleId = params.id;
      return response;
    })
    .catch(() => ({}));

  // Promise meta tags
  const promiseMetatags = fetch(
    `${process.env.API_ENDPOINT}/webgn_meta.cms?pagetype=AS&feedtype=sjson`,
  )
    .then(response => {
      response.articleId = params.id;
      return response;
    })
    .catch(() => ({}));

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_ARTICLESHOW_REQUEST,
    });

    dispatch({
      type: FETCH_BREADCRUMBS_REQUEST,
    });

    return Promise.all(
      [articleData, articleComments, topComments, articleAds, promiseMetatags].map(p =>
        p.catch(() => null),
      ),
    )
      .then(data => {
        const breadCrumbData = data[0] && data[0] ? data[0] : null;
        dispatch(fetchBreadCrumbsSuccess(breadCrumbData));
        dispatch(
          fetchDataSuccess({
            articleData: data[0] && data[0] ? data[0] : null,
            commentsData: data[1],
            topCommentsData: data[2],
            adsData: data[3],
            staticMeta: data[4],
          }),
        );
      })
      .catch(error => {
        dispatch(fetchDataFailure(error));
        dispatch(fetchBreadCrumbsFailure(error));
      });
  };
}

function shouldFetchData(state, params) {
  if (state.articleshow && state.articleshow.data && state.articleshow.data.articleData) {
    return !(state.articleshow.data.articleData.id === params.id);
  }
  return true;
}

function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}

export default function fetchDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData(params));
    }
    return Promise.resolve([]);
  };
}

export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}

function fetchRelatedArticles(ids) {
  // Left section data for all related articles
  const articleLeftPromisesArray = ids.map(id => {
    return fetch(
      `${process.env.API_BASEPOINT}/webgn_articleshow/${id}.cms?feedtype=sjson&version=v9`,
    )
      .then(response => {
        return response;
      })
      .catch(() => null);
  });

  // Right section data for all related articles
  // const articleRightPromisesArray = ids.map(id => {
  //   return fetch(
  //     `${
  //       process.env.API_ENDPOINT
  //     }/webgn_common.cms?feedtype=sjson&msid=${id}&tag=video,ibeatmostread,mostpopularL1,mostpopularL2,trending`,
  //   )
  //     .then(response => {
  //       return response;
  //     })
  //     .catch(() => ({})); // Returns object from catch, else undefined returned.
  // });

  // Top comments data
  const topCommentsParams = Object.assign({}, Config.Comments.topComments);

  // const topCommentsUrl = `${process.env.API_ENDPOINT}/commentsdata.cms?${objToQueryStr(
  //   topCommentsParams,
  // )}`;

  const articleTopCommentsPromisesArray = ids.map(id => {
    return fetch(
      `${process.env.API_ENDPOINT}/commentsdata.cms?${objToQueryStr(topCommentsParams)}&msid=${id}`,
    ).catch(() => null);
  });

  // All comments data for all related articles

  const commentsParams = Object.assign({}, Config.Comments.allComments);

  const articleAllCommentsPromisesArray = ids.map(id => {
    return fetch(
      `${process.env.API_ENDPOINT}/commentsdata.cms?${objToQueryStr(commentsParams)}&msid=${id}`,
    ).catch(() => null);
  });

  // Ads data for all related articles

  const adsDataPromisesArray = ids.map(id => {
    return fetch(
      `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=articleshow&msid=${id}`,
    )
      .then(response => {
        response.articleId = id;
        return response;
      })
      .catch(() => null);
  });

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_RELATED_ARTICLES_REQUEST,
    });

    return Promise.all([
      ...articleLeftPromisesArray,
      ...articleTopCommentsPromisesArray,
      ...articleAllCommentsPromisesArray,
      ...adsDataPromisesArray,
    ]).then(
      // Use ids to map article ids ( helpful in case one article goes blank and we need
      // to filter it from the bottom article navigation links)
      data => dispatch(fetchRelatedArticlesSuccess(data, ids.length, ids)),
      error => dispatch(fetchRelatedArticlesFailure(error)),
    );
  };
}

export function fetchRelatedArticlesData(ids) {
  return (dispatch, getState) => {
    return dispatch(fetchRelatedArticles(ids));
  };
}
