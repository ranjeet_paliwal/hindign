/* eslint-disable implicit-arrow-linebreak */
import fetch from '../../utils/fetch/fetch';
import { UPDATE_NAV_SUCCESS } from '../actions';

export const actionTypes = {
  FETCH_VIDEOLIST_REQUEST: 'FETCH_VIDEOLIST_REQUEST',
  FETCH_VIDEOLIST_SUCCESS: 'FETCH_VIDEOLIST_SUCCESS',
  FETCH_VIDEOLIST_FAILURE: 'FETCH_VIDEOLIST_FAILURE',
  UPDATE_VIDEOLIST_REQUEST: 'UPDATE_VIDEOLIST_REQUEST',
  UPDATE_VIDEOLIST_SUCCESS: 'UPDATE_VIDEOLIST_SUCCESS',
};

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_VIDEOLIST_FAILURE,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_VIDEOLIST_SUCCESS,
    payload: data,
  };
}
function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}

function fetchData({ query, params }) {
  const { id } = params; // id is defined in routes.js path expression
  const pageno = query && query.curpg ? parseInt(query.curpg) : 1;

  const promiseList = fetch(
    `${process.env.API_ENDPOINT}/webgn_videolist/${id}.cms?feedtype=sjson&pagetype=list&curpg=${pageno}`,
  );

  const promiseAds = fetch(
    `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=articlelist&msid=${id}`,
  );

  const headerData = fetch(
    `${process.env.API_ENDPOINT}/webgn_nav.cms?type=navigation&msid=${id}&feedtype=sjson`,
  );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_VIDEOLIST_REQUEST,
    });

    return Promise.all([promiseList, promiseAds, headerData].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            videolistData: data[0],
            adsData: data[1],
            headerData: data[2],
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function shouldFetchData(state, params) {
  const { id } = params;
  if (state && state.videolist && state.videolist.data && state.videolist.data.videolistData) {
    return !(state.videolist.data.videolistData.id === id);
  }
  return true;
}

function updateListSuccess(data) {
  return {
    type: actionTypes.UPDATE_VIDEOLIST_SUCCESS,
    payload: data,
  };
}

function updateListFailure() {}

function updateList(params) {
  const { id, pgno } = params;
  return dispatch => {
    return fetch(
      `${process.env.API_ENDPOINT}/webgn_videolist/${id}.cms?feedtype=sjson&pagetype=list&curpg=${pgno}`,
    ).then(data => dispatch(updateListSuccess(data)), error => dispatch(updateListFailure(error)));
  };
}

export function updateListData(params) {
  return dispatch => {
    return dispatch(updateList(params));
  };
}

export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}
export function fetchDataIfNeeded({ query, params, history, isForcedUpdate }) {
  return (dispatch, getState) => {
    if (isForcedUpdate || shouldFetchData(getState(), params)) {
      return dispatch(fetchData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}
