/* eslint-disable implicit-arrow-linebreak */
import fetch from '../../../utils/fetch/fetch';
import { UPDATE_NAV_SUCCESS } from '../../actions';

export const actionTypes = {
  FETCH_GADGET_LIST_REQUEST: 'FETCH_GADGET_LIST_REQUEST',
  FETCH_GADGET_LIST_SUCCESS: 'FETCH_GADGET_LIST_SUCCESS',
  UPDATE_GADGET_LIST_SUCCESS: 'UPDATE_GADGET_LIST_SUCCESS',
};

const siteConfig = require(`../../../../common/${process.env.SITE}`);

function fetchDataFailure(error) {
  // console.log(error);
}

function updateGadgetFailure(error) {
  // console.log(error);
}

function fetchDataSuccess(listData) {
  return {
    type: actionTypes.FETCH_GADGET_LIST_SUCCESS,
    payload: listData,
  };
}

function fetchData({ query, params }) {
  // Main article data (LHS)
  const { device } = params; // articleId is defined in routes.js path expression

  // const pageno = query && query.curpg ? parseInt(query.curpg) : 1;
  const gadgetCategory =
    (siteConfig && siteConfig.gadgetMapping && siteConfig.gadgetMapping[device]) || '';
  // console.log('fetchData [COM Detail]', compareString, device);
  // get articlelist data

  const promiseAds = fetch(
    `${process.env.API_ENDPOINT}/webgn_ads.cms?pagetype=compare&feedtype=sjson`,
  );

  const promisePopularNLatest = fetch(
    `${
      process.env.API_ENDPOINT
    }/pwagn_comparisonlist.cms?pagetype=comparelist&category=${gadgetCategory}&platform=desktop&feedtype=sjson`,
  );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GADGET_LIST_REQUEST,
    });

    // dispatch(fetchHeaderData(articleId));

    return Promise.all([promisePopularNLatest, promiseAds].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            compareData: data[0],
            ads: data[1],
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function updateGadgetSuccess(data) {
  return {
    type: actionTypes.UPDATE_GADGET_LIST_SUCCESS,
    payload: data,
  };
}

function updateGadget(deviceType) {
  // const { articleId, pgno } = params;
  // console.log('updateGadget', deviceType);

  return dispatch => {
    return fetch(
      `${
        process.env.API_ENDPOINT
      }/pwagn_comparisonlist.cms?pagetype=comparelist&category=${deviceType}&platform=desktop&feedtype=sjson`,
    ).then(
      data => dispatch(updateGadgetSuccess(data)),
      error => dispatch(updateGadgetFailure(error)),
    );
  };
}

export function updateGadgetData(deviceType) {
  return dispatch => {
    return dispatch(updateGadget(deviceType));
  };
}
function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}
export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}
function shouldFetchData(state, params) {
  const { articleId } = params;
  // if (state && state.articlelist && state.articlelist.data && state.articlelist.data.articleData) {
  //   return !(state.articlelist.data.articleData.id === articleId);
  // }

  if (state && state.compareList && !state.compareList.data) {
    return true;
  }

  if (
    state &&
    state.compareList &&
    state.compareList.data &&
    state.compareList.data.listData &&
    state.compareList.data.listData.compareData &&
    state.compareList.data.listData.compareData.popularGadgetPair &&
    state.compareList.data.listData.compareData.popularGadgetPair.compare &&
    state.compareList.data.listData.compareData.popularGadgetPair.compare[0]
  ) {
    const gadgetCategory =
      state.compareList.data.listData.compareData.popularGadgetPair.compare[0].category;
    console.log('gadgetCategory', gadgetCategory);
    if (gadgetCategory && params && params.device && params.device.indexOf(gadgetCategory) === -1) {
      return true;
    }
    return false;
  }

  return false;
}

export function fetchDataIfNeeded({ query, params, history }) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}
