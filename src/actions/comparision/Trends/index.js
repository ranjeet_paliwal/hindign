/* eslint-disable implicit-arrow-linebreak */
import fetch from '../../../utils/fetch/fetch';
import { UPDATE_NAV_SUCCESS } from '../../actions';

export const actionTypes = {
  FETCH_GADGET_TRENDS_REQUEST: 'FETCH_GADGET_TRENDS_REQUEST',
  FETCH_GADGET_TRENDS_SUCCESS: 'FETCH_GADGET_TRENDS_SUCCESS',
  UPDATE_GADGET_TRENDS_SUCCESS: 'UPDATE_GADGET_TRENDS_SUCCESS',
};

const siteConfig = require(`../../../../common/nbt`);

function fetchDataFailure(error) {
  // console.log(error);
}

function updateGadgetFailure(error) {
  // console.log(error);
}

function fetchDataSuccess(listData) {
  return {
    type: actionTypes.FETCH_GADGET_TRENDS_SUCCESS,
    payload: listData,
  };
}

function fetchData({ query, params, pathname }) {
  // Main article data (LHS)
  const { device } = params; // articleId is defined in routes.js path expression
  //const { compareString } = pathname || '';

  // const pageno = query && query.curpg ? parseInt(query.curpg) : 1;
  const gadgetCategory =
    (siteConfig && siteConfig.gadgetMapping && siteConfig.gadgetMapping[device]) || '';
  console.log('query name', pathname);
  // get articlelist data

  const promiseAds = fetch(
    `${process.env.API_ENDPOINT}/webgn_ads.cms?pagetype=compare&feedtype=sjson`,
  );

  const promisePopularNLatest = fetch(
    `${
      process.env.API_ENDPOINT
    }/pwagn_comparetrends.cms?pagetype=${pathname}&category=${gadgetCategory}&feedtype=sjson`,
  );

  // console.log(
  //   `${
  //     process.env.API_ENDPOINT
  //   }/pwagn_comparetrends.cms?pagetype=${compareString}&category=${gadgetCategory}&feedtype=sjson`,
  // );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_GADGET_TRENDS_REQUEST,
    });

    // dispatch(fetchHeaderData(articleId));

    return Promise.all([promisePopularNLatest, promiseAds].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            compareData: data[0],
            ads: data[1],
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function updateGadgetSuccess(data) {
  return {
    type: actionTypes.UPDATE_GADGET_TRENDS_SUCCESS,

    payload: data,
  };
}

function updateGadget(deviceType) {
  // const { articleId, pgno } = params;
  // console.log('updateGadget', deviceType);

  return dispatch => {
    return fetch(
      `${
        process.env.API_ENDPOINT
      }/pwagn_comparetrends.cms?pagetype=latest-comparisons&category=${deviceType}&feedtype=sjson`,
    ).then(
      data => dispatch(updateGadgetSuccess(data)),
      error => dispatch(updateGadgetFailure(error)),
    );
  };
}

export function updateGadgetData(deviceType) {
  return dispatch => {
    return dispatch(updateGadget(deviceType));
  };
}
function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}
export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}
function shouldFetchData(state, params, pathname) {
  const { articleId } = params;

  if (state && state.trendsComparision && !state.trendsComparision.data) {
    return true;
  }

  if (
    state &&
    state.trendsComparision &&
    state.trendsComparision.data &&
    state.trendsComparision.data.compare &&
    state.trendsComparision.data.compare[0] &&
    state.trendsComparision.data.compare[0].category
    //&& state.trendsComparision.data.
    //need to check feed path name
  ) {
    const gadgetCategory = state.trendsComparision.data.compare[0];
    if (gadgetCategory && params && params.device && params.device.indexOf(gadgetCategory) === -1) {
      return true;
    }
    return false;
  }

  return true;
}

export function fetchDataIfNeeded({ query, params, history, pathname }) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params, pathname)) {
      return dispatch(fetchData({ query, params, history, pathname }));
    }
    return Promise.resolve([]);
  };
}
