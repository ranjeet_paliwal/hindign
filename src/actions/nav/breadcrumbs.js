export const FETCH_BREADCRUMBS_REQUEST = 'FETCH_BREADCRUMBS_REQUEST';
export const FETCH_BREADCRUMBS_SUCCESS = 'FETCH_BREADCRUMBS_SUCCESS';
export const FETCH_BREADCRUMBS_FAILURE = 'FETCH_BREADCRUMBS_FAILURE';

export function fetchBreadCrumbsSuccess(data) {
  return {
    type: FETCH_BREADCRUMBS_SUCCESS,
    payload: data,
  };
}

export function fetchBreadCrumbsFailure(error) {
  return {
    type: FETCH_BREADCRUMBS_FAILURE,
    payload: error,
  };
}
