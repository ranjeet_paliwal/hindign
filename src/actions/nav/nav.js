/* eslint-disable no-case-declarations */
/* eslint-disable default-case */
/* eslint-disable operator-linebreak
   eslint-disable prefer-const */
import fetch from '../../utils/fetch/fetch';
import * as actionTypes from '../actions';

/* Fetching Nav Data */
function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_FAILURE_NAV,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  const updatedData = {};

  updatedData.topnav = data[0] ? data[0] : {};
  updatedData.footer = data[1] ? data[1] : {};
  updatedData.navData = data[2] ? data[2] : {};
  return {
    type: actionTypes.FETCH_SUCCESS_NAV,
    meta: {
      receivedAt: Date.now(),
    },
    payload: updatedData,
  };
}

function fetchData() {
  const topnavData = fetch(`${process.env.API_ENDPOINT}/webgn_nav.cms?feedtype=sjson&type=topnav`);

  const footerData = fetch(`${process.env.API_ENDPOINT}/webgn_nav.cms?feedtype=sjson&type=footer`);

  const navBarMainLinks = fetch(
    `${process.env.API_ENDPOINT}/webgn_nav.cms?feedtype=sjson&type=navbar`,
  ).catch(() => ({}));

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_REQUEST_NAV,
    });

    return Promise.all([topnavData, footerData, navBarMainLinks].map(p => p.catch(e => ({}))))
      .then(data => dispatch(fetchDataSuccess(data)))
      .catch(error => dispatch(fetchDataFailure(error)));
  };
}

function shouldFetchData(state) {
  const nav = state.nav;
  if (!nav.value.length || !nav.hoverData.video || !nav.hoverData.news) {
    return true;
  }
  return false;
}

function fetchHoverDataSuccess(data, secName) {
  return {
    type: actionTypes.FETCH_SUCCESS_HOVER,
    payload: {
      data,
      secName,
    },
  };
}

function fetchHoverDataFailure(secName) {
  return {
    type: actionTypes.FETCH_FAILURE_HOVER,
    payload: {
      secName,
    },
  };
}

function fetchHoverData(secName) {
  const dataApi = `${process.env.API_ENDPOINT}/webgn_combine.cms?type=list&count=10&feedtype=sjson`;
  let sectionData;
  let leftNavData;
  switch (secName) {
    // For articleshow  header home link hover

    case 'gadgets-news':
    case 'tips-tricks':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });

        const sectionId = {
          'gadgets-news': '66130905',
          'tips-tricks': '66131052',
        };

        sectionData = fetch(`${dataApi}&secid=${sectionId[secName]}`).catch(e => null);
        Promise.all([sectionData]).then(
          data => {
            const linksData =
              data[0] && data[0].section && data[0].section ? data[0].section : null;
            const response = { linksData };
            // console.log('secName fetch hoversucess', linksData);
            dispatch(fetchHoverDataSuccess(response, secName));
          },
          error => dispatch(fetchHoverDataFailure(secName)),
        );
      };

    case 'video':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });
        fetch(
          `${
            process.env.API_ENDPOINT
          }/webgn_homelist.cms?type=video&count=6&feedtype=sjson&secid=20104392`,
        )
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };

    case 'photo':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });
        fetch(
          `${
            process.env.API_ENDPOINT
          }/webgn_homelist.cms?type=photo&count=6&feedtype=sjson&secid=19829828`,
        )
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };

    case 'reviews':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });
        fetch(
          `${
            process.env.API_ENDPOINT
          }/webgn_combine.cms?type=article&tag=solrreview&count=15&feedtype=sjson`,
        )
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };

    case 'compare':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });
        fetch(`${process.env.API_ENDPOINT}/tech/webgn_homelist.cms?tag=compare&feedtype=sjson`)
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };

    case 'gadgets':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });
        fetch(`${process.env.API_ENDPOINT}/webgn_gadgetsnav.cms?feedtype=sjson`)
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };

    case 'shop':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });
        fetch(`${process.env.API_ENDPOINT}/tech/webgn_homelist.cms?tag=gadgetshop&feedtype=sjson`)
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };

    // case 'gadgets':
    //   return dispatch => {
    //     dispatch({
    //       type: actionTypes.FETCH_REQUEST_HOVER,
    //       secName,
    //     });

    //     leftNavData = fetch(
    //       `https://navbharattimes.indiatimes.com/webgn_nav.cms?type=navbar&section=navleft&feedtype=sjson&subsection=videos`,
    //     ).catch(e => null);

    //     // Call for all sections
    //     sectionData = fetch(
    //       `${
    //         process.env.API_ENDPOINT
    //       }/webgn_combine.cms?type=video&count=12&feedtype=sjson&secid=4901886,4901881,59429326,11872954,11873677,50094540,4901877,5556078,4901872,59541120,57791552,48972527,19946472,4901869`,
    //     ).catch(e => null);

    //     Promise.all([leftNavData, sectionData])
    //       .then(data => {
    //         leftNavData = data[0];
    //         sectionData = data[1];
    //         dispatch(fetchHoverDataSuccess({ leftNavData, sectionData }, secName));
    //       })
    //       .catch(e => {
    //         dispatch(fetchHoverDataFailure(secName));
    //       });
    //   };

    case 'state':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });

        fetch(`${process.env.API_ENDPOINT}/webgn_nav.cms?type=navbar&section=rajya&feedtype=sjson#`)
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };

    case 'travel':
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });

        fetch(
          `${
            process.env.API_ENDPOINT
          }/webgn_combine.cms?type=list&count=6&feedtype=sjson&secid=64682779,64682864,64682800,4012427,64682845,64682758`,
        )
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };

    default:
      return dispatch => {
        dispatch({
          type: actionTypes.FETCH_REQUEST_HOVER,
          secName,
        });

        fetch(
          `${
            process.env.API_ENDPOINT
          }/webgn_combine.cms?type=list&count=6&feedtype=sjson&secid=2303512,2325387,67179225`,
        )
          .then(response => {
            dispatch(fetchHoverDataSuccess(response, secName));
          })
          .catch(e => {
            dispatch(fetchHoverDataFailure(secName));
          });
      };
  }
}

export function fetchHoverSection(sectionName) {
  console.log('secname dispatch', sectionName);
  return (dispatch, getState) => {
    if (shouldFetchData(getState())) {
      return dispatch(fetchHoverData(sectionName));
    }
  };
}

export function fetchDataIfNeeded(params) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState())) {
      return dispatch(fetchData(params));
    }

    return Promise.resolve([]);
  };
}
