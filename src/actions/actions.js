export const FETCH_REQUEST_NAV = 'FETCH_REQUEST_NAV';
export const FETCH_SUCCESS_NAV = 'FETCH_SUCCESS_NAV';
export const FETCH_FAILURE_NAV = 'FETCH_FAILURE_NAV';

export const FETCH_REQUEST_HOVER = 'FETCH_REQUEST_HOVER';
export const FETCH_SUCCESS_HOVER = 'FETCH_SUCCESS_HOVER';
export const FETCH_FAILURE_HOVER = 'FETCH_FAILURE_HOVER';

export const FETCH_REQUEST_NEWS = 'FETCH_REQUEST_NEWS';
export const FETCH_SUCCESS_NEWS = 'FETCH_SUCCESS_NEWS';
export const FETCH_FAILURE_NEWS = 'FETCH_FAILURE_NEWS';

export const FETCH_REQUEST_MOSTREAD = 'FETCH_REQUEST_MOSTREAD';
export const FETCH_SUCCESS_MOSTREAD = 'FETCH_SUCCESS_MOSTREAD';
export const FETCH_FAILURE_MOSTREAD = 'FETCH_FAILURE_MOSTREAD';

export const FETCH_REQUEST_CONFIG = 'FETCH_REQUEST_CONFIG';
export const FETCH_SUCCESS_CONFIG = 'FETCH_SUCCESS_CONFIG';
export const FETCH_FAILURE_CONFIG = 'FETCH_FAILURE_CONFIG';

export const UPDATE_SIGN_DATA = 'UPDATE_SIGN_DATA';

export const STORE_USER_DETAILS = 'STORE_USER_DETAILS';
export const REMOVE_USER_DETAILS = 'REMOVE_USER_DETAILS';

// Active section on Route Change
export const UPDATE_NAV_REQUEST = 'UPDATE_NAV_REQUEST';
export const UPDATE_NAV_SUCCESS = 'UPDATE_NAV_SUCCESS';
export const UPDATE_NAV_FAILURE = 'UPDATE_NAV_FAILURE';

// Header data on route change
export const FETCH_HEADER_DATA_REQUEST = 'FETCH_HEADER_DATA_REQUEST';
export const FETCH_HEADER_DATA_SUCCESS = 'FETCH_HEADER_DATA_SUCCESS';
export const FETCH_HEADER_DATA_FAILURE = 'FETCH_HEADER_DATA_FAILURE';

// Page header data actions
export const FETCH_PAGE_HEADER_REQUEST = 'FETCH_PAGE_HEADER_REQUEST';
export const FETCH_PAGE_HEADER_SUCCESS = 'FETCH_PAGE_HEADER_SUCCESS';
export const FETCH_PAGE_HEADER_FAILURE = 'FETCH_PAGE_HEADER_FAILURE';
