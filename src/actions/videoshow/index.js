/* eslint-disable implicit-arrow-linebreak */
import fetch from '../../utils/fetch/fetch';
import { UPDATE_NAV_SUCCESS } from '../actions';

export const actionTypes = {
  FETCH_VIDEOSHOW_REQUEST: 'FETCH_VIDEOSHOW_REQUEST',
  FETCH_VIDEOSHOW_SUCCESS: 'FETCH_VIDEOSHOW_SUCCESS',
  FETCH_VIDEOSHOW_FAILURE: 'FETCH_VIDEOSHOW_FAILURE',
  UPDATE_VIDEOSHOW_REQUEST: 'UPDATE_VIDEOSHOW_REQUEST',
  UPDATE_VIDEOSHOW_SUCCESS: 'UPDATE_VIDEOSHOW_SUCCESS',
  UPDATE_VIDEOSHOW_FAILURE: 'UPDATE_VIDEOSHOW_FAILURE',
};

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_VIDEOSHOW_FAILURE,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_VIDEOSHOW_SUCCESS,
    payload: data,
  };
}

function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}
export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}

function updateVideoShowDataSuccess(videoData) {
  return {
    type: actionTypes.UPDATE_VIDEOSHOW_SUCCESS,
    payload: videoData,
  };
}

function updateVideoShowDataFailure(error) {
  return {
    type: actionTypes.UPDATE_VIDEOSHOW_FAILURE,
    payload: error,
  };
}

function fetchData({ params }) {
  //
  const { id } = params; // id is defined in routes.js path expression

  // Main  data for video show page.
  const videoShowData = fetch(
    `${process.env.API_ENDPOINT}/webgn_videoshow/${id}.cms?feedtype=json`,
  ).then(vsData => {
    const { sectioninfo } = vsData;
    let msid = null;
    if (sectioninfo) {
      msid = sectioninfo.msid;
    }
    if (msid) {
      return fetch(
        `${process.env.API_ENDPOINT}/webgn_nav.cms?type=navigation&msid=${msid}&feedtype=sjson`,
      ).then(
        headerData => {
          return { headerData, videoShowData: vsData };
        },
        error => {
          return { videoShowData: vsData, headerData: null };
        },
      );
    }
    return { videoShowData: vsData, headerData: null };
  });

  // Ads data
  const promiseAds = fetch(
    `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=videoshow&msid=${id}`,
  );

  // const headerData = fetch(
  //   `${process.env.API_ENDPOINT}/webgn_nav.cms?type=navigation&msid=${id}&feedtype=sjson`,
  // );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_VIDEOSHOW_REQUEST,
    });

    return Promise.all([videoShowData, promiseAds].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            videoData: data[0] ? data[0].videoShowData : null,
            adsData: data[1],
            headerData: data[0] ? data[0].headerData : null,
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function shouldFetchData(state, params) {
  const { videoshow } = state;
  const videoData =
    (videoshow &&
      videoshow.data &&
      videoshow.data.videoData &&
      videoshow.data.videoData.items &&
      videoshow.data.videoData.items[0]) ||
    null;
  if (videoData && videoData.id === params.id) {
    return false;
  }
  return true;
}

export function updateVideoShowData(videoId) {
  return dispatch => {
    dispatch({
      type: actionTypes.UPDATE_VIDEOSHOW_REQUEST,
    });

    fetch(`${process.env.API_ENDPOINT}/webgn_videoshow/${videoId}.cms?feedtype=json`).then(
      videoData => dispatch(updateVideoShowDataSuccess(videoData)),
      error => dispatch(updateVideoShowDataFailure(error)),
    );
  };
}

export function fetchDataIfNeeded({ query, params, history }) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}
