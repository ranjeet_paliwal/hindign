import fetch from '../../utils/fetch/fetch';

import { _getfeedCategory } from '../../common-utility';

export const FETCH_GADGETLIST_REQUEST = 'FETCH_GADGETLIST_REQUEST';
export const FETCH_GADGETLIST_SUCCESS = 'FETCH_GADGETLIST_SUCCESS';
export const FETCH_GADGETLIST_FAILURE = 'FETCH_GADGETLIST_FAILURE';

export const FETCH_NEXT_GADGETLIST_REQUEST = 'FETCH_NEXT_GADGETLIST_REQUEST';
export const FETCH_NEXT_GADGETLIST_SUCCESS = 'FETCH_NEXT_GADGETLIST_SUCCESS';
export const FETCH_NEXT_GADGETLIST_FAILURE = 'FETCH_NEXT_GADGETLIST_FAILURE';

const staticRoutes = [
  '/tech/mobile-phones/i-smart',
  '/tech/mobile-phones/hi-tech',
  '/tech/mobile-phones/sony-ericsson',
  '/tech/mobile-phones/t-mobile',
];

function fetchGadgetListDataFailure(error) {
  return {
    type: FETCH_GADGETLIST_FAILURE,
    payload: error.message,
  };
}
function fetchGadgetListDataSuccess(data) {
  return {
    type: FETCH_GADGETLIST_SUCCESS,
    payload: data,
  };
}

function fetchGadgetListData(state, params, query, router, dataType, categoryoverride) {
  // debugger;
  // const pathname =
  //   router && router.location && router.location.pathname
  //     ? router.location.pathname.substr(router.location.pathname.indexOf('/tech') + 5)
  //     : '';
  const pathname =
    router && router.location && router.location.pathname ? router.location.pathname : '';
  const perpage = params && params.msid ? '&perpage=5' : '';

  const category = categoryoverride
    ? _getfeedCategory(categoryoverride)
    : params && params.category
    ? _getfeedCategory(params.category)
    : '';

  // const category = 'mobile';
  const uriPath = (pathname + perpage).replace('=', '%253D');
  let apiUrl = `${
    process.env.API_ENDPOINT
  }/pwagn_gadgetlist.cms?&pagetype=gadgetlist&category=${category}&feedtype=sjson&uri=${uriPath}`;

  let brandName = '';
  if (pathname && staticRoutes.includes(pathname) && !params.brand) {
    brandName = pathname.split('/').pop();
    apiUrl += '&brandname=' + brandName;
  }

  params && params.brand
    ? (apiUrl += params.brand ? `&brandname=${params.brand}` : '')
    : params && params.filters
    ? (apiUrl += params.filters ? `&${params.filters}` : '')
    : dataType
    ? (apiUrl += `&sort=${dataType}`)
    : (apiUrl += '');
  pathname.indexOf('upcoming') > -1 ? (apiUrl += '&upcoming=1') : null;
  return dispatch => {
    dispatch({
      type: FETCH_GADGETLIST_REQUEST,
      payload: params,
    });

    const gadgetsListData = fetch(apiUrl).catch(e => null);
    const gadgetLatestData = fetch(
      `${
        process.env.API_ENDPOINT
      }/pwagn_gadgetlist.cms?&pagetype=gadgetlist&category=${category}&feedtype=sjson&uri=/${category}/filters/sort=latest&sort=latest`,
    ).catch(e => null);
    const gadgetAdsData = fetch(
      `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=gadgetslist`,
    ).catch(e => null);

    return Promise.all([gadgetsListData, gadgetLatestData, gadgetAdsData])
      .then(
        gadgetsData =>
          dispatch(
            fetchGadgetListDataSuccess({
              gadgetListData: gadgetsData[0],
              gadgetLatestData: gadgetsData[1],
              gadgetAdsData: gadgetsData[2],
            }),
          ),
        error => dispatch(fetchGadgetListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchGadgetListDataFailure(error));
      });
  };
}

function shouldFetchGadgetListData(state, params, query) {
  return true;
  if (typeof state.gadgetlist.item === 'undefined') {
    return true;
  }
  return false;
}

export function fetchGadgetListDataIfNeeded(params, query, router, dataType, categoryoverride) {
  // debugger;
  return (dispatch, getState) => {
    if (shouldFetchGadgetListData(getState(), params, query)) {
      return dispatch(
        fetchGadgetListData(getState(), params, query, router, dataType, categoryoverride),
      );
    }
    return Promise.resolve([]);
  };
}

// perpetcual
function fetchNextGadgetListDataFailure(error) {
  return {
    type: FETCH_NEXT_GADGETLIST_FAILURE,
    payload: error.message,
  };
}
function fetchNextGadgetListDataSuccess(data, curpg) {
  return {
    type: FETCH_NEXT_GADGETLIST_SUCCESS,
    payload: data,
    curpg,
  };
}

function fetchNextGadgetListData(state, params, query, router, curpg, dataType) {
  // const pathname =
  //   router && router.location && router.location.pathname
  //     ? router.location.pathname.substr(router.location.pathname.indexOf('/tech') + 5)
  //     : '';
  const pathname =
    router && router.location && router.location.pathname ? router.location.pathname : '';
  const perpage = params && params.msid ? '&perpage=5' : '';
  const category = params && params.category ? _getfeedCategory(params.category) : '';
  // let category = 'mobile';
  let apiUrl = `${
    process.env.API_ENDPOINT
  }/pwagn_gadgetlist.cms?pagetype=gadgetlist&category=${category}&feedtype=sjson&uri=${pathname}&curpg=${curpg}${perpage}`;
  params && params.brand
    ? (apiUrl += params.brand ? `&brandname=${params.brand}` : '')
    : params && params.filters
    ? (apiUrl += params.filters ? `&${params.filters}` : '')
    : dataType
    ? (apiUrl += `&sort=${dataType}`)
    : (apiUrl += '');
  pathname.indexOf('upcoming') > -1 ? (apiUrl += '&upcoming=1') : null;

  return dispatch => {
    dispatch({
      type: FETCH_NEXT_GADGETLIST_REQUEST,
      payload: params,
      curpg,
    });
    return fetch(apiUrl)
      .then(
        data => {
          dispatch(fetchNextGadgetListDataSuccess(data, curpg));
        },
        error => dispatch(fetchNextGadgetListDataFailure(error)),
      )
      .catch(error => {
        dispatch(fetchNextGadgetListDataFailure(error));
      });
  };
}

export function fetchNextGadgetListDataIfNeeded(params, query, router, curpg, dataType) {
  return (dispatch, getState) => {
    if (shouldFetchGadgetListData(getState(), params, query)) {
      return dispatch(fetchNextGadgetListData(getState(), params, query, router, curpg, dataType));
    }
    return Promise.resolve([]);
  };
}
