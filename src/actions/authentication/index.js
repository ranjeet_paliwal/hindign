import * as actionTypes from '../actions';

function logoutUser() {
  return {
    type: actionTypes.REMOVE_USER_DETAILS,
  };
}

function storeUserData(userData) {
  return {
    type: actionTypes.STORE_USER_DETAILS,
    payload: userData,
  };
}

export function storeUserDetails(userData) {
  return (dispatch, getState) => {
    dispatch(storeUserData(userData));
  };
}

export function logUserOut() {
  return (dispatch, getState) => {
    dispatch(logoutUser());
  };
}
