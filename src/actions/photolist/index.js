/* eslint-disable implicit-arrow-linebreak */
import fetch from '../../utils/fetch/fetch';
import { UPDATE_NAV_SUCCESS } from '../actions';

export const actionTypes = {
  FETCH_PHOTOLIST_REQUEST: 'FETCH_PHOTOLIST_REQUEST',
  FETCH_PHOTOLIST_SUCCESS: 'FETCH_PHOTOLIST_SUCCESS',
  FETCH_PHOTOLIST_FAILURE: 'FETCH_PHOTOLIST_FAILURE',
  UPDATE_PHOTOLIST_REQUEST: 'UPDATE_PHOTOLIST_REQUEST',
  UPDATE_PHOTOLIST_SUCCESS: 'UPDATE_PHOTOLIST_SUCCESS',
};

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_PHOTOLIST_FAILURE,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_PHOTOLIST_SUCCESS,
    payload: data,
  };
}

function fetchData({ query, params }) {
  const { id } = params; // id is defined in routes.js path expression
  const pageno = query && query.curpg ? parseInt(query.curpg) : 1;

  const promiseList = fetch(
    `${process.env.API_ENDPOINT}/webgn_photolist/${id}.cms?feedtype=sjson&pagetype=list&curpg=${pageno}&platform=gnweb`,
  );

  const promiseAds = fetch(
    `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=articlelist&msid=${id}`,
  );

  const headerData = fetch(
    `${process.env.API_ENDPOINT}/webgn_nav.cms?type=navigation&msid=${id}&feedtype=sjson`,
  );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_PHOTOLIST_REQUEST,
    });

    // dispatch(fetchHeaderData(id));
    // promiseMetatags , promisequicklinks added but unsed as of 20/08/2019

    return Promise.all([promiseList, promiseAds, headerData].map(p => p.catch(() => null))).then(
      data =>
        dispatch(
          fetchDataSuccess({
            photolistData: data[0],
            adsData: data[1],
            headerData: data[2],
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function shouldFetchData(state, params) {
  const { id } = params;
  if (state && state.photolist && state.photolist.data && state.photolist.data.photolistData) {
    return !(state.photolist.data.photolistData.id === id);
  }
  return true;
}

function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}

function updateListSuccess(data) {
  return {
    type: actionTypes.UPDATE_PHOTOLIST_SUCCESS,
    payload: data,
  };
}
export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}

function updateListFailure() {}

function updateList(params) {
  const { id, pgno } = params;
  return dispatch => {
    return fetch(
      `https://navbharattimesfeeds.indiatimes.com/pwaeeds/webgn_photolist/${id}.cms?feedtype=sjson&pagetype=list&curpg=${pgno}`,
    ).then(data => dispatch(updateListSuccess(data)), error => dispatch(updateListFailure(error)));
  };
}

export function updateListData(params) {
  return dispatch => {
    return dispatch(updateList(params));
  };
}

export function fetchDataIfNeeded({ query, params, history, isForcedUpdate }) {
  return (dispatch, getState) => {
    if (isForcedUpdate || shouldFetchData(getState(), params)) {
      return dispatch(fetchData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}
