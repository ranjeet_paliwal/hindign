/* eslint-disable implicit-arrow-linebreak */
import fetch from '../../utils/fetch/fetch';
import { fetchHeaderData } from '../nav/nav';
import { UPDATE_NAV_SUCCESS } from '../actions';

export const actionTypes = {
  FETCH_REVIEWLIST_REQUEST: 'FETCH_REVIEWLIST_REQUEST',
  FETCH_REVIEWLIST_SUCCESS: 'FETCH_REVIEWLIST_SUCCESS',
  FETCH_REVIEWLIST_FAILURE: 'FETCH_REVIEWLIST_FAILURE',
  UPDATE_REVIEWLIST_REQUEST: 'UPDATE_REVIEWLIST_REQUEST',
  UPDATE_REVIEWLIST_SUCCESS: 'UPDATE_REVIEWLIST_SUCCESS',
};

function fetchDataFailure(error) {
  return {
    type: actionTypes.FETCH_REVIEWLIST_FAILURE,
    payload: error.message,
  };
}

function fetchDataSuccess(data) {
  return {
    type: actionTypes.FETCH_REVIEWLIST_SUCCESS,
    payload: data,
  };
}

function fetchData({ query, params }) {
  // Main review data (LHS)

  const pageno = query && query.curpg ? parseInt(query.curpg) : 1;

  // get articlelist data
  const promiseList = fetch(
    `${
      process.env.API_ENDPOINT
    }/pwagn_articlelist/66063640.cms?feedtype=sjson&tag=solrreview&platform=web&curpg=${pageno}`,
  );

  // Right review data (RHS)
  const promiseListRHS = fetch(
    `${
      process.env.API_ENDPOINT
    }/webgn_common/66130905.cms?tag=ibeatmostread,trending&platform=web&count=10&feedtype=sjson`,
  );

  // get ads data

  const promiseAds = fetch(
    `${process.env.API_ENDPOINT}/webgn_ads.cms?feedtype=sjson&pagetype=reviewlist&msid=66063640`,
  );

  // get meta data
  const promiseMetatags = fetch(
    `${process.env.API_ENDPOINT}/webgn_meta.cms?pagetype=AS&feedtype=sjson`,
  );

  // get web_nav data
  const navBarMainLinks = fetch(
    `${process.env.API_ENDPOINT}/webgn_nav.cms?feedtype=sjson&type=navbar`,
  );

  return dispatch => {
    dispatch({
      type: actionTypes.FETCH_REVIEWLIST_REQUEST,
    });
    return Promise.all(
      [promiseList, promiseListRHS, promiseAds, promiseMetatags, navBarMainLinks].map(p =>
        p.catch(() => null),
      ),
    ).then(
      data =>
        dispatch(
          fetchDataSuccess({
            articleData: data[0],
            articleRhsData: data[1],
            adsData: data[2],
            staticMeta: data[3],
            pageHeaderData: data[4],
          }),
        ),
      error => dispatch(fetchDataFailure(error)),
    );
  };
}

function shouldFetchData(state, params) {
  const articleId = 66063640;
  if (state && state.reviewlist && state.reviewlist.data && state.reviewlist.data.articleData) {
    return !(state.reviewlist.data.articleData.id === articleId);
  }
  return true;
}

function updateListSuccess(data) {
  return {
    type: actionTypes.UPDATE_REVIEWLIST_SUCCESS,
    payload: data,
  };
}

function updateNavSuccess(data) {
  return {
    type: UPDATE_NAV_SUCCESS,
    payload: data,
  };
}

function updateListFailure() {}

function updateList(params) {
  const { pgno } = params;
  return dispatch => {
    return fetch(
      `${
        process.env.API_ENDPOINT
      }/pwagn_articlelist/66063640.cms?feedtype=sjson&pagetype=list&platform=web&curpg=${pgno}`,
    ).then(data => dispatch(updateListSuccess(data)), error => dispatch(updateListFailure(error)));
  };
}
export function updateListData(params) {
  return dispatch => {
    return dispatch(updateList(params));
  };
}

export function updateNavData(params) {
  return dispatch => {
    return dispatch(updateNavSuccess(params));
  };
}

export function fetchDataIfNeeded({ query, params, history }) {
  return (dispatch, getState) => {
    if (shouldFetchData(getState(), params)) {
      return dispatch(fetchData({ query, params, history }));
    }
    return Promise.resolve([]);
  };
}
