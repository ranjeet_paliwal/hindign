import React from 'react';
import PropTypes from 'prop-types';
import Thumb from '../Thumb/Thumb';
import CtnAd from '../../containers/articleshow/subsections/CtnAd';
import Link from '../Link/Link';

const Config = require(`../../../common/${process.env.SITE}`);

const PhotogalleryLeft = ({
  data,
  socialShare,
  showComments,
  photoCount,
  shiftBy,
  isFirstPhotoArticle,
  firstpicUrl,
  pIndex,
}) => {
  if (!data) {
    // TODO: Add loader
    // Return loader here when built
    return null;
  }

  const totalItems = data && Array.isArray(data) ? data.length : '';
  const adInBetween = {
    index: [0],
    shown: false,
  };
  if (totalItems >= 4) {
    adInBetween.shown = true;
    if (totalItems >= 10) {
      adInBetween.index = [9, 2];
    } else {
      adInBetween.index = [2];
    }
  }

  return (
    <React.Fragment>
      {data && Array.isArray(data)
        ? data.map((photoData, index) => {
            if (!photoData) {
              return null;
            }

            return (
            <React.Fragment key={photoData.id}>
                <div className="photo_block" id={`photo-block-${photoData.id}`}>
                <div className="photo-item-container">
                    <div className="title-container">
                    <span className="photo_count">
                        {`${index + 1 + shiftBy}/${photoCount}`}
                        <span className="title_leftborder" />
                      </span>
                    <h2>{(photoData && photoData.hl) || ''}</h2>
                  </div>
                    <div data-msid="70881024">
                    <meta
                        itemProp="thumbnailUrl"
                        content="https://navbharattimes.indiatimes.com/photo/msid-70881024/pvsindhu-manasi-joshi-pullela-gopichand.jpg"
                      />
                    <meta
                        itemProp="contentUrl"
                        content="https://navbharattimes.indiatimes.com/photo/msid-70881024/pvsindhu-manasi-joshi-pullela-gopichand.jpg"
                      />
                    <Thumb
                        width="630"
                        alt={photoData.hl}
                        src={`${process.env.WEBSITE_URL}/photo/msid-${
                        photoData ? photoData.id : ''
                      }/${photoData.imgtitle || ''}.jpg?imgsize=11979`}
                        title={photoData.hl}
                      />
                  </div>
                    <SocialShareBox
                    socialShare={socialShare}
                    data={photoData}
                    showComments={showComments}
                    />
                  </div>

                <div itemProp="caption description" className="readmore_span" id="desc_70881024">
                    <p>{photoData.cap}</p>
                  </div>
                {adInBetween.shown && adInBetween.index.includes(index) ? (
                    <CtnAd
                    height="40"
                    position={photoData.id}
                    width="1000"
                    className="colombia"
                    slotId={Config.CTN.id_photoshow_between}
                    />
                  ) : null}
                {index === totalItems - 1 ? (
                    <CtnAd
                    height="40"
                    position={
                        isFirstPhotoArticle ? `${photoData.id}0` : `${photoData.id}${pIndex + 1}`
                      }
                    width="1000"
                    className="colombia"
                    slotId={Config.CTN.id_photoshow_last}
                    />
                  ) : null}
                {index === 0 && shiftBy ? (
                    <span onClick={() => window.open(firstpicUrl)} className="start_from_first">
                      Go to first gallery
                  </span>
                  ) : null}
              </div>
              </React.Fragment>
            );
          })
        : null}
    </React.Fragment>
  );
};

PhotogalleryLeft.propTypes = {
  data: PropTypes.object,
  socialShare: PropTypes.func,
  showComments: PropTypes.func,
  photoCount: PropTypes.string,
  shiftBy: PropTypes.number,
  isFirstPhotoArticle: PropTypes.bool,
  pIndex: PropTypes.number,
};

const SocialShareBox = ({ socialShare, data, showComments }) => (
  <div className="cptn_bg">
    <div className="photo_social">
      <span
        data-type="fb"
        className="social_outer"
        onClick={() => socialShare('fb', { url: data.wu, text: data.hl })}
      >
        <i className="nbt-home-sprite fbk" />
      </span>
      <span
        className="social_outer"
        onClick={() => socialShare('twitter', { url: data.wu, text: data.hl })}
      >
        <i className="nbt-home-sprite twitr" />
      </span>
    </div>
    <div className="photo_comment" onClick={() => showComments(data.id)}>
      <small>{Config.ArticleShow.text_writecomment}</small>
      {/* <span className="cmnt_cnt_bbl" /> */}
      <i className="cmnticon" />
    </div>
  </div>
);

SocialShareBox.propTypes = {
  socialShare: PropTypes.func,
  data: PropTypes.object,
  showComments: PropTypes.func,
};

export default PhotogalleryLeft;
