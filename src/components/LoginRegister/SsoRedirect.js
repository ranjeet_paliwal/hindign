import React, { PureComponent } from 'react';

import { connect } from 'react-redux';
import { storeUserDetails } from '../../actions/authentication';

const Config = require(`../../../common/${process.env.SITE}`);

class SsoRedirect extends PureComponent {
  componentDidMount() {
    const { dispatch } = this.props;
    let LoginType;

    /* if (typeof window !== 'undefined') {
      window.opener.location = '/';
      window.close();
    } */

    if (typeof document !== 'undefined') {
      // alert(document.location.href.indexOf('www.googleapis.com'));
      // nbt.indiatimes.com/login?code=4/9wBHd4lCk83nNQVQyrlg4RE5bMG4kcCv5hAoPggm3VkDRLmlOaLzHuQP8fDzKQaZw6PwWJ_1BKildcBbqfoG5BE&scope=openid+email+profile+https://www.googleapis.com/auth/profile.language.read+https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email+https://www.googleapis.com/auth/profile.agerange.read
      LoginType = document.location.href.indexOf('www.googleapis.com') !== -1 ? 'google' : 'fb';
    }

    if (
      this.props &&
      this.props.location &&
      this.props.location.query &&
      this.props.location.query.code
    ) {
      const Code = this.props.location.query.code;

      const redirectURI = Config.Links.socialRedirectURI;

      setTimeout(() => {
        let jssoObj;
        if (typeof window !== 'undefined') {
          jssoObj = new JssoCrosswalk('nbt', 'WEB');
        }

        if (LoginType === 'google') {
          jssoObj.googleplusLogin(Code, redirectURI, response => {
            if (response.code === 200) {
              const userDetailsCb = userData => {
                dispatch(storeUserDetails(userData));
                window.opener.location.reload(true);
                window.close();
              };

              jssoObj.getUserDetails(userDetailsCb);

              // window.opener.location.href = document.referrer;
              // self.close();

              // window.close();
            }
          });
        }
        if (LoginType === 'fb') {
          jssoObj.facebookLogin(Code, redirectURI, response => {
            if (response.code === 200) {
              const userDetailsCb = userData => {
                dispatch(storeUserDetails(userData));
                window.opener.location.reload(true);
                window.close();
              };

              jssoObj.getUserDetails(userDetailsCb);

              // localStorage.setItem('message', JSON.stringify('login success'));
              // window.opener.location.href = document.referrer;
            }
          });
        }
      }, 1000);
    }
  }

  render() {
    return (
      <div className="please-wait">
        <span>Please wait</span>Processing Login..
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { ...state.authentication };
}

export default connect(mapStateToProps)(SsoRedirect);
