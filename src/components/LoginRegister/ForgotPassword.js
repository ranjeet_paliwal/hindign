import React, { PureComponent } from 'react';
import './LoginRegister.scss';
import PropTypes from 'prop-types';

import { errorConfig } from '../../../common/nbt';

class ForgotPassword extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      newPassword: '',
      passwordChanged: false,
      showSuccessScreen: false,
      serverMessage: {
        message: '',
        isError: false,
        isPasswordRel: false,
      },
    };
    this.forgotPasswordSubmit = this.forgotPasswordSubmit.bind(this);
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.regenerateOTP = this.regenerateOTP.bind(this);
    this.togglePassword = this.togglePassword.bind(this);
    this.resetServerMessage = this.resetServerMessage.bind(this);
    this.checkSubmitEnabled = this.checkSubmitEnabled.bind(this);
  }

  onFormFieldChange(e, key) {
    const { value } = e.target;
    const newState = { [key]: value };
    if (key === 'newPassword') {
      newState.passwordChanged = true;
    }
    this.setState(newState);
  }

  forgotPasswordSubmit(e) {
    const { isNumber, userName, showLoader, hideLoader } = this.props;
    const { otp, newPassword, serverMessage } = this.state;
    e.preventDefault();

    const cb = response => {
      hideLoader();
      if (response.code === 200) {
        // Add forgot password confirmation screen
        this.setState({
          showSuccessScreen: true,
        });
      } else {
        const isError = true;
        let message;
        let isPasswordRel = false;
        if (response.code === 418 && response.message === 'PASSWORD_MATCHES_LAST_THREE') {
          message = errorConfig.MATCH_LAST_THREE;
          isPasswordRel = true;
        } else if (response.code === 414 && response.message === 'WRONG_OTP') {
          if (isNumber) {
            message = errorConfig.WRONG_OTP_MOBILE;
          } else {
            message = errorConfig.WRONG_OTP_EMAIL;
          }
          isPasswordRel = false;
        } else if (response.code === 415) {
          message = errorConfig.EXPIRED_OTP;
          isPasswordRel = false;
        } else if (response.code === 416) {
          message = errorConfig.LIMIT_EXCEEDED;
          isPasswordRel = false;
        } else if (response.code === 418) {
          message = errorConfig.CONNECTION_ERROR;
          isPasswordRel = true;
        } else {
          message = errorConfig.SERVER_ERROR;
          isPasswordRel = true;
        }

        const newServerMessage = { ...serverMessage, message, isError, isPasswordRel };
        this.setState({
          serverMessage: newServerMessage,
        });
      }
    };

    showLoader();
    if (isNumber) {
      window.JSSO_INSTANCE.verifyMobileForgotPassword(userName, otp, newPassword, newPassword, cb);
    } else {
      window.JSSO_INSTANCE.verifyEmailForgotPassword(userName, otp, newPassword, newPassword, cb);
    }

    this.resetServerMessage();
  }

  resetServerMessage() {
    const serverMessage = {
      message: '',
      isError: false,
      isPasswordRel: false,
    };

    this.setState({
      serverMessage,
    });
  }

  togglePassword() {
    const { passwordShown } = this.state;
    this.setState({
      passwordShown: !passwordShown,
    });
  }

  regenerateOTP() {
    const { isNumber, userName, showLoader, hideLoader } = this.props;
    const cb = r => {
      hideLoader();
      this.setState({
        serverMessage: {
          message: 'OTP has been successfully sent.',
          isError: false,
          isPasswordRel: false,
        },
      });
    };

    if (isNumber) {
      window.JSSO_INSTANCE.getMobileForgotPasswordOtp(userName, cb);
    } else {
      window.JSSO_INSTANCE.getEmailForgotPasswordOtp(userName, cb);
    }
    showLoader();

    this.resetServerMessage();
  }

  checkSubmitEnabled() {
    const { otp, newPassword } = this.state;
    const isNumber = val => /^\d+$/.test(val);
    const hasNumber = val => /[0-9]/.test(val);
    const hasSpecialChars = val => /[!@#$%^&*()]/.test(val);
    const hasLowerCase = val => /[a-z]/.test(val);
    const isCorrectLength = val => val.length >= 6 && val.length <= 14;

    return (
      hasNumber(newPassword) &&
      hasSpecialChars(newPassword) &&
      hasLowerCase(newPassword) &&
      isCorrectLength(newPassword) &&
      isNumber(otp) &&
      otp.length === 6
    );
  }

  render() {
    const { userName } = this.props;
    const {
      otp,
      serverMessage,
      newPassword,
      passwordShown,
      passwordChanged,
      showSuccessScreen,
    } = this.state;

    if (showSuccessScreen) {
      return (
        <div id="lang-success-screen">
          <div className="signin-section">
            <h4 className="heading">
              <span>Forgot Password</span>
            </h4>
            <div className="fp-success">
              <i className="" />
              <div className="fp-success-msg">Password changed successfully.</div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div id="lang_forgot_password">
        <div className="signin-section">
          <h4 className="heading">
            <span>Forgot Password</span>
          </h4>
          <p id="forgot-password-sent">
            We have sent a 6 digit verification code to <strong>{userName}</strong>
          </p>
          <form className="form" autoComplete={false} onSubmit={this.forgotPasswordSubmit}>
            <input type="hidden" id="fp-inputVal" value="abhinav.inboxme@gmail.com" />
            <ul>
              <li className="input-field password">
                <p>
                  <input
                    type="password"
                    name="otpfp"
                    maxLength="6"
                    placeholder="Enter the verification code"
                    value={otp}
                    onChange={e => this.onFormFieldChange(e, 'otp')}
                  />
                </p>
                <div className="errorMsg" />
                <div
                  className={
                    serverMessage.message && !serverMessage.isPasswordRel && !serverMessage.isError
                      ? 'successMsg'
                      : 'errorMsg'
                  }
                  style={{ display: 'block' }}
                >
                  {serverMessage.message && !serverMessage.isPasswordRel
                    ? serverMessage.message
                    : null}
                </div>
                <span className="regenerate-otp">Didn't receive OTP?</span>
                <a
                  id="sso-fp-regenerate-otp"
                  href="javascript:void(0)"
                  onClick={this.regenerateOTP}
                  className="secondary-link"
                >
                  Re-Generate OTP
                </a>
              </li>
              <li className="input-field password">
                <p>
                  <input
                    autoComplete={false}
                    type={passwordShown ? 'text' : 'password'}
                    name="registerPwd"
                    placeholder="Enter new password"
                    maxLength="14"
                    onChange={e => this.onFormFieldChange(e, 'newPassword')}
                    value={newPassword}
                  />
                  <span
                    className={passwordShown ? 'hide-password' : 'view-password'}
                    onClick={this.togglePassword}
                  />
                </p>
                <span className="subtext">Should not match last 3 passwords.</span>
                <div className="errorMsg" style={{ display: 'block' }}>
                  {serverMessage.message && serverMessage.isPasswordRel
                    ? serverMessage.message
                    : null}
                </div>
                <div
                  className="password-conditions"
                  style={{ display: passwordChanged ? 'block' : 'none' }}
                >
                  <p>Password must have:</p>
                  <ul>
                    <li
                      id="charCnt"
                      className={
                        newPassword.length >= 6 && newPassword.length <= 14 ? 'success' : 'error'
                      }
                    >
                      6-14 characters
                    </li>
                    <li id="lwCnt" className={/[a-z]/.test(newPassword) ? 'success' : 'error'}>
                      1 Lower case character (a-z)
                    </li>
                    <li id="numCnt" className={/[\d]/.test(newPassword) ? 'success' : 'error'}>
                      1 Numeric character (0-9)
                    </li>
                    <li
                      id="spclCharCnt"
                      className={/[!@#$%^&*(),.?":{}|<>]/.test(newPassword) ? 'success' : 'error'}
                    >
                      1 special character (Such as #, $, %, &amp;, !)
                    </li>
                  </ul>
                </div>
              </li>
              <li className="submit">
                <input
                  type="submit"
                  disabled={!this.checkSubmitEnabled()}
                  id="sso-fp-btn"
                  className="submit-btn disabled"
                  value="Verify &amp; Login"
                />
              </li>
            </ul>
          </form>
        </div>
      </div>
    );
  }
}

ForgotPassword.propTypes = {
  userName: PropTypes.string,
  isNumber: PropTypes.bool,
};

export default ForgotPassword;
