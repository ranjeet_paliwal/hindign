/* eslint-disable jsx-a11y/anchor-has-content */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { storeUserDetails } from '../../actions/authentication/index';
import RegisterForm from './RegisterForm';
import ForgotPassword from './ForgotPassword';
import './LoginRegister.scss';
import Thumb from '../Thumb/Thumb';
import Link from '../Link/Link';
import { timesWidgetPostLoginCallBack } from '../../common-utility';

const Config = require(`../../../common/${process.env.SITE}`);

class LoginRegister extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
      otp: '',
      serverMessage: {
        message: '',
        isError: false,
        isPasswordRel: false,
      },
      validations: {
        userName: '',
      },
      passwordShown: false,
      currentStep: 'CHECK_USER_EXISTS',
    };
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.resetServerMessage = this.resetServerMessage.bind(this);
    this.submitForm = this.submitForm.bind(this);
    this.changeCurrentStep = this.changeCurrentStep.bind(this);
    this.generateOtpAndChangeSection = this.generateOtpAndChangeSection.bind(this);
    this.togglePassword = this.togglePassword.bind(this);
    this.regenerateOtp = this.regenerateOtp.bind(this);
    this.checkSubmitEnabled = this.checkSubmitEnabled.bind(this);
    this.showLoader = this.showLoader.bind(this);
    this.hideLoader = this.hideLoader.bind(this);
  }

  componentDidMount() {
    document.querySelector('body').classList.add('disable-scroll');
  }

  componentDidUpdate(prevProps) {
    const { currentStep } = this.state;
    const { loggedIn, closeLoginRegister } = this.props;
    if (!prevProps.loggedIn && loggedIn) {
      if (currentStep !== 'USER_VERIFIED' && currentStep !== 'OTP_GENERATED') {
        setTimeout(closeLoginRegister, 2000);
      } else {
        closeLoginRegister();
      }
    }
  }

  componentWillUnmount() {
    document.querySelector('body').classList.remove('disable-scroll');
  }

  onFormFieldChange(e, key) {
    const { value } = e.target;
    this.setState({
      [key]: value,
    });
  }

  SocialLoginHandler = param => {
    const redirectURI = (Config && Config.Links && Config.Links.socialRedirectURI) || '';

    const authAPIGoogle = `https://accounts.google.com/o/oauth2/auth?response_type=code&client_id=265054015577-n4ep9siuh3vjn02oe9vmgcjoi0p6mk4b.apps.googleusercontent.com&scope=email%20https://www.googleapis.com/auth/plus.login&access_type=online&redirect_uri=${redirectURI}`;
    const authAPIFB = `https://www.facebook.com/v2.7/dialog/oauth?client_id=154495568087154&display=popup&scope=email%2Cuser_birthday%2Cuser_hometown&redirect_uri=${redirectURI}`;

    /*  function message_receive(ev) {
      if (ev.key == 'message') {
        window.localStorage.removeItem('message');
        window.removeEventListener('storage');
        window.location.reload();
      }
    } */

    const authAPI = param === 'fb' ? authAPIFB : authAPIGoogle;
    //  window.a  ddEventListener('storage', message_receive);
    window.open(authAPI);
    this.props.closeLoginRegister(); // close login popup

    /* const userDetailsCb = userData => {
      dispatch(storeUserDetails(userData));
    };

    window.JSSO_INSTANCE.getUserDetails(userDetailsCb); */
  };

  checkSubmitEnabled() {
    const { currentStep, userName } = this.state;
    const isValidEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
      userName,
    );
    const isValidMobileNumber = /^[6-9]\d{9}$/.test(userName);

    switch (currentStep) {
      case 'CHECK_USER_EXISTS':
        return isValidMobileNumber || isValidEmail;

      default:
        return true;
    }
  }

  togglePassword() {
    const { passwordShown } = this.state;
    this.setState({
      passwordShown: !passwordShown,
    });
  }

  resetServerMessage() {
    const serverMessage = {
      message: '',
      isError: false,
      isPasswordRel: false,
    };

    this.setState({
      serverMessage,
    });
  }

  regenerateOtp() {
    const { userName } = this.state;
    const cb = response => {
      this.hideLoader();
      this.setState({
        serverMessage: {
          message: 'OTP has been successfully sent.',
          isError: false,
          isPasswordRel: false,
        },
      });
    };

    this.showLoader();
    if (/^\d+$/.test(userName)) {
      window.JSSO_INSTANCE.getMobileLoginOtp(userName, cb);
    } else {
      window.JSSO_INSTANCE.getEmailLoginOtp(userName, cb);
    }
    this.resetServerMessage();
  }

  changeCurrentStep(currentStep) {
    const { userName } = this.state;
    const cb = response => {
      this.hideLoader();
      this.setState({
        currentStep,
      });
    };

    switch (currentStep) {
      case 'OTP_GENERATED':
        this.showLoader();
        if (/^\d+$/.test(userName)) {
          window.JSSO_INSTANCE.getMobileLoginOtp(userName, cb);
        } else {
          window.JSSO_INSTANCE.getEmailLoginOtp(userName, cb);
        }
        break;

      case 'CHECK_USER_EXISTS':
        this.setState({ currentStep });
        break;

      default:
        break;
    }
  }

  generateOtpAndChangeSection() {
    const { userName, validations } = this.state;
    const newValidations = { ...validations };
    const isValidNumber = /^[6-9]\d{9}$/.test(userName);
    const isValidEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
      userName,
    );

    if (isValidNumber || isValidEmail) {
      const cb = response => {
        this.hideLoader();
        if (response.code === 200 && response.status === 'SUCCESS') {
          this.setState({
            currentStep: 'FORGOT_PASSWORD',
          });
        } else {
          if (
            response.code === 407 ||
            response.code === 408 ||
            response.code === 405 ||
            response.code === 406
          ) {
            newValidations.userName = Config.errorConfig.ACCOUNT_NOT_REGISTERED;
          } else if (response.code === 416) {
            newValidations.userName = Config.errorConfig.LIMIT_EXCEEDED;
          } else if (response.code === 503) {
            newValidations.userName = Config.errorConfig.CONNECTION_ERROR;
          } else {
            newValidations.userName = Config.errorConfig.SERVER_ERROR;
          }
          this.setState({
            validations: newValidations,
          });
        }
      };

      if (isValidNumber) {
        window.JSSO_INSTANCE.getMobileForgotPasswordOtp(userName, cb);
      } else {
        window.JSSO_INSTANCE.getEmailForgotPasswordOtp(userName, cb);
      }
      this.showLoader();
    } else {
      if (userName === '') {
        newValidations.userName = Config.errorConfig.BLANK_EMAIL_OR_MOBILE;
      } else {
        newValidations.userName = Config.errorConfig.INVALID_EMAIL_OR_MOBILE;
      }
      this.setState({
        validations: newValidations,
      });
    }
  }

  /*
205:     UNVERIFIED_EMAIL
206:     UNVERIFIED_MOBILE
212:     VERIFIED_MOBILE
213:     VERIFIED_EMAIL
214:      INVALID_IDENTIFIER
215:     UNREGISTERED_EMAIL
216:     PROXY_OR_DEFUNC_EMAIL */

  submitForm(e) {
    e.preventDefault();
    const { currentStep, userName, otp, password, serverMessage, validations } = this.state;
    const { dispatch } = this.props;
    const newValidations = { ...validations };

    const cb = response => {
      let nextStep = currentStep;

      const userDetailsCb = userData => {
        this.hideLoader();
        dispatch(storeUserDetails(userData));
      };

      switch (currentStep) {
        case 'CHECK_USER_EXISTS':
          if (response.data.statusCode === 212 || response.data.statusCode === 213) {
            nextStep = 'USER_VERIFIED';
          } else if (
            response.data.statusCode === 214 ||
            response.data.statusCode === 215 ||
            response.data.statusCode === 205 ||
            response.data.statusCode === 206
          ) {
            nextStep = 'REGISTER_SECTION';
          } else if (response.data.statusCode === 216) {
            newValidations.userName = Config.errorConfig.INVALID_EMAIL;
            this.setState({
              validations: newValidations,
            });
          }
          this.hideLoader();
          break;

        case 'OTP_GENERATED':
          if (response.code === 200 && response.status === 'SUCCESS') {
            window.JSSO_INSTANCE.getUserDetails(userDetailsCb);
          } else {
            this.hideLoader();
            let message = '';
            let isPasswordRel = false;
            const isError = true;
            if (response.code === 425 && response.status === 'FAILURE') {
              message = Config.errorConfig.WRONG_OTP_MOBILE;
              isPasswordRel = false;
            } else if (true) {
              // Add other messages here
            }
            const newServerMessage = { ...serverMessage, message, isPasswordRel, isError };
            this.setState({
              serverMessage: newServerMessage,
            });
          }

          break;

        case 'USER_VERIFIED':
          if (response.code === 200 && response.status === 'SUCCESS') {
            window.JSSO_INSTANCE.getUserDetails(userDetailsCb);
          } else {
            this.hideLoader();
            let message = '';
            let isPasswordRel = false;
            const isError = true;
            if (response.code === 425 && response.status === 'FAILURE') {
              message = Config.errorConfig.INVALID_CREDENTIALS;
              isPasswordRel = false;
            } else if (response.code === 416 && response.status === 'FAILURE') {
              message = Config.errorConfig.LIMIT_EXCEEDED;
              isPasswordRel = false;
            }
            const newServerMessage = { ...serverMessage, message, isPasswordRel, isError };
            this.setState({
              serverMessage: newServerMessage,
            });
          }
          break;
        default:
          break;
      }
      if (currentStep !== nextStep) {
        this.setState({
          currentStep: nextStep,
          validations: {
            userName: '',
          },
        });
      }
    };

    this.showLoader();

    switch (currentStep) {
      case 'OTP_GENERATED':
        const isNumber = /^\d+$/.test(userName);
        if (isNumber) {
          window.JSSO_INSTANCE.verifyMobileLogin(userName, otp, cb);
        } else {
          window.JSSO_INSTANCE.verifyEmailLogin(userName, otp, cb);
        }
        break;

      case 'CHECK_USER_EXISTS':
        window.JSSO_INSTANCE.checkUserExists(userName, cb);
        break;

      case 'USER_VERIFIED':
        if (/^\d+$/.test(userName)) {
          window.JSSO_INSTANCE.verifyMobileLogin(userName, password, cb);
        } else {
          window.JSSO_INSTANCE.verifyEmailLogin(userName, password, cb);
        }
        break;

      default:
        break;
    }

    this.resetServerMessage();
  }

  showLoader() {
    this.setState({
      showLoader: true,
    });
  }

  hideLoader() {
    this.setState({
      showLoader: false,
    });
  }

  renderCurrentForm() {
    const {
      currentStep,
      userName,
      passwordShown,
      password,
      otp,
      serverMessage,
      validations,
    } = this.state;
    const { dispatch } = this.props;

    switch (currentStep) {
      case 'CHECK_USER_EXISTS':
      case 'OTP_GENERATED':
      case 'USER_VERIFIED':
        return (
          <div id="lang_login">
            <div className="signin-section">
              <figure className="user-icon">
                <Thumb src="https://static.langimg.com/photo/63379366.cms" height="44" width="44" />
              </figure>
              <div id="socialConnectImgDiv">
                <button
                  type="button"
                  id="sso-fb-login"
                  className="fb"
                  datatype="fb"
                  onClick={this.SocialLoginHandler.bind(this, 'fb')}
                >
                  Sign in with Facebook
                </button>
                <span id="sso-fblogin-error" className="errorMsg" />
                <button
                  type="button"
                  id="sso-gplus-login"
                  className="gplus"
                  datatype="google"
                  onClick={this.SocialLoginHandler.bind(this, 'google')}
                >
                  Sign in with Google
                </button>
                <span id="sso-gplus-error" className="errorMsg" />
              </div>
              <h4 className="heading small">
                <span>or go the traditional way</span>
              </h4>
              <form className="form" autoComplete={false} onSubmit={this.submitForm}>
                <ul>
                  <li className="input-field email">
                    <p>
                      <input
                        autoComplete={false}
                        type="text"
                        name="emailId"
                        placeholder="Sign In/Sign Up with Email or Mobile No."
                        maxLength="100"
                        disabled={currentStep !== 'CHECK_USER_EXISTS'}
                        onChange={e => this.onFormFieldChange(e, 'userName')}
                        value={userName}
                      />
                    </p>
                    <div
                      className="errorMsg"
                      style={{ display: validations.userName ? 'block' : 'none' }}
                    >
                      {validations.userName}
                    </div>
                    <a
                      href="javascript:void(0)"
                      id="changeEmailIdDiv"
                      className="secondary-link"
                      onClick={() => this.changeCurrentStep('CHECK_USER_EXISTS')}
                      style={
                        currentStep === 'USER_VERIFIED'
                          ? { display: 'list-item' }
                          : { display: 'none' }
                      }
                    >
                      Change Email Or Mobile No.
                    </a>
                  </li>

                  <li
                    className="input-field password"
                    id="sso-pwdDiv"
                    style={
                      currentStep === 'USER_VERIFIED'
                        ? { display: 'list-item' }
                        : { display: 'none' }
                    }
                  >
                    <p>
                      <input
                        autoComplete={false}
                        type={passwordShown ? 'text' : 'password'}
                        name="password"
                        value={password}
                        onChange={e => this.onFormFieldChange(e, 'password')}
                        placeholder="Password"
                        maxLength="14"
                      />
                      <span
                        className={passwordShown ? 'hide-password' : 'view-password'}
                        onClick={this.togglePassword}
                      />
                    </p>
                    <div
                      className={
                        serverMessage.message &&
                        !serverMessage.isPasswordRel &&
                        !serverMessage.isError
                          ? 'successMsg'
                          : 'errorMsg'
                      }
                      style={{ display: 'block' }}
                    >
                      {serverMessage.message && !serverMessage.isPasswordRel
                        ? serverMessage.message
                        : null}
                    </div>
                    <a
                      id="sso-generate-otp"
                      href="javascript:void(0)"
                      onClick={() => this.changeCurrentStep('OTP_GENERATED')}
                      className="secondary-link"
                    >
                      Generate OTP to Login
                    </a>
                  </li>
                  <li id="sso-login-otp-msg" className="text-field">
                    <p />
                  </li>
                  <li
                    className="input-field password"
                    id="sso-otpLoginDiv"
                    style={
                      currentStep === 'OTP_GENERATED'
                        ? { display: 'list-item' }
                        : { display: 'none' }
                    }
                  >
                    <p>
                      <input
                        type="password"
                        name="otplogin"
                        value={otp}
                        onChange={e => this.onFormFieldChange(e, 'otp')}
                        maxLength="6"
                        placeholder="Enter the verification code"
                      />
                    </p>
                    <div
                      className={
                        serverMessage.message &&
                        !serverMessage.isPasswordRel &&
                        !serverMessage.isError
                          ? 'successMsg'
                          : 'errorMsg'
                      }
                      style={{ display: 'block' }}
                    >
                      {serverMessage.message && !serverMessage.isPasswordRel
                        ? serverMessage.message
                        : null}
                    </div>
                    <span className="regenerate-otp">Didn't receive OTP?</span>
                    <a
                      id="sso-regenerate-otp"
                      href="javascript:void(0)"
                      className="secondary-link"
                      onClick={this.regenerateOtp}
                    >
                      Re-Generate OTP
                    </a>
                  </li>
                  <li id="sso-signInButtonDiv" className="submit">
                    <input
                      type="submit"
                      disabled={!this.checkSubmitEnabled()}
                      className="submit-btn disabled"
                      value="Continue"
                    />
                  </li>
                </ul>
                <a
                  href="javascript:void(0)"
                  id="sso-forgot-pass"
                  onClick={this.generateOtpAndChangeSection}
                  className="forget-password"
                >
                  Forgot Password?
                </a>
              </form>
            </div>
            <div className="powered-by">
              <Thumb src="https://static.toiimg.com/photo/65257504.cms" alt="" />
            </div>
            <div className="teams-logo">
              <span>One Network. One Account</span>
              <Link to="//timesofindia.indiatimes.com/" target="_blank" className="toi" />
              <Link to="//economictimes.indiatimes.com/" target="_blank" className="et" />
              <Link to="//navbharattimes.indiatimes.com/" className="nbt" target="_blank" />
              <Link to="//maharashtratimes.indiatimes.com/" className="sm" target="_blank" />
              <Link to="//www.speakingtree.in/" className="st" target="_blank" />
              <Link to="//gaana.com/" className="gaana" target="_blank" />
              <Link to="//itimes.com/" className="itimes" target="_blank" />
              <Link to="//www.timespoints.com/#/login" className="tp" target="_blank" />
            </div>
          </div>
        );

      case 'REGISTER_SECTION':
        return (
          <RegisterForm
            userName={userName}
            showLoader={this.showLoader}
            hideLoader={this.hideLoader}
            isEmail={false}
            dispatch={dispatch}
            changeCurrentStep={this.changeCurrentStep}
          />
        );

      case 'FORGOT_PASSWORD':
        return (
          <ForgotPassword
            userName={userName}
            showLoader={this.showLoader}
            hideLoader={this.hideLoader}
            isNumber={/^\d+$/.test(userName)}
            changeCurrentStep={this.changeCurrentStep}
          />
        );

      default:
        return null;
    }
  }

  render() {
    const { closeLoginRegister } = this.props;
    const { showLoader } = this.state;

    return (
      <div id="login-popup" className="active">
        <button type="button" className="close-btn" onClick={closeLoginRegister}>
          +
        </button>
        <div id="signin_box" className={showLoader ? 'loader' : ''}>
          {this.renderCurrentForm()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.authentication,
  };
}

LoginRegister.propTypes = {
  closeLoginRegister: PropTypes.func,
  dispatch: PropTypes.func,
  loggedIn: PropTypes.bool,
};

export default connect(mapStateToProps)(LoginRegister);
