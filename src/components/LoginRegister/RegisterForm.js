import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import RegisterVerifiedLogin from './RegisterVerifiedLogin';
import './LoginRegister.scss';
import { errorConfig } from '../../../common/nbt';
import Link from '../Link/Link';

class RegisterForm extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showRegSuccessLogin: false,
      passwordShown: false,
      confPasswordShown: false,
      captchaId: null,
      fullName: '',
      password: '',
      confirmPassword: '',
      ssoid: '',
      mobileNumber: '',
      email: '',
      recaptcha: '',
      termsAndConditions: true,
      sendOffersAndPromos: true,
      serverMessage: '',
      showPersonalizedContent: true,
      validations: {
        fullName: '',
        password: '',
        mobileNumber: '',
        email: '',
        confirmPassword: '',
      },
    };
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.onCheckBoxToggle = this.onCheckBoxToggle.bind(this);
    this.registerUser = this.registerUser.bind(this);
    this.recaptchaResponse = this.recaptchaResponse.bind(this);
    this.recaptchaErrorCallback = this.recaptchaErrorCallback.bind(this);
    this.recaptchaExpiredCallback = this.recaptchaExpiredCallback.bind(this);
    this.isFormValid = this.isFormValid.bind(this);
    this.runValidations = this.runValidations.bind(this);
    this.togglePasswordShow = this.togglePasswordShow.bind(this);
  }

  componentDidMount() {
    const captchaId = window.grecaptcha.render('recaptcha-container', {
      sitekey: '6LcXeh0TAAAAAO1DsEX1iEF8n8-E_hQB67bIpxIw',
      theme: 'light',
      callback: this.recaptchaResponse,
      'error-callback': this.recaptchaErrorCallback,
      'expired-callback': this.recaptchaExpiredCallback,
    });

    this.captchaId = captchaId;
  }

  onFormFieldChange(e, key) {
    const { value } = e.target;
    const { isPasswordChanged } = this.state;
    const newState = { [key]: value };

    if (key === 'password' && !isPasswordChanged) {
      newState.isPasswordChanged = true;
    }
    this.setState(newState, () => this.runValidations(key, value));
  }

  onCheckBoxToggle(e, key) {
    const value = !this.state[key];
    this.setState(
      {
        [key]: value,
      },
      () => {
        if (key === 'termsAndConditions') {
          this.runValidations(key, value);
        }
      },
    );
  }

  togglePasswordShow(key) {
    this.setState({
      [key]: !this.state[key],
    });
  }

  recaptchaResponse(recaptcha) {
    this.setState({
      recaptcha,
    });
  }

  recaptchaErrorCallback(data) {}

  recaptchaExpiredCallback(data) {
    this.setState({
      recaptcha: '',
    });
  }

  isFormValid() {
    const { validations, recaptcha } = this.state;
    const areFormFieldsValid = Object.values(validations).every(v => v === '');
    return areFormFieldsValid && recaptcha;
  }

  runValidations(key, value) {
    let isValid = false;
    let errorMessage = '';
    const { validations, password, confirmPassword, termsAndConditions } = this.state;
    const newValidations = { ...validations };
    switch (key) {
      case 'fullName':
        isValid = /^[a-zA-Z ]*$/.test(value);
        break;
      case 'email':
        if (value !== '') {
          isValid = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
            value,
          );
        } else {
          isValid = true;
        }
        break;

      case 'password':
        isValid = password === confirmPassword;
        break;

      case 'mobileNumber':
        if (value !== '') {
          isValid = /^[6-9]\d{9}$/.test(value);
        } else {
          isValid = true;
        }
        break;

      case 'confirmPassword':
        isValid = password === confirmPassword;
        break;

      case 'termsAndConditions':
        isValid = termsAndConditions;
        break;

      default:
        break;
    }

    const errorMessages = {
      fullName: 'INVALID_NAME',
      email: 'INVALID_EMAIL',
      mobileNumber: 'INVALID_MOBLE_NUMBER',
      confirmPassword: 'INVALID_PASSWORD',
      password: 'INVALID_CONFIRMPASSWORD',
      termsAndConditions: 'INVALID_TERMS_AND_CONDITIONS',
    };

    errorMessage = isValid ? '' : errorConfig[errorMessages[key]];
    if (key === 'password') {
      newValidations.confirmPassword = errorMessage;
    } else {
      newValidations[key] = errorMessage;
    }

    this.setState({ validations: newValidations });
  }

  /** *        registerUserRecaptcha(
  firstName,
  lastName,
   gender,
    dob,
    email,
     mobile,
      password,
      isSendOffer,
       recaptcha,
        termsAccepted,
         shareDataAllowed,
          callback)
404:      UNAUTHORIZED_ACCESS
503:      CONNECTION_ERROR
504:      CONNECTION_TIMEOUT
420:       INVALID_NAME
421:       INVALID_GENDER
413:       INVALID_REQUEST
403:       INVALID_EMAIL
402:       INVALID_MOBILE
417:       INVALID_PASSWORD
429:       ALREADY_REGISTERED_USER
400:       TRANSACTION_ERROR
401:       INVALID_CHANNEL
451:       INVALID_CAPTCHA * */

  registerUser(e) {
    e.preventDefault();
    const {
      fullName,
      mobileNumber,
      password,
      sendOffersAndPromos,
      recaptcha,
      termsAndConditions,
      showPersonalizedContent,
      validations,
    } = this.state;
    const newValidations = { ...validations };

    const { userName, showLoader, hideLoader } = this.props;
    const spaceIndex = fullName.indexOf(' ');
    const firstName = fullName.slice(0, spaceIndex);
    const lastName = fullName.slice(spaceIndex + 1);
    const isNumber = /^\d+$/.test(userName);

    let mobileNumberReg;
    let email;
    if (isNumber) {
      mobileNumberReg = userName;
      email = mobileNumber;
    } else {
      mobileNumberReg = mobileNumber;
      email = userName;
    }

    const cb = response => {
      hideLoader();
      if (response.code === 200 && response.status === 'SUCCESS') {
        this.setState({ showRegSuccessLogin: true, ssoid: response.data.ssoid });
      } else {
        let serverMessage = '';
        if (response.code === 429) {
          serverMessage = errorConfig.USER_ALREADY_REGISTERED;
        } else if (response.code === 416) {
          serverMessage = errorConfig.LIMIT_EXCEEDED;
        } else if (response.code === 503) {
          serverMessage = errorConfig.CONNECTION_ERROR;
        } else {
          serverMessage = errorConfig.SERVER_ERROR;
        }

        this.setState({
          serverMessage,
          recaptcha: '',
        });

        window.grecaptcha.reset(this.captchaId);
        // else if  Add error cases here
      }
    };

    window.JSSO_INSTANCE.registerUserRecaptcha(
      firstName,
      lastName,
      '',
      '',
      email,
      mobileNumberReg,
      password,
      sendOffersAndPromos,
      recaptcha,
      termsAndConditions ? '1' : '0',
      showPersonalizedContent ? '1' : '0',
      cb,
    );

    showLoader();
  }

  render() {
    const {
      showRegSuccessLogin,
      ssoid,
      fullName,
      mobileNumber,
      password,
      confirmPassword,
      isPasswordChanged,
      sendOffersAndPromos,
      termsAndConditions,
      showPersonalizedContent,
      email,
      validations,
      confPasswordShown,
      passwordShown,
      serverMessage,
    } = this.state;
    const { changeCurrentStep, userName, dispatch, showLoader, hideLoader } = this.props;
    const isEmail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/.test(
      userName,
    );

    if (showRegSuccessLogin) {
      return (
        <RegisterVerifiedLogin
          changeCurrentStep={changeCurrentStep}
          userName={userName}
          showLoader={showLoader}
          hideLoader={hideLoader}
          dispatch={dispatch}
          ssoid={ssoid}
          loginType={/^\d+$/.test(userName) ? 'mobile' : 'email'}
        />
      );
    }

    return (
      <div id="lang_register">
        <div className="signup-section">
          <h4 className="heading">
            <span>Complete Your Profile</span>
          </h4>
          <form className="form" autoComplete={false} onSubmit={this.registerUser}>
            <input type="hidden" id="register-inputVal" value={userName} />
            <ul>
              <li className="input-field email">
                <p>
                  <input
                    autoComplete={false}
                    type="text"
                    name="emailId"
                    maxLength="100"
                    disabled
                    value={userName}
                  />
                </p>
                <a
                  href="javascript:void(0)"
                  onClick={() => changeCurrentStep('CHECK_USER_EXISTS')}
                  id="changeRegisterEmailId"
                  className="secondary-link"
                >
                  Change Email Or Mobile No.
                </a>
              </li>
              <li className="input-field user-name">
                <p>
                  <input
                    autoComplete={false}
                    type="text"
                    name="fullname"
                    placeholder="Full Name"
                    maxLength="30"
                    value={fullName}
                    onChange={e => this.onFormFieldChange(e, 'fullName')}
                  />
                </p>
                <div
                  className="errorMsg"
                  style={{ display: validations.fullName ? 'block' : 'none' }}
                >
                  {validations.fullName}
                </div>
              </li>
              <li className="input-field password">
                <p>
                  <input
                    autoComplete={false}
                    type={passwordShown ? 'text' : 'password'}
                    name="registerPwd"
                    placeholder="Password"
                    maxLength="14"
                    value={password}
                    onChange={e => this.onFormFieldChange(e, 'password')}
                  />
                  <span
                    onClick={() => this.togglePasswordShow('passwordShown')}
                    className={passwordShown ? 'hide-password' : 'view-password'}
                  />
                </p>
                <div
                  className="password-conditions"
                  style={{ display: isPasswordChanged ? 'block' : 'none' }}
                >
                  <p>Password must have:</p>
                  <ul>
                    <li
                      id="charCnt"
                      className={
                        password.length >= 6 && password.length <= 14 ? 'success' : 'error'
                      }
                    >
                      6-14 characters
                    </li>
                    <li id="lwCnt" className={/[a-z]/.test(password) ? 'success' : 'error'}>
                      1 Lower case character (a-z)
                    </li>
                    <li id="numCnt" className={/[\d]/.test(password) ? 'success' : 'error'}>
                      1 Numeric character (0-9)
                    </li>
                    <li
                      id="spclCharCnt"
                      className={/[!@#$%^&*(),.?":{}|<>]/.test(password) ? 'success' : 'error'}
                    >
                      1 special character (Such as #, $, %, &amp;, !)
                    </li>
                  </ul>
                </div>
              </li>
              <li className="input-field password">
                <p>
                  <input
                    autoComplete={false}
                    type={confPasswordShown ? 'text' : 'password'}
                    name="registerCnfrmPwd"
                    placeholder="Confirm Password"
                    maxLength="14"
                    value={confirmPassword}
                    onChange={e => this.onFormFieldChange(e, 'confirmPassword')}
                  />
                  <span
                    onClick={() => this.togglePasswordShow('confPasswordShown')}
                    className={confPasswordShown ? 'hide-password' : 'view-password'}
                  />
                </p>
                <div
                  className="errorMsg"
                  style={{ display: validations.confirmPassword ? 'block' : 'none' }}
                >
                  {validations.confirmPassword}
                </div>
              </li>
              {isEmail ? (
                <li className="input-field mobile-no">
                  <p>
                    <span className="country-code">+91 - </span>
                    <input
                      autoComplete={false}
                      type="text"
                      name="mobile"
                      maxLength="10"
                      value={mobileNumber}
                      onChange={e => this.onFormFieldChange(e, 'mobileNumber')}
                      placeholder="Mobile Number (Optional)"
                    />
                  </p>
                  <div
                    className="errorMsg"
                    style={{ display: validations.mobileNumber ? 'block' : 'none' }}
                  >
                    {validations.mobileNumber}
                  </div>
                </li>
              ) : (
                <li className="input-field email">
                  <p className="error">
                    <input
                      autoComplete={false}
                      type="text"
                      name="emailId"
                      maxLength="100"
                      value={email}
                      onChange={e => this.onFormFieldChange(e, 'email')}
                      placeholder="Email (Optional)"
                    />
                  </p>
                  <div
                    className="errorMsg"
                    style={{ display: validations.email ? 'block' : 'none' }}
                  >
                    {validations.email}
                  </div>
                </li>
              )}
              <li className="recaptcha-wrapper">
                <div id="recaptcha-container" />
                <div className="errorMsg" />
              </li>
              <li className="checkbox">
                <p>
                  <input
                    type="checkbox"
                    id="agree"
                    name="agree"
                    checked={termsAndConditions}
                    onChange={e => this.onCheckBoxToggle(e, 'termsAndConditions')}
                  />
                  <label htmlFor="agree">
                    I am at least 16 years old and agree with the
                    <Link to="https://www.indiatimes.com/termsandcondition/" target="_blank">
                      Terms &amp; Conditions
                    </Link>
                    and
                    <Link to="https://www.indiatimes.com/privacypolicy/" target="_blank">
                      Privacy Policy
                    </Link>
                    of Times of India
                  </label>
                </p>
                <div
                  className="errorMsg"
                  style={{ display: validations.termsAndConditions ? 'block' : 'none' }}
                >
                  {validations.termsAndConditions}
                </div>
              </li>
              <li className="checkbox">
                <p>
                  <input
                    type="checkbox"
                    id="promotions"
                    name="promotions"
                    checked={sendOffersAndPromos}
                    onChange={e => this.onCheckBoxToggle(e, 'sendOffersAndPromos')}
                  />
                  <label htmlFor="promotions">Send me offers and promotions</label>
                </p>
                <div className="errorMsg" />
              </li>
              <li className="checkbox">
                <p>
                  <input
                    type="checkbox"
                    id="sharedDataAllowed"
                    name="sharedDataAllowed"
                    checked={showPersonalizedContent}
                    onChange={e => this.onCheckBoxToggle(e, 'showPersonalizedContent')}
                  />
                  <label htmlFor="sharedDataAllowed">
                    Please show me personalized content and advertisements as per the
                    <Link
                      rel="nofollow noreferrer"
                      to="https://www.indiatimes.com/privacypolicy/"
                      target="_blank"
                    >
                      Privacy Policy
                    </Link>
                  </label>
                </p>
                <div className="errorMsg" style={{ display: serverMessage ? 'block' : 'none' }}>
                  {serverMessage}
                </div>
              </li>
              <li className="submit">
                <input
                  type="submit"
                  id="sso-registerBtn"
                  disabled={!this.isFormValid()}
                  className="submit-btn"
                  value="Update"
                />
              </li>
            </ul>
          </form>
        </div>
      </div>
    );
  }
}

RegisterForm.propTypes = {
  dispatch: PropTypes.func,
  userName: PropTypes.string,
  changeCurrentStep: PropTypes.func,
};

export default RegisterForm;
