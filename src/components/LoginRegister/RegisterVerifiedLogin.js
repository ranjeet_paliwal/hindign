import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import { storeUserDetails } from '../../actions/authentication';
import { errorConfig } from '../../../common/nbt';
import './LoginRegister.scss';

class RegisterVerifiedLogin extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      otp: '',
      showRegisterSuccessScreen: false,
      serverMessage: '',
      isErrorMessage: false,
    };
    this.onFormFieldChange = this.onFormFieldChange.bind(this);
    this.verifySignUp = this.verifySignUp.bind(this);
    this.regenerateOtp = this.regenerateOtp.bind(this);
  }

  onFormFieldChange(e, key) {
    const { value } = e.target;
    this.setState({
      [key]: value,
    });
  }

  regenerateOtp() {
    const { ssoid, loginType, userName, showLoader, hideLoader } = this.props;
    const cb = response => {
      hideLoader();
      if (response.code === 200) {
        this.setState({
          serverMessage: 'OTP has been successfully sent.',
          isErrorMessage: false,
        });
      }
    };
    if (loginType === 'email') {
      window.JSSO_INSTANCE.resendEmailSignUpOtp(userName, ssoid, cb);
    } else {
      window.JSSO_INSTANCE.resendMobileSignUpOtp(userName, ssoid, cb);
    }
    showLoader();
  }

  verifySignUp(e) {
    e.preventDefault();
    const { dispatch, ssoid, userName, loginType, showLoader, hideLoader } = this.props;
    const { otp } = this.state;

    const cb = response => {
      hideLoader();
      if (response.code === 200 && response.status === 'SUCCESS') {
        window.JSSO_INSTANCE.getUserDetails(r => {
          if (r.code === 200 && r.status === 'SUCCESS') {
            dispatch(storeUserDetails(r));
            this.setState({
              showRegisterSuccessScreen: true,
            });
          }
        });
      } else {
        const { code } = response;
        let errorMessage;
        const isErrorMessage = true;
        switch (code) {
          case 415:
            errorMessage = errorConfig.EXPIRED_OTP;
            break;
          case 414:
            errorMessage =
              loginType === 'email' ? errorConfig.WRONG_OTP_EMAIL : errorConfig.WRONG_OTP_MOBILE;
            break;
          case 416:
            errorMessage = errorConfig.LIMIT_EXCEEDED;
            break;
          case 418:
            errorMessage = errorConfig.CONNECTION_ERROR;
            break;
          default:
            errorMessage = errorConfig.SERVER_ERROR;
            break;
        }
        this.setState({
          serverMessage: errorMessage,
          isErrorMessage,
        });
      }
    };
    if (loginType === 'email') {
      window.JSSO_INSTANCE.verifyEmailSignUp(userName, ssoid, otp, cb);
    } else {
      window.JSSO_INSTANCE.verifyMobileSignUp(userName, ssoid, otp, cb);
    }
    showLoader();
  }

  render() {
    const { userName, ssoid, loginType, changeCurrentStep } = this.props;
    const { otp, showRegisterSuccessScreen, serverMessage, isErrorMessage } = this.state;
    let cname = '';
    if (serverMessage) {
      if (isErrorMessage) {
        cname = 'errorMsg';
      } else {
        cname = 'successMsg';
      }
    }

    if (showRegisterSuccessScreen) {
      return (
        <div id="lang-success-screen">
          <div className="signin-section">
            <h4 className="heading">
              <span> Complete your profile </span>
            </h4>
            <div className="register-success">
              <div className="verified">
                <div>
                  {/^\d+$/.test(userName) ? 'Mobile Number verified:' : 'Email Id verified:'}
                </div>
                <div>
                  <strong>{userName}</strong>
                  <i className="tick" />
                </div>
              </div>
              <div className="success-wrapper">
                <i className="success-user" />
                <div className="fp-success-msg">Thank you for registering.</div>
              </div>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div id="verifyotp-password">
        <div className="signin-section">
          <h4 className="heading">
            <span>Complete Your Profile</span>
          </h4>
          <p>
            {`We have sent a 6 digit verification code on your ${
              loginType === 'email' ? 'Email Address' : 'Mobile Number'
            }`}
          </p>
          <form className="form" autoComplete={false} onSubmit={this.verifySignUp}>
            <input type="hidden" id="verify-inputVal" value={userName} />
            <input type="hidden" id="verify-email" value={userName} />
            <input type="hidden" id="verify-ssoid" value={ssoid} />
            <input type="hidden" id="verify-logintype" value={loginType} />
            <ul>
              <li classNameName="input-field mobile-no">
                <p>
                  {loginType === 'email' ? null : <span className="country-code">+91 - </span>}
                  <input
                    autoComplete="off"
                    type="text"
                    name="verify-emailid"
                    maxLength="100"
                    disabled
                    value={userName}
                  />
                </p>
                <a
                  href="javascript:void(0)"
                  id="changeRegisterEmailId"
                  className="secondary-link"
                  onClick={() => changeCurrentStep('CHECK_USER_EXISTS')}
                >
                  Change Email/Mobile No.
                </a>
              </li>
              <li className="input-field password">
                <p>
                  <input
                    type="password"
                    name="otpverify"
                    maxLength="6"
                    onChange={e => this.onFormFieldChange(e, 'otp')}
                    value={otp}
                    placeholder="Enter the verification code"
                  />
                </p>

                <div className={cname} style={{ display: serverMessage ? 'block' : 'none' }}>
                  {serverMessage}
                </div>
                <span className="regenerate-otp">Didn't receive OTP?</span>
                <a
                  id="sso-verify-regenerate-otp"
                  href="javascript:void(0)"
                  className="secondary-link"
                  onClick={this.regenerateOtp}
                >
                  Re-Generate OTP
                </a>
              </li>
              <li className="submit">
                <input
                  type="submit"
                  id="sso-verify-btn"
                  className="submit-btn disabled"
                  value="Verify and Login"
                />
              </li>
            </ul>
          </form>
          <div className="mandatory-box">
            <p>
              *Email or mobile no. verification is mandatory to complete the registration process.
            </p>
          </div>
        </div>
      </div>
    );
  }
}

RegisterVerifiedLogin.propTypes = {
  userName: PropTypes.string,
  ssoid: PropTypes.string,
  loginType: PropTypes.string,
  changeCurrentStep: PropTypes.func,
};

export default RegisterVerifiedLogin;
