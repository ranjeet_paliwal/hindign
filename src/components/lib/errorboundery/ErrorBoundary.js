/* eslint-disable react/destructuring-assignment */
import React from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      hasError: false,
      error: null,
      info: null,
    };
  }

  componentDidCatch(error, info) {
    this.setState({
      hasError: true,
      error,
      info,
    });
  }

  render() {
    const { hasError, error, info } = this.state;
    if (hasError) {
      return (
        <div style={{ display: 'none' }} className="error-block">
          <p>The error: {error.toString()}</p>
          <p>Where it occured: {info.componentStack}</p>
        </div>
      );
    }
    return this.props.children;
  }
}

ErrorBoundary.propTypes = {
  children: PropTypes.any,
  error: PropTypes.string,
  info: PropTypes.string,
};

export default ErrorBoundary;
