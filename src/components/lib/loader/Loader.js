import React from 'react';
import './fake-loader.scss';

const Loader = props => {
  // return <div>Loading...</div>;
  const { loader } = props;

  return loader ? (
    <div className="section">
      <div className="table fake-listview">
        <div className="table_row">
          <span className="table_col img_wrap">
            <div className="dummy-img" />
          </span>
          <span className="table_col con_wrap">
            <span className="blur-div" />
            <span className="blur-div" style={{ width: '70%' }} />
            <span className="blur-div small" style={{ width: '40%' }} />
          </span>
        </div>
      </div>
      <div className="table fake-listview">
        <div className="table_row">
          <span className="table_col img_wrap">
            <div className="dummy-img" />
          </span>
          <span className="table_col con_wrap">
            <span className="blur-div" />
            <span className="blur-div" style={{ width: '70%' }} />
            <span className="blur-div small" style={{ width: '40%' }} />
          </span>
        </div>
      </div>
      <div className="table fake-listview">
        <div className="table_row">
          <span className="table_col img_wrap">
            <div className="dummy-img" />
          </span>
          <span className="table_col con_wrap">
            <span className="blur-div" />
            <span className="blur-div" style={{ width: '70%' }} />
            <span className="blur-div small" style={{ width: '40%' }} />
          </span>
        </div>
      </div>
      <div className="table fake-listview">
        <div className="table_row">
          <span className="table_col img_wrap">
            <div className="dummy-img" />
          </span>
          <span className="table_col con_wrap">
            <span className="blur-div" />
            <span className="blur-div" style={{ width: '70%' }} />
            <span className="blur-div small" style={{ width: '40%' }} />
          </span>
        </div>
      </div>
    </div>
  ) : (
    'Loading..'
  );
};
export default Loader;
