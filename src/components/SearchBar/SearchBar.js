import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';

const Config = require(`../../../common/${process.env.SITE}`);

class SearchBar extends PureComponent {
  state = {
    query: '',
    results: [],
    totalDataLength: 0,
    gadgetReviewsLength: 0,
    gadgetNewsLength: 0,
    currentlyActive: 0,
    GadgetsNews: null,
    GadgetsReviews: null,
  };

  getInfo = () => {
    const { query } = this.state;
    fetch(
      `https://navbharattimes.indiatimes.com/tech/single_search.cms?keyword=${query}&tag=product`,
    )
      .then(response => response.json())
      .then(searchData => {
        const NewsData = searchData.filter(data => {
          return data.news;
        });
        const ReviewsData = searchData.filter(data => {
          return data.reviews;
        });

        let gadgetNewsLength = 0;
        let gadgetReviewsLength = 0;
        let totalLength = 0;

        const GadgetsNews =
          NewsData[0] && NewsData[0].news && NewsData[0].news.data ? NewsData[0].news.data : null;

        if (GadgetsNews) {
          gadgetNewsLength = GadgetsNews.length;
        }

        const GadgetsReviews =
          ReviewsData[0] && ReviewsData[0].reviews && ReviewsData[0].reviews.data
            ? ReviewsData[0].reviews.data
            : null;

        if (GadgetsReviews) {
          gadgetReviewsLength = GadgetsReviews.length;
        }

        totalLength = gadgetReviewsLength + gadgetNewsLength;

        this.setState({
          results: searchData,
          totalDataLength: totalLength,
          gadgetReviewsLength,
          gadgetNewsLength,
          GadgetsNews,
          GadgetsReviews,
          currentlyActive: 0,
        });
      });
  };

  handleMenuChange = e => {
    // Up arrow 38
    // Down arrow 40
    let { currentlyActive } = this.state;
    const { totalDataLength, GadgetsNews, GadgetsReviews } = this.state;

    if (e.keyCode === 13 && currentlyActive <= totalDataLength && currentlyActive >= 0) {
      if (currentlyActive >= 1 && currentlyActive <= GadgetsNews.length) {
        this.props.router.push(GadgetsNews[currentlyActive - 1].url);
      } else if (currentlyActive > GadgetsNews.length && currentlyActive <= GadgetsReviews.length) {
        this.props.router.push(GadgetsReviews[currentlyActive - 1].url);
      }
    }

    if ((e.keyCode === 38 || e.keyCode === 40) && totalDataLength > 0) {
      if (currentlyActive <= totalDataLength && currentlyActive >= 0) {
        if (
          (currentlyActive === 0 && e.keyCode === 38) ||
          (currentlyActive === totalDataLength && e.keyCode === 40)
        ) {
          // TODO: Focus back on the search widget
          return false;
        }
        currentlyActive = e.keyCode === 38 ? currentlyActive - 1 : currentlyActive + 1;
      }
      this.setState({
        currentlyActive,
      });
    }
  };

  handleInputChange = e => {
    const { query } = this.state;

    // console.log('query state', query);
    this.setState(
      {
        query: e.target.value,
      },
      () => {
        if (this.state.query && this.state.query.length > 1) {
          // console.log('query length', query.length);
          if (this.state.query.length > 3) {
            this.getInfo();
          }
        } else if (this.state.query.length === 1) {
          this.setState({
            results: [],
            totalDataLength: 0,
            gadgetReviewsLength: 0,
            gadgetNewsLength: 0,
            currentlyActive: 0,
            GadgetsNews: null,
            GadgetsReviews: null,
          });
        }
      },
    );
  };

  render() {
    const { results, gadgetNewsLength, currentlyActive } = this.state;
    const NewsData = results.filter(data => {
      return data.news;
    });
    const ReviewsData = results.filter(data => {
      return data.reviews;
    });

    const GadgetsNews =
      NewsData[0] && NewsData[0].news && NewsData[0].news.data ? NewsData[0].news.data : null;

    const GadgetsReviews =
      ReviewsData[0] && ReviewsData[0].reviews && ReviewsData[0].reviews.data
        ? ReviewsData[0].reviews.data
        : null;

    return (
      <form>
        <ul className="search_container">
          <li className="serchicon">
            <div className="search-with-icon">
              <input
                placeholder={Config.Locale.Search_Placeholder}
                onChange={this.handleInputChange}
                onKeyDown={this.handleMenuChange}
              />
              <b />
            </div>
          </li>
        </ul>
        {GadgetsNews || GadgetsReviews ? (
          <div className="gn-search-result">
            <ul>
              {GadgetsNews ? <li className="head">{Config.Locale.news}</li> : ''}
              {GadgetsNews
                ? GadgetsNews.map((value, index) => {
                    let artUrl = '';
                    if (value.url.indexOf('/tech') != -1) {
                      artUrl = value.url.replace('/tech', '');
                    } else {
                      artUrl = value.url;
                    }
                    return (
                    <li
                        className={currentlyActive === index + 1 ? 'active' : ''}
                        id={`search_data_${index + 1}`}
                        key={`prod${value.Product_name}`}
                      >
                        <Link to={`${process.env.WEBSITE_URL}${artUrl}`} title={value.Product_name}>
                        {value.Product_name}
                      </Link>
                      </li>
                    );
                  })
                : ''}

              {GadgetsReviews ? <li className="head">{Config.Locale.review}</li> : ''}
              {GadgetsReviews
                ? GadgetsReviews.map((value, index) => {
                    let RevireUrl = '';
                    if (value.url.indexOf('miscellaneous') != -1) {
                      RevireUrl = value.url.replace('/miscellaneous', '');
                    } else if (
                      value.url.indexOf('http://navbharattimes.indiatimes.com/tech') != -1
                    ) {
                      RevireUrl = value.url.replace(
                        'http://navbharattimes.indiatimes.com/tech',
                        '',
                      );
                    } else {
                      RevireUrl = value.url;
                    }
                    return (
                    <li
                        id={`search_data_${gadgetNewsLength + index + 1}`}
                        key={`prod${value.Product_name}`}
                        className={currentlyActive === gadgetNewsLength + index + 1 ? 'active' : ''}
                      >
                        <Link
                        to={`${process.env.WEBSITE_URL}${RevireUrl}`}
                        title={value.Product_name}
                      >
                        {value.Product_name}
                      </Link>
                      </li>
                    );
                  })
                : ''}
            </ul>
          </div>
        ) : (
          ''
        )}
      </form>
    );
  }
}

SearchBar.propTypes = {
  value: PropTypes.array,
  params: PropTypes.any,
};

export default SearchBar;
