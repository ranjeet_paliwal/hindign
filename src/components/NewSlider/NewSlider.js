/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
// Check out my free youtube video on how to build a thumbnail gallery in react
// https://www.youtube.com/watch?v=GZ4d3HEn9zg

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';
import Thumb from '../Thumb/Thumb';
import { truncateStr, getKeyByValue, getBuyLink } from '../../common-utility';

import './HP_slider.scss';

import CtnAd from '../CtnAd/CtnAd';
import config from '../../reducers/config/config';

const siteConfig = require(`../../../common/${process.env.SITE}`);
const buyButton =
  (siteConfig && siteConfig.Locale && siteConfig.Locale.tech && siteConfig.Locale.tech.purchase) ||
  '';

class Slider extends Component {
  constructor(props) {
    super(props);
    const { sliderData, sliderWidth, size } = this.props;
    this.state = {
      datalength: Math.ceil(parseInt(sliderData) / parseInt(size)),
      data: sliderData,
      sliderWidth,
      currentIndex: 0,
      translateValue: 0,
    };
  }

  goToPrevSlide = () => {
    const { currentIndex } = this.state;
    if (currentIndex === 0) return;

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1,
      translateValue: parseInt(prevState.translateValue) + parseInt(this.slideWidth()),
    }));
  };

  goToNextSlide = datalength => {
    // Exiting the method early if we are at the end of the images array.
    // We also want to reset currentIndex and translateValue, so we return
    // to the first image in the array.
    const { currentIndex } = this.state;
    if (currentIndex === parseInt(datalength) - 1) {
      return this.setState({
        currentIndex: 0,
        translateValue: 0,
      });
    }

    // This will not run if we met the if condition above
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue + -this.slideWidth(),
    }));

    return 0;
  };

  slideWidth = () => {
    // const self = this;
    // const margin = self.props.margin ? parseInt(self.props.margin) : 0;
    // return self.props.width ? parseInt(self.props.width) + parseInt(margin) : 300;
    return this.state.sliderWidth;
  };

  getUpdatedData = (data, size) => {
    const newArr = [];
    const newdata = [...data];
    while (newdata.length > 0) {
      newArr.push(newdata.splice(0, size));
    }
    return newArr;
  };

  render() {
    const { translateValue, data } = this.state;
    const {
      type,
      size,
      width,
      height,
      secname,
      SliderClass,
      islinkable,
      link,
      sliderWidth,
      clicked,
      headingRequired,
      moreLinkRequired,
      moreLinkText,
      sliderData,
      category,
      utmParameters,
      categoryseo,
      tag,
      clickedEvent,
    } = this.props;
    // console.log('from Props', sliderData);
    // console.log('from State', data);
    const updatedArr = this.getUpdatedData(sliderData, size);

    let heading = secname;
    let MoreLink = moreLinkText;
    if (islinkable) {
      heading = (
        <React.Fragment>
          <Link title={secname} to={link}>
            {secname}
          </Link>
        </React.Fragment>
      );

      MoreLink = (
        <Link className="more-btn" title={secname} to={link}>
          {MoreLink}
        </Link>
      );
    }
    return (
      <React.Fragment>
        <div className={type === 'embedslider' ? 'full_section_slider' : ''}>
          {secname && headingRequired !== 'no' ? <h2>{heading}</h2> : null}
          {secname && moreLinkRequired !== 'no' ? (
            <React.Fragment>{MoreLink}</React.Fragment>
          ) : null}
          <div className={`slider ${SliderClass}`}>
            <div
              className="slider_content"
              style={{
                transform: `translateX(${translateValue}px)`,
                transition: 'transform ease-out 0.45s',
              }}
            >
              <ul>
                {Array.isArray(updatedArr)
                  ? updatedArr.map(item => {
                      return (
                        <ManageSlider
                          key={
                            type === 'compareListPopular' || 'compareListLastest'
                              ? item[0]._id
                              : item.id
                          }
                          utmParameters={utmParameters}
                          type={type}
                          data={item}
                          width={width}
                          height={height}
                          sliderWidth={sliderWidth}
                          clicked={clicked}
                          sliderData={sliderData}
                          category={category}
                          categoryseo={categoryseo}
                          tag={tag}
                          clickedEvent={clickedEvent}
                        />
                      );
                    })
                  : null}
              </ul>
            </div>

            <LeftArrow goToPrevSlide={this.goToPrevSlide} />

            <RightArrow goToNextSlide={this.goToNextSlide.bind(this, updatedArr.length)} />
          </div>
        </div>
      </React.Fragment>
    );
  }
}

// eslint-disable-next-line consistent-return
const ManageSlider = ({
  type,
  data,
  width,
  height,
  sliderWidth,
  clicked,
  sliderData,
  utmParameters,
  category,
  categoryseo,
  tag,
  clickedEvent,
}) => {
  let slider = '';
  const secData = '';
  switch (type) {
    case 'embedslider':
      slider = (
        <li>
          {Array.isArray(data)
            ? data.map(item => {
                const title = item.hl;
                return (
                  <div className="embedslide" key={item.imageid}>
                    <Thumb
                      imgId={item.imageid}
                      imgSize={item.imgsize}
                      alt={title}
                      title={title}
                      width={width}
                      height={height}
                      link={item.wu}
                      islinkable={false}
                    />
                    <div className="embedslide_desc">{item.caption}</div>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;
    case 'gnbrand':
      slider = (
        <li>
          {sliderData.map(item => {
            const Imgpath = `${process.env.IMG_URL}/thumb/${
              siteConfig.SiteInfo.defaultimg
            }.cms?width=150`;

            const gadgetImg = item.image.includes('jpg') ? item.image : Imgpath;
            //  'https://navbharattimes.indiatimes.com/thumb/66951362.cms?width=150'
            return (
              <div className="slide" key={item.imageid}>
                <Thumb
                  src={gadgetImg}
                  islinkable="true"
                  link={`https://navbharattimes.indiatimes.com/tech/${categoryseo}/${item.uname}`}
                />
                <span className="txt_container">
                  <Link
                    title={item.uname}
                    to={`https://navbharattimes.indiatimes.com/tech/${categoryseo}/${item.uname}`}
                  >
                    {item.uname}
                  </Link>
                </span>
              </div>
            );
          })}
        </li>
      );
      break;

    case 'photogallery':
    case 'videos':
      slider = (
        <li>
          {data.map((item, index) => {
            // console.log("===item==",item);
            const random = Math.floor(Math.random() * 1000);

            // console.log("===random==",random);
            // console.log("===type==",type);
            // console.log("===id_photos_slider==",siteConfig.CTN.id_photos_slider);
            // console.log("===id_videos_slider==",siteConfig.CTN.id_videos_slider);

            let widgetslotId = '';
            if (type === 'photogallery') {
              widgetslotId = siteConfig.CTN.id_photos_slider;
            } else {
              widgetslotId = siteConfig.CTN.id_videos_slider;
            }

            // console.log("===widgetslotId==",widgetslotId);

            if (item.type === 'ctn' && widgetslotId) {
              return (
                <div
                  className={`ctnad slide ${type === 'photogallery' ? 'photo' : 'video'}icon`}
                  key={index}
                >
                  <CtnAd
                    articleId={item.position + random}
                    position={item.position + random}
                    isslider={item.isslider}
                    height="225"
                    width="300"
                    className="colombia"
                    slotId={widgetslotId}
                  />
                </div>
              );
            }
            return (
              <div
                className={`slide ${type === 'photogallery' ? 'photo' : 'video'}icon`}
                key={item.imageid}
              >
                <Thumb
                  imgId={item.imageid}
                  imgSize={item.imgsize}
                  width="300"
                  height="168"
                  alt={item.hl}
                  title={item.hl}
                  islinkable="true"
                  link={item.wu}
                />
                <span className="txt_container">
                  <Link title={item.hl} to={item.wu} className="text_ellipsis">
                    {item.hl}
                  </Link>
                </span>
              </div>
            );
          })}
        </li>
      );
      break;

    case 'videosAS':
    case 'photogalleryAS':
    case 'newsSlider':
      slider = (
        <li>
          {data.map((item, index) => {
            // console.log("===item==",item);
            const random = Math.floor(Math.random() * 1000);
            // console.log("===random==",random);
            // console.log("===id_videos_slider==",siteConfig.CTN.id_videos_slider);
            const widgetslotId = siteConfig.CTN.id_photos_slider_inner;
            // console.log("===widgetslotId==",widgetslotId);
            let iconClassName = '';
            if (type === 'photogalleryAS') {
              iconClassName = 'photo';
            } else if (type === 'videosAS') {
              iconClassName = 'video';
            }

            if (item.type === 'ctn') {
              return (
                <div
                  className={`ctnad slide ${type === 'photogalleryAS' ? 'photo' : 'video'}icon`}
                  key={index}
                >
                  <CtnAd
                    articleId={item.position + random}
                    position={item.position + random}
                    isslider={item.isslider}
                    height="225"
                    width="300"
                    className="colombia"
                    slotId={widgetslotId}
                  />
                </div>
              );
            }
            return (
              <div className={`slide ${iconClassName}icon`} key={item.imageid}>
                <Thumb
                  imgId={item.imageid}
                  imgSize={item.imgsize}
                  width={width || '200'}
                  height={height || '112'}
                  alt={item.hl}
                  title={item.hl}
                  islinkable="true"
                  link={item.wu}
                />
                <span className="txt_container">
                  <Link title={item.hl} to={item.wu} className="text_ellipsis">
                    {item.hl}
                  </Link>
                </span>
              </div>
            );
          })}
        </li>
      );
      break;

    case 'trending':
      slider = (
        <li>
          {sliderData.map((item, index) => {
            const amztitle = item.uname;
            const amzga = `${amztitle}_${item.affiliate.price}`;
            const affiliateTag =
              (siteConfig && siteConfig.affiliateTags && siteConfig.affiliateTags.HP) || '';
            const price = item.affiliate.price;
            const title = item.uname;
            const url = item.affiliate.wu;
            const buyURL = getBuyLink({
              affiliateData: item.affiliate,
              url,
              price,
              title,
              amzga,
              tag: affiliateTag,
            });

            let rumourClass = '';
            if (item.rumoured == 1 || item.upcoming == 1) {
              if (item.rumoured == 1 && item.upcoming == 1) {
                rumourClass = 'rumoured';
              } else {
                rumourClass = item.rumoured == 1 ? 'rumoured' : 'upcoming';
              }
            }
            return (
              <div className={`slide ${rumourClass}`} key={`gadgets_${index}`}>
                <div className="top-caption" />
                <span className="prod_img">
                  <Thumb src={item.imgsrc} islinkable="true" link={item.wu} />
                </span>
                <span className="title">
                  <Link title={item.hl} to={item.wu} className="text_ellipsis">
                    {item.hl}
                  </Link>
                </span>
                {/* <span className="rating">
                  {item.criticrating && item.userrating !== 'NaN' ? (
                    <span>
                      {siteConfig.Locale.critics}: <b>{item.criticrating}</b>/5
                    </span>
                  ) : ''}
                </span> */}
                <span className="rating">
                  {item.userrating && item.userrating !== 'NaN' ? (
                    <span>
                      {siteConfig.Locale.critics}:
                      {!item.criticrating || Number.parseInt(item.criticrating) === 0 ? (
                        'NA'
                      ) : (
                        <React.Fragment>
                          <b>{Number.parseFloat(item.criticrating / 2).toFixed(1)}</b>/5
                        </React.Fragment>
                      )}
                      {' | '}
                      {siteConfig.Locale.user}:
                      {Number.parseInt(item.userrating) !== 0 ? (
                        <React.Fragment>
                          <b>{item.userrating}</b> / 5
                        </React.Fragment>
                      ) : (
                        <Link to={`${process.env.BASE_URL}/tech/${item.cat}/${item.uname}#rate`}>
                          {siteConfig.Locale.submitrating}
                        </Link>
                      )}
                    </span>
                  ) : (
                    <Link to={`${process.env.BASE_URL}/tech/${item.cat}/${item.uname}#rate`}>
                      {siteConfig.Locale.submitrating}
                    </Link>
                  )}
                </span>
                {item.price ? (
                  <span className="price">
                    <b className="symbol_rupees">{item.price} </b>
                  </span>
                ) : (
                  ''
                )}
                {item.affiliate && item.affiliate.name ? (
                  <span className="btn_wrap">
                    <a href={buyURL} target="_blank" className="buy_btn">
                      {siteConfig.Locale.tech.buyFrom}
                    </a>
                  </span>
                ) : (
                  ''
                )}
                {/* {item.affiliatesinfo &&
                item.affiliatesinfo.exactmatches &&
                item.affiliatesinfo.exactmatches.amazon &&
                item.affiliatesinfo.exactmatches.amazon.listprice ? (
                  <span className="price">
                    <b className="symbol_rupees">
                      {item.affiliatesinfo.exactmatches.amazon.listprice.amount}{' '}
                    </b>
                  </span>
                ) : (
                  ''
                )} */}
              </div>
            );
          })}
        </li>
      );
      break;

    case 'amazonwidgetslider':
      const amazTag = tag;
      slider = (
        <li>
          {sliderData.map(item => {
            if (item.affiliate === 'amazon') {
              const price = item.price;
              const amzga = `${item.pid}_${price}`;
              const affiliateTag = amazTag;
              // const affiliateTag =
              //   (siteConfig && siteConfig.affiliateTags && siteConfig.affiliateTags.CD) || '';

              const title = item.title;
              const url = item.url;
              const buyURL = getBuyLink({
                url,
                price,
                title,
                amzga,
                tag: affiliateTag,
              });

              // console.log('buyURL', buyURL);
              const listPrice = item.listPrice;
              return (
                <div className="slide" key={item.pid}>
                  <Link
                    title={item.title}
                    to={buyURL}
                    rel="nofollow"
                    className="table_row"
                    target="_blank"
                  >
                    <span className="img_wrap">
                      <Thumb
                        src={item.imageUrl}
                        imgSize=""
                        width=""
                        height=""
                        alt={item.title}
                        title={item.title}
                        islinkable=""
                        link=""
                      />
                    </span>
                    <span className="con_wrap">
                      <h4 className="text_ellipsis">{item.title}</h4>

                      <span className="price_tag">
                        &#8377; {price}
                        {listPrice !== undefined && listPrice > 0 && price != listPrice ? (
                          <b>&#8377; {listPrice}</b>
                        ) : null}
                      </span>
                    </span>
                    <span className="btn_wrap">
                      <button className="buy_btn">{buyButton}</button>
                    </span>
                  </Link>
                </div>
              );
            }
          })}
        </li>
      );

      break;

    case '2gudwidgetslider':
      // console.log('2gudwidgetslider', sliderData);
      slider = (
        <li className="gn-amazon-listitem">
          {data.map(item => {
            const twogudga = `${item.title}_${item.price}`;
            const affid = 'gadgetsnow';
            const afname = '2gud';
            const url = encodeURIComponent(`${item.url}&afname=${afname}&affextparam2=${tag}`);
            const price = item.price;
            const listPrice = item.listPrice;
            const affiliateUrl = `https://navbharattimes.indiatimes.com/tech/affiliate_2gud.cms?url=${url}&price=${price}&title=${
              item.title
            }&twogud_ga=${twogudga}&affid=${affid}&afname=${afname}&affextparam2=${tag}`;

            // console.log(process.env.WEBSITE_URL);
            // console.log("tag :"+tag);
            // console.log("twogudga :"+twogudga);
            // console.log("url :"+url);
            // console.log("price :"+price);
            // console.log("listPrice :"+listPrice);
            // console.log("affiliateUrl :"+affiliateUrl);

            return (
              <div className="slide" key={item.title}>
                <Link title={item.title} to={affiliateUrl} rel="nofollow" target="_blank">
                  <span className="img_wrap">
                    <Thumb
                      src={item.imageUrl}
                      imgSize=""
                      width=""
                      height=""
                      alt={item.title}
                      title={item.title}
                      islinkable=""
                      link=""
                    />
                  </span>

                  <span className="con_wrap">
                    <h4 className="text_ellipsis">{item.title}</h4>

                    <span className="price_tag">
                      &#8377; {price}
                      {listPrice !== undefined && listPrice > 0 && price != listPrice ? (
                        <b>&#8377; {listPrice}</b>
                      ) : null}
                    </span>
                  </span>
                  <span className="btn_wrap">
                    <button className="buy_btn">{buyButton}</button>
                  </span>
                </Link>
              </div>
            );
          })}
        </li>
      );
      break;
    case 'photoshowright_slider': {
      slider = (
        <li>
          {Array.isArray(data)
            ? data.map(item => {
                return (
                  <React.Fragment>
                    <Link title={item.hl} className="title" to={item.wu}>
                      <Thumb
                        imgId={item.imageid}
                        imgSize={item.imgsize}
                        width={width}
                        height={height}
                      />
                    </Link>
                  </React.Fragment>
                );
              })
            : null}
        </li>
      );
      break;
    }

    case 'suggestedLinkable':
      {
        let gadgetsName = '';
        if (data && data[0]) {
          if (Array.isArray(data[0].regional)) {
            const filiterdData = data[0].regional.filter(item => {
              return item && item.host === siteConfig.SiteInfo.hostid;
            });
            gadgetsName = filiterdData && filiterdData[0] && filiterdData[0].name;
          } else {
            gadgetsName = data[0].name;
          }
        }

        slider = (
          <li>
            {Array.isArray(data)
              ? data.map(item => {
                  const imagePath =
                    item.imageMsid && Array.isArray(item.imageMsid)
                      ? item.imageMsid[0]
                      : item.imageMsid;
                  return (
                    <div className="slide" key={item.uname}>
                      <Link
                        to={`/tech/${siteConfig.gadgetCategories[item.category]}/${item.uname}`}
                      >
                        <span className="prod_img">
                          <Thumb imgId={imagePath} width="200" resizeMode="4" />
                        </span>
                        <span className="title text_ellipsis">{gadgetsName}</span>
                      </Link>
                    </div>
                  );
                })
              : null}
          </li>
        );
      }
      break;

    case 'relatedNews':
      {
        const utmLinks = Object.entries(utmParameters)
          .map(paramArray => `${paramArray[0]}=${paramArray[1]}`)
          .join('&');

        slider = (
          <li>
            {Array.isArray(data)
              ? data.map((item, index) => {
                  return (
                    <div className="slide" key={item.imageid}>
                      <Link to={`${item.wu}?${utmLinks}?utm_campaign=article${index + 1}`}>
                        <span className="prod_img">
                          <Thumb imgId={item.imageid} width="200" title={item.hl} resizeMode="4" />
                        </span>
                        <span className="title text_ellipsis">{item.hl}</span>
                      </Link>
                    </div>
                  );
                })
              : null}
          </li>
        );
      }
      break;

    case 'suggested': {
      slider = (
        <li>
          {Array.isArray(data)
            ? data.map(item => {
                const amzga = `${item.productname}_${item.price}`;
                const affiliateTag =
                  (siteConfig && siteConfig.affiliateTags && siteConfig.affiliateTags.CL) || '';
                const price = item.price;
                const title = item.productname;

                const buyURL = getBuyLink({
                  affiliateData: item.affiliate,
                  price,
                  title,
                  amzga,
                  tag: affiliateTag,
                });
                const imagePath =
                  item.imageMsid && Array.isArray(item.imageMsid)
                    ? item.imageMsid[0]
                    : item.imageMsid;

                const regionalName =
                  item.regional &&
                  Array.isArray(item.regional) &&
                  item.regional.filter(data => data.host == '53');

                return (
                  <div className="slide" key={item.uname + item.price}>
                    <span
                      className="plus_icon"
                      onClick={clickedEvent.bind(this, {
                        Product_name: regionalName[0] && regionalName[0].name,
                        seoname: item.uname,
                      })}
                    />
                    <Link to={`/tech/${siteConfig.gadgetCategories[item.category]}/${item.uname}`}>
                      <span className="prod_img">
                        <Thumb imgId={imagePath} width="200" resizeMode="4" />
                      </span>
                      {/* <Link title={title} to={item.wu}></Link> */}
                      <span className="title text_ellipsis"> {item && item.name}</span>
                    </Link>
                    {Number.parseInt(item.price) === 0 ? (
                      ''
                    ) : (
                      <span className="price">
                        <b className="symbol_rupees">{item.price}</b>
                      </span>
                    )}

                    <span className="btn_wrap">
                      <Link to={buyURL} target="_blank" className="btn-buy">
                        {siteConfig.Locale.tech.buyFrom}
                      </Link>
                    </span>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;
    }
    case 'compareListLatest':
    case 'compareListPopular': {
      let toolTip = '';
      let liData = '';

      if (Array.isArray(data)) {
        liData = data.map(item => {
          const seoPath =
            item && item._id && item._id.indexOf('-vs-') !== -1
              ? item._id
                  .split('-vs-')
                  .sort()
                  .join('-vs-')
              : '';
          if (item && item.product_name) {
            toolTip = `${siteConfig.Locale.comparedotxt} ${item.product_name.join(' vs ')}`;
          }

          const deviceCategory =
            siteConfig && siteConfig.gadgetMapping && item.category
              ? getKeyByValue(siteConfig.gadgetMapping, item.category)
              : '';
          return item.product_name.map((device, index) => {
            return (
              <div className="slide" key={device}>
                <Link
                  to={`/tech/compare-${deviceCategory}/${seoPath}`}
                  key={device}
                  title={toolTip}
                >
                  <span className="prod_img">
                    <Thumb
                      imgId={item.imageMsid[index]}
                      width="200"
                      resizeMode="4"
                      title="no"
                      title={toolTip}
                    />
                  </span>
                  <span className="title text_ellipsis">{device}</span>
                  <span className="vs">VS</span>
                </Link>
              </div>
            );
          });
        });
      }

      slider = <li>{liData}</li>;
      break;
    }
    default:
      slider = (
        <li>
          {Array.isArray(data)
            ? data.map(item => {
                const title = type === 'showcase' ? item.syn : item.hl;
                return (
                  <div className="slide" key={item.imageid}>
                    <Thumb
                      imgId={item.imageid}
                      imgSize={item.imgsize}
                      width={width}
                      height={height}
                      link={item.wu}
                      islinkable="true"
                    />

                    <span className="txt_container">
                      <Link title={title} to={item.wu}>
                        {truncateStr(title, 80)}
                      </Link>
                    </span>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;
  }
  return slider;
};

Slider.propTypes = {
  type: PropTypes.string,
  sliderData: PropTypes.array,
  secname: PropTypes.string,
  currentIndex: PropTypes.any,
  translateValue: PropTypes.number,
  size: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  SliderClass: PropTypes.string,
  islinkable: PropTypes.string,
  link: PropTypes.string,
  sliderWidth: PropTypes.string,
  clicked: PropTypes.func,
  headingRequired: PropTypes.string,
  category: PropTypes.string,
};

ManageSlider.propTypes = {
  data: PropTypes.array,
  width: PropTypes.string,
  height: PropTypes.string,
  SliderClass: PropTypes.string,
};

const LeftArrow = props => {
  const { goToPrevSlide } = props;
  // eslint-disable-next-line jsx-a11y/click-events-have-key-events
  return <span className="btnPrev" onClick={goToPrevSlide} />;
};

const RightArrow = props => {
  const { goToNextSlide } = props;
  return <span pg="nxtphoto" className="btnNext" onClick={goToNextSlide} />;
};

LeftArrow.propTypes = {
  goToPrevSlide: PropTypes.func,
};

RightArrow.propTypes = {
  goToNextSlide: PropTypes.func,
};

export default Slider;
