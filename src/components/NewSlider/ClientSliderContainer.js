import React, { Component } from 'react';
import PropTypes from 'prop-types';

import fetch from '../../utils/fetch/fetch';
import Slider from './NewSlider';

class ClientSliderContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      fetchError: false,
      isLoading: true,
    };
  }

  componentDidMount() {
    const { sliderParams } = this.props;
    const { type, id } = sliderParams;

    switch (type) {
      case 'tech':
      case 'auto':
      case 'infographics':
      case 'expert_advice':
        fetch(
          `${
            process.env.API_ENDPOINT
          }/webgn_combine.cms?type=list&count=10&feedtype=sjson&secid=${id}`,
        )
          .then(data => {
            const parsedLink =
              data && data.section && data.section.newsItem && data.section.newsItem.override
                ? data.section.newsItem.override
                : '';

            this.setState({
              data,
              link: parsedLink,
              fetchError: false,
              isLoading: false,
            });
          })
          .catch(e => {
            this.setState({
              fetchError: true,
              isLoading: false,
            });
          });
        break;
      case 'superhitblog':
      case 'readerblog':
        fetch(
          `${
            process.env.API_ENDPOINT
          }/webgn_combine.cms?tag=${id}&type=article&donotshowurl=1&feedtype=sjson`,
        ).then(data => {
          const parsedLink =
            data && data.section && data.section.newsItem && data.section.newsItem.override
              ? data.section.newsItem.override
              : '';

          this.setState({
            data,
            link: parsedLink,
            fetchError: false,
            isLoading: false,
          });
        });
        break;
      default:
    }
  }

  render() {
    const { data, link } = this.state;
    const { sliderProps, sliderParams } = this.props;

    if (!data) {
      // Todo Add loader
      return <div>Loading...</div>;
    }

    const propsWithData = {
      ...sliderProps,
      link,
      sliderData:
        data &&
        data.section &&
        data.section.newsItem &&
        data.section.newsItem.items &&
        Array.isArray(data.section.newsItem.items)
          ? data.section.newsItem.items
          : [],
      secname:
        data && data.section && data.section.newsItem && data.section.newsItem.secname
          ? data.section.newsItem.secname
          : '',
      type: sliderParams.type,
    };

    return <Slider {...propsWithData} />;
  }
}

ClientSliderContainer.propTypes = {
  sliderProps: PropTypes.object,
  sliderParams: PropTypes.object,
};

export default ClientSliderContainer;
