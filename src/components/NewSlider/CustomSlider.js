/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
// Check out my free youtube video on how to build a thumbnail gallery in react
// https://www.youtube.com/watch?v=GZ4d3HEn9zg

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';
import Thumb from '../Thumb/Thumb';
import { truncateStr, getKeyByValue } from '../../common-utility';

import './HP_slider.scss';

const siteConfig = require(`../../../common/${process.env.SITE}`);

class CustomSlider extends Component {
  constructor(props) {
    super(props);
    const { sliderData, sliderWidth, size } = this.props;
    this.state = {
      datalength: Math.ceil(parseInt(sliderData) / parseInt(size)),
      data: sliderData,
      sliderWidth,
      currentIndex: 0,
      translateValue: 0,
    };
  }

  goToPrevSlide = () => {
    const { currentIndex } = this.state;
    if (currentIndex === 0) return;

    this.setState(prevState => ({
      currentIndex: prevState.currentIndex - 1,
      translateValue: parseInt(prevState.translateValue) + parseInt(this.slideWidth()),
    }));
  };

  goToNextSlide = datalength => {
    // Exiting the method early if we are at the end of the images array.
    // We also want to reset currentIndex and translateValue, so we return
    // to the first image in the array.
    const { currentIndex } = this.state;
    if (currentIndex === parseInt(datalength) - 1) {
      return this.setState({
        currentIndex: 0,
        translateValue: 0,
      });
    }

    // This will not run if we met the if condition above
    this.setState(prevState => ({
      currentIndex: prevState.currentIndex + 1,
      translateValue: prevState.translateValue + -this.slideWidth(),
    }));

    return 0;
  };

  slideWidth = () => {
    // const self = this;
    // const margin = self.props.margin ? parseInt(self.props.margin) : 0;
    // return self.props.width ? parseInt(self.props.width) + parseInt(margin) : 300;
    return this.state.sliderWidth;
  };

  getUpdatedData = (data, size) => {
    const newArr = [];
    const newdata = [...data];
    while (newdata.length > 0) {
      newArr.push(newdata.splice(0, size));
    }
    return newArr;
  };

  render() {
    const { translateValue, data } = this.state;
    const {
      type,
      size,
      width,
      height,
      secname,
      SliderClass,
      islinkable,
      link,
      sliderWidth,
      clicked,
      headingRequired,
      moreLinkRequired,
      moreLinkText,
      sliderData,
      category,
      categoryseo,
      tag,
      clickedEvent,
      deviceCount,
    } = this.props;

    const updatedArr = this.getUpdatedData(sliderData, size);

    let heading = secname;
    let MoreLink = moreLinkText;
    if (islinkable) {
      heading = (
        <React.Fragment>
          <Link title={secname} to={link}>
            {secname}
          </Link>
        </React.Fragment>
      );

      MoreLink = (
        <Link className="more-btn" title={secname} to={link}>
          {MoreLink}
        </Link>
      );
    }
    return (
      <React.Fragment>
        <div className={type === 'embedslider' ? 'full_section_slider' : ''}>
          {secname && headingRequired !== 'no' ? <h2>{heading}</h2> : null}
          {secname && moreLinkRequired !== 'no' ? (
            <React.Fragment>{MoreLink}</React.Fragment>
          ) : null}
          <div className={`slider ${SliderClass}`}>
            <div
              className="slider_content"
              style={{
                transform: `translateX(${translateValue}px)`,
                transition: 'transform ease-out 0.45s',
              }}
            >
              <ul>
                {Array.isArray(updatedArr)
                  ? updatedArr.map(item => {
                      return (
                        <ManageSlider
                          key={
                            type === 'compareListPopular' || 'compareListLastest'
                              ? item[0]._id
                              : item.id
                          }
                          type={type}
                          data={item}
                          width={width}
                          height={height}
                          sliderWidth={sliderWidth}
                          clicked={clicked}
                          sliderData={sliderData}
                          category={category}
                          categoryseo={categoryseo}
                          tag={tag}
                          clickedEvent={clickedEvent}
                        />
                      );
                    })
                  : null}
              </ul>
            </div>
            {deviceCount > 6 && <LeftArrow goToPrevSlide={this.goToPrevSlide} />}
            {deviceCount > 6 && <RightArrow goToNextSlide={this.goToNextSlide} />}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

// eslint-disable-next-line consistent-return
const ManageSlider = ({
  type,
  data,
  width,
  height,
  sliderWidth,
  clicked,
  sliderData,
  category,
  categoryseo,
  tag,
  clickedEvent,
}) => {
  let slider = '';
  switch (type) {
    case 'suggested': {
      let buyURL = '';
      slider = (
        <li>
          {Array.isArray(data)
            ? data.map(item => {
                if (item.affiliate && item.affiliate.exact && item.affiliate.exact[0]) {
                  buyURL = item.affiliate.exact[0].url;
                } else if (item.affiliate && item.affiliate.related && item.affiliate.related[0]) {
                  buyURL = item.affiliate.related[0].url;
                }

                return (
                  <div className="slide" key={item.imageid}>
                    <span
                      className="plus_icon"
                      onClick={clickedEvent.bind(this, {
                        Product_name: item.regional.name,
                        seoname: item.uname,
                      })}
                    />
                    <span className="prod_img">
                      <Thumb imgId={item.imageMsid[0]} width="200" resizeMode="4" />
                    </span>
                    {/* <Link title={title} to={item.wu}></Link> */}
                    <span className="title text_ellipsis">{item.regional.name}</span>
                    <span className="price">
                      <b className="symbol_rupees">{item.price}</b>
                    </span>

                    <span className="btn-buy">
                      <Link to={buyURL} target="_blank">
                        खरीदे
                      </Link>
                    </span>
                  </div>
                );
              })
            : null}
        </li>
      );
      break;
    }
    case 'compareListLatest':
    case 'compareListPopular':
      slider = (
        <li>
          {Array.isArray(data)
            ? data.map(item => {
                const seoPath =
                  item && item._id && item._id.indexOf('-vs-') !== -1
                    ? item._id
                        .split('-vs-')
                        .sort()
                        .join('-vs-')
                    : '';
                const deviceCategory =
                  siteConfig && siteConfig.gadgetMapping && item.category
                    ? getKeyByValue(siteConfig.gadgetMapping, item.category)
                    : '';
                return item.product_name.map((device, index) => {
                  return (
                    <Link to={`/tech/compare-${deviceCategory}/${seoPath}`} key={device}>
                      <div className="slide">
                        <span className="prod_img">
                          <Thumb imgId={item.imageMsid[index]} width="200" resizeMode="4" />
                        </span>
                        <span className="title text_ellipsis">{device}</span>
                        <span className="vs">VS</span>
                      </div>
                    </Link>
                  );
                });
              })
            : null}
        </li>
      );
      break;

    default:
      slider = <div>sss</div>;
      break;
  }
  return slider;
};

CustomSlider.propTypes = {
  type: PropTypes.string,
  sliderData: PropTypes.array,
  secname: PropTypes.string,
  currentIndex: PropTypes.any,
  translateValue: PropTypes.number,
  size: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  SliderClass: PropTypes.string,
  islinkable: PropTypes.string,
  link: PropTypes.string,
  sliderWidth: PropTypes.string,
  clicked: PropTypes.func,
  headingRequired: PropTypes.string,
  category: PropTypes.string,
};

ManageSlider.propTypes = {
  data: PropTypes.array,
  width: PropTypes.string,
  height: PropTypes.string,
  SliderClass: PropTypes.string,
};

const LeftArrow = props => {
  const { goToPrevSlide } = props;
  // eslint-disable-next-line jsx-a11y/click-events-have-key-events
  return <span className="btnPrev" onClick={goToPrevSlide} />;
};

const RightArrow = props => {
  const { goToNextSlide } = props;
  return <span pg="nxtphoto" className="btnNext" onClick={goToNextSlide} />;
};

LeftArrow.propTypes = {
  goToPrevSlide: PropTypes.func,
};

RightArrow.propTypes = {
  goToNextSlide: PropTypes.func,
};

export default CustomSlider;
