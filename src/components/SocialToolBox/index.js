/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import './SocialTool.scss';

const socialTools = React.memo(props => {
  const {
    fb,
    fbData,
    google,
    gplusData,
    twitter,
    twitterData,
    email,
    subscribe,
    print,
    printUrl,
    comments,
    showComments,
    showNewsLetterBox,
    showMailBox,
    socialShare,
  } = props;

  return (
    <React.Fragment>
      <div className="social_tools" id="upperSocialtool_68670710">
        {fb ? (
          <span onClick={() => socialShare('fb', fbData)} className="facebook social_icons">
            facebook
          </span>
        ) : null}
        {twitter ? (
          <span
            onClick={() => socialShare('twitter', twitterData)}
            className="twitter social_icons"
          >
            twitter
          </span>
        ) : null}
        {google ? (
          <span onClick={() => socialShare('gplus', gplusData)} className="gplus social_icons">
            google
          </span>
        ) : null}
        {email ? (
          <span className="share_email social_icons" onClick={() => showMailBox()}>
            email
          </span>
        ) : null}
        {comments ? (
          <span className="show_comments social_icons" onClick={() => showComments()}>
            <span className="bubble" id="commentcount2_68670710" />
            <span style={{ display: 'none' }} id="ctcnt" />
            <span style={{ display: 'none' }} id="ctcnt2" />
          </span>
        ) : null}
        {/* {subscribe ? (
          <span onClick={() => showNewsLetterBox()} className="subscribe_newsletter social_icons" />
        ) : null} */}
        {/* {print ? (
          <span
            className="print_this social_icons"
            onClick={() => (typeof window !== 'undefined' ? window.open(printUrl) : null)}
          />
        ) : null} */}
      </div>
    </React.Fragment>
  );
});

socialTools.displayName = 'socialTools';

export default socialTools;
