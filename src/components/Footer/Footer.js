/* eslint-disable operator-linebreak */
import React from 'react';

import proptypes from 'prop-types';
import Link from '../Link/Link';
import Loader from '../lib/loader/Loader';
import './Footer.scss';

const footer = React.memo(props => {
  const { data } = props;
  const sections =
    typeof data !== 'undefined' && data.sections && data.sections.items
      ? data.sections.items
      : null;

  const category =
    typeof data !== 'undefined' && data.sections && data.sections.category
      ? data.sections.category
      : null;

  const sites =
    typeof data !== 'undefined' && data.othersites && data.othersites.items
      ? data.othersites.items
      : null;

  const categorySites =
    typeof data !== 'undefined' && data.othersites && data.othersites.category
      ? data.othersites.category
      : null;
  const appdetails =
    typeof data !== 'undefined' && data.appdetails && data.appdetails.items
      ? data.appdetails.items
      : null;
  const appdetailstext =
    typeof data !== 'undefined' && data.appdetailstext && data.appdetailstext.items
      ? data.appdetailstext.items
      : null;

  const seoLinks =
    typeof data !== 'undefined' && data.glbllinks && data.glbllinks.items
      ? data.glbllinks.items
      : null;
  const seoLinks1 =
    typeof data !== 'undefined' && data.glbllinks1 && data.glbllinks1.items
      ? data.glbllinks1.items
      : null;
  const seoLinks2 =
    typeof data !== 'undefined' && data.glbllinks2 && data.glbllinks2.items
      ? data.glbllinks2.items
      : null;
  const seoLinks3 =
    typeof data !== 'undefined' && data.glbllinks3 && data.glbllinks3.items
      ? data.glbllinks3.items
      : null;
  const seoLinks4 =
    typeof data !== 'undefined' && data.glbllinks4 && data.glbllinks4.items
      ? data.glbllinks4.items
      : null;
  const seoLinks5 =
    typeof data !== 'undefined' && data.glbllinks5 && data.glbllinks5.items
      ? data.glbllinks5.items
      : null;
  const aboutUS =
    typeof data !== 'undefined' && data.aboutus && data.aboutus.items ? data.aboutus.items : null;
  const otherinfo = typeof data !== 'undefined' && data.otherinfo ? data.otherinfo : null;
  const fbLike = typeof data !== 'undefined' && data.fb && data.fb.items ? data.fb.items : null;

  return (
    <footer className="footer">
      <div className="ELECTION_TOPBAND_BOTTOM" />
      <div className="top_social_links">
        <div className="first">
          <a className="gnLogo" href="https://navbharattimes.indiatimes.com/tech.cms">
            Gadget Now
          </a>
        </div>
        <div className="second">
          <a  rel="nofollow" target="_blank" href="https://www.facebook.com/navbharattimes">
            <span className="nbt_gadget_sprite icon_facebook" />
          </a>
          <a rel="nofollow"  target="_blank" href="https://twitter.com/navbharattimes">
            <span className="nbt_gadget_sprite icon_twitter" />
          </a>
        </div>
        <div className="third">
          {appdetailstext &&
            Array.isArray(appdetailstext) &&
            appdetailstext.map(item => {
              return (
                <b key={item.h1}>
                  {item.h1}
                  <br />
                  {item.h2}
                  {item.h3}
                </b>
              );
            })}

          {appdetails && typeof appdetails !== 'undefined' && appdetails.length > 0 ? (
            appdetails.map(item => {
              return (
                <Link
                  key={item.override}
                  to={item.override}
                  rel={item.rel}
                  target="_blank"
                  className={`nbt_gadget_sprite icon_${item.hl}`}
                >
                  {item.hl}
                </Link>
              );
            })
          ) : (
            <Loader />
          )}
        </div>
      </div>

      <div className="headingsWithOtherSites">
        <div className="inside-links">
          <h4> {typeof category !== 'undefined' ? category : null} </h4>
          {sections && typeof sections !== 'undefined' && sections.length > 0 ? (
            sections.map(item => {
              return (
                <Link key={item.override} rel={item.rel} to={item.override} target="_blank">
                  {item.hl}
                </Link>
              );
            })
          ) : (
            <Loader />
          )}
        </div>

        <div className="other-sites">
          <h4> {typeof categorySites !== 'undefined' ? categorySites : null} </h4>
          {sites && typeof sites !== 'undefined' && Array.isArray(sites) && sites.length > 0 ? (
            sites.map(item => {
              return (
                <Link key={item.override} to={item.override} rel={item.rel} target="_blank">
                  {item.hl}
                </Link>
              );
            })
          ) : (
            <Loader />
          )}
        </div>
      </div>

      <div className="popluarCategories">
        <ul>
          <li className="wdth25">
            <h3>Top Stories</h3>
            {seoLinks &&
            typeof seoLinks !== 'undefined' &&
            Array.isArray(seoLinks) &&
            seoLinks.length > 0 ? (
              seoLinks.map(item => {
                return (
                    <Link key={item.override} to={item.override} rel={item.rel} target="_blank">
                    {item.hl}
                  </Link>
                );
              })
            ) : (
                <Loader />
            )}
          </li>
          <li className="wdth25">
            <h3>Top Categories</h3>
            {seoLinks1 &&
            typeof seoLinks1 !== 'undefined' &&
            Array.isArray(seoLinks1) &&
            seoLinks1.length > 0 ? (
              seoLinks1.map(item => {
                return (
                    <Link key={item.override} to={item.override} rel={item.rel} target="_blank">
                    {item.hl}
                  </Link>
                );
              })
            ) : (
                <Loader />
            )}
          </li>
          <li>
            <h3>Trending Stories</h3>
            {seoLinks2 &&
            typeof seoLinks2 !== 'undefined' &&
            Array.isArray(seoLinks2) &&
            seoLinks2.length > 0 ? (
              seoLinks2.map(item => {
                return (
                    <Link key={item.override} to={item.override} rel={item.rel} target="_blank">
                    {item.hl}
                  </Link>
                );
              })
            ) : (
                <Loader />
            )}
          </li>
        </ul>
        <ul>
          <li>
            <h3>मोबाइल फोन</h3>
            {seoLinks3 &&
            typeof seoLinks3 !== 'undefined' &&
            Array.isArray(seoLinks3) &&
            seoLinks3.length > 0 ? (
              seoLinks3.map(item => {
                return (
                    <Link key={item.override} to={item.override} rel={item.rel} target="_blank">
                    {item.hl}
                  </Link>
                );
              })
            ) : (
                <Loader />
            )}
          </li>
          <li>
            <h3>Popular Gadgets</h3>
            {seoLinks4 &&
            typeof seoLinks4 !== 'undefined' &&
            Array.isArray(seoLinks4) &&
            seoLinks4.length > 0 ? (
              seoLinks4.map(item => {
                return (
                    <Link key={item.override} to={item.override} rel={item.rel} target="_blank">
                    {item.hl}
                  </Link>
                );
              })
            ) : (
                <Loader />
            )}
          </li>
          <li>
            <h3>Latest Mobiles</h3>
            {seoLinks5 &&
            typeof seoLinks5 !== 'undefined' &&
            Array.isArray(seoLinks5) &&
            seoLinks5.length > 0 ? (
              seoLinks5.map(item => {
                return (
                    <Link key={item.override} to={item.override} rel={item.rel} target="_blank">
                    {item.hl}
                  </Link>
                );
              })
            ) : (
                <Loader />
            )}
          </li>
        </ul>
      </div>

      <div className="con-copy-right">
        <div className="times-links">
          {aboutUS && typeof aboutUS !== 'undefined' && aboutUS.length > 0 ? (
            aboutUS.map(item => {
              return (
                <Link key={item.override} to={item.override} rel={item.rel} target="_blank">
                  {item.hl}
                </Link>
              );
            })
          ) : (
            <Loader />
          )}
        </div>
        {otherinfo && (
          <div className="other-info">
            {otherinfo.copyright}
            <Link to={otherinfo.tss.override}> {otherinfo.tss.hl}</Link> <br />
            {otherinfo.sr}
          </div>
        )}
      </div>
    </footer>
  );
});

footer.displayName = 'footer';

footer.propTypes = {
  data: proptypes.object,
};

export default footer;
