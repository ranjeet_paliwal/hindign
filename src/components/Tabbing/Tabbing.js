import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Slider from '../NewSlider/NewSlider';
import CommonLoader from '../Loaders/CommonLoader';
import Link from '../Link/Link';

const Config = require('../../../common/nbt');

class Tabbing extends Component {
  state = {
    LTData: [],
    cat: '',
    subcat: '',
    loading: true,
  };

  componentDidMount() {
    this.GetLatestData('mobile');
  }

  GetLatestData = mb => {
    this.setState({ loading: true });
    const api1 = `${
      process.env.API_ENDPOINT
    }/webgn_recentlatest.cms?tag=recentreviewed&category=${mb}&perpage=10&limit=49&hostid=${
      Config.SiteInfo.hostid
    }&feedtype=sjson `;
    fetch(api1)
      .then(response => response.json())
      .then(data => {
        this.setState({
          LTData: data,
          cat: mb,
          subcat: 'latest',
          loading: false,
        });
      });
  };

  GetTrendingData = mb => {
    this.setState({ loading: true });
    const api1 = `${
      process.env.API_ENDPOINT
    }/webgn_recentlatest.cms?tag=trending&category=${mb}&perpage=10&limit=49&hostid=${
      Config.SiteInfo.hostid
    }&feedtype=sjson `;
    fetch(api1)
      .then(response => response.json())
      .then(data => {
        this.setState({
          LTData: data,
          cat: mb,
          subcat: 'trending',
          loading: false,
        });
      });
  };

  render() {
    const { LTData, loading } = this.state;
    const { navData } = this.props;
    let LoaderImg;
    if (loading) {
      LoaderImg = <CommonLoader />;
    }
    const LTsecname = LTData && LTData.newsItem && LTData.newsItem.secname;
    const LTseo = LTData && LTData.newsItem && LTData.newsItem.categoryseo;
    const LTMoreUrl = `${process.env.WEBSITE_URL}/${LTseo}/filters/sort=${LTsecname}`;
    const isValidTrendingData =
      (LTData && LTData.newsItem && LTData.newsItem.items && LTData.newsItem.items.length > 0) ||
      null;
    return (
      <React.Fragment>
        <div>
          <h2>
            <Link to={Config.seoConfig.gadgetlistUrl}>{Config.Locale.gadgets}</Link>
          </h2>
          <ul className="scroll">
            {navData && navData.value && navData.value.navData && navData.value.navData.gadgets
              ? navData.value.navData.gadgets.map(value => {
                  return (
                    <li
                      onClick={this.GetLatestData.bind(this, value.secname)}
                      key={`GadName${value.secname}`}
                      className={
                        this.state.cat === value.secname
                          ? `active gdt-${value.secname}`
                          : `gdt-${value.secname}`
                      }
                    >
                      <b />
                      <label> {value.hl}</label>
                    </li>
                  );
                })
              : ''}
          </ul>
        </div>

        <ul className="gn-tabs">
          <li
            onClick={this.GetLatestData.bind(this, this.state.cat)}
            className={this.state.subcat === 'latest' ? 'active' : ''}
          >
            {Config.Locale.latest}
          </li>
          <li
            onClick={this.GetTrendingData.bind(this, this.state.cat)}
            className={this.state.subcat === 'trending' ? 'active' : ''}
          >
            {Config.Locale.trending}
          </li>
        </ul>
        <div className="inline-gadgets-content">
          {LoaderImg}
          {isValidTrendingData ? (
            <Slider
              type="trending"
              size="3"
              sliderData={
                LTData && LTData.newsItem && LTData.newsItem.items ? LTData.newsItem.items : ''
              }
              width="200"
              height="280"
              SliderClass="trendingslider"
              islinkable="true"
              sliderWidth="630"
              categoryseo={
                LTData && LTData.newsItem && LTData.newsItem.categoryseo
                  ? LTData.newsItem.categoryseo
                  : ''
              }
            />
          ) : (
            ''
          )}
        </div>
        <Link className="more-btn" to={LTMoreUrl}>
          {Config.Locale.seemoregadgets}
        </Link>
      </React.Fragment>
    );
  }
}

Tabbing.propTypes = {
  value: PropTypes.array,
  navData: PropTypes.array,
};

function mapStateToProps(state) {
  return {
    ...state.news,
    navData: state.nav,
  };
}

export default connect(mapStateToProps)(Tabbing);
