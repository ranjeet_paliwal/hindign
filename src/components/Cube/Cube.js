/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react';
import { initEvent, getCookie, setCookie } from '../../common-utility';

class Cube extends PureComponent {
  constructor(props) {
    super(props);
    const { cubeConfig } = props;
    this.state = {
      cubeStatus: false,
    };

    const pageList = this.getPageList(cubeConfig);

    if (typeof window !== 'undefined') {
      initEvent('scroll', () => {
        if (typeof getCookie === 'function' && !getCookie('cubeVisible')) {
          if (window.pageYOffset >= 500) {
            const isHome =
              (window.location &&
                (window.location.pathname === '/' || window.location.pathname === 'default.cms')) ||
              false;
            if (
              isHome &&
              pageList &&
              (pageList.indexOf('/') !== -1 || pageList === 'all') &&
              Cube.getDisplayStatus(cubeConfig)
            ) {
              // cubeDisplayStatus = true;
              this.setState({ cubeStatus: true });
            }
          }
        }
      });
    }
  }

  componentDidMount() {
    const { cubeConfig } = this.props;

    const pageList = this.getPageList(cubeConfig);

    const isHome =
      (window.location &&
        (window.location.pathname === '/' || window.location.pathname === 'default.cms')) ||
      false;
    if (typeof getCookie === 'function' && !getCookie('geoInfo')) {
      Cube.loadGeoScript(data => {
        // console.log('ddd', data);
        if (data) {
          const geoInfo = {
            country: data.CountryCode,
            region: data.region_code,
            city: data.city,
          };
          setCookie('geoInfo', JSON.stringify(geoInfo), 365);
        }
        if (
          !isHome &&
          (pageList.indexOf('/') !== -1 || pageList === 'all') &&
          Cube.getDisplayStatus(cubeConfig)
        ) {
          // cubeDisplayStatus = true;
          this.setState({ cubeStatus: true });
        }
      });
    }

    if (
      !isHome &&
      (pageList.indexOf('/') !== -1 || pageList === 'all') &&
      Cube.getDisplayStatus(cubeConfig)
    ) {
      // cubeDisplayStatus = true;
      this.setState({ cubeStatus: true });
    }

    // this.processCubeRender(pageList, location);
  }

  componentDidUpdate(prevProps) {
    // alert('dddddddd');

    const { props } = this;
    const { cubeConfig } = props;

    // const pageList =
    //   cubeConfig.pages.indexOf(',') !== -1 ? cubeConfig.pages.split(',') : cubeConfig.pages;

    if (props.location !== prevProps.location) {
      this.setState({ cubeStatus: false });
    }

    // this.processCubeRender(pageList, location);

    const pageList = this.getPageList(cubeConfig);

    const isHome =
      (window.location &&
        (window.location.pathname === '/' || window.location.pathname === 'default.cms')) ||
      false;
    if (
      !isHome &&
      (pageList.indexOf('/') !== -1 || pageList === 'all') &&
      Cube.getDisplayStatus(cubeConfig)
    ) {
      // cubeDisplayStatus = true;
      document.querySelector('#cubeFrm iframe').setAttribute('src', cubeConfig.path);
      this.setState({ cubeStatus: true });
    }

    // this.processCubeRender(pageList, location);
  }

  getPageList = cubeConfig => {
    return cubeConfig && cubeConfig.pages && cubeConfig.pages.indexOf(',') !== -1
      ? cubeConfig.pages.split(',')
      : cubeConfig.pages;
  };

  // processCubeRender(pageList, location) {
  //   const isHome =
  //     (location && (location.pathname === '/' || location.pathname === 'default.cms')) || false;
  //   if (Array.isArray(pageList)) {
  //     for (let i = 0; i < pageList.length; i++) {
  //       const item = pageList[i].trim();
  //       if (location.pathname.indexOf(item) !== -1 && item !== '/') {
  //         this.setState({ cubeStatus: true });
  //         break;
  //       }
  //     }
  //   } else if ((pageList === 'all' || pageList === 'ros') && !isHome) {
  //     // has single keyword: all, ros
  //     this.setState({ cubeStatus: true });
  //   }
  // }

  render() {
    const { cubeConfig } = this.props;
    const { cubeStatus } = this.state;

    return typeof getCookie === 'function' && !getCookie('cubeVisible') && cubeStatus ? (
      <div id="cubeFrm" style={cubeConfig.styles}>
        <iframe
          width={cubeConfig.width}
          height={cubeConfig.height}
          src={`${cubeConfig.path}`}
          title="cube"
          marginWidth="0"
          marginHeight="0"
          hspace="0"
          vspace="0"
          frameBorder="0"
          scrolling="no"
          data-cookie-expiretime={cubeConfig.cookie}
        />
      </div>
    ) : (
      ''
    );
  }
}

Cube.checkPageStatus = function checkPageStatus(pageList) {
  const isHome =
    (window.location &&
      (window.location.pathname === '/' || window.location.pathname === 'default.cms')) ||
    false;
  if (isHome && (pageList.indexOf('/') !== -1 || pageList === 'all')) {
    return true;
  }
  if (Array.isArray(pageList)) {
    for (let i = 0; i < pageList.length; i++) {
      const item = pageList[i].trim();
      if (window.location.pathname.indexOf(`/${pageList}/`) !== -1 && item !== '/') {
        // $("#cubeFrm").show();
        return true;
        // break;
      }
    }
    return false;
  }
  //
  if (
    (pageList === 'all' ||
      pageList === 'ros' ||
      window.location.pathname.indexOf(`/${pageList}/`) !== -1) &&
    !isHome
  ) {
    // has single keyword: all, ros
    // $("#cubeFrm").show();
    return true;
  }

  return false;
};

Cube.loadGeoScript = function loadGeoScript(callback) {
  const script = document.createElement('script');
  script.type = 'text/javascript';
  if (script.readyState) {
    // IE
    script.onreadystatechange = () => {
      if (script.readyState === 'loaded' || script.readyState === 'complete') {
        script.onreadystatechange = null;
        if (typeof window !== 'undefined' && window.geoinfo) {
          callback(window.geoinfo);
        }
      }
    };
  } else {
    // Others
    script.onload = () => {
      if (typeof window !== 'undefined' && window.geoinfo) {
        callback(window.geoinfo);
      }
    };
  }
  script.async = false;
  script.src = 'https://geoapi.indiatimes.com/?cb=1';
  document.getElementsByTagName('head')[0].appendChild(script);
};

Cube.checkGeoStatus = function checkGeoStatus(config) {
  let geoInfo = getCookie('geoInfo');
  geoInfo = JSON.parse(geoInfo);
  if (config.country === 'all' || config.country.indexOf(geoInfo.country) !== -1) {
    return true;
  }
  if (config.state.indexOf(geoInfo.region) !== -1) {
    return true;
  }
  if (config.city.indexOf(geoInfo.city) !== -1) {
    return true;
  }
  return false;
};

Cube.getDisplayStatus = function getDisplayStatus(config) {
  if (typeof getCookie === 'function' && getCookie('cubeVisible')) {
    // if cube is cosed
    return false;
  }

  if (config && config.status === 'true') {
    const pageList = config.pages.indexOf(',') !== -1 ? config.pages.split(',') : config.pages;

    if (Cube.checkGeoStatus(config)) {
      // returns true|false
      if (Cube.checkPageStatus(pageList)) {
        return true;
      }
      return false;
    }
    return false;
  }

  return false;
};

export default Cube;
