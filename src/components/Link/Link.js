/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/mouse-events-have-key-events */

import React from 'react';
import { Link as LinkNew } from 'react-router';

const handleLinks = url => {
  if (
    typeof url !== 'undefined' &&
    url &&
    //url === '/' ||
    //url === process.env.WEBSITE_URL ||
    //url.indexOf('/photolist/') !== -1 ||
    (url.indexOf('/tech/mobile-phones') !== -1 ||
      url.indexOf('/tech/laptops') !== -1 ||
      url.indexOf('/tech/cameras') !== -1 ||
      url.indexOf('/tech/tablets') !== -1 ||
      url.indexOf('/tech/smartwatch') !== -1 ||
      url.indexOf('/tech/fitnessband') !== -1 ||
      url.indexOf('/tech/compare') !== -1)
    // url.indexOf('/tech/') !== -1 ||
    // url.indexOf('/photoarticlelist/') !== -1 ||
    // url.indexOf('/videolist/') !== -1 ||
    // url.indexOf('/photoshow/') !== -1 ||
    // url.indexOf('/videoshow/') !== -1 ||
    // url.indexOf('/articlelist/') !== -1 ||
    // url.indexOf('reviews') !== -1 ||
    // url.indexOf('/articleshow/') !== -1
    // url.indexOf('photogallery.navbharattimes.indiatimes.com') === -1
  ) {
    return true;
  }
  return false;
};

const Link = props => {
  const { title, children, to, className, onMouseOver, clicked, rel, itemProp, target } = props;
  const alttitle = typeof title !== 'undefined' ? title : '';
  let newRel =
    to &&
    (to.includes(process.env.BASE_URL) ||
      !to.includes('http') ||
      !to.includes('https') ||
      to.includes('photogallery.navbharattimes.indiatimes.com'))
      ? rel
      : 'nofollow';

  //Replace multiple space with single space then convert space with -
  newRel =
    newRel &&
    newRel
      .replace(/ +/g, ' ')
      .replace(/ /g, '-')
      .toLowerCase();

  const parsedURL = to && to.includes('affiliate_amazon.cms') ? to : to && to.toLowerCase();
  return handleLinks(to) && target != '_blank' ? (
    <LinkNew
      className={className}
      onMouseOver={onMouseOver}
      onClick={clicked}
      title={alttitle}
      rel={newRel || ''}
      itemProp={itemProp}
      to={parsedURL}
    >
      {children}
    </LinkNew>
  ) : (
    <a
      href={parsedURL}
      target={target === 'notblank' ? '' : '_blank'}
      className={className}
      onMouseOver={onMouseOver}
      onClick={clicked}
      title={alttitle}
      rel={newRel || ''}
      itemProp={itemProp}
    >
      {children}
    </a>
  );
};

export default Link;
