import React from 'react';

const NextReviewLoader = props => (
  <div className="loading-next-story">
    <img src="https://static.langimg.com/photo/35958799.cms" />
  </div>
);

export default NextReviewLoader;
