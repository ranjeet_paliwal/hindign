import React from 'react';

const ArticleSHowFakeLoader = props => (
  <div className="fake-articleshow">
    <div className="breadcrumb">
      <span className="blur-div" />
    </div>
    <div className="contentarea">
      <div className="leftmain">
        <span className="blur-div" />

        <div className="top-story table fake-listview">
          <div className="table_row">
            <span className="table_col img_wrap">
              <div className="dummy-img" />
            </span>
            <span className="table_col con_wrap">
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
              <span className="blur-div" />
            </span>
          </div>
        </div>
        <span className="blur-div" />
        <span className="blur-div" />
        <span className="blur-div" />
        <span className="blur-div" />
        <span className="blur-div" />
        <span className="blur-div" />
      </div>
      <div className="rightnav">
        <div className="ads">
          <div className="dummy-img" />
        </div>
        <div className="video_list">
          <ul>
            <li>
              <div className="dummy-img" />
              <span className="blur-div" />
              <span className="blur-div" />
            </li>
            <li>
              <div className="dummy-img" />
              <span className="blur-div" />
              <span className="blur-div" />
            </li>
            <li>
              <div className="dummy-img" />
              <span className="blur-div" />
              <span className="blur-div" />
            </li>
            <li>
              <div className="dummy-img" />
              <span className="blur-div" />
              <span className="blur-div" />
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
);

export default ArticleSHowFakeLoader;
