import React from 'react';

const CommonLoader = props => (
  <div className="loader-in-center">
    <img src="https://static.langimg.com/photo/35958799.cms" />
  </div>
);

export default CommonLoader;
