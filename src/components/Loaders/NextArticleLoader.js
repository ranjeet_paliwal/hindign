import React from 'react';

const NextArticleLoader = props => (
  <div className="loading-next-story">
    <img src="https://static.langimg.com/photo/35958799.cms" />
  </div>
);

export default NextArticleLoader;
