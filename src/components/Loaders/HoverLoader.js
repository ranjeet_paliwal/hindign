import React from 'react';

const HoverLoader = props => (
  <div className="menu_content wdt_laoding">
    <span className="spinner_circle" />
  </div>
);

export default HoverLoader;
