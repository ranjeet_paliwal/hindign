import React, { PureComponent } from 'react';

import fetch from '../../utils/fetch/fetch';

class StatusCheck extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      status: '',
      isError: false,
    };
  }

  componentDidMount() {
    fetch(`${process.env.API_ENDPOINT}/webgn_dbstatus.cms?feedtype=sjson`)
      .then(response => {
        this.setState({ status: response.status });
      })
      .catch(e => this.setState({ isError: true }));
  }

  render() {
    const { status, isError } = this.state;
    if (isError) {
      return <div>Some error occurred while accessing api response</div>;
    }
    return <div>{status}</div>;
  }
}

export default StatusCheck;
