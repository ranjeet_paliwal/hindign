/* eslint-disable indent */
import React from 'react';
import propTypes from 'prop-types';

import Link from '../../../Link/Link';
import Thumb from '../../../Thumb/Thumb';
import './HP_widgets.scss';

import { truncateStr } from '../../../../common-utility';
import config from '../../../../reducers/config/config';

import CtnAd from '../../../CtnAd/CtnAd';

const Config = require('../../../../../common/nbt.js');

const newsWidget = React.memo(props => {
  const {
    type,
    newsContent,
    secname,
    secid,
    imgrequired,
    count,
    widgetClassName,
    syn,
    trimTo,
    trimToTitle,
    headingRequired,
    headingLinkable,
    widgetType,
  } = props;

  let secData = '';
  let updatedDATA = '';
  const widgetClass = widgetClassName || '';
  const className = imgrequired === 'all' ? 'all_image_view' : 'first_image_view';
  let width;
  let height;
  const category = Config.Locale.tech.category;

  switch (type) {
    case 'misc':
      if (newsContent && Array.isArray(newsContent)) {
        secData = newsContent.filter(data => {
          return data.id === secid;
        });
      }
      // console.log('secData', newsContent);

      if (widgetType === 'mostread') {
        var widgetslotId = 325702;
      } else {
        var widgetslotId = 323822;
      }
      updatedDATA =
        secData[0] && Array.isArray(secData[0].items) ? secData[0].items.slice(0, count) : null;
      return (
        <div className={`${className} ${widgetClass}`}>
          {headingRequired !== 'no' && secData[0] ? (
            <h2>
              {headingLinkable ? (
                <Link title={secData[0].secname} to={secData[0].wu}>
                  {secData[0].secname}
                </Link>
              ) : (
                <span>{secData[0].secname}</span>
              )}
            </h2>
          ) : null}
          <div className="wdt_content">
            <ul>
              {(imgrequired === 'all' || imgrequired === 'none') && updatedDATA
                ? updatedDATA.map((value, index) => {
                    width = secid === 'solrreview' ? '240' : '96';
                    height = secid === 'solrreview' ? '175' : '71';
                    const utmparam = `${
                      value.wu
                    }?utm_source=techmostreadwidget&utm_medium=referral`;
                    const finalLink =
                      secname === 'ibeatmostshared' || secname === 'ibeatmostread'
                        ? utmparam
                        : value.wu;
                    return (
                      <React.Fragment key={value.imageid}>
                        <li>
                          {imgrequired === 'all' ? (
                            <span className="img_container">
                              <Thumb
                                imgId={value.imageid}
                                imgSize={value.imgsize}
                                width={width}
                                height={height}
                                alt={value.hl}
                                title={value.hl}
                                islinkable="true"
                                link={finalLink}
                              />
                            </span>
                          ) : null}

                          <span className="txt_container">
                            {value.cr && secname === 'moviereview' ? (
                              <div className="name">
                                {secData[0] ? (
                                  <Link title={secData[0] && secData[0].secname} to={value.wu}>
                                    {secData[0] && secData[0].secname}
                                  </Link>
                                ) : (
                                  ''
                                )}
                              </div>
                            ) : null}
                            <Link title={value.hl} to={finalLink}>
                              {truncateStr(value.hl, trimToTitle)}
                            </Link>
                            {value.cr && secname === 'moviereview' ? (
                              <span className={`rating rtimg${value.cr.split('.').join('')}`}>
                                {value.cr}
                              </span>
                            ) : null}
                            {syn ? <span>{truncateStr(value.syn, trimTo)}</span> : null}
                          </span>
                        </li>

                        {(secname === 'ibeatmostread' || secname === 'ibeatmostshared') &&
                        (index === 2 || index === 6 || index === 10 || index === 14) ? (
                          <li>
                            <div className="ctnad">
                              <CtnAd
                                articleId={index}
                                position={index}
                                height="225"
                                width="300"
                                className="colombia"
                                slotId={widgetslotId}
                              />
                            </div>
                          </li>
                        ) : null}
                      </React.Fragment>
                    );
                  })
                : null}

              {imgrequired === 'first' && updatedDATA
                ? updatedDATA.map((value, index) => {
                    return (
                      <li className={index === 0 ? 'first' : ''} key={value.hl}>
                        <Link
                          title={value.hl}
                          to={value.wu}
                          className={value.isvdo ? 'video_icon' : null}
                        >
                          {value.hl}
                        </Link>
                        {index === 0 ? (
                          <React.Fragment>
                            <span className="img_container">
                              <Thumb
                                imgId={value.imageid}
                                imgSize={value.imgsize}
                                width="96"
                                height="71"
                                islinkable="true"
                                link={value.wu}
                                alt={value.hl}
                                title={value.hl}
                              />
                            </span>
                            <span className="txt_container">{value.syn}</span>
                          </React.Fragment>
                        ) : null}
                      </li>
                    );
                  })
                : null}
            </ul>
          </div>
        </div>
      );
    case 'trending':
      if (newsContent && Array.isArray(newsContent)) {
        secData = newsContent.filter(data => {
          return data.id === secid;
        });
      }
      // console.log('Trending Data1111', secData);
      return (
        <div className={`section trending-topics ${widgetClass}`}>
          <h2>
            <span>{secData[0].secname}</span>
          </h2>
          <ul>
            {secData &&
              secData[0] &&
              secData[0].items.map(item => {
                return (
                  <li key={item.hl}>
                    <Link to={`${item.wu}?utm_source=trendingwidget-hp`}>{item.hl}</Link>
                  </li>
                );
              })}
          </ul>
        </div>
      );
    case 'review':
      if (newsContent && Array.isArray(newsContent)) {
        secData = newsContent.filter(data => {
          return data.id === secid;
        });
      }
      updatedDATA =
        secData[0] && Array.isArray(secData[0].items) ? secData[0].items.slice(0, count) : null;
      const randNum = Math.floor(Math.random() * 100001);
      return (
        <div className={`${widgetClass}`}>
          {headingRequired !== 'no' && secData[0] ? (
            <h2>
              <Link title={secData[0].secname} to={secData[0].wu}>
                {secData[0].secname}
              </Link>
            </h2>
          ) : null}
          <ul>
            {updatedDATA &&
              updatedDATA.map((value, index) => {
                const rvwidth = index <= 1 ? '310' : '110';
                const rvheight = index <= 1 ? '232' : '83';
                const getCat = category.filter(Data => {
                  return Data.key === value.GadgetsCategory;
                });
                const categoryName = getCat && getCat[0] ? getCat[0].keyword : '';
                return (
                  <React.Fragment key={`review${value.id}`}>
                    <li>
                      <span className="img_container">
                        <Thumb
                          imgId={value.imageid}
                          imgSize={value.imgsize}
                          width={rvwidth}
                          height={rvheight}
                          islinkable="true"
                          link={value.wu}
                          alt={value.hl}
                          title={value.hl}
                        />
                      </span>
                      <span className="txt_container">
                        <Link title={value.hl} to={value.wu} className="text_ellipsis">
                          {value.hl}
                        </Link>
                        <span className="more_txt">
                          {value.GadgetsModelDisplayName ? (
                            <Link
                              to={`${process.env.WEBSITE_URL}/${categoryName}/${
                                value.GadgetsPrimaryGadget
                              }`}
                            >
                              {value.GadgetsModelDisplayName}
                            </Link>
                          ) : (
                            ''
                          )}

                          {value.GadgetsBrand ? (
                            <Link
                              to={`${process.env.WEBSITE_URL}/${categoryName}/${
                                value.GadgetsBrand
                              }`}
                            >
                              {value.GadgetsBrand}
                            </Link>
                          ) : (
                            ''
                          )}
                          {value.GadgetsCategory ? (
                            <Link to={`${process.env.WEBSITE_URL}/${categoryName}`}>
                              {value.GadgetsCategory}
                            </Link>
                          ) : (
                            ''
                          )}
                        </span>
                        {value.cr ? (
                          <span className="rating_txt">
                            {Config.Locale.tech.criticsrating}:{' '}
                            <b>
                              <small>{value.cr}</small>/5
                            </b>
                          </span>
                        ) : (
                          ''
                        )}
                      </span>
                    </li>
                    {index === 3 ? (
                      <li>
                        <div className="ctnad">
                          <CtnAd
                            articleId={value.id}
                            position={randNum}
                            height="225"
                            width="300"
                            className="colombia"
                            slotId="323826"
                          />
                        </div>
                      </li>
                    ) : null}
                  </React.Fragment>
                );
              })}
          </ul>
          <Link className="more-btn" to={secData && secData[0] && secData[0].wu}>
            {Config.Locale.readMoreReview}
          </Link>
        </div>
      );

    case 'TipsTricks':
      if (newsContent && Array.isArray(newsContent)) {
        secData = newsContent.filter(data => {
          return data.id === secid;
        });
      }
      updatedDATA =
        secData[0] && Array.isArray(secData[0].items) ? secData[0].items.slice(0, count) : null;
      return (
        <div className={`${widgetClass}`}>
          {headingRequired !== 'no' && secData[0] ? (
            <h2>
              <Link title={secData[0].secname} to={secData[0].wu}>
                {secData[0].secname}
              </Link>
            </h2>
          ) : null}
          <ul>
            {updatedDATA &&
              updatedDATA.map((value, index) => {
                const rvwidth = index < 1 ? '630' : '110';
                const rvheight = index < 1 ? '354' : '83';
                const resizeMde = index < 1 ? '75' : '4';
                return (
                  <React.Fragment key={`review${value.id}`}>
                    <li>
                      <span className="img_container">
                        <Thumb
                          imgId={value.imageid}
                          imgSize={value.imgsize}
                          width={rvwidth}
                          height={rvheight}
                          islinkable="true"
                          link={value.wu}
                          alt={value.hl}
                          title={value.hl}
                          resizeMode={resizeMde}
                        />
                      </span>
                      <span className="txt_container">
                        <Link title={value.hl} to={value.wu}>
                          {value.hl}
                        </Link>
                      </span>
                    </li>
                    {index === 2 ? (
                      <li>
                        <div className="ctnad">
                          <CtnAd
                            articleId={value.id}
                            position={value.id}
                            height="225"
                            width="300"
                            className="colombia"
                            slotId="323826"
                          />
                        </div>
                      </li>
                    ) : null}
                  </React.Fragment>
                );
              })}
          </ul>

          <Link className="more-btn" to={secData[0].wu}>
            {Config.Locale.seemore}
          </Link>
        </div>
      );

    default:
      return '';
  }
});

newsWidget.displayName = 'newsWidget';

newsWidget.propTypes = {
  newsContent: propTypes.array,
  type: propTypes.string,
  secname: propTypes.string,
  secid: propTypes.string,
  imgrequired: propTypes.string,
  count: propTypes.number,
  widgetClassName: propTypes.string,
  syn: propTypes.string,
  trimTo: propTypes.number,
  trimToTitle: propTypes.number,
  headingRequired: propTypes.string,
  widgetType: propTypes.string,
};

export default newsWidget;
