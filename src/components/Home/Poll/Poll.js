/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { PureComponent } from 'react';
import './PollCard.scss';
import { connect } from 'react-redux';
import { getCookie, setCookie } from '../../../common-utility';
import Link from '../../Link/Link';

require('es6-promise').polyfill();
require('isomorphic-fetch');

const Config = require(`../../../../common/${process.env.SITE}`);

class Poll extends PureComponent {
  state = {
    item: [],
    pollresult: [],
    mathq: '',
    matha: '',
    selected: '',
    msg: '',
    statusmsg: '',
  };

  constructor(props) {
    super(props);
    this.textInput = React.createRef();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.openLoginRegister = this.openLoginRegister.bind(this);
    // this.closeLoginRegister = this.closeLoginRegister.bind(this);
  }

  // eslint-disable-next-line react/sort-comp
  getpollHandler = () => {
    const { pollid } = this.props;
    const urlparam = pollid ? `msid=${pollid}&platform=web&feedtype=sjson` : 'feedtype=sjson';
    const apiURL = `${process.env.API_ENDPOINT}/pwagn_poll.cms?${urlparam}`;
    fetch(apiURL)
      .then(res => res.json())
      .then(result => {
        this.setState({
          item: result,
        });
      });
  };

  handleSubmit(event, pid) {
    const chkpoll = getCookie(`POLLID${pid}`);

    event.preventDefault();
    if (this.state.selected === '') {
      this.setState({
        msg: 'Please select one option',
      });
      return false;
    }

    if (this.textInput.current.value != this.state.matha) {
      event.preventDefault();
      this.setState({
        msg: 'Please answer simple math question',
      });
      return false;
    }

    event.preventDefault();

    // eslint-disable-next-line react/destructuring-assignment
    const userData = this.state.item ? this.state.item : null;
    const apiPath = `${
      process.env.API_ENDPOINT
    }/feeds/pollsubmission.cms?PRadio=1&txtPolliD=${pid}&clname=VSNL&feedtype=sjson&${Math.random()}`;
    fetch(apiPath, {
      method: 'POST',
      body: JSON.stringify(userData),
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    }).then(response => {
      response.json().then(data => {
        const newState = { pollresult: data };
        if (chkpoll) {
          newState.statusmsg = Config.Locale.poll.alreadyvotemsg;
        } else {
          // eslint-disable-next-line no-unused-expressions
          newState.statusmsg =
            data.polllist[0].status === 'successfully' ? Config.Locale.poll.sucessmsg : '';
          setCookie(`POLLID${data.polllist[0].id}`, data.polllist[0].id, 1, '/', '', true);
        }
        this.setState(newState);
      });
    });
  }

  openLoginRegister() {
    this.props.openLoginRegister();
  }

  // closeLoginRegister() {
  //   this.props.closeLoginRegister();
  // }

  putMathQ() {
    const f = Math.floor(Math.random() * 10);
    const s = Math.floor(Math.random() * 6);
    const o = Math.floor(Math.random() * 2);
    let str = '';
    // eslint-disable-next-line prefer-template
    str = f + ' + ' + s;

    this.setState({
      matha: eval(str),
      mathq: str,
    });
  }

  // eslint-disable-next-line react/sort-comp
  componentDidMount() {
    this.getpollHandler();
    this.putMathQ(2);
  }

  render() {
    const { showLoginRegister, loggedIn } = this.props;
    const pollData = this.state;
    // eslint-disable-next-line react/destructuring-assignment
    const pollResult = this.state.pollresult;

    return (
      <div className="section poll_card">
        <h2>
          <Link to={Config.Locale.poll.pollLink}>{Config.Locale.poll.ptext}</Link>
        </h2>
        <div
          className="poll_content"
          style={pollResult.polllist ? { display: 'none' } : { display: 'block' }}
        >
          <div className="ques">{pollData && pollData.item ? pollData.item.polltext : ''}</div>
          <form
            onSubmit={event =>
              this.handleSubmit(event, pollData && pollData.item ? pollData.item.pollid : '')
            }
          >
            <ul>
              {pollData && pollData.item && Array.isArray(pollData.item.polloption)
                ? pollData.item.polloption.map(value => {
                    return (
                      <li key={value.optionno}>
                        <React.Fragment key={value}>
                          <label htmlFor={value}>
                            <input
                              type="radio"
                              id="poll"
                              name="pollop"
                              value={value.optiontext}
                              onChange={e => this.setState({ selected: e.target.value })}
                            />
                            <span>{value.optiontext}</span>
                          </label>
                        </React.Fragment>
                      </li>
                    );
                  })
                : null}
            </ul>
            <div className="caption">{Config.Locale.poll.qtext}</div>
            <div className="qna">
              <span>{pollData ? pollData.mathq : ''}</span>
              <label htmlFor="username" style={{ display: 'none' }}>
                Username
              </label>
              <input type="text" name="username" ref={this.textInput} />
              <input
                type="text"
                className="visibility-hide"
                value={pollData ? pollData.matha : ''}
              />
              <input type="submit" value={Config.Locale.poll.votetxt} className="btn-blk" />
            </div>
            <div className="error-msg">{pollData.msg ? pollData.msg : null}</div>
          </form>
        </div>
        {pollResult.polllist ? (
          <div className="poll_result">
            <span>
              <span className="confirm_msg">{pollData.statusmsg ? pollData.statusmsg : ''}</span>
              {loggedIn === false ? (
                <span className="login-before-result">
                  {Config.Locale.poll.msg_nonlogin}
                  <button type="button" onClick={showLoginRegister} className="btn-login">
                    LOGIN
                  </button>
                </span>
              ) : (
                <div className="result_value">
                  {pollResult.polllist[0] && Array.isArray(pollResult.polllist[0].options)
                    ? pollResult.polllist[0].options.map(value => {
                        return (
                          <React.Fragment key={value}>
                            <label htmlFor={value}>
                              {value.optiondesc}
                              <b> - {value.perc}</b>
                            </label>
                            <div className="progress progress-top">
                              <span
                                className={`progress progress-child option_${value.optionno}`}
                                style={{ width: `${value.perc}%` }}
                              />
                            </div>
                          </React.Fragment>
                        );
                      })
                    : null}
                </div>
              )}
            </span>
          </div>
        ) : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.authentication,
    config: state.config,
  };
}
export default connect(mapStateToProps)(Poll);
