/* eslint-disable react/destructuring-assignment */
/* eslint-disable object-shorthand */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable lines-between-class-members */
/* eslint-disable class-methods-use-this */
import React, { PureComponent } from 'react';
import Input from '../../UI/Input/Input';
import Button from '../../UI/Button/Button';
import './NewsLetter.scss';

const Config = require(`../../../../common/${process.env.SITE}`);
const signs = Config.ZodiacSigns.map(item => {
  return { value: item.name, displayValue: item.regName };
});

class NewsLetter extends PureComponent {
  state = {
    orderForm: {
      morn: {
        elementType: 'input',
        elementConfig: {
          type: 'checkbox',
          value: 1,
        },
        value: '',
        checked: false,
        validation: {
          required: true,
        },
        label: 'सुबह',
        valid: false,
        ClassName: 'option-mor',
        // touched: false,
      },
      eve: {
        elementType: 'input',
        elementConfig: {
          type: 'checkbox',
          value: 3,
        },
        value: '',
        validation: {
          required: true,
        },
        valid: false,
        checked: false,
        label: 'शाम',
        ClassName: 'option-eve',
        // touched: false,
      },
      signs: {
        elementType: 'select',
        elementConfig: {
          options: signs,
        },
        value: '',
        validation: {},
        valid: true,
        ClassName: 'select-sign',
      },
      email: {
        elementType: 'input',
        elementConfig: {
          type: 'text',
          placeholder: 'Your E-Mail',
        },
        value: '',
        validation: {
          required: true,
          isEmail: true,
        },
        valid: false,
        ClassName: 'enter-email',
        // touched: false,
      },
    },
    formIsValid: true,
    error: '',
  };

  submitEventHandler = event => {
    event.preventDefault();

    const isvalid = this.checkValidity();

    if (isvalid) {
      alert('Great!!!');
      const orderForm = this.state.orderForm;
      const Email = orderForm.email.value.trim();

      const frequency =
        orderForm.morn.value && orderForm.eve.value
          ? `${orderForm.morn.value}|${orderForm.eve.value}`
          : orderForm.morn.value || orderForm.eve.value;
      const sign = orderForm.signs.value;
    }
  };

  inputChangedHandler = (event, inputIdentifier) => {
    const updatedOrderForm = {
      ...this.state.orderForm,
    };
    const updatedFormElement = {
      ...updatedOrderForm[inputIdentifier],
    };
    if (typeof updatedFormElement.checked === 'boolean') {
      updatedFormElement.checked = !updatedFormElement.checked;
      updatedFormElement.value = updatedFormElement.checked ? event.target.value : '';
    } else {
      updatedFormElement.value = event.target.value;
    }

    // updatedFormElement.value = event.target.value;
    /*  updatedFormElement.valid = this.checkValidity(
      updatedFormElement.value,
      updatedFormElement.validation,
    ); */
    // updatedFormElement.touched = true;
    updatedOrderForm[inputIdentifier] = updatedFormElement;
    // const value = target.type === 'checkbox' ? target.checked : target.value;
    // let formIsValid = true;

    this.setState({ orderForm: updatedOrderForm });
  };

  checkValidity() {
    let isValid = true;
    let error = '';

    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    const formElements = this.state;

    const Email = formElements.orderForm.email.value.trim();
    if (!formElements.orderForm.morn.value && !formElements.orderForm.eve.value) {
      isValid = false;
      error = 'email frequency should be checked';
    } else if (!Email) {
      isValid = false;
      error = 'email should not be blank';
    } else if (!pattern.test(Email)) {
      isValid = false;
      error = 'In-valid email';
    }
    if (isValid) {
      return true;
    }
    this.setState({ formIsValid: isValid, error: error });
    /*  if (rules.isEmail) {
      const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      isValid = pattern.test(value) && isValid;
    } */

    return isValid;
  }
  render() {
    const formElementsArray = [];
    for (const key in this.state.orderForm) {
      formElementsArray.push({
        id: key,
        config: this.state.orderForm[key],
      });
    }

    let form = (
      <form onSubmit={this.submitEventHandler}>
        {formElementsArray.map(formElement => (
          <Input
            key={formElement.id}
            elementType={formElement.config.elementType}
            elementConfig={formElement.config.elementConfig}
            value={formElement.config.value}
            label={formElement.config.label}
            ClassName={formElement.config.ClassName}
            // shouldValidate={formElement.config.validation}
            // touched={formElement.config.touched}
            changed={event => this.inputChangedHandler(event, formElement.id)}
          />
        ))}
        <Button btnType="Success">जॉइन करें</Button>
      </form>
    );
    if (this.state.loading) {
      form = 'loading...';
    }
    return (
      <div className="section subscribe_newsletter">
        <h2>
          सबस्क्राइब <strong>न्यूज़लेटर</strong>
        </h2>
        {!this.state.formIsValid ? <div className="error-msg">{this.state.error}</div> : null}
        {form}
      </div>
    );
  }
}

export default NewsLetter;
