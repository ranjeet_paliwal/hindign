import React from 'react';
import proptypes from 'prop-types';
import Link from '../../Link/Link';
import Loader from '../../lib/loader/Loader';
import Thumb from '../../Thumb/Thumb';
import './HP_top_content.scss';

import CtnAd from '../../CtnAd/CtnAd';

const Config = require('../../../../common/nbt');

const Headlines = React.memo(props => {
  const { data, position, ClassName, secname } = props;
  if (position === 'top') {
    return (
      <div className="hp_top_story">
        <div className={ClassName}>
          {/* <h2>{Config.Locale.technews}</h2> */}
          <div className={ClassName}>
            <ul>
              {data && Array.isArray(data) ? (
                data.map((val, index) => {
                  // const width = index === 0 ? '600' : '180';
                  // const height = index === 0 ? '400' : '150';
                  return index === 0 ? (
                    <li className="lead_post">
                      <span className="img_wrap">
                        <Thumb
                          imgId={val.imageid}
                          imgSize={val.imgsize}
                          width="630"
                          height="354"
                          alt={val.hl}
                          title={val.hl}
                          islinkable="true"
                          link={val.wu}
                          resizeMode="75"
                        />
                      </span>
                      <span className="con_wrap">
                        <Link to={val.wu} title={val.hl}>
                          {val.hl}
                        </Link>
                      </span>
                    </li>
                  ) : null;
                })
              ) : (
                <Loader />
              )}
              {data && Array.isArray(data) ? (
                data.map((val, index) => {
                  const random = Math.round(Math.random() * 10000);
                  if (val.type === 'ctn' && (index > 0 && index < 10)) {
                    return (
                      <li>
                        <div className="ctnad">
                          <CtnAd
                            articleId={index}
                            position={random}
                            height="225"
                            width="300"
                            className="colombia"
                            slotId={Config.CTN.id_homepage_headlines_CTN}
                          />
                        </div>
                      </li>
                    );
                  }
                  return index > 0 && index < 10 ? (
                    <React.Fragment key={val.id}>
                      <li>
                        <span className="imag_wrap">
                          <Thumb
                            imgId={val.imageid}
                            imgSize={val.imgsize}
                            width="200"
                            height="112"
                            alt={val.hl}
                            title={val.hl}
                            islinkable="true"
                            link={val.wu}
                            resizeMode="4"
                          />
                        </span>
                        <span className="con_wrap">
                          <Link to={val.wu} title={val.hl}>
                            {val.hl}
                          </Link>
                        </span>
                      </li>
                    </React.Fragment>
                  ) : null;
                })
              ) : (
                <Loader />
              )}
            </ul>

            <Link to={Config.Locale.Headline.headlinelink} className="more-btn">
              {Config.Locale.Headline.headlinetext}
            </Link>
          </div>
        </div>
      </div>
    );
  }
  if (position === 'right') {
    return (
      <div className="section all_image_view">
        <ul>
          {data && Array.isArray(data) ? (
            data.map((val, index) => {
              if (val.type === 'ctn' && (index > 9 && index < 18)) {
                return (
                  <li>
                    <div className="ctnad">
                      <CtnAd
                        articleId={index}
                        position={index}
                        height="225"
                        width="300"
                        className="colombia"
                        slotId={Config.CTN.id_homepage_headlines_CTN}
                      />
                    </div>
                  </li>
                );
              }
              return index > 9 && index < 18 ? (
                <li key={val.id}>
                  <span className="img_container">
                    <Thumb
                      imgId={val.imageid}
                      imgSize={val.imgsize}
                      width="96"
                      height="72"
                      alt={val.hl}
                      title={val.hl}
                      islinkable="true"
                      link={val.wu}
                    />
                  </span>
                  <span className="txt_container">
                    <Link to={val.wu} title={val.hl}>
                      {val.hl}
                    </Link>
                  </span>
                </li>
              ) : null;
            })
          ) : (
            <Loader />
          )}
        </ul>
      </div>
    );
  }
});

Headlines.displayName = 'Headlines';

Headlines.propTypes = {
  data: proptypes.array,
  position: proptypes.string,
  ClassName: proptypes.string,
};

export default Headlines;
