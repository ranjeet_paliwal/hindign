import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Thumb from '../../Thumb/Thumb';
import { analyticsGA, AnalyticsComscore } from '../../../analytics';

class MinitvPlayer extends PureComponent {
  state = {
    slikeid: '',
    seolocation: '',
  };

  constructor(props) {
    super(props);
    const { slikeid, seolocation } = this.props;
    this.state = { slikeid, seolocation };
    this.Playvideo = this.Playvideo.bind(this);
  }

  Playvideo = vid => {
    const { vidtitle } = this.props;
    const self = this;
    window.SPL = window.SPL || {};
    window.SPL.config = {
      sdk: {
        apikey: 'nbtweba5ec97054033e061',
      },
    };
    const jsWebUrl =
      typeof window.location !== 'undefined'
        ? `${window.location.protocol}//${window.location.host}`
        : '';
    // eslint-disable-next-line prefer-const
    let config = {
      page: {
        origin: jsWebUrl,
      },
      player: {
        skipAd: false,
        adSection: 'live-tv-mini',
        autoplay: false,
        startFromSec: -1,
        showQuality: true,
      },
      controls: {
        controlsType: 'custom',
        showFullScreen: true,
        showAutoPlayNext: true,
        showShare: true,
        volume: 100,
        mute: false,
        autoPlayNext: true,
        showVolumeSeekBar: true,
      },
      video: {
        id: vid,
        image: '',
        title: vidtitle,
      },
      events: {
        onAdEvent(evtname, data) {},
        onPlayerEvent(evtname) {
          const evtAction = `user-initiated/${self.state.seolocation}`;
          const evtLabel = 'videoshome/flash/ADAPTIVE';
          if (evtname == 'onPlayStart') {
            setTimeout(() => {
              AnalyticsComscore.invokeComScore();
              analyticsGA.event('VIDEOVIEW', evtAction, evtLabel);
            }, 2000);
          }
        },
      },
    };
    function retry() {
      const retryInterval = 100;
      if (typeof window.S !== 'undefined' && typeof window.S.load === 'function') {
        window.S.load(`playerContainer${vid}`, config, inst => {
          player = inst;
        });
      } else {
        setTimeout(() => {
          retry();
        }, retryInterval);
      }
    }
    retry();
  };

  render() {
    const { slikeid } = this.state;
    const { imageid, imgsize, height, width } = this.props;
    return (
      <React.Fragment>
        <div className="posrel_mini" style={{ height: `${height}px`, width: `${width}px` }}>
          <div id={`outerPlayer${slikeid}`} className="outerPlayer">
            <div id={`playerContainer${slikeid}`} className="playerContainer">
              <Thumb
                imgId={imageid}
                width={width}
                height={height}
                alt={imageid}
                imgSize={imgsize}
              />
              <div className="videoicons" onClick={() => this.Playvideo(slikeid)} />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

MinitvPlayer.propTypes = {
  imageid: PropTypes.string,
  imgsize: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
};

export default MinitvPlayer;
