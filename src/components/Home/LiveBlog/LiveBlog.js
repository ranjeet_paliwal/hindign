/* eslint-disable react/prop-types */
/* eslint-disable indent */
import React, { PureComponent } from 'react';
import './LiveBlog.scss';
import Link from '../../Link/Link';

const Config = require('../../../../common/nbt');

require('es6-promise').polyfill();
require('isomorphic-fetch');

class LiveBlog extends PureComponent {
  state = {
    lbData: [],
  };

  // eslint-disable-next-line react/sort-comp
  getliveblogHandler = (type, dataSource) => {
    let apilb = Config.API.liveBlogIndia;
    if (type === 'liveblog') {
      apilb = dataSource;
    }
    fetch(apilb)
      .then(res => res.json())
      .then(result => {
        this.setState({
          lbData: result,
        });
      });
  };

  componentDidMount() {
    const { type, dataSource } = this.props;
    this.getliveblogHandler(type, dataSource);
    setInterval(() => {
      this.getliveblogHandler(type, dataSource);
    }, 30000); // 3 sec
  }

  render() {
    const { cssClass } = this.props;
    const { lbData } = this.state;
    const updatedItems = lbData.lbcontent;
    const lbUrl = (lbData && lbData.lbcontainer && lbData.lbcontainer.wu) || '';

    return updatedItems && Array.isArray(updatedItems) && updatedItems.length > 0 ? (
      <React.Fragment>
        <div className={`section ${cssClass}`}>
          <h3>
            <Link target="_blank" id="headerlink" to={lbUrl}>
              <span>{Config.Locale.liveupdate}</span>
            </Link>
          </h3>
          <ul>
            {updatedItems.map(value => {
              return (
                <li key={value.title}>
                  <span className="time">{value.shortDateTime}</span>
                  {value.hoverridLink && value.hoverridLink !== 'null' ? (
                    <Link to={value.hoverridLink}>{value.title}</Link>
                  ) : (
                    <span>{value.title}</span>
                  )}
                </li>
              );
            })}
          </ul>
          <div className="moreblogs">
            <Link target="_blank" id="morelink" to={lbUrl}>
              More Breaking News >>
            </Link>
          </div>
        </div>
      </React.Fragment>
    ) : (
      ''
    );
  }
}

export default LiveBlog;
