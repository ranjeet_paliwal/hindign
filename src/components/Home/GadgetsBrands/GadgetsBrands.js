import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Slider from '../../NewSlider/NewSlider';
import CommonLoader from '../../Loaders/CommonLoader';

const Config = require(`../../../../common/${process.env.SITE}`);

require('es6-promise').polyfill();
require('isomorphic-fetch');

class GadgetsBrand extends Component {
  state = {
    Branddata: [],
    Gadgetcat: '',
    loading: true,
  };

  componentDidMount() {
    this.GetBrandData('mobile');
  }

  GetBrandData = mb => {
    this.setState({ loading: true });
    const api1 = `${process.env.API_ENDPOINT}/pwagn_brandlist.cms?feedtype=sjson&category=${mb} `;
    fetch(api1)
      .then(response => response.json())
      .then(data => {
        this.setState({
          Branddata: data,
          Gadgetcat: mb,
          loading: false,
        });
      });
  };

  render() {
    const { data } = this.props;
    const { Branddata, Gadgetcat, loading } = this.state;
    let LoaderImg;
    if (loading) {
      LoaderImg = <CommonLoader />;
    }
    // console.log('brandlist', data);
    return (
      <React.Fragment>
        <h2>
          <a target="_blank" title="" rel="">
            ब्रैंड्स
          </a>
        </h2>

        <ul className="tabs">
          {data
            ? data.map(value => {
                return (
                  <li
                    onClick={this.GetBrandData.bind(this, value.secname)}
                    className={Gadgetcat === value.secname ? 'active' : ''}
                  >
                    {value.hl}
                  </li>
                );
              })
            : ''}
        </ul>
        <div className="brands_content">
          {LoaderImg}
          {Branddata && Branddata.brand ? (
            <Slider
              type="gnbrand"
              size="5"
              sliderData={Branddata.brand}
              category={Branddata.category}
              categoryseo={Branddata.categoryseo}
              width="150"
              height="100"
              SliderClass="brandslider"
              islinkable="true"
              sliderWidth="960"
            />
          ) : (
            ''
          )}
        </div>
      </React.Fragment>
    );
  }
}
GadgetsBrand.propTypes = {
  value: PropTypes.array,
  data: PropTypes.array,
  children: PropTypes.node,
  dispatch: PropTypes.any,
  params: PropTypes.any,
};

// GadgetsBrand.fetchData = function({ dispatch, params }) {
//   return dispatch(fetchDataIfNeeded(params));
// };

// function mapStateToProps(state) {
//   return {
//     ...state.astrodata,
//     config: state.config,
//   };
// }

export default GadgetsBrand;
