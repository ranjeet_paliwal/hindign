/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable indent */
import React from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';

const trendingTopics = React.memo(props => {
  const { data, secName } = props;
  /* {typeof secName !== 'undefined' ? <h2>{secName}</h2> : null} */

  return data && Array.isArray(data) ? (
    <div className="section trending-topics">
      <h2>
        <a href="javascript:void(0)">{secName}</a>
      </h2>
      <ul>
        {data.map(item => {
          return (
            <li key={item.hl}>
              #
              <Link title={item.wu} to={item.wu}>
                {item.hl}
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  ) : null;
});

trendingTopics.propTypes = {
  data: PropTypes.array,
  secName: PropTypes.strings,
};

trendingTopics.displayName = 'trendingTopics';

export default trendingTopics;
