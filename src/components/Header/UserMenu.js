/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';
import PropTypes from 'prop-types';
import Thumb from '../Thumb/Thumb';
import ErrorBoundary from '../lib/errorboundery/ErrorBoundary';
import Link from '../Link/Link';

const Config = require(`../../../common/${process.env.SITE}`);

const UserMenu = props => {
  const { loggedIn, userData, logUserOut, showLoginRegister } = props;
  let userImage;
  if (loggedIn && userData && userData.data) {
    userImage = userData.data.dp ? userData.data.dp : userData.data.thumb;
  }

  // const userImage = loggedIn ? userData.data.dp : Config.Thumb.userImage;

  if (!loggedIn) {
    return (
      <span onClick={showLoginRegister} className="user-isloggedin">
        Login
      </span>
    );
  }

  return (
    <div className="user_area">
      <span
        className="logout-window"
        data-plugin="user-notloggedin"
        style={{
          float: 'left',
          position: 'relative',
          display: 'block',
        }}
      >
        {/* <div id="widget-head" /> */}
      </span>
      {userData && userData.data ? (
        <span
          data-plugin="user-isloggedin"
          style={{ float: 'left', display: `${loggedIn ? 'block' : 'none'}` }}
        >
          <div className="user-controls">
            <Link
              href={`https://jsso.indiatimes.com/sso/identity/profile/edit?channel=${
                process.env.SITE
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <Thumb alt="" src={userImage} width="32" height="32" className="user-image" />
              <span className="user_name">
                <span data-plugin="user-name" className="name">
                  {loggedIn ? `Hi ${userData.data.firstName}` : ''}
                </span>
                <i className="icon_down " />
              </span>
            </Link>
            <div className="dropdown">
              <ul>
                <li>
                  <Link
                    to={`https://jsso.indiatimes.com/sso/identity/profile/edit?channel=${
                      process.env.SITE
                    }`}
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Edit Profile
                  </Link>
                </li>
                <li>
                  <Link
                    target="_blank"
                    rel="noopener noreferrer"
                    to="https://mytimes.indiatimes.com/?channel=nbt"
                  >
                    My Times
                  </Link>
                </li>
                <li>
                  <span onClick={logUserOut} data-plugin="user-logout">
                    LOGOUT
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </span>
      ) : (
        ''
      )}
    </div>
  );

  return null;
};

UserMenu.propTypes = {
  loggedIn: PropTypes.bool,
  userData: PropTypes.object,
  logUserOut: PropTypes.func,
  showLoginRegister: PropTypes.func,
};

export default UserMenu;
