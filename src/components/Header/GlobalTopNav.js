import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import UserMenu from './UserMenu';
import Link from '../Link/Link';
import { logUserOut } from '../../actions/authentication';

const Config = require(`../../../common/${process.env.SITE}`);

class GlobalTopNav extends Component {
  constructor(props) {
    super(props);
    this.state = {
      iframeVisible: false,
    };
    this.toggleIframe = this.toggleIframe.bind(this);
    this.logUserOut = this.logUserOut.bind(this);
  }

  toggleIframe() {
    const { iframeVisible } = this.state;
    this.setState({ iframeVisible: !iframeVisible });
  }

  logUserOut() {
    const { dispatch } = this.props;
    const cb = () => {
      dispatch(logUserOut());
      TPWidget.logoutAction();
    };
    if (typeof JSSO_INSTANCE !== 'undefined') {
      JSSO_INSTANCE.signOutUser(cb);

      const Img = new Image();
      Img.src = 'https://jsso.indiatimes.com/sso/identity/profile/logout/external?channel=nbt';
    }
  }

  render() {
    const { data, loggedIn, userData, showLoginRegister } = this.props;

    const socialData = typeof data !== 'undefined' && data.social ? data.social : null;
    return (
      <div className="globalnav">
        {!this.props.isNotHome ? <h1>टेक्नॉलजी न्यूज</h1> : ''}
        <DateTime
          updatedTime={
            data && data.datetime && data.datetime.item && data.datetime.item.date
              ? data.datetime.item.date
              : ''
          }
        />
        <SocialLinks socialData={socialData} />

        <UserMenu
          loggedIn={loggedIn}
          userData={userData}
          showLoginRegister={showLoginRegister}
          logUserOut={this.logUserOut}
        />
      </div>
    );
  }
}

GlobalTopNav.propTypes = {
  dispatch: PropTypes.func,
  data: PropTypes.object,
  loggedIn: PropTypes.bool,
  userData: PropTypes.object,
  showLoginRegister: PropTypes.func,
  isNotHome: PropTypes.bool,
};

const DateTime = ({ updatedTime }) => (
  <ul>
    <li>UPDATED: {updatedTime}</li>
  </ul>
);

DateTime.propTypes = {
  updatedTime: PropTypes.array,
};
const SocialLinks = ({ socialData }) => (
  <div className="social_share" id="socialDiv">
    {socialData &&
      socialData.length > 0 &&
      socialData.map(item => {
        return (
          <Link
            key={item.cls}
            to={item.override}
            rel={item.rel}
            className={`nbt-home-sprite ${item.cls}`}
          />
        );
      })}
  </div>
);

SocialLinks.propTypes = {
  socialData: PropTypes.array,
};

function mapStateToProps(state) {
  return {
    ...state.nav,
    ...state.authentication,
    config: state.config,
  };
}

export default connect(mapStateToProps)(GlobalTopNav);
