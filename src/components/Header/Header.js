/* eslint-disable react/no-multi-comp */
/* eslint-disable jsx-a11y/mouse-events-have-key-events */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import propTypes from 'prop-types';
import Link from '../Link/Link';
import { fetchHoverSection } from '../../actions/nav/nav';
import { logUserOut } from '../../actions/authentication';
import HoverSection from './Sections/HoverSection';
import SearchBar from '../SearchBar/SearchBar';
import './Header.scss';

const Config = require(`../../../common/${process.env.SITE}`);

class Header extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      SetActiveClass: '',
      sectionExpanded: false,
      sectionName: '',
      sectionData: null,
    };
    this.logUserOut = this.logUserOut.bind(this);

    this.renderHeader = this.renderHeader.bind(this);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== location.pathname) {
      this.hideCurrentSection();
    }
  }

  hideCurrentSection() {
    this.setState({
      sectionExpanded: false,
      sectionName: '',
      sectionData: null,
      SetActiveClass: '',
    });
  }

  logUserOut() {
    const { dispatch } = this.props;
    const cb = () => {
      dispatch(logUserOut());
      TPWidget.logoutAction();
      const loginDiv = document.getElementById('tpwidget-prelogin');
      loginDiv.onclick = this.props.showLoginRegister;
    };
    if (typeof JSSO_INSTANCE !== 'undefined') {
      JSSO_INSTANCE.signOutUser(cb);

      const Img = new Image();
      Img.src = 'https://jsso.indiatimes.com/sso/identity/profile/logout/external?channel=nbt';
    }
  }

  showMenu(secname) {
    const { dispatch } = this.props;
    this.setState({
      sectionExpanded: true,
      sectionName: secname,
      SetActiveClass: secname,
    });
    dispatch(fetchHoverSection(secname));
  }

  hideMenu(event) {
    event.preventDefault();
    this.setState({
      sectionExpanded: false,
      SetActiveClass: '',
    });
  }

  renderHeader() {
    const { type, location, hoverData, NavData } = this.props;
    const { pathname } = location;
    const data = NavData || '';
    const ActiveSec =
      this.props.activeSection && this.props.activeSection.sectionName
        ? this.props.activeSection.sectionName
        : '';
    // const basePath = process.env.BASE_URL;
    // console.log('navprop', this.props);
    // console.log(`activesec${ActiveSec}`);

    return (
      <React.Fragment>
        <div onMouseLeave={this.hideMenu.bind(this)}>
          {data && data.level1 && data.level1.length && type !== 'topnav' ? (
            <div
              className="first-level-menu"
              itemType="http://www.schema.org/SiteNavigationElement"
              itemScope
            >
              <div className="go-to-nbt">
                <a href="https://navbharattimes.indiatimes.com/">
                  <span>Go to</span> NBT
                </a>
              </div>
              <h3
                className={
                  pathname === '/' || pathname === '/default.cms' || pathname === '/tech'
                    ? 'home active'
                    : 'home'
                }
                itemProp="name"
              >
                <Link
                  className="nbt-home-sprite"
                  to="/tech"
                  title="Home"
                  itemProp="url"
                  target="notblank"
                >
                  {Config.seoConfig.englishName}
                </Link>
              </h3>
              <ul>
                {data.level1.map((item, index) => {
                  return (
                    <li
                      key={item.hl}
                      onMouseOver={this.showMenu.bind(this, item.secname)}
                      itemProp="name"
                      id={item.id}
                      className={`nav-${item.secname} ${
                        item.secname === ActiveSec ? 'active' : ''
                      }`}
                    >
                      <Link className="nav-item" itemProp="url" to={item.override}>
                        {item.hl}
                      </Link>
                    </li>
                  );
                })}
                {this.state.sectionExpanded ? (
                  <HoverSection
                    sectionName={this.state.sectionName}
                    sectionData={hoverData}
                    hideCurrentSection={this.hideCurrentSection}
                    hoverData={this.props.hoverData || []}
                  />
                ) : (
                  ''
                )}
                <li className="nav_right">
                  <SearchBar router={this.props.router} />
                </li>
              </ul>
            </div>
          ) : null}
        </div>
      </React.Fragment>
    );
  }

  render() {
    return this.renderHeader();
  }
}

function mapStateToProps(state) {
  return {
    ...state.nav,
    ...state.authentication,
    config: state.config,
  };
}

Header.propTypes = {
  data: propTypes.array,
  type: propTypes.string,
  dispatch: propTypes.array,
};

export default connect(mapStateToProps)(Header);
