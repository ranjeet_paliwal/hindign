import React from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';
import Thumb from '../Thumb/Thumb';

const Masthead = React.memo(props => {
  const { data } = props;
  return typeof data === 'object' && data.item && data.item.imageid ? (
    <div className="masthead">
      <Link to={`${process.env.BASE_URL}${data.item.wu}`}>
        <Thumb
          imgId={data.item.imageid}
          imgSize={data.item.imgsize}
          width="1000"
          height="50"
          alt={data.item.imageid}
        />
      </Link>
    </div>
  ) : (
    ''
  );
});

Masthead.displayName = 'Masthead';

Masthead.propTypes = {
  data: PropTypes.object,
};

export default Masthead;
