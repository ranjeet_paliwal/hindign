import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import LeftNavSection from './LeftNavSection';
import { truncateStr } from '../../../common-utility';
import Link from '../../Link/Link';
import HoverLoader from '../../Loaders/HoverLoader';

class TwoCardSection extends PureComponent {
  constructor(props) {
    super(props);
    const { hoverData } = props;
    const currentData =
      hoverData && hoverData && Array.isArray(hoverData.section) && hoverData.section.length > 0
        ? hoverData.section[0]
        : null;
    this.state = {
      currentData,
      currentSectionOpened: 0,
    };
    this.renderCurrentSection = this.renderCurrentSection.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // To render first section after video  data finishes loading
    const { hoverData } = nextProps;
    const dataFetched = Boolean(
      hoverData && hoverData && Array.isArray(hoverData.section) && hoverData.section.length > 0,
    );
    if (!prevState.currentData && dataFetched) {
      return { currentData: hoverData.section[0], currentSectionOpened: 0 };
    }
    return prevState;
  }

  renderCurrentSection(index) {
    const { hoverData } = this.props;
    let currentData = null;
    if (hoverData && Array.isArray(hoverData.section) && index < hoverData.section.length) {
      currentData = hoverData.section[index];

      currentData.items =
        currentData.items.length > 13 ? currentData.items.slice(0, 13) : currentData.items;
    }
    this.setState({
      currentData,
      currentSectionOpened: index,
    });
  }

  render() {
    const { hideCurrentSection, links, hoverData, secName } = this.props;
    const { currentSectionOpened, currentData } = this.state;
    if (!hoverData || hoverData.isFetching) {
      return <HoverLoader />;
    }
    return (
      <div className="menu_content moviemasti" onMouseLeave={hideCurrentSection}>
        <div className="topsubmenu" id={secName}>
          <LeftNavSection
            links={links}
            currentSectionOpened={currentSectionOpened}
            renderCurrentSection={this.renderCurrentSection}
          />
          <div id="nav_gadgets" className="nav_gadgets right_line">
            <div id={currentData && currentData.id ? currentData.id : ''}>
              <ul>
                {currentData && Array.isArray(currentData.items)
                  ? currentData.items.map(gadgetData => (
                      <li key={gadgetData.imageid}>
                      <Link className="navVideoText" to={gadgetData.wu}>
                          {gadgetData.hl ? truncateStr(gadgetData.u_name, 40) : null}
                        </Link>
                    </li>
                    ))
                  : null}
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TwoCardSection.propTypes = {
  hideCurrentSection: PropTypes.func,
  links: PropTypes.array,
  hoverData: PropTypes.object,
  secName: PropTypes.string,
};

export default TwoCardSection;
