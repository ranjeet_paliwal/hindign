import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../Link/Link';
import Thumb from '../../Thumb/Thumb';

const ImageCard = props => {
  const { data, secName } = props;

  return data && Array.isArray(data.items) ? (
    <div className="top_photogallery">
      <div style={{ display: 'block' }}>
        {
          <div className="topmoregallerys">
            {typeof data !== 'undefined' &&
              Array.isArray(data.items) &&
              data.items.map(linkData =>
                linkData.type !== 'ctn' ? (
                  <span key={linkData.id}>
                    <Link key={linkData.id} to={linkData.wu}>
                      <Thumb
                        className="lazyload"
                        height="70"
                        width="110"
                        alt={linkData.hl}
                        imgId={linkData.imageid}
                        imgSize={linkData.imgsize}
                      />
                      <span className="txt text_ellipsis">{linkData.hl}</span>
                    </Link>
                  </span>
                ) : (
                  ''
                ),
              )}
          </div>
        }
      </div>
    </div>
  ) : null;
};

ImageCard.propTypes = {
  data: PropTypes.object,
  secName: PropTypes.string,
};

export default ImageCard;
