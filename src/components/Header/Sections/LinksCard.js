import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../Link/Link';
import Thumb from '../../Thumb/Thumb';

const LinksCard = props => {
  const { data, secName, firstImg } = props;
  const firstItem = data && Array.isArray(data.items) ? data.items[0] : {};
  return data && Array.isArray(data.items) ? (
    <div id="nav_movie" className="top_headline">
      <div style={{ display: 'block' }}>
        {firstImg ? (
          <div>
            <Link to={firstItem.wu}>
              <Thumb
                className="lazyload"
                height="55"
                width="74"
                src={secName === 'astro' ? firstItem.imageid : undefined}
                alt={firstItem.hl}
                imgId={firstItem.imageid}
                imgSize={firstItem.imgsize}
              />
            </Link>
            <Link className="nav_top_link" to={firstItem.wu}>
              {firstItem.hl}
            </Link>
            <ul className="top_list">
              {typeof data !== 'undefined' &&
                Array.isArray(data.items) &&
                data.items.slice(1).map(linkData => (
                  <li key={linkData.id}>
                    <Link key={linkData.id} to={linkData.wu}>
                      {linkData.hl}
                    </Link>
                  </li>
                ))}
            </ul>
          </div>
        ) : (
          <ul className="top_list">
            {typeof data !== 'undefined' &&
              Array.isArray(data.items) &&
              data.items.slice(0).map(linkData => (
                <li key={linkData.id}>
                  <Link key={linkData.id} to={linkData.wu}>
                    {linkData.hl}
                  </Link>
                </li>
              ))}
          </ul>
        )}
      </div>
    </div>
  ) : null;
};

LinksCard.propTypes = {
  data: PropTypes.object,
  secName: PropTypes.string,
};

export default LinksCard;
