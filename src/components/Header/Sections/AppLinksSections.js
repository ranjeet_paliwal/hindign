/* eslint-disable operator-linebreak */
import React from 'react';
import Link from '../../Link/Link';
import Thumb from '../../Thumb/Thumb';

const AppLinksSection = React.memo(data => {
  return (
    <div className="menu_contentL2 navappsdrop">
      <div className="topsubmenu" id="applinks">
        <ul>
          {data &&
            Array.isArray(data) &&
            data.map(value => (
              <li key={value.sid}>
                <Link target="_blank" to={value.link}>
                  <strong>{value.name}</strong>
                  <Thumb src={value.img} alt={value.name} width="148" height="155" />
                </Link>
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
});

AppLinksSection.displayName = 'AppLinksSection';

export default AppLinksSection;
