import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../Link/Link';

const LeftNavSection = props => {
  const { links, renderCurrentSection, currentSectionOpened } = props;
  return (
    <ul className="top_sub_menu right_line">
      {Array.isArray(links) &&
        links.map((l, idx) => (
          <li
            className={currentSectionOpened === idx ? 'current' : ''}
            key={l.secname}
            onMouseEnter={() => renderCurrentSection(idx)}
          >
            <Link to={l.wu}>{l.u_secname}</Link>
          </li>
        ))}
    </ul>
  );
};

LeftNavSection.propTypes = {
  links: PropTypes.array,
  renderCurrentSection: PropTypes.func,
  currentSectionOpened: PropTypes.number,
};

export default LeftNavSection;
