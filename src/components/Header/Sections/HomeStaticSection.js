import React from 'react';
import PropTypes from 'prop-types';

import Link from '../../Link/Link';
import HoverLoader from '../../Loaders/HoverLoader';

const HomeStaticSection = React.memo(({ data, hideCurrentSection }) => {
  if (!data) {
    return <HoverLoader />;
  }

  return (
    <div className="menu_content home_links" onMouseLeave={hideCurrentSection}>
      <div className="topsubmenu">
        {data && Array.isArray(data)
          ? data.map((v, i) => {
              return (
              <ul key={`ul_homestatic_${i}`}>
                  {v &&
                    Array.isArray(v) &&
                    v.map(links => (
                      <li key={`link_${links.name}`}>
                        <Link target="_blank" to={links.link}>
                          {links.name}
                        </Link>
                      </li>
                    ))}
                </ul>
              );
            })
          : null}
      </div>
    </div>
  );
});

HomeStaticSection.displayName = 'HomeStaticSection';

HomeStaticSection.propTypes = {
  data: PropTypes.array,
  hideCurrentSection: PropTypes.func,
};
export default HomeStaticSection;
