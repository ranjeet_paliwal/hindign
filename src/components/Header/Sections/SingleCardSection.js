import React from 'react';
import PropTypes from 'prop-types';
import LinksCard from './LinksCard';
import HoverLoader from '../../Loaders/HoverLoader';
import ImageCard from './ImageCard';

const SingleCardSection = props => {
  const { hideCurrentSection, secName, hoverData } = props;
  console.log('secname', secName);
  let data = null;

  if (hoverData && hoverData.data && hoverData.data.linksData) {
    data = hoverData.data.linksData;
  }
  if (
    hoverData &&
    hoverData.data &&
    hoverData.data.linksData &&
    hoverData.data.linksData.newsItem
  ) {
    data = hoverData.data.linksData.newsItem;
  }

  if (hoverData && hoverData.data && hoverData.data.section && hoverData.data.section.newsItem) {
    data = hoverData.data.section.newsItem;
  }

  if (!hoverData || hoverData.isFetching) {
    return <HoverLoader />;
  }
  let showfirstimg;
  if (secName === 'compare' || secName === 'shop') {
    showfirstimg = false;
  } else {
    showfirstimg = true;
  }

  if (secName === 'photo' || secName === 'video' || secName === 'videos') {
    return (
      <div className="menu_content" onMouseLeave={hideCurrentSection}>
        <div className="topsubmenu" id={secName}>
          <ImageCard data={data} secName={secName} />
        </div>
      </div>
    );
  }
  return (
    <div className="menu_content" onMouseLeave={hideCurrentSection}>
      <div className="topsubmenu" id={secName}>
        <LinksCard data={data} secName={secName} firstImg={showfirstimg} />
      </div>
    </div>
  );
};

SingleCardSection.propTypes = {
  secName: PropTypes.string,
  hideCurrentSection: PropTypes.func,
  hoverData: PropTypes.object,
};

export default SingleCardSection;
