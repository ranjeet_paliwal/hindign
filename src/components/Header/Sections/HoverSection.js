/* eslint-disable operator-linebreak */
import React from 'react';
import PropTypes from 'prop-types';
import SingleCardSection from './SingleCardSection';
import TwoCardSection from './TwoCardSection';
import Link from '../../Link/Link';
import HomeStaticSection from './HomeStaticSection';

const HoverSection = props => {
  const { hideCurrentSection, hoverData, sectionName } = props;

  let links;
  let hData;
  switch (sectionName) {
    case 'gadgets-news':
    case 'tips-tricks':
    case 'reviews':
    case 'compare':
    case 'shop':
      return (
        <SingleCardSection
          secName={sectionName}
          hideCurrentSection={hideCurrentSection}
          hoverData={hoverData && hoverData[sectionName] ? hoverData[sectionName] : null}
        />
      );

    case 'video':
    case 'videos':
      // eslint-disable-next-line no-case-declarations ,operator-linebreak
      return (
        <SingleCardSection
          secName={sectionName}
          hideCurrentSection={hideCurrentSection}
          hoverData={hoverData && hoverData[sectionName] ? hoverData[sectionName] : null}
        />
      );

    case 'photo':
      // eslint-disable-next-line no-case-declarations ,operator-linebreak
      return (
        <SingleCardSection
          secName={sectionName}
          hideCurrentSection={hideCurrentSection}
          hoverData={hoverData && hoverData[sectionName] ? hoverData[sectionName] : null}
        />
      );

    case 'gadgets':
      links =
        hoverData &&
        hoverData[sectionName] &&
        hoverData[sectionName].data &&
        hoverData[sectionName].data.section
          ? hoverData[sectionName].data.section
          : null;
      hData =
        hoverData &&
        hoverData[sectionName] &&
        hoverData[sectionName].data &&
        hoverData[sectionName].data
          ? hoverData[sectionName].data
          : null;
      return (
        <TwoCardSection
          links={links}
          secName={sectionName}
          hoverData={hData}
          hideCurrentSection={hideCurrentSection}
        />
      );
    case 'homeStatic':
      hData =
        hoverData &&
        hoverData[sectionName] &&
        hoverData[sectionName].data &&
        hoverData[sectionName].data.homesections
          ? hoverData[sectionName].data.homesections
          : null;
      return <HomeStaticSection data={hData} hideCurrentSection={hideCurrentSection} />;

    default:
      return null;
  }
};

HoverSection.propTypes = {
  sectionName: PropTypes.string,
  sectionData: PropTypes.object,
  hideCurrentSection: PropTypes.func,
  hoverData: PropTypes.object,
};

export default HoverSection;
