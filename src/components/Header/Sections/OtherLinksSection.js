import React from 'react';
import PropTypes from 'prop-types';

import Link from '../../Link/Link';
import HoverLoader from '../../Loaders/HoverLoader';

const OtherLinksSection = ({ data }) => {
  if (!data) {
    return <HoverLoader />;
  }

  return (
    <div className="menu_content navor">
      <div className="topsubmenu" id="morelinks">
        {data &&
          data.data &&
          data.data.sections &&
          Array.isArray(data.data.sections.items) &&
          data.data.sections.items.map((linkData, idx) => {
            return (
              <ul key="right_l" className="right_line">
                {linkData.map((ld, i) => (
                  <li key={`li__${i}`}>
                    <Link
                      className={ld.cls || ''}
                      // style={ld.style || {}}
                      target="_blank"
                      to={ld.override}
                    >
                      {ld.hl}
                    </Link>
                  </li>
                ))}
              </ul>
            );
          })}
      </div>
    </div>
  );
};

OtherLinksSection.propTypes = {
  data: PropTypes.object,
};

export default OtherLinksSection;
