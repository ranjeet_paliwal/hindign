/* eslint-disable react/prop-types */
import React from 'react';
import PropageTypes from 'prop-types';
import { connect } from 'react-redux';

import Thumb from '../../Thumb/Thumb';
import Link from '../../Link/Link';

const Config = require(`../../../../common/${process.env.SITE}`);

// const config = require(`../../../../common/${process.env.SITE}`);
function getPageType(loc) {
  let pageType;
  // console.log('===loc===', loc);
  if (loc && loc.pathname === '/') {
    pageType = 'HP';
  } else if (loc && loc.pathname.indexOf('articlelist') !== -1) {
    pageType = 'AL';
  } else if (loc && loc.pathname.indexOf('articleshow') !== -1) {
    pageType = 'AS';
  } else if (loc && loc.pathname.indexOf('photoshow') !== -1) {
    pageType = 'PS';
  } else if (
    loc &&
    loc.pathname &&
    loc.pathname.indexOf('compare-') !== -1 &&
    loc.pathname.split('/').length <= 3
  ) {
    pageType = 'CL';
  } else if (
    loc &&
    loc.pathname &&
    loc.pathname.indexOf('compare-') !== -1 &&
    loc.pathname.split('/').length > 3
  ) {
    pageType = 'CS';
  } else if (
    loc &&
    loc.pathname &&
    loc.pathname.indexOf('compare-') !== -1 &&
    loc.pathname.split('/').length > 3
  ) {
    pageType = 'CS';
  } else {
    pageType = '';
  }
  // console.log('===pageType===', pageType);
  return pageType;
}

const LogoBar = React.memo(props => {
  const { width, height, dateTimeData, isNotHome, loc } = props;

  let sectionInfo = null;
  // let pageType = null;
  let articleId = null;
  const pageType = getPageType(loc);
  if (
    pageType === 'AS' &&
    props.AS &&
    props.AS.data &&
    props.AS.data.articleData &&
    props.AS.data.articleData.id
  ) {
    const articleInfo = props.AS.data.articleData;
    articleId = articleInfo.id;
    sectionInfo =
      articleInfo.sectioninfo &&
      Array.isArray(articleInfo.sectioninfo) &&
      articleInfo.sectioninfo[0];
  } else if (pageType === 'AL' && props.AL && props.AL.data && props.AL.data.articleData) {
    sectionInfo = props.AL.data.articleData.sectioninfo;
  }

  return (
    <React.Fragment>
      <div className="logo">
        <h2>
          <Link to={process.env.WEBSITE_URL}>
            <Thumb
              // src={`${process.env.IMG_URL}/${Config.SiteInfo.logo}`}
              // width={width}
              src={`${Config.SiteInfo.logo}`}
              width="130"
              height={height}
              alt={Config.Locale.logotitle}
              title={Config.Locale.logotitle}
            />
          </Link>
        </h2>
        <LogobarCaption isNotHome={isNotHome} pagetype={pageType} sectionInfo={sectionInfo} />
      </div>
      <Atf728 articleId={articleId} pagetype={pageType} sectionInfo={sectionInfo} />
    </React.Fragment>
  );
});

const LogobarCaption = ({ dateTime, sectionInfo, pagetype }) => {
  const articleID = (sectionInfo && sectionInfo.msid) || '';
  let logoContent;
  if (pagetype === 'AL' && sectionInfo) {
    logoContent = (
      <h1 className="section_name">
        {sectionInfo && sectionInfo.h1}
        <span>{sectionInfo && sectionInfo.subh1}</span>
      </h1>
    );
  } else if (pagetype === 'AS' && sectionInfo) {
    logoContent = (
      <Link to={sectionInfo && sectionInfo.wu} className="section_name">
        {(sectionInfo && sectionInfo.hl) || ''}
        {Config.NewsPrefix.indexOf(articleID) !== -1 ? ` ${Config.Locale.hidiSamachar}` : null}
      </Link>
    );
  } else {
    logoContent = <span className="date">{dateTime}</span>;
  }
  return logoContent;
};

const Atf728 = ({ sectionInfo, pagetype, articleId }) => {
  const uniqueId = Date.now();
  switch (pagetype) {
    case 'AL': {
      return (
        <div className="atf-topad add_atf ATF_728" style={{ 'font-size': '0px' }}>
          ATF_728
        </div>
      );
    }
    case 'AS':
      return (
        <div className={`atf-topad add_atf  ATF_728_${articleId}`} style={{ 'font-size': '0px' }}>
          ATF_728
        </div>
      );

    case 'CL': {
      return (
        <div className="atf-topad add_atf  ATF_728" style={{ 'font-size': '0px' }}>
          ATF_728_CL
        </div>
      );
    }
    case 'CS': {
      return (
        <div className="atf-topad add_atf  ATF_728" style={{ 'font-size': '0px' }}>
          ATF_728_CS
        </div>
      );
    }
    case 'HP': {
      return <div className="add_atf HP_ATF" />;
    }

    default: {
      return (
        <div className="atf-topad add_atf  ATF_728" style={{ 'font-size': '0px' }}>
          {`ATF_728_${uniqueId}`}
        </div>
      );
    }
  }
};

LogobarCaption.propageTypes = {
  isNotHome: PropageTypes.bool,
  dateTime: PropageTypes.object,
  data: PropageTypes.object,
};

LogoBar.displayName = 'LogoBar';

LogoBar.propageTypes = {
  width: PropageTypes.string,
  height: PropageTypes.string,
  alt: PropageTypes.string,
  dateTimeData: PropageTypes.object,
  isNotHome: PropageTypes.bool,
  data: PropageTypes.object,
};

function mapStateToProps(state) {
  return { AL: state.articlelist, AS: state.articleshow };
}
export default connect(mapStateToProps)(LogoBar);
