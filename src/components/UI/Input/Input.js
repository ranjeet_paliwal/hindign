/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import PropTypes from 'prop-types';

const input = props => {
  let inputElement = null;

  switch (props.elementType) {
    case 'input':
      inputElement = <input {...props.elementConfig} onChange={props.changed} />;
      break;

    case 'select':
      inputElement = (
        <select name={props.name} onChange={props.changed}>
          {props.elementConfig.options.map(option => (
            <option key={option.value} value={option.value}>
              {option.displayValue}
            </option>
          ))}
        </select>
      );
      break;
    default:
      inputElement = (
        <input {...props.elementConfig} value={props.value} onChange={props.changed} />
      );
  }

  return (
    <span className={props.ClassName}>
      {props.label ? <label>{props.label}</label> : null}
      {inputElement}
    </span>
  );
};

input.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  selectedOption: PropTypes.string,
  controlFunc: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
};

export default input;
