import React from 'react';

const button = props => (
  <button disabled={props.disabled} onClick={props.clicked} className="btn">
    {props.children}
  </button>
);

export default button;
