/* eslint-disable operator-linebreak */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Link from '../Link/Link';
import './Breadcrumb.scss';

import CtnAd from '../CtnAd/CtnAd';

const Config = require(`../../../common/${process.env.SITE}`);

const BreadCrumbs = React.memo(props => {
  const { data } = props;

  return data ? (
    <React.Fragment>
      <div className={data['@class']}>
        {data.ul ? (
          <ul itemType={data.ul['@itemtype']} itemScope={data.ul['@itemscope']}>
            {data.ul.li &&
              Array.isArray(data.ul.li) &&
              data.ul.li.map((liData, index) => {
                if ('a' in liData) {
                  return (
                    <li
                      itemProp={liData['@itemprop']}
                      itemScope={liData['@itemscope']}
                      itemType={liData['@itemtype']}
                    >
                      <Link itemProp={liData.a['@itemprop']} to={liData.a['@href']}>
                        <span itemProp={liData.a.span['@itemprop']}>{liData.a.span['#text']}</span>
                      </Link>

                      {'meta' in liData ? (
                        <meta
                          itemProp={liData.meta['@itemprop']}
                          content={
                            liData.meta['@itemprop'] === 'position'
                              ? index + 1
                              : liData.meta['@content']
                          }
                        />
                      ) : null}
                    </li>
                  );
                }
                return typeof liData !== 'undefined' &&
                  typeof liData.span !== 'undefined' &&
                  typeof liData.span.span !== 'undefined' &&
                  liData.span['@itemprop'] ? (
                  <li
                      itemProp={liData['@itemprop']}
                      itemScope={liData['@itemscope']}
                      itemType={liData['@itemtype']}
                  >
                      <span itemProp={liData.span['@itemprop']} content={liData.span['@content']}>
                      <span itemProp={liData.span.span['@itemprop']}>
                          {liData.span.span['#text'] ? liData.span.span['#text'] : liData.span.span}
                        </span>
                    </span>
                      {'meta' in liData ? (
                      <meta
                          itemProp={liData.meta['@itemprop']}
                          content={
                          liData.meta['@itemprop'] === 'position'
                            ? index + 1
                            : liData.meta['@content']
                        }
                        />
                    ) : null}
                    </li>
                ) : (
                    <li>
                    <span>
                        {liData.span.span['#text'] ? liData.span.span['#text'] : liData.span.span}
                      </span>
                  </li>
                );
              })}
          </ul>
        ) : null}
      </div>
      <div className="ATF_AS_STRIPPD" />
      <div className="PPD" />
      <div className="ctnppd">
        <CtnAd
          articleId="12345"
          height="40"
          width="1000"
          className="colombia"
          slotId={Config.CTN.stripped_pbd_article_show}
        />
      </div>
    </React.Fragment>
  ) : null;
});

BreadCrumbs.displayName = 'BreadCrumbs';

BreadCrumbs.propTypes = {
  data: PropTypes.object,
};

export default BreadCrumbs;
