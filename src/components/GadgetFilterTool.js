import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchFilterListDataIfNeeded } from '../actions/gadgetfiltertool/gadgetfiltertool';
import ErrorBoundary from './lib/errorboundery/ErrorBoundary';
import { analyticsGA, AnalyticsComscore } from '../analytics';

const Config = require('../../common/nbt');

class GadgetFilterTool extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { dispatch, params, router, value, category } = this.props;
    const { query } = this.props.router.location.pathname;

    GadgetFilterTool.fetchFilterListData({ dispatch, query, params, router }).then(data => {});
  }

  componentDidUpdate() {
    const { params } = this.props;
    if (document.querySelectorAll('input[type="checkbox"]:checked').length > 0) {
      document.querySelector('b[id="clearAll"]').style.display = 'block';
    } else {
      document.querySelector('b[id="clearAll"]').style.display = 'none';
    }

    const brandCheckBoxes = document.querySelectorAll('.filtercontent-brand li input');
    if (brandCheckBoxes) {
      for (var i = 0; i < brandCheckBoxes.length; i++) {
        if (
          params &&
          params.brand &&
          brandCheckBoxes[i].value.toLowerCase() == params.brand.toLowerCase()
        ) {
          brandCheckBoxes[i].checked = 'checked';
        }
      }
    }
  }

  removeFilter(obj) {
    // console.log('selected option closed', obj.target.parentElement.id);
    if (obj.target.getAttribute('class') == 'close_icon') {
      document.getElementById(obj.target.parentElement.id).remove();
      const getInputboxId = obj.target.parentElement.id.substring(3);
      eval(`document.filtercriteria.${getInputboxId}.checked=false`);
      this.filterApply(obj.target.parentElement.id.substring(3));
      if (getInputboxId.includes('brand')) {
        const listItems = document.querySelectorAll('.filtercontent-brand li');
        if (listItems.length > 0) {
          for (let i = 0; i < listItems.length; i++) {
            listItems[i].classList.remove('hide');
          }
        }
        document.getElementById('brand').value = '';
      }
      if (getInputboxId.includes('screen_size')) {
        const listItemsScreens = document.querySelectorAll('.filtercontent-screen-size li');
        if (listItemsScreens.length > 0) {
          for (let i = 0; i < listItemsScreens.length; i++) {
            listItemsScreens[i].classList.remove('hide');
          }
        }
        document.getElementById('screen-size').value = '';
      }
    }
  }

  filterCases(obj) {
    // debugger;
    if (obj.target.checked) {
      // console.log('selected option', obj.target.value);
      const closenode = document.createElement('span');
      closenode.className = 'close_icon';
      closenode.innerText = 'Close';
      const div = document.createElement('div');
      div.className = 'filterkey';
      div.id = `div${obj.target.name}`;
      // alert(obj.target.parentElement.innerText);
      if (
        obj.target.getAttribute('datavalues') == 'rumoured' ||
        obj.target.getAttribute('datavalues') == 'upcoming'
      )
        div.innerHTML = obj.target.getAttribute('datavalues');
      else div.innerHTML = obj.target.parentElement.innerText;
      div.appendChild(closenode);
      document.getElementById('filterapplied').appendChild(div);
      this.filterApply(obj.target.value);
    } else {
      // alert('remove');
      document.getElementById(`div${obj.target.getAttribute('name')}`).remove();
      this.filterApply(obj.target.value);
    }
  }

  filterApply(obj) {
    // debugger;
    const { router } = this.props;
    const filterkeyword = [];
    let filtercriteria = '';
    let sortby = '';
    let filterby = '';
    let upcoming = false;

    const checkBox = document.getElementsByTagName('input');
    for (let b = 0; b < checkBox.length; b++) {
      if (checkBox[b].type == 'radio') {
        checkBox[b].checked && checkBox[b].value != 'popular'
          ? (sortby += `sort=${checkBox[b].value}`)
          : null;
      }
      // if(checkBox[b].type=='checkbox' && checkBox[b].checked==true && checkBox[b].getAttribute("datavalues")=='upcoming'){
      //     prepend=checkBox[b].getAttribute("datavalues");
      // }
      // else
      if (checkBox[b].type == 'checkbox' && checkBox[b].checked == true) {
        if (checkBox[b].getAttribute('datavalues') == 'upcoming') {
          upcoming = true;
        } else if (filterkeyword.includes(checkBox[b].getAttribute('datavalues'))) {
          filterby += `%7C${checkBox[b].value}`;
        } else {
          filterkeyword.push(checkBox[b].getAttribute('datavalues'));
          filterby += filterby != '' || sortby != '' ? '&' : '';
          filterby += `${checkBox[b].getAttribute('datavalues')}=${checkBox[b].value}`;
        }
      }
    }

    // handle if single brand is selected no filters parms added
    if (
      sortby == '' &&
      filterkeyword.length > 0 &&
      filterkeyword.length < 2 &&
      filterkeyword[0].toLowerCase() == 'brand' &&
      filterby != '' &&
      filterby.indexOf('%7C') < 0
    ) {
      const temparray = filterby.split('=');
      filtercriteria += `/${temparray[1] && temparray[1].toLowerCase()}`;
    } else {
      if (sortby != '') filtercriteria += sortby; // append sorting if required
      if (filterby != '') filtercriteria += filterby; // append filter if required
      if (sortby != '' || filterby != '') filtercriteria = `/filters/${filtercriteria}`;
    }

    let routepath = router.location.pathname;
    if (upcoming) routepath = `/tech/upcoming-${this.props.params.category}`;
    else routepath = `/tech/${this.props.params.category}`;
    const array = routepath.split('/');
    const routearray = [];
    if (array.length > 2) {
      // Re Manipulate routepath if route have already filters

      for (let i = 0; i < array.length; i++) {
        if (i < 3) routearray.push(array[i]);
      }
      const updateroute = routearray.join('/');
      routepath = updateroute;
    }

    if (filtercriteria != '') {
      this.props.router.push(routepath + filtercriteria);
    } else {
      this.props.router.push(routepath);
    }

    analyticsGA.event('Web GS', 'GDS_category_filter', filtercriteria);
    // if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
    //   analyticsGA.pageview(window.location.origin + location.pathname);
    // }
  }

  restForm(obj) {
    // clear check box

    const checkBox = document.getElementsByTagName('input');
    for (let b = 0; b < checkBox.length; b++) {
      if (checkBox[b].type == 'radio') {
        checkBox[b].checked = false;
      }
      if (checkBox[b].type == 'checkbox') {
        checkBox[b].checked = false;
      }
      if (checkBox[b].type == 'text') {
        const listItems = document.querySelectorAll('.filtercontent-brand li');
        const listItemsScreens = document.querySelectorAll('.filtercontent-screen-size li');
        if (listItems.length > 0) {
          for (let i = 0; i < listItems.length; i++) {
            listItems[i].classList.remove('hide');
          }
        }
        if (listItemsScreens.length > 0) {
          for (let i = 0; i < listItemsScreens.length; i++) {
            listItemsScreens[i].classList.remove('hide');
          }
        }
        // }
        checkBox[b].value = '';
      }
    }
    document.getElementById('filterapplied').innerHTML = '';
    this.filterApply(checkBox);
  }

  HtmlSearch(obj) {
    // debugger;
    const keyword = obj.target.value.toLowerCase();
    if (keyword == '') {
      // show all check box
      const allcheckbox = obj.target.parentElement.parentElement.children;
      for (var i = 1; i < allcheckbox.length; i++) {
        // let inputbox=allcheckbox[i].getElementsByTagName("input");
        allcheckbox[i].classList.remove('hide');
      }
    } else {
      // showcase based on keyword
      const allcheckbox = obj.target.parentElement.parentElement.children;
      for (var i = 1; i < allcheckbox.length; i++) {
        const inputbox = allcheckbox[i].getElementsByTagName('input');

        const inputval = inputbox[0].value.toLowerCase();
        if (inputval.indexOf(keyword) < 0) allcheckbox[i].classList.add('hide');
      }
    }
  }

  render() {
    const _this = this;
    const { techgadgetfilter, category, params } = this.props;

    return (
      <React.Fragment>
        <div>
          <h2 className="sectionHead">
            <span>{Config.Locale.tech.filtertxt}</span>
            <b id="clearAll" onClick={() => this.restForm(this)}>
              {Config.Locale.tech.clearall}
            </b>
          </h2>
        </div>
        <form name="filtercriteria" id="filtercriteria">
          <React.Fragment>
            <ErrorBoundary>
              <div
                className="filterapplied"
                id="filterapplied"
                onClick={this.removeFilter.bind(this)}
              />
              <div className="filters-options" id="sortData">
                {techgadgetfilter && techgadgetfilter.facets && techgadgetfilter.facets.length > 0
                  ? techgadgetfilter.facets.map((item, index) => {
                      let filtercase = item.q_param;
                      return (
                        // eslint-disable-next-line react/jsx-key
                        <div className="options_item">
                          <h3>{item.d_name}</h3>
                          <ul className="hide" className={`filtercontent-${item.q_param}`}>
                            {item.q_param == 'brand' || item.q_param == 'screen-size' ? (
                              <div className="search">
                                <input
                                  onChange={_this.HtmlSearch.bind(this)}
                                  placeholder={Config.Locale.tech.search}
                                  data-search-for={item.q_param}
                                  className="filters-search-box"
                                  type="text"
                                  autoComplete="off"
                                  id={item.q_param}
                                />
                                <span className="search_icon" />
                              </div>
                            ) : null}

                            {item && item.values && item.values.length > 0 ? (
                              item.values.map((item, index) => {
                                return (
                                  <li>
                                    <label>
                                      <input
                                        onClick={_this.filterCases.bind(this)}
                                        index={index}
                                        datavalues={filtercase}
                                        type="checkbox"
                                        name={filtercase.replace('-', '_') + index}
                                        value={item.qname}
                                      />
                                      {item.d_name}
                                    </label>
                                  </li>
                                );
                              })
                            ) : item && item.values && item.values.val ? (
                              <li>
                                <label>
                                  <input
                                    onClick={_this.filterCases.bind(this)}
                                    index="0"
                                    datavalues={filtercase}
                                    type="checkbox"
                                    name={`${filtercase.replace('-', '_')}0`}
                                    value={item.values.val.qname}
                                  />
                                  {item.values.val.d_name}
                                </label>
                              </li>
                            ) : null}
                          </ul>
                        </div>
                      );
                    })
                  : null}
              </div>
            </ErrorBoundary>
          </React.Fragment>
        </form>
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.gadgetfiltertool,
  };
}
GadgetFilterTool.fetchFilterListData = ({
  dispatch,
  params,
  query,
  router,
  category,
  keyword,
  index,
}) => {
  return dispatch(fetchFilterListDataIfNeeded(params, query, router, category, keyword, index));
};
export default connect(mapStateToProps)(GadgetFilterTool);
