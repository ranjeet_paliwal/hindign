import React from 'react';
import PropTypes from 'prop-types';

const CtnAd = ({ articleId, position, slotId, style, className, height, width, isslider }) => {
  const plachlderId = articleId
    ? `div-clmb-ctn-${slotId}-1-${articleId}`
    : `div-clmb-ctn-${slotId}-1-${position}`;

  // console.log('isslider ', isslider);

  return (
    <div
      className={className}
      data-adheight={height}
      data-adwidth={width}
      data-section="tech"
      data-slot={slotId}
      data-position={position || 1}
      style={style}
      id={plachlderId}
      data-msid={articleId || 0}
      data-slide={isslider === '1' ? 'slider' : ''}
    />
  );
};

CtnAd.propTypes = {
  articleId: PropTypes.integer,
  slotId: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
  position: PropTypes.number,
  isslider: PropTypes.number,
};

export default CtnAd;
