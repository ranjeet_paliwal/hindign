/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { PureComponent } from 'react';
import Videocommon from '../../containers/articleshow/subsections/Videocommon';
// import Link from '../Link/Link';
import './MiniTV.scss';

require('es6-promise').polyfill();
require('isomorphic-fetch');

const Config = require(`../../../common/${process.env.SITE}`);

class MiniTV extends PureComponent {
  state = {
    videoData: {},
    isActive: false,
    isSticky: false,
  };

  componentDidMount() {
    fetch(`${process.env.API_ENDPOINT}${Config.API.minitv}`)
      .then(response => response.json())
      .then(data => {
        if (data !== null) {
          this.setState({
            videoData: data,
            isActive: false,
            isSticky: true,
          });
        }
      });
  }

  clickCloseHandler = () => {
    this.setState({
      isActive: false,
      isSticky: false,
    });
  };

  openMiniTVHandler = () => {
    this.setState({
      isActive: false,
      isSticky: true,
    });
  };

  closeMiniTVHandler = () => {
    this.setState({
      isActive: true,
      isSticky: false,
    });
  };

  render() {
    const { videoData, isSticky, isActive } = this.state;
    if (isSticky) {
      return (
        <div className="streaming_box">
          <div className="box_head">
            <span className="livetv_title">{videoData && videoData.hl}</span>
            <span className="close_icon" onClick={this.closeMiniTVHandler}>
              X
            </span>
            <div className="clear" />
          </div>
          {/* <iframe frameBorder="0" width="100%" height="91%" src={miniTVURL} title="Live TV" /> */}
          <Videocommon
            slikeid={videoData.slikeid}
            height="216"
            width="320"
            imageid={videoData.imageid}
            imgsize={videoData.imgsize}
            seolocation={videoData.seolocation}
            vidtitle={videoData.hl}
            isDockable={false}
            adsection="live-tv-mini"
            autoplaynextvideo={false}
          />
        </div>
      );
    }

    if (isActive) {
      return (
        <div className="liveS_container">
          <div onClick={this.openMiniTVHandler}>
            <div className="watchlive">
              <span className="sbtn">WATCH</span>
              <h3>{videoData.hl}</h3>
              <a className="live_link">{videoData.syn}</a>
            </div>
          </div>
          <span className="close_s" onClick={this.clickCloseHandler}>
            x
          </span>
        </div>
      );
    }

    return null;
  }
}

export default MiniTV;
