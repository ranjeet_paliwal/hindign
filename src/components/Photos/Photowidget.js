import React, { PureComponent } from 'react';
import Slider from '../NewSlider/NewSlider';

const Config = require(`../../../common/${process.env.SITE}`);

class PhotoWidget extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      PhotosData: '',
      isError: false,
    };
  }

  componentDidMount() {
    fetch(
      'https://navbharattimesfeeds.indiatimes.com/pwafeeds/webgn_homelist.cms?type=photo&count=11&feedtype=sjson&secid=19829828&platform=webgn',
    )
      .then(response => response.json())
      .then(data => {
        this.setState({ PhotosData: data });
      })
      .catch(e => this.setState({ isError: true }));
  }

  render() {
    const { PhotosData } = this.state;
    const { type } = this.props;
    return (
      <div className="full_section wdt_latest_videos">
        {typeof PhotosData === 'object' && PhotosData.section && PhotosData.section.newsItem ? (
          <Slider
            type={type === 'AS' ? 'photogalleryAS' : 'photogallery'}
            size="3"
            sliderData={PhotosData.section.newsItem.items}
            width="300"
            height="168"
            SliderClass="photoslider"
            islinkable="true"
            link={
              PhotosData.section.newsItem && PhotosData.section.newsItem.wu
                ? PhotosData.section.newsItem.wu
                : ''
            }
            secname={Config.Locale.gadget.photo}
            sliderWidth={type === 'AS' ? '270' : '930'}
            headingRequired="yes"
            moreLinkRequired="yes"
            moreLinkText={Config.Locale.allphotos}
          />
        ) : (
          ''
        )}
      </div>
    );
  }
}

export default PhotoWidget;
