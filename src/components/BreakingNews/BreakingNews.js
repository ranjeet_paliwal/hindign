/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { PureComponent } from 'react';
import Link from '../Link/Link';
import './BreakingNews.scss';

const Config = require(`../../../common/${process.env.SITE}`);

require('es6-promise').polyfill();
require('isomorphic-fetch');

class BreakingNews extends PureComponent {
  state = {
    bnews: {
      isActive: false,
      content: null,
      isMoving: false,
    },
  };

  componentDidMount() {
    // const { bnews } = this.state;
    fetch(Config.API.bnews)
      .then(response => response.json())
      .then(data => {
        if (data.item.Status === 'true') {
          this.setState({
            bnews: {
              isActive: true,
              content: data.item,
              isMoving: true,
            },
          });
        }
      })
      .catch({});
    setTimeout(() => {
      const { bnews } = this.state;
      if (bnews.isActive) {
        this.initMove();
      }
    }, 2000);
  }

  CloseBNewsHandler = () => {
    this.setState({
      bnews: {
        isActive: false,
      },
    });
  };

  initMove() {
    const marqueePlaceholder = document.querySelector('.marquee_content');
    const position = 0;
    const fullWidth = marqueePlaceholder.offsetWidth;

    const moving = false;
    if (marqueePlaceholder.offsetWidth >= 345 && !moving) {
      this.move(position, fullWidth);
    }
  }

  move(position, fullWidth) {
    const marqueePlaceholder = document.querySelector('.marquee_content');
    if (marqueePlaceholder) {
      marqueePlaceholder.style.left = `${position - 2}px`;
      if (marqueePlaceholder.style.left === `-${fullWidth}px`) {
        position = fullWidth;
      }
      position -= 1;
      setTimeout(() => {
        this.move(position, fullWidth);
      }, 10);
    }
  }

  render() {
    let bnewsData = null;
    const { bnews } = this.state;
    if (bnews.isActive) {
      bnewsData = bnews.content.weburl ? (
        <Link to={bnews.content.weburl} target="_blank">
          {bnews.content.hl}
        </Link>
      ) : (
        bnews.content.hl
      );
    }
    const style = {
      position: 'relative',
      left: '0px',
      float: 'left',
    };
    style['white-space'] = 'nowrap';
    style['font-size'] = '14px';
    style['line-height'] = '36px';
    style['font-weight'] = 'normal';
    style.color = '#044e97';
    style.top = '-2px';

    return bnews.isActive ? (
      <div className="bnews-placeholder">
        <span className="left">अभी अभी</span>
        <span className="center">
          <b className="marquee_content" style={style}>
            {bnewsData}
          </b>
        </span>
        <span className="right">
          <span onClick={this.CloseBNewsHandler} className="close_icon" />
        </span>
      </div>
    ) : null;
  }
}

export default BreakingNews;
