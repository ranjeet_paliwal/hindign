import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Link from '../Link/Link';
import Tabs from '../Tabs/Tabs';
import './MostRead.scss';
import { fetchDataIfNeeded } from '../../actions/home/mostread/mostread';

class MostRead extends PureComponent {
  componentDidMount() {
    const { dispatch, params } = this.props;
    MostRead.fetchData({ dispatch, params });
  }

  render() {
    const { value } = this.props;
    return typeof value !== 'undefined' && value.section ? (
      <div className="section most_read first_image_view">
        <Tabs>
          {typeof value === 'object' &&
            value.section &&
            value.section.map(obj => {
              return (
                <div label={obj.secname} key={obj.id}>
                  <ul
                    key={obj.id}
                    className={obj.id === 'ibeatarticle' ? 'content_most_read' : 'content_live_cmt'}
                  >
                    {obj.items.map(val => {
                      return (
                        <li key={val.hl}>
                          <Link title={val.hl} to={val.wu}>
                            {val.hl}
                          </Link>
                          <span className="desc">
                            <b>{val.username ? val.username : null}</b>
                            {val.optext ? val.optext : null}
                          </span>
                        </li>
                      );
                    })}
                  </ul>
                  {obj.id === 'ibeatarticle' ? (
                    <div className="moremostviewed">
                      <Link to={obj.override}>और >></Link>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
              );
            })}
        </Tabs>
      </div>
    ) : null;
  }
}

MostRead.propTypes = {
  value: PropTypes.array,
  children: PropTypes.node,
  dispatch: PropTypes.any,
  params: PropTypes.any,
};

MostRead.fetchData = function({ dispatch, params }) {
  return dispatch(fetchDataIfNeeded(params));
};

function mapStateToProps(state) {
  return {
    ...state.mostread,
    config: state.config,
  };
}

export default connect(mapStateToProps)(MostRead);
