import React, { PureComponent } from 'react';

class AmazonWidget extends PureComponent {
  render() {
    const { category, phrase } = this.props;
    return (
      <div>
        <script
          // eslint-disable-next-line react/no-danger
          dangerouslySetInnerHTML={{
            __html: `amzn_assoc_placement = "adunit0";
                                amzn_assoc_search_bar = "false";
                                amzn_assoc_tracking_id = "nbt0b-21";
                                amzn_assoc_search_bar_position = "bottom";
                                amzn_assoc_ad_mode = "search";
                                amzn_assoc_ad_type = "smart";
                                amzn_assoc_marketplace = "amazon";
                                amzn_assoc_region = "IN";
                                amzn_assoc_title = "Shop Related Products";
                                amzn_assoc_default_search_phrase = "${phrase}";
                                amzn_assoc_default_category = "${category}";
                                amzn_assoc_linkid = "a30ec4c8ea3d4e43b502161e67b0c9ea";
                            `,
          }}
        />
        <script src="https://z-eu.amazon-adsystem.com/widgets/onejs?MarketPlace=IN" />
      </div>
    );
  }
}

export default AmazonWidget;
