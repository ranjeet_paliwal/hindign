import React from 'react';
import { Helmet } from 'react-helmet';

const siteConfig = 'https://navbharattimes.indiatimes.com';

export const PageMeta = props => {
  // let {meta} = props;
  const meta = props;
  return (
    <React.Fragment>
      {meta ? (
        <Helmet>
          {/* common meta tag */}
          <title>{meta.title}</title>
          <meta name="description" content={meta.desc} />
          <meta name="keywords" content={meta.key} />
          <link rel="canonical" href={meta.canonical} />
          {meta.pagetype == 'home' ? <meta content="Last-Modified" content={meta.modified} /> : ''}
          {meta.pagetype == 'home' ? (
            <meta name="msvalidate.01" content={meta.bingwebmasters} />
          ) : (
            ''
          )}
          <link rel="publisher" title={siteConfig.wapsitename} href={siteConfig.gplus} />

          {/* nofollow noindex */}
          {meta.noindex ? <meta content="NOINDEX,NOFOLLOW" name="robots" /> : ''}

          {/* Item Prop */}
          <meta itemProp="name" content={meta.title} />
          <meta itemProp="description" content={meta.desc} />
          <meta itemProp="image" content={meta.metaimg} />
          <meta itemProp="url" content={meta.redirectUrl ? meta.redirectUrl : meta.canonical} />
          {meta.publishcontent ? (
            <meta itemProp="datePublished" content={meta.publishcontent} />
          ) : (
            ''
          )}
          {meta.modifiedcontent ? (
            <meta itemProp="dateModified" content={meta.modifiedcontent} />
          ) : (
            ''
          )}
          <meta itemProp="provider" content={siteConfig.wapsitename} />

          {/* Twitter Card */}
          <meta content="summary" name="twitter:card" />
          <meta name="twitter:domain" content={siteConfig.weburl} />
          <meta name="twitter:title" content={meta.title} />
          <meta name="twitter:description" content={meta.desc} />
          <meta name="twitter:image" content={meta.metaimg} />
          <meta name="twitter:url" content={meta.redirectUrl ? meta.redirectUrl : meta.canonical} />
          <meta content="summary_large_image" name="twitter:card" />
          <meta name="twitter:site" content={siteConfig.wapsitename} />
        </Helmet>
      ) : null}
    </React.Fragment>
  );
};

export default { PageMeta };
