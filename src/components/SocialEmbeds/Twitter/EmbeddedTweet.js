import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { loadJS } from '../../../common-utility';

const Config = require(`../../../../common/${process.env.SITE}`);

export default class EmbeddedTweet extends Component {
  static propTypes = {
    /**
     * Tweet id that needs to be shown
     */
    tweetId: PropTypes.string.isRequired,
    /**
     * Additional options to pass to twitter widget plugin
     */
    options: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.embedContainer = React.createRef();
  }

  renderWidget() {
    if (!window.twttr) {
      console.error('Failure to load window.twttr in TwitterTweetEmbed, aborting load.');
      return;
    }
    if (!this.isMountCanceled) {
      window.twttr.widgets.createTweet(
        this.props.tweetId,
        this.embedContainer.current,
        this.props.options,
      );
    }
  }

  componentDidMount() {
    if (!window.twttr) {
      loadJS(Config.twitterWidgetJs, () => {
        this.renderWidget();
      });
    } else {
      this.renderWidget();
    }
  }

  componentWillUnmount() {
    this.isMountCanceled = true;
  }

  render() {
    return <div ref={this.embedContainer} className="embed_elements" />;
  }
}
