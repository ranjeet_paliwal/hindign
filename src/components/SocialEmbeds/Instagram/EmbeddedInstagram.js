import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { loadJS } from '../../../common-utility';

const Config = require(`../../../../common/${process.env.SITE}`);

export default class EmbeddedInstagram extends Component {
  static propTypes = {
    /**
     * Tweet id that needs to be shown
     */
    tweetId: PropTypes.string.isRequired,
    /**
     * Additional options to pass to twitter widget plugin
     */
    options: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.embedContainer = React.createRef();
  }

  renderWidget() {
    if (!window.instgrm) {
      console.error('Failure to load window.instgrm in EmbeddedInstagram, aborting load.');
      return;
    }
    if (!this.isMountCanceled) {
      window.instgrm.Embeds.process();
    }
  }

  componentDidMount() {
    if (!window.instgrm) {
      loadJS(Config.instagramWidgetJs, () => {
        this.renderWidget();
      });
    } else {
      this.renderWidget();
    }
  }

  componentWillUnmount() {
    this.isMountCanceled = true;
  }

  render() {
    return <div className="embed_elements">{this.props.children}</div>;
  }
}
