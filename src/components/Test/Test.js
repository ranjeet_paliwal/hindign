/* eslint-disable no-nested-ternary */
/* eslint-disable no-return-assign */
/* eslint-disable no-underscore-dangle */
/* eslint-disable max-len */
import React, { Component } from 'react';
import EsiAd from './EsiAds';
import siteConfig from './nbt';
// import fetch from '../../utils/fetch/fetch';
const content = '';
const content2 = {
  seodata: {
    title:
      'Hindi News: हिन्दी न्यूज़, Latest News in Hindi, Breaking Hindi News, लेटेस्ट हिंदी न्यूज़, ब्रेकिंग न्यूज़ | Navbharat Times',
    meta: [
      {
        name: 'description',
        content:
          'Hindi News: Get Breaking News in Hindi, Latest Hindi News and Hindi News Headlines. पढ़ें देश भर की ताज़ा ख़बरें, Taja Hindi Samachar,  लेटेस्ट हिंदी खबर, Navbharat Times Hindi News Website पर।',
      },
      {
        name: 'Keywords',
        content:
          'Hindi News, हिंदी न्यूज़, Latest News in Hindi, Latest Hindi News, Hindi News Headlines, हिन्दी ख़बर, Breaking News in Hindi, Breaking Hindi News, Hindi Newspaper, headlines in hindi, news headlines in hindi, Taja Samachar, Hindi Samachar',
      },
      { name: 'google-site-verification', content: 'ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw' },
      { name: 'msvalidate.01', content: '1E6490AA5F9D0438A03769318F2A1088' },
      { name: 'language', content: 'hindi' },
      { name: 'robots', content: 'noodp' },
      { name: 'viewport', content: '' },
      {
        name: 'apple-itunes-app',
        content: 'app-id= 656093141, app-argument=https://navbharattimes.indiatimes.com',
      },
      { name: 'twitter:card', content: 'summary' },
      {
        name: 'twitter:title',
        content:
          'Hindi News: हिन्दी न्यूज़, Latest News in Hindi, Breaking Hindi News, लेटेस्ट हिंदी न्यूज़, ब्रेकिंग न्यूज़ | Navbharat Times',
      },
      { name: 'twitter:url', content: 'https://navbharattimes.indiatimes.com' },
      { name: 'twitter:domain', content: 'https://navbharattimes.indiatimes.com' },
      { name: 'twitter:creator', content: '@NavbharatTimes' },
      { name: 'twitter:site', content: '@NavbharatTimes' },
      {
        property: 'og:title',
        content:
          'Hindi News: हिन्दी न्यूज़, Latest News in Hindi, Breaking Hindi News, लेटेस्ट हिंदी न्यूज़, ब्रेकिंग न्यूज़ | Navbharat Times',
      },
      {
        property: 'og:description',
        content:
          'Hindi News: Get Breaking News in Hindi, Latest Hindi News and Hindi News Headlines. पढ़ें देश भर की ताज़ा ख़बरें, Taja Hindi Samachar,  लेटेस्ट हिंदी खबर, Navbharat Times Hindi News Website पर।',
      },
      { property: 'og:site_name', content: 'Navbharat Times' },
      { property: 'og:type', content: 'website' },
      { property: 'og:url', content: 'https://navbharattimes.indiatimes.com' },
      { property: 'og:locale', content: 'hi_IN' },
      { property: 'og:image', content: 'https://static.langimg.com/photo/33600677.cms' },
      { 'http-equiv': 'Last-Modified', content: 'Wednesday, July 24, 2019  04:30:39 PM' },
      { 'http-equiv': 'Content-Language', content: 'hi' },
      { content: '2019', itemprop: 'copyrightYear' },
      {
        itemid: 'https://navbharattimes.indiatimes.com',
        itemtype: 'http://schema.org/Organization',
        itemprop: 'copyrightHolder provider sourceOrganization',
      },
      { itemprop: 'name', content: 'Navbharat Times' },
      { content: 'http://www.facebook.com/navbharattimes', property: 'article:publisher' },
    ],
    links: [
      { rel: 'mcanonical', href: 'https://m.navbharattimes.indiatimes.com' },
      { rel: 'canonical', itemprop: 'url', href: 'https://navbharattimes.indiatimes.com' },
      { rel: 'alternate', media: 'handheld', href: 'https://m.navbharattimes.indiatimes.com' },
    ],
  },
  esi: '',
  cad: {
    _col_up: 'true',
    '129249~1~home':
      "<div style=\"display:none;\"> </div><style> </style><style>.NgKRiGnc h3,.NgKRiGnc p {margin:0;padding:0;}.NgKRiGnc {height: 165px !important;float: left;padding-bottom: 0 !important; border-radius: 3px; width: 143px; margin-right: 19px; position: relative;min-height: 150px;}.NgKRiGnc > div > a:first-child{height: 108px !important; background-color: #e1e1e1;}.NgKRiGnc > div a img {max-width:100%;max-height:100%;margin:auto !important; position:absolute;left:0;right:0;top:0;bottom:0;}.NgKRiGnc > div a {width:100%;float: left;text-decoration: none;padding-bottom: 0 !important;cursor: pointer;position: relative; margin-bottom: 4px;}.NgKRiGnc > div h3 {word-break: normal;height: auto; float: left; color: #044e97; max-height: 41px;overflow: hidden;display: -webkit-box;-webkit-line-clamp: 2;    -webkit-box-orient: vertical; font-weight: normal; font-family: arial;font-size: 12px !important; line-height: 20px;padding-right: 22px;}.NgKRiGnc > div p {padding: 2px 6px 3px; height: auto; float: left; color: #ffffff; height: 36px !important; max-height: 18px!important;overflow: hidden; display: -webkit-box; -webkit-line-clamp: 1;-webkit-box-orient: vertical; width: 132px;font-size: 11px !important;font-family: arial;font-weight: bold;    background-color: #000; position: absolute; bottom: 0;line-height: 18px;opacity: 0.75; font-weight: bold; word-break: break-all;}.NgKRiGnc > div > a > div {float: left; height: 50px; width: 100%;}.NgKRiGnc > div h3:after { content: '';width: 22px;height: 22px; background-image:url(//navbharattimes.indiatimes.com/gineb/commons/images/colombia-icon.png); background-repeat: no-repeat; background-position:top left; top: 20px;right: 0;position: absolute;display: inline-block;}.NgKRiGnc:hover > div h3::after {background-image:url(//navbharattimes.indiatimes.com/gineb/commons/images/colombia_red_small.png); background-repeat: no-repeat;background-position:top left;}.NgKRiGnc > div > div {display:none;}.NgKRiGnc:hover > div > div {position: absolute; right: 0; top: 0;display: block !important; width: 20px; height: 20px;}.NgKRiGnc > div > div a, .NgKRiGnc > div > div span { height: 20px !important;}.NgKRiGnc > div > div > a > img { width: 19px !important;height:15px !important;}</style><script> </script><script> </script><div style=\"display:none;\"> </div><div class=\"NgKRiGnc ctQONlcn \">    <div class=\"KHdxgmi\" ><a onclick=\"dNiBSpGDrBZ('//navbharattimes.indiatimes.com/bdcnp/74308696.cms?RJb=ycjf&fpc=8e9301c3-13df-4ce4-843c-ca5a46b98f79-6mhk&hT=9OK9&i=32845517&GV=bO65&r=NzVlZDgyYjktOWFlOS00ZDZlLWIwYzAtYzVjMjNjMWRmNjIxLTFzandrOjEyOTI0OTo5bms6MzQ1MDk1MzE6Mjo1MjU5OTgzOjEwMzU6MDowOjQuOTAyMTAwMDAwMDAwMDAxOmhvbWU6MToxOjkxMDowOjo6Mjo0NS4xMjQuMTA4LjY1Ok1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzc1LjAuMzc3MC4xMDAgU2FmYXJpLzUzNy4zNjo6MC4wOjA6MDoyMDU0NDgtMTo2OjE6MTowOjI6OjoxNTYzOTY2MDQwMjEwOmZhbHNlOjczMjIwOjE&s=navbharattimes.indiatimes.com%252Fweb_meta_test%252F696089404.cms&u=aHR0cHM6Ly9zZXJ2aW5nLnBsZXhvcC5uZXQvcHNlcnZpbmcvYnJpZGdlXzAwMi5odG0_YT00JnQ9aHR0cHMlM2ElMmYlMmZwcmVnLjcwVHJhZGVzLmNvbSUyZmFzZXJ2aW5nJTJmNCUyZjElMmYxNTk5JTJmNDFfZW5fNTg5NjEuaHRtJTNmY3BsJTNkMzkyOTAlMjZweGwlM2QzJTI2U2VyaWFsSWQlM2QxMTg4NzQ1JTI2Rm9ybUlkJTNkMjA3NiZhZHY9MSZmPTE4ODUzNQ')\" rel=\"nofollow\"><img src=\"//navbharattimes.indiatimes.com/gineb/910/images/8/1814016171fbfda033466f42be716c84_1562852032506_0.jpg\"><p>Ad: 70trades.com</p></a><a onclick=\"dNiBSpGDrBZ('//navbharattimes.indiatimes.com/bdcnp/74308696.cms?RJb=ycjf&fpc=8e9301c3-13df-4ce4-843c-ca5a46b98f79-6mhk&hT=9OK9&i=32845517&GV=bO65&r=NzVlZDgyYjktOWFlOS00ZDZlLWIwYzAtYzVjMjNjMWRmNjIxLTFzandrOjEyOTI0OTo5bms6MzQ1MDk1MzE6Mjo1MjU5OTgzOjEwMzU6MDowOjQuOTAyMTAwMDAwMDAwMDAxOmhvbWU6MToxOjkxMDowOjo6Mjo0NS4xMjQuMTA4LjY1Ok1vemlsbGEvNS4wIChYMTE7IExpbnV4IHg4Nl82NCkgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzc1LjAuMzc3MC4xMDAgU2FmYXJpLzUzNy4zNjo6MC4wOjA6MDoyMDU0NDgtMTo2OjE6MTowOjI6OjoxNTYzOTY2MDQwMjEwOmZhbHNlOjczMjIwOjE&s=navbharattimes.indiatimes.com%252Fweb_meta_test%252F696089404.cms&u=aHR0cHM6Ly9zZXJ2aW5nLnBsZXhvcC5uZXQvcHNlcnZpbmcvYnJpZGdlXzAwMi5odG0_YT00JnQ9aHR0cHMlM2ElMmYlMmZwcmVnLjcwVHJhZGVzLmNvbSUyZmFzZXJ2aW5nJTJmNCUyZjElMmYxNTk5JTJmNDFfZW5fNTg5NjEuaHRtJTNmY3BsJTNkMzkyOTAlMjZweGwlM2QzJTI2U2VyaWFsSWQlM2QxMTg4NzQ1JTI2Rm9ybUlkJTNkMjA3NiZhZHY9MSZmPTE4ODUzNQ')\" rel=\"nofollow\"><div><h3>Take Control Of Your Finance, Join Our Trading Community</h3></div></a></div></div><script> </script><!-- 129249 1 --><div style=\"display:none;\"> </div><div style=\"display:none;\"> </div><script>        var coldetect;            function dNiBSpGDrBZ(par)      {     if(typeof otab == 'function'){ otab(par,'');   } else{if(typeof canRun == 'undefined' || (typeof canRun != 'undefined' && !canRun)) {                 window.open(par,'_self');}else{window.open(par,'_blank'); }        }        };  (function(){  if (window.frameElement !== null){  window.canRun = true;document.body.style.margin=\"0px\"; window.frameElement.style.height = '180px';  }  })();try{trev('//navbharattimes.indiatimes.com/nbnpn/notify.htm?d=%7B%22skuIds%22%3A%2234509531-5259983-0-1-0%22%2C%22fdId%22%3A0%2C%22imprId%22%3A%2275ed82b9-9ae9-4d6e-b0c0-c5c23c1df621-1sjwk%22%2C%22adsltId%22%3A%22129249%22%2C%22fpc%22%3A%228e9301c3-13df-4ce4-843c-ca5a46b98f79-6mhk%22%2C%22pv%22%3A%22PV_MACRO%22%2C%22i%22%3Afalse%2C%22ci%22%3A%2232845517%22%2C%22ist%22%3A1563966040210%2C%22vst%22%3A%22ETS%22%2C%22o%22%3Afalse%7D','NgKRiGnc');tpImp([[]],'NgKRiGnc',[]);}catch(e){};</script>",
    _col_ab_call:
      '<script>try{axs("//navbharattimes.indiatimes.com/acms/jsAds/getContent?maxads=-1151124339&IwVx=-1336196560&adslot=-463885566")}catch(e){};</script>\n',
    _col_script:
      '<script>function otab(t,n){var e="_blank";try{e="undefined"==typeof isq()||"undefined"!=typeof isq()&&isq()?"_top":"_blank"}catch(c){}window.open(t,e)}function trev(t,n){try{"undefined"==typeof ntsss.getInstance().cd[n]&&ntsss.getInstance().push([t],n)}catch(e){}}function tpImp(t,n,e){try{if(""!=n)for(var c=0;c<t.length;c++)ntsss.getInstance().push(t[c],n)}catch(r){}}var ntsss=function(){var t,n=function(){var t,n=1,e=!1,c=[],r=[],o=!1,i=[],a=function(t,n){function e(){try{if(!o){o=!0;for(var t=0;t<r.length;t++)r[t].fn.call(window,r[t].ctx);r=[]}}catch(n){}}function c(){try{"complete"===document.readyState&&e()}catch(t){}}try{t=t||"ready",n=n||window;var r=[],o=!1,i=!1;n[t]=function(t,n){try{if(o)return void setTimeout(function(){t(n)},1);r.push({fn:t,ctx:n}),"complete"===document.readyState||!document.attachEvent&&"interactive"===document.readyState?setTimeout(e,1):i||(document.addEventListener?(document.addEventListener("DOMContentLoaded",e,!1),window.addEventListener("load",e,!1)):(document.attachEvent("onreadystatechange",c),window.attachEvent("onload",e)),i=!0)}catch(a){}}}catch(a){}},u=function(){try{t="",a("ready",this),ready(function(){o=!0,n()});var n=h(s,500);window.onscroll=function(){n()},window.addEventListener?window.addEventListener("scroll",function(){n()}):window.attachEvent&&window.attachEvent("scroll",function(){n()})}catch(e){}},d=function(){try{t!=window.location.href&&(n=1,t=window.location.href,e=!0,"function"==typeof fcomp&&fcomp())}catch(c){}},f=function(t,n){try{"undefined"==typeof c[n]?c[n]={imprurl:t,classname:n,notify:!1}:"undefined"==typeof r[n]&&(c[n].imprurl=c[n].imprurl.concat(t),r[n]=1),o&&s()}catch(e){}},s=function(){try{d();for(var t in c){var r=c[t];if(r.hasOwnProperty("notify")&&!r.notify){var o=m(r.classname);if(o.length>0&&l(o[0])){r.notify=!0,1==n&&(n=0,r.imprurl[0]=r.imprurl[0].indexOf("?")>-1?r.imprurl[0]+"&pv=1":r.imprurl[0]+"?pv=1");for(var a=0;a<r.imprurl.length;a++)y(r.imprurl[a]);var u=c.splice(t,1);u=null}}}e&&i.length>0&&"undefined"!=typeof nnnmm&&(e=!1,nnnmm.getInstance().ajxc(i.shift()))}catch(f){}},l=function(t){try{if(t.getBoundingClientRect){var n=t.getBoundingClientRect(),e=window.innerHeight||document.body.clientHeight||document.documentElement.clientHeight,c=n.top,r=n.bottom,o=r-c,i=e>=c&&c+o>=0;return 0==c&&0==r?!1:i}return!1}catch(a){return!1}},m=function(t){try{var n=[],e="."+t;return"undefined"!=typeof document.querySelectorAll?n=document.querySelectorAll(e):(document.getElementsByClassName=function(t){for(var n=[],e=this.getElementsByTagName("*"),c=0;c<e.length;c++)(" "+e[c].className+" ").indexOf(" "+t+" ")>-1&&n.push(e[c]);return n},n=document.getElementsByClassName(t)),n}catch(c){}},y=function(t){try{return(new Image).src=t,!0}catch(n){return!1}},h=function(t,n,e){try{n||(n=250);var c,r;return function(){var o=e||this,i=+new Date,a=arguments;c&&c+n>i?(clearTimeout(r),r=setTimeout(function(){c=i,t.apply(o,a)},n)):(c=i,t.apply(o,a))}}catch(o){}};return u(),{push:f,setpv:d,ajxcArr:i,cd:c,tp:r}};return{getInstance:function(){return t||(t=n()),t}}}();\nfunction axs(){ntsss.getInstance().ajxcArr.push(arguments[0])}function isq(){return nnnmm.getInstance().isq()}var nnnmm=function(){var t,e=function(){function t(){n=new y}function e(t,e){var r=s.createElement("div");try{r.style.cssText="height: 1px !important; width: 1px !important; position: absolute !important; top: -1234px !important; left: \u201412345px !important;",r.className=e,r.id=t,s.body.appendChild(r)}catch(n){}return r}function r(){var t=e("AdBanner","colombiatracked adBox ctn_ads_rhs_organic ctn_ads_twins");setTimeout(function(){try{var e=t.getBoundingClientRect();e.height>0?l("@l@"):C()}catch(r){}},500)}var n,o=0,a="",c=window.location.host,i="ce_nbapd",d="@v0",h=0,u=!0,f=!1,s=document,g=function(){var t=!1;try{var e=n.decode(v(i)).split("@");e.length>0?t="l"==e[1]?!1:!0:m(a)}catch(r){}return t},l=function(t){try{S(i,n.encode(c+t+(new Date).getTime(),"321","e"),"90")}catch(e){}},m=function(t){h=0,o=0,u=!0,p(t)},p=function(t){var e=new XMLHttpRequest,n="1500";a=t,e.onreadystatechange=function(){(1===e.readyState||4===e.readyState)&&0===e.status&&u&&l("@pa@0"),e.status>0&&u&&(u=!1,l("@l@"))},e.ontimeout=function(t){f?C():(f=!0,r())},e.open("GET",t,!0),e.timeout=Number(n)+h,e.send()},C=function(){var t="2";u&&(o<Number(t)?(h+=1e3,p(a),o++):l("@pt@0"))},v=function(t){try{for(var e=t+"=",r=s.cookie.split(";"),n=0;n<r.length;n++){for(var o=r[n];" "==o.charAt(0);)o=o.substring(1,o.length);if(0==o.indexOf(e))return o.substring(e.length,o.length)}}catch(a){}return null},S=function(t,e,r){try{var n;r?(date=new Date,date.setTime(date.getTime()+24*r*60*60*1e3),n="; expires="+date.toGMTString()):n="";var o=window.location.host;1===o.split(".").length?s.cookie=t+"="+e+n+"; path=/":(domain=(window.location.host.match(/([^.]+)\\.\\w{2,3}(?:\\.\\w{2})?$/)||[])[0],s.cookie=t+"="+e+n+"; path=/; domain="+domain)}catch(a){}},A={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(t){var e,r,n,o,a,c,i,d="",h=0;for(t=A._utf8_encode(t);h<t.length;)e=t.charCodeAt(h++),r=t.charCodeAt(h++),n=t.charCodeAt(h++),o=e>>2,a=(3&e)<<4|r>>4,c=(15&r)<<2|n>>6,i=63&n,isNaN(r)?c=i=64:isNaN(n)&&(i=64),d=d+this._keyStr.charAt(o)+this._keyStr.charAt(a)+this._keyStr.charAt(c)+this._keyStr.charAt(i);return d},encodeUrlSafe:function(t){return A.encode(t).replace(/\\+/g,"-").replace(/\\//g,"_").replace(/\\=/g,"~")},decode:function(t){var e,r,n,o,a,c,i,d="",h=0;for(t=t.replace(/[^A-Za-z0-9\\+\\/\\=]/g,"");h<t.length;)o=this._keyStr.indexOf(t.charAt(h++)),a=this._keyStr.indexOf(t.charAt(h++)),c=this._keyStr.indexOf(t.charAt(h++)),i=this._keyStr.indexOf(t.charAt(h++)),e=o<<2|a>>4,r=(15&a)<<4|c>>2,n=(3&c)<<6|i,d+=String.fromCharCode(e),64!=c&&(d+=String.fromCharCode(r)),64!=i&&(d+=String.fromCharCode(n));return d=A._utf8_decode(d)},decodeUrlSafe:function(t){return A.decode(t.replace(/\\-/g,"+").replace(/\\_/g,"/").replace(/\\~/g,"="))},_utf8_encode:function(t){t=t.replace(/\\r\\n/g,"\\n");for(var e="",r=0;r<t.length;r++){var n=t.charCodeAt(r);128>n?e+=String.fromCharCode(n):n>127&&2048>n?(e+=String.fromCharCode(n>>6|192),e+=String.fromCharCode(63&n|128)):(e+=String.fromCharCode(n>>12|224),e+=String.fromCharCode(n>>6&63|128),e+=String.fromCharCode(63&n|128))}return e},_utf8_decode:function(t){for(var e="",r=0,n=c1=c2=0;r<t.length;)n=t.charCodeAt(r),128>n?(e+=String.fromCharCode(n),r++):n>191&&224>n?(c2=t.charCodeAt(r+1),e+=String.fromCharCode((31&n)<<6|63&c2),r+=2):(c2=t.charCodeAt(r+1),c3=t.charCodeAt(r+2),e+=String.fromCharCode((15&n)<<12|(63&c2)<<6|63&c3),r+=3);return e}},y=function(){function t(t,e){var r=t.length/2;return 0==e?t.substring(0,r):t.substring(r,t.length)}function e(t){for(var e=[],r=0,n=t.length;n>=r;r++)e.push(t.charAt(n-r));return e.join("")}function r(r){var n=e(t(r,0))+t(r,1),a=o(n,"321","e")+d;return A.encodeUrlSafe(a)}function n(r){var n=A.decodeUrlSafe(r).replace(d,""),a=o(n,"321","d");return e(t(a,0))+t(a,1)}var o=function(t,e,r){for(var n="",o=0,a=t,c=0;c<a.length;c++){var i=e[o],d="";o<e.length-1?o++:o=0,d="d"==r?parseInt(a[c].charCodeAt(0))-parseInt(i):parseInt(a[c].charCodeAt(0))+parseInt(i),n+=String.fromCharCode(d)}return n},e=function(t){for(var e=[],r=0,n=t.length;n>=r;r++)e.push(t.charAt(n-r));return e.join("")};return{encode:r,decode:n}};return t(),{isq:g,ajxc:m,ob:!1,ck:(new y).decode}};return{getInstance:function(){return t||(t=e()),t}}}();\n</script>\n<noscript><iframe style="display:none;visibility:hidden" width="0" height="0" src="/a_nbt_tpircs.cms"></iframe></noscript>\n',
  },
};

class Test extends Component {
  // cookie Name : c_ut( either of 4,5,6,7 if ad blocker is on) else random value
  // https://eisamay.indiatimes.com/wesi.cms
  // <script async="" src="https://static.clmbtech.com/ctn/commons/js/colombia_v2.js"></script>

  state = {
    content: '',
    esi: false,
  };

  componentDidMount() {
    const _this = this;
    console.log('PropsInfo1', this.props);
    // console.log("=======componentDidMount=========");

    // fetch('https://navbharattimes.indiatimes.com/web_meta_test/696089404.cms?feedtype=sjson')
    //   .then(response => {
    //     return response.json();
    //   })
    //   .then(data => {
    //     // console.log('=======data=========');
    //     // console.log(data);
    //     this.setState({
    //       content: data,
    //       esi: true,
    //     });
    //   })
    //   .catch(ex => {
    //     // console.log('exception : ', ex);
    //   });

    const esiParents = document.querySelectorAll('#esi_parent');
    if (esiParents != undefined) {
      esiParents.forEach((element, index) => {
        // Get inner data sting and create HTML at run time
        const esiData = element.querySelector('#esi_con');
        const text = esiData.innerText;
        const esicon = document.createElement('div');
        esicon.innerHTML = text;
        const parent = document.getElementById('esi_parent');
        element.insertBefore(esicon, element.lastChild);
        element.removeAttribute('id');
        _this.getScriptTags(text, esiData);
      });
    }
  }

  // Convert String to HTML object
  getScriptTags = (str, ele) => {
    const reg = /<script\b[^>]*>([\s\S]*?)<\/script>/gm;
    let match;
    while ((match = reg.exec(str))) {
      const src = document.createElement('script');
      src.innerHTML = match[1];
      ele.innerHTML = '';
      ele.appendChild(src);
      ele.removeAttribute('id');
      ele.style.display = 'block';
      // return src;
    }
  };

  render() {
    const _this = this;
    console.log('PropsInfo', this.props);
    // let {adtype, mstype, elemtype, ctnstyle, content, query, esiad, pagetype, index} = _this.props;
    // const { esiad } = _this.props;
    // onst {  content } = this.state;
    // let index = 2;
    const index = index && index != undefined ? index : 1;

    // console.log('=======content=========');
    // console.log(content);

    // const pos = 'header';
    const pagetype = 'homepage';
    const mstype = 'atf';
    const slotname = mstype || ctnstyle;

    // check for ESI
    // let showEsi = process.env.SITE == 'eisamay' && esiad ? true : false;
    const showEsi = true;
    const esi = !!(content && content.cad && content.cad != undefined && showEsi);

    const slots =
      siteConfig.ads.ctnslots && siteConfig.ads.ctnslots[pagetype]
        ? siteConfig.ads.ctnslots[pagetype]
        : null;

    // let msid =
    //   pagetype == 'photoshow' && content && content.it && content.it.id
    //     ? content.it.id
    //     : content && content.id
    //     ? content.id
    //     : '';

    const elemtype = 'li';
    const ctnstyle = 'div';

    const adtype = 'ctn';

    const slot = siteConfig.ads.ctnslots[slotname];
    const slotid = `${slot}-${index}-0`;
    const slotesi = `${slot}~${index}~home`;

    // console.log('index :' + index);
    // console.log('slot :' + slot);
    // console.log('slotid :' + slotid);
    // console.log('slotesi :' + slotesi);

    // console.log('process.env.SITE :' + process.env.SITE);
    // console.log('slots :' + slots);
    // console.log('slotname :' + slotname);
    // console.log('esi :' + esi);
    // console.log('adtype :' + adtype);

    const query = this.props.location.query;
    // console.log('Query String', query);

    return (
      // this for CSR ESI only
      esi == true ? (
        <div>
          esi ctn ads render
          {content.cad._col_script && content.cad._col_ab_call ? (
            <div>
              {
                // window != undefined && window.esi != true ?
                <span id="esi_col" style={{ display: 'none' }}>
                  {content.cad._col_script}
                </span>
                // :
                // null
              }
              {
                // window != undefined && window.colab == true ?
                <span id="esi_ab" style={{ display: 'none' }}>
                  {content.cad._col_ab_call}
                </span>
                // :
                // null
              }
            </div>
          ) : null}
          <div id="esi_parent">
            <div id="esi_con" style={{ display: 'none' }}>
              {content.cad[slotesi]}
            </div>
          </div>
        </div>
      ) : // this for SSR ESI only
      // ((query && query.wesi == 1 && showEsi && slots != undefined) || (_isCSR() && window && window.isesi == 1 && showEsi && slots != undefined)) ?
      query && query.wesi == 1 && showEsi && slots != undefined ? (
          <div>
          {' '}
          wesi ctn ads render
          <div className="esicon">
              {/* Ftech */}

              {/* {pos == 'header' ? EsiAd({adunit:slotname}).ssr({slots:slots, type:'fetch'}) : null} */}
              {EsiAd({ adunit: slotname }).ssr({ slots, type: 'fetch' })}
              {/* {slotname == 'ctnshow' ? EsiAd({adunit:slotname}).ssr({slots:slots, type:'fetch'}) : null} */}

              {/* Draw */}
              {/* {pos == 'body' ? EsiAd({adunit:slotname}).ssr({slots:slots, type:'slot'}) : null} */}
              {EsiAd({ adunit: slotname }).ssr({ slots, type: 'slot' })}
              {/* {EsiAd({adunit:slotname}).adComment({type:'slot'})} */}

              {/* {_isCSR() && window != undefined ? window.esi = true : false} */}
            </div>
        </div>
      ) : // JS Ads
      adtype && adtype == 'ctn' ? (
        elemtype && elemtype == 'li' ? (
              <div>
            js li ctn render
            <li
                  ctn-style={ctnstyle}
                  id={`div-clmb-ctn-${slotid}`}
                  data-slot={slot}
                  data-position={index}
                  data-section="0"
                  data-plugin="ctn"
                  className="ad1 listBlyBlk colombia js rendering"
                  ref={ele => (_this.adElem = ele)}
                />
          </div>
        ) : (
              <div>
            js div ctn render
            <div
                  ctn-style={ctnstyle}
                  id={`div-clmb-ctn-${slotid}`}
                  data-slot={slot}
                  data-position={index}
                  data-section="0"
                  data-plugin="ctn"
                  className="ad1 listBlyBlk colombia js rendering"
                  ref={ele => (_this.adElem = ele)}
                />
          </div>
        )
      ) : elemtype && elemtype == 'li' ? (
            <div>
          js li ctn render
          <li
                ctn-style={ctnstyle}
                id={`div-clmb-ctn-${slotid}`}
                data-slot={slot}
                data-position={index}
                data-section="0"
                data-plugin="ctn"
                className="ad1 listBlyBlk colombia js rendering"
                ref={ele => (_this.adElem = ele)}
              />
        </div>
      ) : (
            <div>
          js div ctn render
          <div
                ctn-style={ctnstyle}
                id={`div-clmb-ctn-${slotid}`}
                data-slot={slot}
                data-position={index}
                data-section="0"
                data-plugin="ctn"
                className="ad1 listBlyBlk colombia js rendering"
                ref={ele => (_this.adElem = ele)}
              />
        </div>
      )
    );
  }
}

export default Test;
