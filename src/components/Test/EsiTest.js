render() {
    let _this = this;
    let {adtype, mstype, elemtype, ctnstyle, content, query, esiad, pagetype} = _this.props;
    let slotname = mstype ? mstype : ctnstyle;
    //check for ESI
    let showEsi = process.env.SITE == 'eisamay' && esiad ? true : false;
    let esi = (content && content.cad && content.cad != undefined && showEsi) ? true : false;
    let slots = (siteConfig.ads.ctnslots && siteConfig.ads.ctnslots[pagetype]) ? siteConfig.ads.ctnslots[pagetype] : null
    let msid = (pagetype == 'photoshow' && content && content.it && content.it.id) ? content.it.id : (content && content.id ? content.id : '')
    
    return (
        // this for CSR ESI only
        (esi == true) ?
          <div>
            {
              content.cad._col_script && content.cad._col_ab_call ? 
              <div>
                {
                  window != undefined && window.esi != true ? 
                  <span id="esi_col" style={{display:'none'}}>{content.cad._col_script}</span>
                  :
                  null
                }
                {
                  window != undefined && window.colab == true ?
                  <span id="esi_ab" style={{display:'none'}}>{content.cad._col_ab_call}</span>
                  :
                  null
                }
              </div>
              :
              null
            }
            <div id="esi_parent">
              <div id="esi_con" style={{display:'none'}}>{content.cad[siteConfig.ads.ctnslots[slotname] + '~' + msid + '~0']}</div>
            </div>
          </div>
        :
        // this for SSR ESI only
        ((query && query.wesi == 1 && showEsi && slots != undefined) || (_isCSR() && window && window.isesi == 1 && showEsi && slots != undefined)) ? 
          <div className="esicon">
            {/* Ftech */}

            {slotname == 'atf' ? EsiAd({adunit:slotname}).ssr({slots:slots, type:'fetch'}) : null}

            {/* {slotname == 'ctnshow' ? EsiAd({adunit:slotname}).ssr({slots:slots, type:'fetch'}) : null} */}
            
            {/* Draw */}
            {EsiAd({adunit:slotname}).ssr({slots:slots, type:'slot'})}
            {/* {EsiAd({adunit:slotname}).adComment({type:'slot'})} */}

            {_isCSR() && window != undefined ? window.esi = true : false}
          </div>
        :
        // JS Ads
        (adtype && adtype == 'ctn') ?
        
            (elemtype && elemtype == "li" ? 
            <li ctn-style={ctnstyle} data-slot={mstype} data-plugin="ctn" className = "ad1" ref = {(ele) => _this.adElem = ele}></li>
            : 
            <div ctn-style={ctnstyle} data-slot={mstype} data-plugin="ctn" className = "ad1" ref = {(ele) => _this.adElem = ele}></div>)
        
          :
        
            (elemtype && elemtype == "li" ? 
            <li data-adtype={mstype} className = {"ad1 " + mstype} ref = {(ele) => _this.adElem = ele}></li>
            : 
            <div data-adtype={mstype} className = {"ad1 " + mstype} ref = {(ele) => _this.adElem = ele} style={_this.props.style}></div>)
          
        
    )
  }


}
