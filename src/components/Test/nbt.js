module.exports = {
  wapsitename: 'EI Samay',
  wapsiteregionalname: 'এই সময়',
  languagemeta: 'bn',
  language: 'Bengali',
  channelCode: 'eisamay',
  languagefullName: 'Bengali News',
  androidpackageid: 2147477985,
  androidpackage: 'com.eisamay.reader',
  deeplinkappid: 'eisamayreaderactivities',
  deeplinkpubid: 'pub=4:EI Samay',
  iosapp_name: 'EI Samay App',
  weburl: 'https://eisamay.indiatimes.com',
  mweburl: 'https://eisamay.indiatimes.com',
  cookieLessDomain: 'https://static.nbt.indiatimes.com',
  footerDesktopLink: 'https://eisamay.indiatimes.com?mobile=no',
  domain: 'indiatimes.com',
  webdomain: 'eisamay.indiatimes.com',
  mwebdomain: 'eisamay.indiatimes.com',
  photogallerydomain: 'photogallery.eisamay.indiatimes.com',
  logo: 'nbt-logo.jpg',
  logo_id: 'https://eisamay.indiatimes.com/photo/54938689.cms',
  fb: 'https://www.facebook.com/eisamay.com/',
  twitter: 'https://twitter.com/eisamay',
  gplus: 'https://plus.google.com/+EisamayOfficial',
  fullutm: '&utm_campaign=eismobile&utm_medium=referral',
  slikeshareutm: 'utm_source=slikevideo&utm_medium=referral',
  shortutm: 'mz', // Using for micron
  icon: 'https://eisamay.indiatimes.com/icons/ei-samay_favicon.ico',
  breakingNewsFeedUrl: 'https://langnetstorage.indiatimes.com/LANGBNews/eisamaybnews.htm',
  fbsdkurl: 'https://connect.facebook.net/bn_IN/sdk.js#xfbml=1&version=v3.0',
  fbpage: 'https://www.facebook.com/eisamay.com/',
  livebloghpfeed: 'https://langnetstorage.indiatimes.com/Score/pwa_eisamayLiveblog_hp.htm',
  livebloghostname: 'es', // Using to getch liveblog callback feed
  navfeedurl: 'https://eisamay.indiatimes.com/off-url-forward/wdt_navjson_v2.cms',
  liveblogbnbewsurl: 'https://eisamay.indiatimes.com/bangla-news-live/liveblog/62537291.cms',
  // logo_schema dimension should be 600*60
  logo_schema: 'https://static.langimg.com/thumb/msid-54938689,width-140,resizemode-4/ei-samay.jpg',
  siteid: '2147477985',
  fbpixelid: '',
  css:
    '$body-bgcolor:#fff;    $navbar-top-bgcolor:#fff;    $navbar-bg-color:#F5CC10;    $navbar-border-top:#c8cccb;    $navbar-border-shadow:#D8B40D;    $navbar-active-border-bottom-color:#000;    $navbar-link-color:#000;    $hamburger-bg-color:#747474;    $sidenav-bg-color:#fff;    $sidenav-color:#000;    $sidenav-border-bottom:#F5F5F5;    $sidenav-arrow-color:#D1D1D1;    $layer-bg:rgba(0,0,0,0.5);    $breadcrumb-home-color:#DA4848;    $card-border-bottom:#F5F5F5;    $card-bg-color:#8e8e93;    $card-text-color:#191919;    $card-date-color:#bababa;    $home-section-border-bottom:#f5f5f5;    $home-section-heading-border-bottom:#F5CC10;    $home-section-arrow-color:#000000;    $home-section-more-color:#858585;    $home-section-more-border:#CCCCCC;    $footer-background:#585858;    $footer-border-top:#333;    $footer-border-bottom:#7b7b7b;    $footer-heading-color:#e6e6e6;    $footer-text-color:#fff;    $footer-link-color:#d0d0d0;    $footer-trending-color:#95989a;    $footer-copyright-color:#d0d0d0;',
  fontContent:
    "@font-face {font-family: NotoSans Regular;src: url('https://static.eis.indiatimes.com/fonts/NotoSansBengali-Regular.eot');src: url('https://static.eis.indiatimes.com/fonts/NotoSansBengali-Regular.eot?#iefix') format('embedded-opentype'),url('https://static.eis.indiatimes.com/fonts/NotoSansBengali-Regular.woff') format('woff'),url('https://static.eis.indiatimes.com/fonts/NotoSansBengali-Regular.ttf') format('truetype');font-weight: normal;font-style: normal;font-display: swap;}",
  locale: {
    live_blog: 'লাইভ ব্লগ',
    commentary: 'Commentary',
    scorecard: 'Scorecard',
    photo_maza: 'ফটো গ্যালারি',
    key_words: 'এই বিষয়ে আরও পড়ুন:',
    more_from_topics: 'এই বিষয়ে আরও পড়ুন',
    social_download: {
      title: 'এই সময় পড়ুন ও সব খবরের আপডেট পান',
      fblike: 'ফেসবুক পেজ লাইক করুন।',
      appdownload: 'অ্যাপ ডাউনলোড করুন',
      extrasharetxt: '\n\nখবর ভালো লেগেছে?',
      fblikeURL:
        'https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Feisamay.com%2Fdocs%2Fplugins%2F&width=87&layout=button_count&action=like&show_faces=false&share=false&height=21&appId=154495568087154',
    },
    share_popup_header: 'শেয়ার করুন',
    read_more: 'Read More...',
    read_less: 'Read Less...',
    view_gallery_from_start: 'শুরু থেকে দেখুন',
    next_gallery: 'পরবর্তী ফটো গ্যালারি',
    next_article: {
      moviereview: 'পরের রিভিউ',
      articleshow: 'পরের খবর',
      default: 'পরের খবর',
    },
    search_placeholder: 'সার্চ করুন',
    search: 'সার্চ',
    highlight: 'হাইলাইটস',
    orientation_heading: 'ডিভাইসটি ঘুরিয়ে নিন',
    orientation_text:
      'আমরা এখনই ল্যান্ডস্কেপ মোডকে সাপোর্ট করছি না। আপনাকে পোর্ট্রেট মোডে ফিরে যেতে অনুরোধ করা হচ্ছে।',
    videos: 'ভিডিয়ো',
    related_videos: 'সম্পর্কিত ভিডিয়ো',
    default_horizontal_tile_width: 133, // These widths are used for slider
    default_slideshow_tile_width: 120,
    related_articles_horizontal_tile_width: 230,
    relatedarticle: 'সম্পর্কিত খবর',
    read_more_listing: ' থেকে আরও',
    critics_review: 'সমালোচকের রিভিউ',
    twitte_reaction: 'ট্যুইটারে চর্চা',
    critics_rating: 'সমালোচকের রেটিং',
    movie_review: 'মুভি রিভিউ',
    film_review: 'মুভি রিভিউ',
    review_movie: 'রিভিউ লিখুন',
    rate_movie: 'রেটিং দিন',
    slide_to_rate_movie: 'রেটিং দিতে স্কেল স্লাইড করুন',
    reader_rating: 'পাঠকের রেটিং',
    movie_review_tile_bg: 'https://static.langimg.com/moviereview_list_bg/photo/65022261.cms',
    movie_review_detail_bg: 'https://static.langimg.com/moviereview_list_bg/photo/65594589.cms',
    breaking_news: 'ব্রেকিং নিউজ',
    live_video: 'লাইভ ভিডিয়ো', // Using for minitv widget
    view_text: 'দেখুন',
    aur_jaane: 'আরও পড়ুন',
    oops: 'দুঃখিত. ',
    thanks: 'ধন্যবাদ.. ',
    offlineText: 'আপনি অফলাইন হয়েছেন।',
    onlineText: 'এবার আপনি অনলাইন।',
    openinapp: 'অ্যাপে পড়ুন',
    live_updates: 'লাইভ আপডেট',
    footerheading: 'খবর এক ঝলকে', // News at Glance
    fblikecardtext: 'এই সময় ফেসবুক পেজ <br /> লাইক করুন ', // Using in Service Drawer
    appdownloadcardtext: 'এই সময় অ্যাপের সঙ্গে <br/> যুক্ত হন', // Using in Service Drawer
    installapp: 'ইনস্টল করুন',
    login: 'লগ ইন',
    logout: 'লগ আউট',
    seemoreimages: 'এমন আরও ফটো দেখুন', // Using in Photomazzashow
    read_more_liveblog: 'আরও পড়ুন',
    crossbrand: 'এই খবরটি আগে hyperlink- এ- dl- তারিখে পাবলিশ হয়েছিল',
    appexclusive: 'অ্যাপ এক্সক্লুসিভ',
    votesuccessfully: 'আপনার ভোট গ্রহণ করা হয়েছে',
    votealready: 'আপনি আগেই ভোট দিয়েছেন',
    thanks: 'ধন্যবাদ',
    submit_vote: 'ভোট দিন',
    photoshow_app_install: {
      read_more: 'আরও ফটো দেখতে অ্যাপ ইনস্টল করুন',
      download: 'অ্যাপ ডাউনলোড করুন',
    },
    comment_text: 'মন্তব্য করুন',
    download_text: 'ডাউনলোড',
    wp_broadcast: {
      description: 'দিনের সেরা খবর এবার হোয়াটসঅ্যাপে',
      btnText: 'সাবসক্রাইব',
      url:
        'https://widget.whatsbroadcast.com/widget_more/8531f3a382edfe5c49a332a5fb08a2aa/?show=walink&text=',
    },
    disclaimer: 'ডিসক্লেইমার ',
    live_tv: 'LIVE TV',
    data_hub: 'ডেটা হাব',
    fantasy_game: 'আপনি যখন অর্থমন্ত্রী',
  },
  imageconfig: {
    thumbid: '67091961',
    liveblogthumb: '56295200',
    imagedomain: 'https://static.langimg.com/thumb/msid-',
    bigimage: 'width-540,resizemode-4', // Article Inline Images, will maintain aspect ratio of original image
    smallthumb: 'width-135,height-102,resizemode-4', // Listing small thumbs, Aspect Ratio 4:3
    smallslideshowthumb: 'width-135,height-102,resizemode-1', // Used for external photogallery strip on top news, Aspect Ratio 4:3
    midthumb: 'width-200,height-150,resizemode-1', // Photo Listing widget thumb, etc Aspect Ratio 4:3
    largethumb: 'width-540,height-300,resizemode-4', // Lead Images Aspect Ratio 16:9
    smallwidethumb: 'width-200,height-111,resizemode-4', // For video listing widgets Aspect Ratio 16:9
    largewidethumb: 'width-540,height-300,resizemode-4', // For videowidget lead image and videoshow Aspect Ratio 16:9
    posterthumb: 'width-210,height-281,resizemode-4', // Moview Posters
  },
  sso: {
    // BASE_URL: 'https://eisamay.indiatimes.com',
    BASE_URL: 'https://eisamay.indiatimes.com',
    SSO_CHANNEL: 'eisamay-wap',
    MYTIMES_URL_USER: 'https://myt.indiatimes.com/mytimes/profile/info/v1/?ssoid=',
    SSO_URL_LOGIN: 'https://jsso.indiatimes.com/sso/identity/login',
    SSO_URL_LOGOUT: 'https://jsso.indiatimes.com/sso/identity/profile/logout/external',
    SSO_URL_GETTICKET:
      'https://jsso.indiatimes.com/sso/crossdomain/getTicket?version=v1&platform=wap&channel=',
    MYTIMES_ALREADY_RATED: 'https://myt.indiatimes.com/mytimes/alreadyRated?appKey=EIS&',
    MYTIMES_ADD_ACTIVITY: 'https://myt.indiatimes.com/mytimes/addActivity?appKey=EIS&',
    SSO_TRUECALLER_visibility: 4,
  },
  pages: {
    astro: '17335279',
    sports: '2279790',
    jokes: '12545605',
    business: '2279786',
    education: '2279784',
    tech: '2355187',
    automobile: '2355188',
    citizenreporter: '54234500',
    automobile: '2355188',
    elections: '56662129',
  },
  redis: {
    host: '192.168.24.51',
    port: '6379',
    db: '1',
  },
  // To print label in listing node at bottom right corner [templatename, label]
  listNodeLabels: {
    articlelist: ['articlelist'],
    news: ['articleshow'],
    photo: ['photoshow'],
    slideshow: ['photoshow'],
    video: ['videoshow'],
    videolist: ['videolist'],
    photolist: ['photoarticlelist'],
    lb: ['liveblog', 'লাইভ আপডেট'],
    moviereview: ['moviereview', 'মুভি রিভিউ'],
    poll: ['pollsurvey', 'পোল'],
  },

  // Used In AnchorLinkCard
  routes: [
    '/electionlist/',
    'education.cms',
    'business.cms',
    'elections.cms',
    '/articlelist/',
    '/photoarticlelist/',
    '/photoshow/',
    '/photomazaashow/',
    '/photomazaa/',
    '/articleshow/',
    '/moviereview/',
    '/liveblog/',
    '/videolist/',
    '/videoshow/',
  ],

  homepagewidgets: [
    { msid: 50075780, type: 'video', rlvideoid: '' }, // Video
    { msid: '', type: 'schedule' },
    { msid: 15897455, type: 'poll', secname: 'পোল' }, // Poll
    { msid: '', type: 'weather' },
    // { msid: 2279808, type: 'list', rlvideoid: "4901886" }, //state
    { msid: 52503852, type: 'photo', rlvideoid: '' }, // Photo
    { msid: 15819609, type: 'list' }, // West Bengal News

    { msid: '', type: 'servicedrawer' },
    { msid: 15819599, type: 'list', rlvideoid: '' }, // India
    { msid: 56687897, type: 'list', rlvideoid: '' }, // Business

    {
      msid: '',
      type: 'poster',
      image: 'https://static.langimg.com/astro_banner/photo/65011858.cms',
      heading: 'রাশিফল',
      title: 'কেমন যাবে আপনার দিন',
      override: '/astro.cms',
    },
    // { msid: 12545518, type: 'list', rlvideoid: "59429326" }, //Jokes
    { msid: 23000116, type: 'list', rlvideoid: '50075792' }, // Sports
    { msid: 15819570, type: 'list', rlvideoid: '50075784' }, // Entertainment

    {
      msid: '',
      type: 'poster',
      image: 'https://static.langimg.com/auto_banner/photo/65012407.cms',
      heading: 'বাহন-কাহন',
      title: 'গাড়ি ও বাইক সম্পর্কিত সব খবর ও রিভিউ পড়ুন।',
      override: 'https://eisamay.indiatimes.com/auto-news/articlelist/63397979.cms',
    },
    { msid: 15992436, type: 'list', rlvideoid: '' }, // Lifestyle
    { msid: 64760430, type: 'list', rlvideoid: '' }, // Tech

    // { msid: 2325387, type: 'list', rlvideoid: "" }, // Movie Review
  ],

  topdrawer: [
    { secname: 'ভিডিয়ো', link: '/videos/articlelist/16683331.cms', classname: 'one' },
    { secname: 'খেলা', link: '/sports/articlelist/23000116.cms', classname: 'two' },
    { secname: 'টেক', link: '/tech/articlelist/64760430.cms', classname: 'three' },
    {
      secname: 'লেন্সবন্দি',
      link: '/photo-gallery/photoarticlelist/52503852.cms',
      classname: 'four',
    },
    { secname: 'অ্যাস্ট্রো', link: '/astro.cms', classname: 'five' },
  ],

  staticPagesDir: '/var/www/html/testpages/',

  slike: {
    apikey: 'eisamaymweb205lang',
    snapchatapikey: 'lng54mweb5afm96lgz9', // TO DO
    ampapikey: 'lng205googleamp5a5qglkkg9',
    default_slikeid_minitv: '1x13qpaggu', // Times-Now
    nextvideo_domain: 'https://navbharattimes.indiatimes.com',
    default_pl_videolist: '/feeds/videpostroll_v5_slike/50075780.cms?feedtype=json&callback=cached',
  },

  slikeAdCodes: {
    business: '15819574,50075800',
    cricket: '16570436',
    city: '15819618',
    editorial: '15819584',
    entertainment: '15819570,50075784',
    home: '2147477985',
    international: '15819594',
    lifestyle: '15992436',
    nation: '15819599',
    soccer: '15819590',
    sports: '23000116,50075792',
    state: '15819609',
    world: '15819594',
    rabibaroari: '20742861',
    kolkattewali: '20742872',
    ros: 'other',
  },

  gRecaptchaKey: '6LcxSGUUAAAAAPsGPKVpEkLKxnuluuqDKa1AivEN', // TO DO

  ga: {
    gverifyCode: 'aazPHkz9wF1xs6eEAk49ugaknkewzNLhvWhr_fyXOIM',
    gatrackid: 'UA-29031733-3',
    gaprofile:
      "['superpacer.in', 'atmadeep.com', 'womenbikerally.com','pujoeisamay.com', 'eisamay.com', 'esyoungscholars.com']",
  },

  appexclusive_ga: {
    category: 'Wap to App install',
    action: 'click',
    label: 'articlelist_topsection',
  },
  poll_ga: {
    poll_submit: {
      category: 'pollSurvey',
      action: 'submit',
      label: 'pollsubmit',
    },
    poll_result_loggedIn: {
      category: 'pollSurvey',
      action: 'viewresult',
      label: 'loggedin',
    },
    poll_result_nonloggedIn: {
      category: 'pollSurvey',
      action: 'viewresult',
      label: 'non-loggedin',
    },
  },
  photoshow_app_install: {
    active: true, // true || false only
    show_slides: 6,
  },
  sso_ga: {
    truecaller_requestscreen: {
      category: 'True Caller',
      action: 'Click',
      label: 'Screen Request',
    },
    truecaller_screen: {
      category: 'True Caller',
      action: 'Click',
      label: 'Screen View',
    },
    truecaller_screencontinue: {
      category: 'True Caller',
      action: 'Click',
      label: 'Continue',
    },
    truecaller_loginsuccess: {
      category: 'True Caller',
      action: 'Click',
      label: 'Login Successful',
    },
  },
  comscore: { c1: '2', c2: '6036484' },
  applinks: {
    android: {
      hrnav: 'https://go.onelink.me/kN2v/c5cd7eb4',
      asshow: 'https://go.onelink.me/kN2v/1f768843',
      photoshow: 'https://go.onelink.me/kN2v/e659dfb5',
      serviceDrawer: 'https://go.onelink.me/kN2v/c5cd7eb4',
      social: 'https://go.onelink.me/kN2v/44345e76',
      oip_appexclusive:
        'https://navbharattimes.indiatimes.com/off-url-forward/eis_openinapp.cms?msid=',
    },
  },
  appdeeplink: {
    domain: 'https://go.onelink.me',
    shortcode: 'kN2v',
    c: 'Internal_Campaign',
    pid: {
      hrnav: 'WAP_HRNAV',
      serviceDrawer: 'WAP_BOTTOM',
      news: 'WAP_MWEB',
      article_end: 'WAP_AS',
      photo: 'WAP_PG',
      toparticle: 'WAP_Top%20Articles',
      extphoto: 'WAP_Home_Photostrip',
      photoshow: 'WAP_Photoshow',
    },
    openapp: 'eisamayreaderactivities%3A%2F%2Fopen-%2524%257C%2524-pub%3D4%3AEI%2520Samay',
    opennews:
      'eisamayreaderactivities%3A%2F%2Fopen-%2524%257C%2524-pub%3D4%3AEI%2520Samay-%2524%257C%2524-type%3Dnews-%2524%257C%2524-domain%3Dt-%2524%257C%2524-id%3D%7BMSID%7D',
    openphoto:
      'eisamayreaderactivities%3A%2F%2Fopen-%2524%257C%2524-pub%3D4%3AEI%2520Samay-%2524%257C%2524-type%3Dphoto-%2524%257C%2524-domain%3Dt-%2524%257C%2524-id%3D%7BMSID%7D',
    openextphoto:
      'eisamayreaderactivities%3A%2F%2Fopen-%2524%257C%2524-pub%3D4%3AEI%2520Samay-%2524%257C%2524-type%3Dphoto-%2524%257C%2524-domain%3Dp-%2524%257C%2524-id%3D%7BMSID%7D',
  },
  pushNotification: {
    cookieName: 'ntf_es',
    subscriptionLink: 'https://esnotifications.indiatimes.com/pm_subscribe.cms?channel=ES_mobile',
    translation: {
      heading: 'কোনও খবর হবে না মিস',
      receive: 'পান',
      alerts: 'অ্যালার্টস',
      join: 'সাবসক্রাইব করুন',
    },
  },

  weather: {
    geoLocationApi: 'https://geoapi.indiatimes.com/?cb=1',
    defaultCity: 'DELHI',
    citiesJson: 'https://navbharattimes.indiatimes.com/pwafeeds/pwa_geocities.cms',
    weatherFeed: 'https://navbharattimes.indiatimes.com/pwafeeds/pwa_weathertoday.cms?lat=',
    heading: 'আজকের তাপমাত্রা',
  },

  oneTapSignIn: {
    smartlockurl: 'https://smartlock.google.com/client',
    isViewGaTracked: false,
    eventGAObj: {
      hitType: 'event',
      eventCategory: 'Login',
      eventAction: 'One_Tap',
      eventLabel: '',
    },
    text: {
      oneTapSuccessMsg: 'আপনি সফলভাবে লগ ইন করেছেন',
      oneTapErrorMsg: 'Oops! কোনও সমস্যা হয়েছে। দ্বিতীয়বার চেষ্টা করুন।',
      oneTapBtnText: 'দ্বিতীয়বার চেষ্টা করুন',
    },
    constants: {
      onetapIgnoreDays: 15,
      onetapCookie: 'oneTapNextShow',
      onetapIframeCheckTimer: 2000,
      smartlockJsLoadTimer: 10000,
      clientId: '879025834833-nqo8qbkj4rl86gi5m1q63vj8v5mignl6.apps.googleusercontent.com',
      channel: 'eisamay-wap',
      jssoAPIGetURL: 'https://jsso.indiatimes.com/sso/services/gponetaplogin/verify',
    },
  },

  ads: {
    gptUrl: 'https://www.googletagservices.com/tag/js/gpt.js',
    ctnAddress: 'https://static.clmbtech.com/ctn/commons/js/colombia_v2.js',
    dmpUrl: 'https://static.clmbtech.com/ase/7270/134/aa.js',
    ctnads: {
      inline: '205999',
      ampinline: '',
      ampinlinehome: '',
      ctnlist: '205999',
      ctnlist1: '205999',
      // "ctnlist": "320650",
      ctnshow: '323128',
      ctnmidart: '323127',
      // "ctnhometop" : "316371",  // caraousal ad code, just for test
      // "ctnhometop" : "206698",
      ctnhome: '205998',
      ctnhometop: '206705', // updated
      ctnhomevideo: '322378', // video ad , updated
      ctninlineshow: '208371',
      ctnslider: '210645',
      ctnVideoURL: 'https://eisamay.indiatimes.com/ads_native_video_dev.cms',
      ctnbighome: '322679',
      ctnbiglist: '322680',
      ctnbigphoto: '322681',
      ctnint: '324739',
      adspositionsPhoto: '-3-,-5-,-9-,-14-,-20-,-27-,-36-,-45-,-55-',
      adspositionsLiveblog: '-5-,-10-,-15-,-20-,-25-,-30-,-35-,-40-',
      ctncookievalue: 'ce_nbapd',
    },
    ctnslots: {
      atf: '129249',
      atf2: '129249',
      atf3: '129249',
      btf: '129126',
      ctnshow: '323128',
      ctnbiglist: '322680',
      ctnbigphoto: '322681',
      midart: '326757',
      articleshow: 'ctnshow,atf,midart',
      liveblog: 'ctnbiglist,atf',
      photoshow: 'atf',
      videoshow: 'atf',
      videolist: 'atf',
      homepage: 'atf,atf2,atf3,btf',
    },
    dfpads: {
      amp: {
        atf: '',
        fbn: '',
        mrec: '',
      },
      topatf: '/7176/ESMY_Mweb/ESMY_MWeb_Others/ESMY_MWeb_ROS_OTH_Fixed',
      home: {
        atf: {
          id: 'div-gpt-ad-1509362163867-0',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Home/ESMY_Mweb_HP_ATF',
          mlb: [
            [[1, 1], [[320, 50], [300, 250], [320, 100]]],
            [[480, 200], [468, 60]],
            [[768, 200], [728, 90]],
          ],
        },
        btf: {
          id: 'div-gpt-ad-1509362163867-1',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Home/ESMY_Mweb_HP_BTF',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        fbn: {
          id: 'div-gpt-ad-1509362163867-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Home/ESMY_Mweb_HP_FBN',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        mrec: {
          id: 'div-gpt-ad-1509362163867-4',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Home/ESMY_Mweb_HP_MREC_1',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        mrec2: {
          id: 'div-gpt-ad-1509362163867-5',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Home/ESMY_Mweb_HP_MREC_2',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
      },
      articleshow: {
        atf: {
          id: 'div-gpt-ad-7625525418682-0',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articleshow/ESMY_Mweb_AS_ATF',
          mlb: [
            [[1, 1], [[320, 50], [300, 250], [320, 100]]],
            [[480, 200], [468, 60]],
            [[768, 200], [728, 90]],
          ],
        },
        btf: {
          id: 'div-gpt-ad-7625525418682-1',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articleshow/ESMY_Mweb_AS_BTF',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        fbn: {
          id: 'div-gpt-ad-7625525418682-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articleshow/ESMY_Mweb_AS_FBN',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        mrec: {
          id: 'div-gpt-ad-7625525418682-4',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articleshow/ESMY_Mweb_AS_MREC_1',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        mrec2: {
          id: 'div-gpt-ad-7625525418682-5',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articleshow/ESMY_Mweb_AS_MREC_2',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        masthead: {
          id: 'div-gpt-ad-7625525418682-5',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articleshow/ESMY_Mweb_AS_Masthead',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
      },
      articlelist: {
        atf: {
          id: 'div-gpt-ad-9408876077435-0',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articlelist/ESMY_Mweb_AL_ATF',
          mlb: [
            [[1, 1], [[320, 50], [300, 250], [320, 100]]],
            [[480, 200], [468, 60]],
            [[768, 200], [728, 90]],
          ],
        },
        btf: {
          id: 'div-gpt-ad-9408876077435-1',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articlelist/ESMY_Mweb_AL_BTF',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        fbn: {
          id: 'div-gpt-ad-9408876077435-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articlelist/ESMY_Mweb_AL_FBN',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        mrec: {
          id: 'div-gpt-ad-9408876077435-4',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articlelist/ESMY_Mweb_AL_MREC_1',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        mrec2: {
          id: 'div-gpt-ad-9408876077435-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Articlelist/ESMY_Mweb_AL_MREC_2',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
      },
      photomazaashow: {
        atf: {
          id: 'div-gpt-ad-8519743375899-0',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_ATF',
          mlb: [
            [[1, 1], [[320, 50], [300, 250], [320, 100]]],
            [[480, 200], [468, 60]],
            [[768, 200], [728, 90]],
          ],
        },
        btf: {
          id: 'div-gpt-ad-8519743375899-1',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_BTF',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        fbn: {
          id: 'div-gpt-ad-8519743375899-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_FBN',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        mrec: {
          id: 'div-gpt-ad-8519743375899-4',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_MREC_1',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        mrec2: {
          id: 'div-gpt-ad-8519743375899-5',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_MREC_2',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
      },
      photoshow: {
        atf: {
          id: 'div-gpt-ad-8519743375899-0',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_ATF',
          mlb: [
            [[1, 1], [[320, 50], [300, 250], [320, 100]]],
            [[480, 200], [468, 60]],
            [[768, 200], [728, 90]],
          ],
        },
        btf: {
          id: 'div-gpt-ad-8519743375899-1',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_BTF',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        fbn: {
          id: 'div-gpt-ad-8519743375899-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_FBN',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        mrec: {
          id: 'div-gpt-ad-8519743375899-4',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_MREC_1',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        mrec2: {
          id: 'div-gpt-ad-8519743375899-5',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_PhotoGallery/ESMY_Mweb_PTG_MREC_2',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
      },
      videoshow: {
        atf: {},
        btf: {
          id: 'div-gpt-ad-2967878352257-1',
          name: '/7176/ESMY_Mweb/ESMY_MWeb_Video/ESMY_MWeb_ROS_VDO_BTF',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        fbn: {
          id: 'div-gpt-ad-2967878352257-2',
          name: '/7176/ESMY_Mweb/ESMY_MWeb_Video/ESMY_MWeb_ROS_VDO_FBN',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        mrec: {
          id: 'div-gpt-ad-2967878352257-4',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Video/ESMY_Mweb_ROS_VDO_MREC_1',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        mrec2: {
          id: 'div-gpt-ad-2967878352257-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Video/ESMY_Mweb_ROS_VDO_MREC_2',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
      },
      liveblog: {
        atf: {
          id: 'div-gpt-ad-1697298151242-0',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Liveblog/ESMY_Mweb_LIVEBLOG_ATF',
          mlb: [
            [[1, 1], [[320, 50], [300, 250], [320, 100]]],
            [[480, 200], [468, 60]],
            [[768, 200], [728, 90]],
          ],
        },
        btf: {
          id: 'div-gpt-ad-1697298151242-1',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Liveblog/ESMY_Mweb_LIVEBLOG_BTF',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        fbn: {
          id: 'div-gpt-ad-1697298151242-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Liveblog/ESMY_Mweb_LIVEBLOG_FBN',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        mrec: {
          id: 'div-gpt-ad-1697298151242-4',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Liveblog/ESMY_Mweb_LIVEBLOG_MREC_1',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        mrec2: {
          id: 'div-gpt-ad-1697298151242-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Liveblog/ESMY_Mweb_LIVEBLOG_MREC_2',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
      },
      others: {
        atf: {
          id: 'div-gpt-ad-9135510865388-0',
          name: '/7176/ESMY_Mweb/ESMY_MWeb_Others/ESMY_MWeb_ROS_OTH_ATF',
          mlb: [
            [[1, 1], [[320, 50], [300, 250], [320, 100]]],
            [[480, 200], [468, 60]],
            [[768, 200], [728, 90]],
          ],
        },
        btf: {
          id: 'div-gpt-ad-9135510865388-1',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Others/ESMY_Mweb_ROS_OTH_BTF',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        fbn: {
          id: 'div-gpt-ad-9135510865388-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Others/ESMY_Mweb_ROS_OTH_FBN',
          mlb: [[[1, 1], [320, 50]], [[480, 200], [468, 60]], [[768, 200], [728, 101]]],
        },
        mrec: {
          id: 'div-gpt-ad-9135510865388-4',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Others/ESMY_Mweb_ROS_OTH_MREC_1',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
        mrec2: {
          id: 'div-gpt-ad-9135510865388-2',
          name: '/7176/ESMY_Mweb/ESMY_Mweb_Others/ESMY_Mweb_ROS_OTH_MREC_2',
          size: [[320, 250]],
          mlb: [[[1, 1], [300, 250]], [[480, 200], [300, 250]], [[768, 200], [728, 101]]],
        },
      },
    },
  },
};
