/* eslint-disable react/prop-types */
/* eslint-disable operator-linebreak */
/* eslint-disable indent */
import React from 'react';
import proptypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Link from '../Link/Link';
// import { LazyLoadImage } from 'react-lazy-load-image-component';

const Thumb = React.memo(props => {
  const {
    src,
    imgId,
    imgSize,
    width,
    height,
    alt,
    title,
    islinkable,
    link,
    lazy,
    isvdo,
    vdodu,
    resizeMode,
  } = props;
  const imgid = imgId === undefined ? '66951362' : imgId;
  // const resizeMde = resizeMode || '4';
  const altNew = alt || 'NBT';
  let imgTitile = title || 'NBT';
  if (imgTitile === 'no') {
    imgTitile = '';
  }

  let imgSrc;

  if (typeof src !== 'undefined') {
    imgSrc = src;
  } else {
    imgSrc = `${process.env.IMG_URL}/thumb/msid-${imgid}`;
    if (width && typeof width !== 'undefined') {
      imgSrc += `,width-${width}`;
    }
    if (height && typeof height !== 'undefined') {
      imgSrc += `,height-${height}`;
    }
    if (imgSize && typeof imgSize !== 'undefined') {
      imgSrc += `,imgsize-${imgSize}`;
    }
    if (resizeMode) {
      imgSrc += `,resizemode-${resizeMode}`;
    }

    imgSrc += '/pic.jpg';
  }

  if (!lazy) {
    return islinkable ? (
      <Link to={link} className={isvdo ? 'video_icon_thumb' : ''}>
        <img
          className="album-img"
          src={imgSrc}
          alt={altNew}
          width={width}
          height={height}
          title={imgTitile}
        />
        {isvdo ? <span className="video_time">{vdodu}</span> : ''}
      </Link>
    ) : (
      <img
        className="album-img"
        src={imgSrc}
        alt={altNew}
        title={imgTitile}
        width={width}
        height={height}
      />
    );
  }

  return islinkable ? (
    <Link to={link} className={isvdo ? 'video_icon_thumb' : ''}>
      <LazyLoad height={200} offset={100} once>
        <img
          className="album-img"
          src={imgSrc}
          alt={altNew}
          title={imgTitile}
          width={width}
          height={height}
        />
        {isvdo ? <span className="video_time">{vdodu}</span> : ''}
      </LazyLoad>
    </Link>
  ) : (
    <LazyLoad height={200} offset={100} once>
      <img
        className="album-img"
        src={imgSrc}
        alt={altNew}
        title={imgTitile}
        width={width}
        height={height}
      />
      {isvdo == 'true' ? <span className="video_time">{vdodu}</span> : ''}
    </LazyLoad>
  );
});

Thumb.displayName = 'Thumb';

Thumb.propTypes = {
  imgId: proptypes.string,
  imgSize: proptypes.string,
  width: proptypes.string,
  height: proptypes.string,
  alt: proptypes.string,
  islinkable: proptypes.bool,
  link: proptypes.string,
  src: proptypes.string,
  lazy: proptypes.bool,
  title: proptypes.string,
};

export default Thumb;
