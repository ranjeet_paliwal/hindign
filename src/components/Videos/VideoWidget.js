import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Slider from '../NewSlider/NewSlider';

const Config = require(`../../../common/${process.env.SITE}`);

class VideoWidget extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      VideosData: '',
      isError: false,
    };
  }

  componentDidMount() {
    fetch(
      `${
        process.env.API_ENDPOINT
      }/webgn_combine.cms?type=video&count=11&feedtype=sjson&secid=20104392&platform=webgn`,
    )
      .then(response => response.json())
      .then(data => {
        this.setState({ VideosData: data });
      })
      .catch(e => this.setState({ isError: true }));
  }

  render() {
    const { VideosData } = this.state;
    const { type } = this.props;
    return (
      <div className="full_section wdt_latest_videos">
        {typeof VideosData === 'object' && VideosData.section && VideosData.section.newsItem ? (
          <Slider
            type={type === 'AS' ? 'videosAS' : 'videos'}
            size="3"
            sliderData={VideosData.section.newsItem.items}
            width="300"
            height="168"
            SliderClass="videoslider"
            islinkable="true"
            link={
              VideosData.section.newsItem && VideosData.section.newsItem.wu
                ? VideosData.section.newsItem.wu
                : ''
            }
            secname={Config.Locale.gadget.video}
            sliderWidth={type === 'AS' ? '270' : '930'}
            headingRequired="yes"
            moreLinkRequired="yes"
            moreLinkText={Config.Locale.allvideos}
          />
        ) : (
          ''
        )}
      </div>
    );
  }
}

VideoWidget.propTypes = {
  type: PropTypes.object,
};

export default VideoWidget;
