import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getCookie, setCookie } from '../../common-utility';

const Config = require(`../../../common/${process.env.SITE}`);

class GadgetsPostreview extends React.Component {
  state = {
    status: false,
    selected: '',
    statusmsg: '',
  };

  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
    this.openLoginRegister = this.openLoginRegister.bind(this);
  }

  openLoginRegister() {
    this.props.openLoginRegister();
  }

  // eslint-disable-next-line consistent-return
  handleSubmit(event) {
    event.preventDefault();
    if (this.state.selected === '') {
      this.setState({
        statusmsg: 'Please select one option',
      });
      return false;
    }

    const optionsel = document.querySelector('input[name="rating"]:checked').value;
    const { reviewID } = this.props;
    event.preventDefault();
    const data = new FormData(event.target);

    fetch(
      `${
        process.env.API_ENDPOINT
      }/rate_gadgets.cms?msid=${reviewID}&getuserrating=1&criticrating=&vote=${optionsel}`,
      {
        method: 'POST',
        body: data,
      },
    ).then(data => {
      this.setState({
        status: true,
        selected: '',
      });
      let reviewAndLoginIds = localStorage.getItem('reviewAndLoginIds');
      const ssoid = getCookie('ssoid');
      if (reviewAndLoginIds) {
        reviewAndLoginIds = JSON.parse(reviewAndLoginIds).concat(`${reviewID}-${ssoid}`);
      } else {
        reviewAndLoginIds = [`${reviewID}-${ssoid}`];
      }
      localStorage.setItem('reviewAndLoginIds', JSON.stringify(reviewAndLoginIds));
    });
  }

  render() {
    const { loggedIn, showLoginRegister, reviewID } = this.props;
    const { status, statusmsg } = this.state;
    if (typeof window !== 'undefined' && window.localStorage) {
      const reviewAndLoginIds = localStorage.getItem('reviewAndLoginIds');
      const ssoid = getCookie('ssoid');

      if (reviewAndLoginIds && Array.isArray(JSON.parse(reviewAndLoginIds)) && ssoid) {
        if (JSON.parse(reviewAndLoginIds).includes(`${reviewID}-${ssoid}`)) {
          return (
            <div>
              <span>You have already submitted Rating!</span>
            </div>
          );
        }
      }
    }

    // uncomment below code after live
    if (!loggedIn) {
      return (
        <div className="content">
          <div className="login-first">
            <h4>Please Login first</h4>
            <button type="button" onClick={showLoginRegister} className="btn">
              LOGIN
            </button>
          </div>
        </div>
      );
    }
    // if (!status && loggedIn) {
    if (!status) {
      return (
        <div className="content">
          <form onSubmit={event => this.handleSubmit(event)}>
            <ul>
              <li>
                <span className="blk_star">
                  <span className="icon_star one" />
                </span>
                <label htmlFor="1star">{Config.Locale.weak}</label>
                <input
                  type="radio"
                  id="1star"
                  name="rating"
                  value="2"
                  onChange={e => this.setState({ selected: e.target.value })}
                />
              </li>
              <li>
                <span className="blk_star">
                  <span className="icon_star two" />
                </span>
                <label htmlFor="2star">{Config.Locale.belowAverage}</label>
                <input
                  type="radio"
                  id="2star"
                  name="rating"
                  value="4"
                  onChange={e => this.setState({ selected: e.target.value })}
                />
              </li>
              <li>
                <span className="blk_star">
                  <span className="icon_star three" />
                </span>
                <label htmlFor="3star">{Config.Locale.worthpurchase}</label>
                <input
                  type="radio"
                  id="3star"
                  name="rating"
                  value="6"
                  onChange={e => this.setState({ selected: e.target.value })}
                />
              </li>
              <li>
                <span className="blk_star">
                  <span className="icon_star four" />
                </span>
                <label htmlFor="4star">{Config.Locale.good}</label>
                <input
                  type="radio"
                  id="4star"
                  name="rating"
                  value="8"
                  onChange={e => this.setState({ selected: e.target.value })}
                />
              </li>
              <li>
                <span className="blk_star">
                  <span className="icon_star five" />
                </span>
                <label htmlFor="5star">{Config.Locale.excellent}</label>
                <input
                  type="radio"
                  id="5star"
                  name="rating"
                  value="10"
                  onChange={e => this.setState({ selected: e.target.value })}
                />
              </li>
            </ul>
            {statusmsg !== '' ? <span className="error">{statusmsg}</span> : ''}
            <button className="btn">{Config.Locale.postRating}</button>
          </form>
        </div>
      );
    }
    return <div className="msg_thanks">{Config.Locale.thanksforreview}</div>;
  }
}
function mapStateToProps(state) {
  return {
    ...state.authentication,
    config: state.config,
  };
}

GadgetsPostreview.propTypes = {
  data: PropTypes.array,
  showLoginRegister: PropTypes.func,
  reviewID: PropTypes.array,
  loggedIn: PropTypes.bool,
};

export default connect(mapStateToProps)(GadgetsPostreview);
