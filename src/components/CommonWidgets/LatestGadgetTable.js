import React from 'react';
import PropTypes, { array } from 'prop-types';

const Config = require(`../../../common/${process.env.SITE}`);

const LatestGadgetsTable = ({ data, params }) => {
  let gadgets = [];
  if (data && data.gadgets && Array.isArray(data.gadgets)) {
    gadgets = data.gadgets.slice(0, 10);
  }
  const category = params && params.category;
  const launchDate = gadgets && gadgets.filter(item => item.launch_date);
  let latestPopularRegional = Config.Locale.popular;
  let latestPopular = 'Popular';
  if (params && params.filters && params.filters.includes('sort=latest')) {
    latestPopularRegional = Config.Locale.latest;
    latestPopular = 'Latest';
  }

  return gadgets && gadgets.length > 0 ? (
    <div className="gl_latestGadgets">
      <h2 className="sectionHead">
        <span>{`${latestPopularRegional} ${
          data.brandheader
        } भारत में  - ${latestPopular} ${data.brandheadereng || ''}  ${
          Config.gadgetMapping[category]
        } in India`}</span>
      </h2>
      <div className="gn_table_layout">
        <table>
          <tbody>
            <tr>
              <th>
                {data.brandheader} कीमतों की लिस्ट <br />
                {data.brandheadereng || ''} {Config.gadgetMapping[category]} Price List
              </th>
              {launchDate && launchDate.length > 0 && (
                <th>
                  {Config.Locale.indiaLaunchDates} <br /> Launch Date in India
                </th>
              )}
              <th>
                {Config.Locale.indiaPrice} <br /> Price in India
              </th>
            </tr>
            {gadgets &&
              gadgets.map(gData => (
                <tr key={gData.uname}>
                  <td>
                    <span className="gadName" title={gData.name}>
                      {gData.name}
                    </span>
                  </td>
                  {launchDate && launchDate.length > 0 && <td>{gData.launch_date || 'NA'}</td>}
                  <td>{gData.price ? `₹ ${gData.price}` : 'NA'}</td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </div>
  ) : (
    ''
  );
};

LatestGadgetsTable.propTypes = {
  data: PropTypes.object,
};

export default LatestGadgetsTable;
