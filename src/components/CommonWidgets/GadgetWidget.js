import React, { Component } from 'react';
import Parser from 'html-react-parser';
import PropTypes from 'prop-types';
import Link from '../Link/Link';
import Thumb from '../Thumb/Thumb';
import { truncateStr, getBuyLink } from '../../common-utility';
import ErrorBoundary from '../lib/errorboundery/ErrorBoundary';
import CommonLoader from '../Loaders/CommonLoader';
import { fetchGadgetListDataIfNeeded } from '../../actions/gadgetlist/gadgetlist';
import { fetchFilterListDataIfNeeded } from '../../actions/gadgetfiltertool/gadgetfiltertool';

const Config = require('../../../common/nbt');

class GadgetWidget extends Component {
  constructor(props) {
    super(props);
    // const sortbystate =  (props.params && props.params.filters || 'sort=popular' );
    const sortbystate =
      props.params && props.params.filters && props.params.filters.includes('sort=')
        ? props.params.filters
        : 'sort=popular';
    this.state = {
      category: '',
      sortbystate,
    };
    this.dataType = 'latest';
  }

  componentWillMount() {
    const { router, pagetype, _category, params } = this.props;
    const category = _category || params.category;
    if (category) this.setState({ category });
    params ? (params.category = category) : null;
  }

  componentDidUpdate(prevProps, prevState) {
    const { params } = this.props;
    if (prevProps.params.category !== params.category) {
      this.setState({
        sortbystate: 'sort=popular',
      });
    }
  }

  setCategory(obj) {
    // debugger
    const { dispatch, params, query, router, pagetype } = this.props;
    this.dataType = 'popular';
    // document.getElementById('popular').className = "active";

    const category = obj.currentTarget.id;
    if (pagetype && pagetype == 'gadgetlist') {
      this.props.router.push(`/tech/${category}`);
      this.restForm();
    }
    if (category) this.setState({ category });
    const dataType = this.dataType;

    const categoryoverride = category;
    if (pagetype && pagetype == 'gadgetlist') {
      dispatch(fetchFilterListDataIfNeeded(params, query, router, categoryoverride));
    } else {
      return dispatch(
        fetchGadgetListDataIfNeeded(params, query, router, dataType, categoryoverride),
      );
    }
  }

  restForm(obj) {
    const checkBox = document.getElementsByTagName('input');
    if (checkBox && checkBox.length > 0) {
      for (let b = 0; b < checkBox.length; b++) {
        if (checkBox[b].type == 'radio') {
          checkBox[b].checked = false;
        }
        if (checkBox[b].type == 'checkbox') {
          checkBox[b].checked = false;
        }
      }
    }
    if (document.getElementById('filterapplied'))
      document.getElementById('filterapplied').innerHTML = '';
  }

  setDataType(obj) {
    // debugger
    const { dispatch, params, query, router } = this.props;

    const dataType = obj.currentTarget.id;

    this.dataType = dataType;
    // let category=this.state.category;
    const category = params.category;
    params ? (params.category = category) : null;
    if (router && router.location.pathname.indexOf('articlelist') < 0) {
      this.props.router.push(`/tech/${params.category}/filters/sort=${dataType}`);
    } else {
      return dispatch(fetchGadgetListDataIfNeeded(params, query, router, dataType));
    }
    this.setState({
      sortbystate: `sort=${dataType}`,
    });
  }

  amz_Content(amzData, type) {
    const uniqueID = Math.random()
      .toString(36)
      .substr(2, 9);
    const affData = amzData.filter(data => {
      return data.Identifier === 'amazon';
    });

    if (affData && affData.length > 0) {
      return (
        <React.Fragment>
          <b>{Config.Locale.tech.buyhere}</b>
          <input type="checkbox" id={`chk-nxt${uniqueID}`} className="chk-nxt" />
          {affData.length > 1 ? <label className="nxt" htmlFor={`chk-nxt${uniqueID}`} /> : ''}
          <ul className="dotted-box">
            {affData.map((amz, index) => {
              if (index <= 1 && amz.Identifier === 'amazon') {
                const amztitle = (amz && amz.name.replace(/\s+/g, '-')) || '';
                const amzga = `${amztitle}_${amz.sort_price}`;
                const affiliateTag =
                  (Config && Config.affiliateTags && Config.affiliateTags.GL) || '';
                const price = amz.sort_price;
                const title = amztitle;
                const url = amz.url;
                const buyURL = getBuyLink({
                  url,
                  price,
                  title,
                  amzga,
                  tag: affiliateTag,
                });
                return this.amz_list(amz, 0, buyURL);
              }
            })}
          </ul>
        </React.Fragment>
      );
    }
    // }
  }

  amz_minilist(amz, index, prodUrl) {
    return (
      <li>
        <span className="buy-btn">
          <a target="_blank" rel="nofollow" href={prodUrl}>
            Buy on <b>Amazon</b>
          </a>
        </span>
      </li>
    );
  }

  amz_list(amz, index, prodUrl) {
    return (
      <li>
        <span className="txt text_ellipsis">{truncateStr(amz.name, 60)}</span>
        {
          <span className="gd_price">
            {Config.Locale.tech.rupeesymbol} {amz.sort_price ? amz.sort_price : null}
          </span>
        }
        <span className="btm">
          <span className={amz.Identifier}>
            <img src="https://navbharattimes.indiatimes.com/photo/58606011.cms" width="65" />
          </span>
          <span className="btn-buy">
            <a target="_blank" rel="nofollow" href={prodUrl}>
              {Config.Locale.tech.purchase}
            </a>
          </span>
        </span>
      </li>
    );
  }

  render() {
    const _this = this;
    // let category=Config.Locale.tech.category;
    const {
      gadgetList,
      sectionLink,
      params,
      router,
      pagetype,
      isFrmTechHome,
      navigationfilter,
    } = this.props;
    const sortbystateCls = _this.state.sortbystate;
    return (
      <React.Fragment>
        <div className="gl_tabs">
          {pagetype == 'gadgetlist' ? (
            <React.Fragment>
              <span>{Config.Locale.tech.sortby}</span>

              <ul className="tabs">
                <li
                  id="latest"
                  className={sortbystateCls === 'sort=latest' ? 'active' : null}
                  onClick={_this.setDataType.bind(this)}
                >
                  {Config.Locale.tech.latest}
                </li>
                <li
                  id="popular"
                  className={sortbystateCls === 'sort=popular' ? 'active' : null}
                  onClick={_this.setDataType.bind(this)}
                >
                  {Config.Locale.tech.popular}
                </li>
                {sortbystateCls === 'sort=rating-asc' ||
                sortbystateCls === 'sort=latest' ||
                sortbystateCls === 'sort=popular' ||
                sortbystateCls === 'sort=price-asc' ||
                sortbystateCls === 'sort=price-desc' ? (
                  <li
                    id="rating-desc"
                    className={sortbystateCls === 'sort=rating-asc' ? 'active' : null}
                    onClick={_this.setDataType.bind(this)}
                  >
                    {Config.Locale.tech.rating}
                  </li>
                ) : (
                  ''
                )}

                {sortbystateCls === 'sort=rating-desc' ? (
                  <li
                    id="rating-asc"
                    className={sortbystateCls === 'sort=rating-desc' ? 'active' : null}
                    onClick={_this.setDataType.bind(this)}
                  >
                    {Config.Locale.tech.rating}
                  </li>
                ) : (
                  ''
                )}

                {sortbystateCls === 'sort=price-asc' ||
                sortbystateCls === 'sort=latest' ||
                sortbystateCls === 'sort=popular' ||
                sortbystateCls === 'sort=rating-desc' ||
                sortbystateCls === 'sort=rating-asc' ? (
                  <li
                    id="price-desc"
                    className={sortbystateCls === 'sort=price-asc' ? 'active' : null}
                    onClick={_this.setDataType.bind(this)}
                  >
                    {Config.Locale.tech.price}
                  </li>
                ) : (
                  ''
                )}

                {sortbystateCls === 'sort=price-desc' ? (
                  <li
                    id="price-asc"
                    className={sortbystateCls === 'sort=price-desc' ? 'active' : null}
                    onClick={_this.setDataType.bind(this)}
                  >
                    {Config.Locale.tech.price}
                  </li>
                ) : (
                  ''
                )}
              </ul>
            </React.Fragment>
          ) : (
            ''
          )}
        </div>

        <div className="bg-gray">
          <div className="gadgets-in-list">
            <ErrorBoundary>
              {gadgetList && gadgetList instanceof Array ? (
                <ul>
                  {gadgetList.map((item, index) => {
                    return (
                      <React.Fragment>
                        {item && item.gadgets && item.gadgets.length > 0 ? (
                          item.gadgets.map((item, index) => {
                            const gadgetName =
                              item &&
                              item.regional &&
                              item.regional instanceof Array &&
                              item.regional.filter(gname => {
                                return gname.host === Config.SiteInfo.hostid;
                              });

                            //  console.log('gadgetName', gadgetName);
                            const showActualPrice = item.affiliate === '' ? item.price : '';
                            const showGadgetPrice =
                              showActualPrice !== '' ? (
                                <span className="gadget_buy">
                                  <span className="symbol_rupees">{showActualPrice}</span>
                                </span>
                              ) : (
                                'NA'
                              );
                            let rumourClass = '';
                            if (item.rumoured == 1 || item.upcoming == 1) {
                              if (item.rumoured == 1 && item.upcoming == 1) {
                                rumourClass = 'rumoured';
                              } else {
                                rumourClass = item.rumoured == 1 ? 'rumoured' : 'upcoming';
                              }
                            }
                            return (
                              <React.Fragment>
                                <li key={`gadgetList${index}`} className={rumourClass}>
                                  <div className="top_spec">
                                    <span className="gadget_img">
                                      <span className="top-caption" />
                                      <Link to={`/tech/${params.category}/${item.lcuname}`}>
                                        <Thumb
                                          imgId={
                                            item.imageMsid instanceof Array
                                              ? item.imageMsid[0]
                                              : item.imageMsid
                                          }
                                          resizeMode="4"
                                          width="150"
                                        />
                                      </Link>
                                    </span>
                                    <span className="gadget_detail">
                                      <span className="gd_name">
                                        <Link
                                          to={`/tech/${params.category}/${item.lcuname}`}
                                          className="text_ellipsis"
                                        >
                                          {item.devicename}
                                        </Link>
                                      </span>
                                      <span className="gd_rating">
                                        <span className={item.criticRating ? 'gd-input' : ''}>
                                          {item.criticRating
                                            ? `${Config.Locale.tech.criticsrating}: `
                                            : null}
                                          {item.criticRating ? (
                                            parseInt(item.criticRating) != 0 ? (
                                              <b>
                                                <small>
                                                  {(parseFloat(item.criticRating) / 2).toFixed(1)}
                                                </small>
                                                /5
                                              </b>
                                            ) : (
                                              <b>NA</b>
                                            )
                                          ) : null}
                                        </span>
                                        <span className="gd-input">
                                          {item.averageRating
                                            ? `${Config.Locale.tech.userrating}: `
                                            : null}
                                          {item.averageRating &&
                                          parseInt(item.averageRating) != 0 ? (
                                            <b>
                                              <small>
                                                {(parseFloat(item.averageRating) / 2).toFixed(1)}
                                              </small>
                                              /5
                                            </b>
                                          ) : (
                                            <Link
                                              to={`/tech/${params.category}/${item.lcuname}#rate`}
                                              className="gd-submit"
                                            >
                                              {Config.Locale.tech.submitrating}
                                            </Link>
                                          )}
                                        </span>
                                      </span>

                                      {item.launch_date ? (
                                        <span className="gd_launch">
                                          {Config.Locale.tech.launchdate}: {item.launch_date}
                                        </span>
                                      ) : null}

                                      {isFrmTechHome ? null : (
                                        <div className="gadget_specs">
                                          <ul>
                                            {Object.keys(item.keyFeatures).map((key, index) => (
                                              <li key={`keyfFeatures${index}`}>
                                                <label>{item.keyFeatures[key].key} : </label>
                                                <b>{item.keyFeatures[key].value}</b>
                                              </li>
                                            ))}
                                          </ul>
                                        </div>
                                      )}
                                    </span>
                                    {item.affiliate &&
                                    item.affiliate.exact &&
                                    Array.isArray(item.affiliate.exact) &&
                                    item.affiliate.exact.length > 0 ? (
                                      <span className="gadget_buy">
                                        {this.amz_Content(item.affiliate.exact, 'exact')}
                                      </span>
                                    ) : item.affiliate && item.affiliate.related.length > 0 ? (
                                      <span className="gadget_buy related">
                                        {this.amz_Content(item.affiliate.related, 'related')}
                                      </span>
                                    ) : (
                                      showGadgetPrice
                                    )}
                                  </div>
                                  <div className="bottom_spec">
                                    <a className="add_to_compare">
                                      <Link
                                        to={`/tech/compare-${params.category}?device=${
                                          item.name
                                        }&name=${item.lcuname}`}
                                      >
                                        <b className="plus_icon" />
                                        {Config.Locale.tech.addtocompare}
                                      </Link>
                                    </a>
                                    {isFrmTechHome ? null : (
                                      <span className="read_more">
                                        <Link
                                          to={`/tech/${_this.state.category}/${
                                            item.lcuname
                                          }?#specifications`}
                                        >
                                          {Config.Locale.tech.fullspecification}
                                        </Link>
                                      </span>
                                    )}
                                  </div>

                                  {}
                                </li>
                              </React.Fragment>
                            );
                          })
                        ) : (
                          <div className="no-data-list">
                            <ul>
                              <li>No data Available, please refine filters.</li>
                            </ul>
                          </div>
                        )}
                        {isFrmTechHome ? (
                          <div>
                            <Link className="more-btn" href={`/tech/${_this.state.category}`}>
                              {Config.Locale.tech.gadgets} {Config.Locale.read_more_listing}
                            </Link>
                          </div>
                        ) : null}
                      </React.Fragment>
                    );
                  })}
                </ul>
              ) : (
                <CommonLoader />
              )
              // <FakeListing showImages={true} />
              }
            </ErrorBoundary>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

GadgetWidget.propTypes = {
  item: PropTypes.object,
};

export default GadgetWidget;
