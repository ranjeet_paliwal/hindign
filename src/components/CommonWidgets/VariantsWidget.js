import React from 'react';
import PropTypes from 'prop-types';

import Link from '../Link/Link';

const Config = require(`../../../common/${process.env.SITE}`);

const GadgetVariants = ({ data, currentlyActive, gadgetCategory }) => {
  return (
    <div className="full_section wdt_varients">
      <h2>
        <span>{Config.Locale.varients}</span>
      </h2>
      <ul>
        <li className="active">
          <span>{currentlyActive}</span>
        </li>
        {data &&
          data.variants.map(value => {
            return (
              // eslint-disable-next-line react/jsx-key
              <li>
                <Link to={`${process.env.WEBSITE_URL}/${gadgetCategory}/${value}`}>
                  {value && value.replace(/-/g, ' ')}
                </Link>
              </li>
            );
          })}
      </ul>
    </div>
  );
};

GadgetVariants.propTypes = {
  data: PropTypes.object,
  currentlyActive: PropTypes.string,
  gadgetCategory: PropTypes.string,
};

export default GadgetVariants;
