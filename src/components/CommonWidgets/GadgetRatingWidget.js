import React from 'react';
import PropTypes from 'prop-types';
import GadgetPostreview from './GadgetPostreview';

const Config = require(`../../../common/${process.env.SITE}`);

function convertDate(d) {
  if (!d) {
    return '';
  }
  var parts = d.split(' ');
  //Fri Oct 23 04:09:36 IST 2020
  var months = {
    Jan: '01',
    Feb: '02',
    Mar: '03',
    Apr: '04',
    May: '05',
    Jun: '06',
    Jul: '07',
    Aug: '08',
    Sep: '09',
    Oct: '10',
    Nov: '11',
    Dec: '12',
  };
  return parts[5] + '-' + months[parts[1]] + '-' + parts[2];
}

const GadgetsRating = ({
  data,
  getreview,
  closereview,
  showReviewCard,
  isReviewDisabled,
  showLoginRegister,
  category,
  loggedIn,
}) => {
  let max = 0;
  const defaultVal = [5, 4, 3, 2, 1];
  const rateArr = {};

  if (!data) {
    return null;
  }
  const {
    isProductAvailable,
    productCategory,
    productName,
    brandName,
    imageLink,
    itemDescription,
    itemPrice,
    sortPrice,
    pid,
    rating,
    ur,
    updatedDate,
    announced,
    canonical,
  } = data;

  // eslint-disable-next-line no-unused-expressions
  if (data && Array.isArray(data.rating)) {
    data.rating.map(item => {
      // debugger;
      const gadRate = Math.round(parseInt(item.rating) / 2);
      rateArr[gadRate] = item.count;
      max = parseInt(item.count) > max ? parseInt(item.count) : max;
    });
  } else if (typeof data.rating !== 'undefined') {
    const gadRate = Math.round(parseInt(data.rating.rating) / 2);
    rateArr[gadRate] = data.rating.count;
    max = parseInt(data.rating.count);
  }

  const plainDescription = itemDescription && itemDescription.replace(/(<([^>]+)>)/gi, '');
  let ratingCount = 0;

  if (rating && Array.isArray(rating)) {
    rating.forEach(item => {
      ratingCount = ratingCount + parseInt(item.count);
    });
  } else if (rating && rating.count) {
    ratingCount = parseInt(rating.count);
  }

  const priceValidUntil = convertDate(updatedDate);
  let userRating = ur && ur != Config.Locale.befirsttoreview ? ur : '';

  return defaultVal ? (
    <div className="full_section wdt_review_rate">
      {(sortPrice && sortPrice != '0') || userRating ? (
        <div itemType="http://schema.org/Product" itemScope>
          <meta itemProp="category" content={productCategory} />
          <meta itemProp="sku" content={pid} />
          <meta itemProp="releaseDate" content={convertDate(announced)} />
          <meta itemProp="name" content={productName} />
          <meta itemProp="brand" content={brandName} />
          {userRating && (
            <span itemType="http://schema.org/AggregateRating" itemScope itemProp="AggregateRating">
              <meta itemProp="ratingValue" content={userRating} />
              <meta itemProp="ratingCount" content={ratingCount} />
              <meta itemProp="worstRating" content="1" />
              <meta itemProp="bestRating" content="5" />
            </span>
          )}

          <meta itemProp="image" content={imageLink} />
          <meta itemProp="description" content={plainDescription} />
          {sortPrice && sortPrice != '0' && (
            <span itemType="http://schema.org/Offer" itemScope itemProp="offers">
              <meta itemProp="priceCurrency" content="INR" />
              <meta itemProp="price" content={sortPrice} />
              <meta itemProp="priceValidUntil" content={priceValidUntil} />
              <meta itemProp="url" content={canonical} />
              {isProductAvailable ? (
                <link href="http://schema.org/InStock" itemProp="availability" />
              ) : (
                ''
              )}
            </span>
          )}
        </div>
      ) : (
        ''
      )}

      <h2>
        <span>{Config.Locale.userreviewandrating}</span>
      </h2>
      <div className="rating-values">
        <div className="user_review">
          <div>औसत रेटिंग {ratingCount} रेटिंग पर आधारित</div>
          {data && data.ur ? (
            <React.Fragment>
              <span>
                {Number.isNaN(Number.parseFloat(data.ur)) ? (
                  <b>{data.ur}</b>
                ) : (
                  <React.Fragment>
                    {data.ur}
                    <sub>/5</sub>
                  </React.Fragment>
                )}
              </span>
              {/* <b>{Config.Locale.averageuserrating}</b> */}
            </React.Fragment>
          ) : (
            ''
          )}
        </div>
        <div className="user_star">
          <ul>
            {defaultVal.map(item => {
              const gadgetWidth =
                typeof rateArr[item] !== 'undefined'
                  ? `${parseInt((rateArr[item] / max) * 100)}%`
                  : '0%';
              return (
                // eslint-disable-next-line react/jsx-key
                <li>
                  <span className="star_txt">{item} &#9733;</span>
                  <span className="star_bg">
                    <span
                      className={`star${item}`}
                      style={{ width: gadgetWidth }}
                      data-attr={typeof rateArr[item] !== 'undefined' ? rateArr[item] : '0'}
                    />
                  </span>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <button
        disabled={isReviewDisabled}
        onClick={getreview}
        showReviewCard={showReviewCard}
        className={`btn-blue ${isReviewDisabled ? 'disabled' : ''}`}
      >
        {Config.Locale.ratedevice}
      </button>
      {showReviewCard ? (
        <div className="rating_popup">
          <div className="container">
            <header>
              <h3>{Config.Locale.ratedevice}</h3>
              <span onClick={closereview} className="close_icon" />
            </header>
            <GadgetPostreview
              reviewID={data && data.id ? data.id : ''}
              showLoginRegister={showLoginRegister}
              loggedIn={loggedIn}
            />
          </div>
        </div>
      ) : (
        ''
      )}
    </div>
  ) : (
    ''
  );
};

GadgetsRating.propTypes = {
  data: PropTypes.object,
  getreview: PropTypes.func,
  closereview: PropTypes.func,
};

export default GadgetsRating;
