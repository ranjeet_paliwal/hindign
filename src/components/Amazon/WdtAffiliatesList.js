/* eslint-disable indent */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Link from '../Link/Link';
import Thumb from '../Thumb/Thumb';
import './WdtAffiliatesList.scss';
import Slider from '../NewSlider/NewSlider';

const siteConfig = require(`../../../common/${process.env.SITE}`);

class WdtAffiliatesList extends PureComponent {
  state = {
    data: {},
  };

  componentDidMount() {
    const { category } = this.props;
    const categoryName = category !== undefined && category ? category : 'mobile';

    // console.log("category :"+category);
    // console.log("categoryName :"+categoryName);
    const APIURL = `https://shop.gadgetsnow.com/api/affiliate-api/affiliatebestsellers.php?category=${categoryName}&format=json&product_per_page=30&showAffiliate=amazon`;

    fetch(APIURL)
      .then(response => response.json())
      .then(data => {
        this.setState({
          data,
        });
      });
  }

  render() {
    const { data } = this.state;
    const { title, tag, noimg, noh2, isslider } = this.props;

    // console.log("title :"+title);
    // console.log("tag :"+tag);
    // console.log("noimg :"+noimg);
    // console.log("noh2 :"+noh2);

    if (data && data.product && Array.isArray(data.product) && isslider === '1') {
      return (
        <div className="wdt_amazon_list horizontal">
          {noh2 !== undefined && noh2 !== 1 ? (
            <h2 className="sectionHead">
              <span>{title !== undefined && title ? title : 'स्पॉन्सर्ड: स्टोर में'}</span>
              <b className="amazon_icon" />
            </h2>
          ) : null}
          {data && data.product ? (
            <LazyLoad>
              <Slider
                type="amazonwidgetslider"
                size="5"
                sliderData={data.product}
                category=""
                SliderClass="brandslider"
                islinkable="true"
                sliderWidth="960"
                tag={tag}
              />
            </LazyLoad>
          ) : null}
        </div>
      );
    }

    if (data && data.product && Array.isArray(data.product)) {
      return (
        <div className="section wdt_amazon_list with-scroll">
          {noh2 !== undefined && noh2 !== 1 ? (
            <h2>
              <span>{title !== undefined && title ? title : 'स्पॉन्सर्ड: स्टोर में'}</span>
              <b className="amazon_icon" />
            </h2>
          ) : null}
          <div className="content">
            <ul>
              {data && data.product && Array.isArray(data.product)
                ? data.product.map(item => {
                    return <LiData key={item.pid} item={item} tag={tag} noimg={noimg} />;
                  })
                : null}
            </ul>
          </div>
        </div>
      );
    }
    return null;
  }
}

const LiData = ({ item, tag, noimg, apnd }) => {
  // console.log('==============LiData========');
  // console.log('item', item);
  // console.log('tag', tag);
  // console.log('noimg', noimg);
  if (item.affiliate === 'amazon') {
    const amzga = `${item.title}_${item.price}`;
    const url = encodeURIComponent(
      `${
        item.url
      }?psc=1&SubscriptionId=AKIAJX7PDW7DEP2LLJLA&tag=${tag}&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B07HGJK535`,
    );
    const affprice = item.price;
    const affpriceActual = item.listPrice;
    const cashback = item.cashback;
    const is_prime = item.is_prime;
    const affiliateUrl = `https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=${url}&price=${affprice}&title=${
      item.title
    }&amz_ga=${amzga}`;

    // console.log("amzga :"+amzga);
    // console.log("url :"+url);
    // console.log("affiliateUrl :"+affiliateUrl);

    return (
      <li key={item.pid} className="gn-amazon-listitem">
        <Link title={item.title} to={affiliateUrl} target="_blank" rel="nofollow">
          {noimg !== undefined && noimg !== 1 ? (
            <span className="img_wrap">
              <LazyLoad>
                <Thumb
                  src={item.imageUrl}
                  imgSize=""
                  width=""
                  height=""
                  alt={item.title}
                  title={item.title}
                  islinkable=""
                  link=""
                />
              </LazyLoad>
            </span>
          ) : null}

          <span className="con_wrap">
            <h4 className="text_ellipsis">{item.title}</h4>
            <span className="price_tag">&#8377; {item.price}</span>
          </span>
          <span className="btn_wrap">
            <button className="buy_btn">{siteConfig.Locale.tech.purchase} </button>
          </span>
        </Link>
      </li>
    );
  }
};

WdtAffiliatesList.propTypes = {
  title: PropTypes.string,
  category: PropTypes.string,
  tag: PropTypes.string,
  noimg: PropTypes.string,
  noh2: PropTypes.string,
  isslider: PropTypes.string,
};

export default WdtAffiliatesList;
