import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';
import Thumb from '../Thumb/Thumb';
import './WdtAmazonSeeAlso.scss';

class WdtAmazonSeeAlso extends PureComponent {
  state = {
    data: {},
  };

  componentDidMount() {
    const { amKeywords, perpage } = this.props;

    // console.log("amKeywords :"+amKeywords);
    // console.log("perpage :"+perpage);

    const APIURL = `${
      process.env.API_ENDPOINT
    }/feed_amazonseealso.cms?am_BrowseNode=976420031&am_Keywords=${amKeywords}&perpage=${perpage}&am_Available=Available&feedtype=sjson`;
    // console.log("APIURL :"+APIURL);

    fetch(APIURL)
      .then(response => response.json())
      .then(data => {
        // console.log("======fetch data=====",data);
        if (data !== null) {
          this.setState({
            data,
          });
        }
      });
  }

  render() {
    const { data } = this.state;
    const { msid, tag, noimg } = this.props;

    //  console.log("msid :"+msid);
    // console.log("tag :"+tag);
    // console.log("noimg :"+noimg);

    return (
      <div className="amazon_seealso">
        <h2 className="heading">See Also</h2>
        <ul>
          {data && data.product && Array.isArray(data.product)
            ? data.product.map(item => {
                return <LiData item={item} tag={tag} noimg={noimg} />;
              })
            : null}
        </ul>
      </div>
    );
  }
}

const LiData = ({ item, tag, noimg }) => {
  // console.log('==============LiData========');
  // console.log('item', item);
  // console.log('tag', tag);
  // console.log('noimg', noimg);
  if (item.affiliate === 'amazon') {
    const amzga = `${item.title}_${item.price}`;
    const urlstr = item.url;
    const url = encodeURIComponent(urlstr.replace('timofind-21', `${tag}`));
    const affiliateUrl = `https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=${url}&price=${
      item.price
    }&title=${item.title}&amz_ga=${amzga}`;

    // console.log("amzga :"+amzga);
    // console.log("url :"+url);
    // console.log("affiliateUrl :"+affiliateUrl);

    return (
      <li key={item.pid} className="">
        <Link title={item.title} to={affiliateUrl} target="_blank" rel="nofollow">
          <div className="tableR">
            {noimg !== undefined && noimg !== '1' ? (
              <div className="tableI">
                <Thumb
                  src={item.imageUrl}
                  imgSize=""
                  width=""
                  height=""
                  alt={item.title}
                  title={item.title}
                  islinkable=""
                  link=""
                />
              </div>
            ) : null}

            <div className="tableC">
              <h4>{item.title}</h4>
              <span className="amprice">
                <strong>&#8377; {item.price}</strong>
              </span>
            </div>

            <div className="tableC logo-prime">
              <i className="logo" />
              {item.is_prime !== undefined && item.is_prime == '1' ? <i className="prime" /> : null}
            </div>
            <div className="tableC">
              <span className="buy-btn">BUY NOW</span>
            </div>
          </div>
        </Link>
      </li>
    );
  }
};

WdtAmazonSeeAlso.propTypes = {
  amKeywords: PropTypes.string,
  perpage: PropTypes.string,
  tag: PropTypes.string,
  msid: PropTypes.string,
  noimg: PropTypes.string,
};

export default WdtAmazonSeeAlso;
