import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';
import Thumb from '../Thumb/Thumb';
import './WdtAmazonDetail.scss';
import Slider from '../NewSlider/NewSlider';
import WdtAffiliatesList from './WdtAffiliatesList';

const siteConfig = require(`../../../common/${process.env.SITE}`);

class WdtAmazonDetail extends PureComponent {
  state = {
    data: {},
  };

  componentDidMount() {
    const { productid } = this.props;
    // console.log(`productid :${productid}`);
    // console.log(process.env.API_ENDPOINT);

    const APIURL = `${
      process.env.API_ENDPOINT
    }/amazonshowwidget.cms?feedtype=json&productid=${productid}`;
    // console.log(`APIURL :${APIURL}`);

    fetch(APIURL)
      .then(response => response.json())
      .then(data => {
        // console.log("data",data);
        if (data !== null) {
          this.setState({
            data,
          });
        }
      });
  }

  render() {
    const { data } = this.state;
    const { affview, noimg, tag, msid, type, title } = this.props;
    let secData = '';
    secData =
      data &&
      data.product &&
      data.product.filter(data => {
        return data.affiliate === 'amazon';
      });

    if (data && affview === '0') {
      if (data && data.product !== undefined && data.product) {
        var productdata = data.product.slice(0, 4);
      } else {
        var productdata = {};
      }
      return (
        <div>
          <h2 className="heading">
            <span>See Also</span>
          </h2>
          <div className="amazon_seealso">
            <ul>
              {productdata && Array.isArray(productdata)
                ? productdata.map(item => {
                  return <LiData item={item} tag={tag} noimg={noimg} apnd="_seealsowidget-21" />;
                })
                : null}
            </ul>
          </div>
        </div>
      );
    }

    if (data && affview === '1') {
      if (data && data.product !== undefined && data.product) {
        var productdata = data.product.slice(0, 2);
        // console.log('productdata',productdata);
      } else {
        var productdata = {};
      }

      return (
        <div>
          <h2 className="heading">
            <span>Buy At</span>
          </h2>
          <div className="amazon_buyat">
            <ul>
              {productdata && Array.isArray(productdata)
                ? productdata.map(item => {
                  return <LiData item={item} tag={tag} noimg={noimg} apnd="_buyatwidget-21" />;
                })
                : null}
            </ul>
          </div>
        </div>
      );
    }

    if (data && affview === '2') {
      if (data && data.product !== undefined && data.product) {
        var productdata = data.product.slice(0, 2);
        // console.log('productdata',productdata);
      } else {
        var productdata = {};
      }

      return (
        <div>
          <h2 className="heading">
            <span>Buy At</span>
          </h2>
          <div className="amazon_buyat">
            <ul>
              {productdata && Array.isArray(productdata)
                ? productdata.map(item => {
                  return <LiData item={item} tag={tag} noimg={noimg} apnd="_buyatwidget-21" />;
                })
                : null}
            </ul>
          </div>
        </div>
      );
    }

    if (data && affview === '3') {
      const amazonlistId = `amazonlist_${msid}`;
      if (data && data.product) {
        return (
          <div
            className={`${
              type === 'articleshow' ? 'wdt_amazon_list horizontal' : 'wdt_amz_horizontal'
            }`}
          >
            <h2 className="sectionHead">
              <span>{title || siteConfig.Locale.tech.buyhere}</span>
              <b className="amazon_icon" />
            </h2>
            <div id={amazonlistId}>
              {data && data.product ? (
                <Slider
                  type="amazonwidgetslider"
                  size={type === 'articleshow' ? '2' : '1'}
                  sliderData={secData}
                  category=""
                  SliderClass="brandslider"
                  islinkable="true"
                  sliderWidth={type === 'articleshow' ? '200' : '600'}
                  tag={tag}
                />
              ) : (
                ''
              )}
            </div>
          </div>
        );
      }
      return null;
    }
    if (affview === '3') {
      return (
        <div>
          <WdtAffiliatesList
            category="mobile"
            amazontag="nbt_web_articlelist_sponsoredwidgetrhs"
            isslider="1"
            noimg="0"
            noh2="0"
          />
        </div>
      );
    }

    return null;
  }
}

const LiData = ({ item, tag, noimg, apnd }) => {
  // console.log('==============LiData========');
  // console.log('item', item);
  // console.log('tag', tag);
  // console.log('noimg', noimg);
  // console.log('apnd:', apnd);

  if (item.affiliate === 'amazon') {
    const amzga = `${item.title}_${item.price}`;
    const urlstr = item.url;
    const url = encodeURIComponent(urlstr.replace('timofind-21', `${tag}${apnd}`));
    const affprice = item.price;
    const affpriceActual = item.listPrice;
    const cashback = item.cashback;
    const isPrime = item.is_prime;
    const affiliateUrl = `https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=${url}&price=${affprice}&title=${
      item.title
    }&amz_ga=${amzga}`;

    // console.log('amzga :' + amzga);
    // console.log('url :' + url);
    // console.log('affiliateUrl :' + affiliateUrl);

    return (
      <li key={item.pid} className="gn-amazon-listitem">
        <Link title={item.title} to={affiliateUrl} rel="nofollow" target="_blank">
          <div className="tableR tableR amazon">
            {noimg !== undefined && noimg !== 1 ? (
              <div className="tableI">
                <Thumb
                  src={item.imageUrl}
                  imgSize=""
                  width="200"
                  height="150"
                  alt={item.title}
                  title={item.title}
                  islinkable=""
                  link=""
                />
              </div>
            ) : null}

            <div className="tableC">
              <h4>{item.title}</h4>
              {affprice !== undefined && affprice > 0 ? (
                <span className="amprice">
                  <strong>&#8377; {affprice}</strong>
                </span>
              ) : null}
              {affpriceActual !== undefined && affpriceActual > 0 && affpriceActual > affprice ? (
                <span className="ampricestk">
                  <strong>&#8377; {affpriceActual}</strong>
                </span>
              ) : null}
              {cashback !== undefined && cashback ? (
                <div className="box-affcb">
                  <span className="affcb">incl. Cashback</span>
                </div>
              ) : null}
            </div>

            <div className="tableC logo-prime">
              <i className="logo" />
              {isPrime !== undefined && isPrime ? <i className="prime" /> : null}
            </div>

            <div className="tableC">
              <span className="buy-btn">BUY NOW</span>
            </div>
          </div>
        </Link>
      </li>
    );
  }
};

WdtAmazonDetail.propTypes = {
  productid: PropTypes.string,
  affview: PropTypes.string,
  noimg: PropTypes.string,
  tag: PropTypes.string,
  msid: PropTypes.string,
  type: PropTypes.string,
  title: PropTypes.string,
};

export default WdtAmazonDetail;
