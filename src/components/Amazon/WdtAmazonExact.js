import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Link from '../Link/Link';
import Thumb from '../Thumb/Thumb';
import './WdtAmazonExact.scss';

class WdtAmazonExact extends PureComponent {
  render() {
    const { data, tag, msid, type, title } = this.props;
    // console.log('tag :', tag);
    // console.log('msid :', msid);
    // console.log('type :', type);
    // console.log('title :', title);

    if (data) {
      return (
        <div className="wdt_amz_horizontal single">
          <h2 className="sectionHead">
            {title || 'यहां खरीदें'}
            <b className="amazon_icon" />
          </h2>
          <div className="slider">
            <div className="slider_content">
              <ul>
                <LiData item={data} tag={tag} type={type} />
              </ul>
            </div>
          </div>
        </div>
      );
    }

    return null;
  }
}

const LiData = ({ item, tag, type }) => {
  // console.log('==============LiData========');
  // console.log('item', item);
  // console.log('tag', tag);
  // console.log('type', type);

  if (item.Identifier === 'amazon') {
    const amzga = `${item.name}_${item.sort_price}`;
    const urlstr = item.url;
    const url = encodeURIComponent(urlstr.replace('timofind-21', `${tag}`));
    const affprice = item.sort_price;
    const affpriceActual = item.ListPrice.Amount;
    const cashback = item.cashback;
    const isPrime = item.is_prime;
    const affiliateUrl = `https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=${url}&price=${affprice}&title=${
      item.name
    }&amz_ga=${amzga}`;

    // console.log(`amzga :${amzga}`);
    // console.log(`url :${url}`);
    // console.log(`affiliateUrl :${affiliateUrl}`);

    return (
      <li key={item.sort_price}>
        <div className="slide">
          <Link title={item.name} to={affiliateUrl} target="_blank" rel="nofollow">
            <span className="img_wrap">
              <Thumb
                src={item.image_url}
                imgSize=""
                width=""
                height=""
                alt={item.name}
                title={item.name}
                islinkable=""
                link=""
              />
            </span>
            <span className="con_wrap">
              <h4 className="text_ellipsis">{item.name}</h4>
              <span className="price_tag">
                {affprice !== undefined && affprice > 0 ? (
                  <React.Fragment>&#8377; {affprice}</React.Fragment>
                ) : null}
                {affpriceActual !== undefined && affpriceActual > 0 && affpriceActual > affprice ? (
                  <b>&#8377; {affpriceActual}</b>
                ) : null}
              </span>
            </span>

            {/* {cashback !== undefined && cashback ? (
                <div className="box-affcb">
                  <span className="affcb">incl. Cashback</span>
                </div>
              ) : null} */}

            {/* <div className="tableC logo-prime">
              <i className="logo" />
              {isPrime !== undefined && isPrime ? <i className="prime" /> : null}
            </div> */}

            <span className="btn_wrap">
              <button className="buy_btn">खरीदे</button>
            </span>
          </Link>
        </div>
      </li>
    );
  }

  return null;
};

WdtAmazonExact.propTypes = {
  data: PropTypes.array,
  tag: PropTypes.string,
  msid: PropTypes.string,
  type: PropTypes.string,
  title: PropTypes.string,
};

export default WdtAmazonExact;
