/* eslint-disable no-unused-expressions */
/* eslint-disable no-multi-assign */
/* eslint-disable wrap-iife */
/* eslint-disable func-names */
import React from 'react';

export function setCookie(name, value, expires, domain) {
  domain = domain || '';
  const today = new Date();
  today.setTime(today.getTime());
  if (expires) {
    expires = expires * 1000 * 60 * 60 * 24;
  }
  const expireTo = new Date(today.getTime() + expires);
  if (typeof document !== 'undefined') {
    document.cookie = `${name}=${escape(value)}; expires=${expireTo}; domain=${domain}`;
  }
}

export function getCookie(cname) {
  if (typeof document !== 'undefined') {
    const name = `${cname}=`;
    const decodedCookie = decodeURIComponent(document.cookie);
    const ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) === ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }
  }
  return '';
}

export function deleteCookie(name, domain) {
  const expireTo = 'Thu, 01 Jan 1970 00:00:01 GMT;';
  if (typeof document !== 'undefined') {
    document.cookie = `${name}=; expires=${expireTo}; domain=${domain}`;
  }
}

export function getDifferenceInHours(startDate, endDate) {
  const timeStart = new Date(startDate).getTime();
  const timeEnd = new Date(endDate).getTime();
  const hourDiff = timeEnd - timeStart; // in ms
  const secDiff = hourDiff / 1000; // in s
  const minDiff = hourDiff / 60 / 1000; // in minutes
  const hDiff = hourDiff / 3600 / 1000; // in hours

  const totalSeconds = Math.floor(secDiff);
  const totalMinutes = Math.floor(minDiff);
  const totalHours = Math.floor(hDiff);
  const totalDays = Math.floor(totalHours / 24);
  const totalMonths = Math.floor(totalDays / 30);
  const totalYears = Math.floor(totalDays / 365);

  let strDuration = '';

  if (totalYears >= 1) {
    strDuration = totalYears === 1 ? `${totalYears} year ago` : `${totalYears} years ago`;
  } else if (totalMonths >= 1) {
    strDuration = totalMonths === 1 ? `${totalMonths} month ago` : `${totalMonths} months ago`;
  } else if (totalDays >= 1) {
    strDuration = totalDays === 1 ? `${totalDays} day ago` : `${totalDays} days ago`;
  } else if (totalHours >= 1) {
    strDuration = totalHours === 1 ? `${totalHours} hour ago` : `${totalHours} hours ago`;
  } else if (totalMinutes >= 1) {
    strDuration = totalMinutes === 1 ? `${totalMinutes} minute ago` : `${totalMinutes} minutes ago`;
  } else if (totalSeconds >= 1) {
    strDuration = totalSeconds === 1 ? `${totalSeconds} second ago` : `${totalSeconds} seconds ago`;
  } else {
    strDuration = ' Just now';
  }
  return strDuration;
}

export function adsPlaceholder(className) {
  return <div className={`${className}`} />;
}

export function truncateStr(str, length) {
  return str && str.length > length ? `${str.substr(0, length)}..` : str;
}

export function addToWatchLater(videoId) {
  return fetch(
    `https://myt.indiatimes.com/myt.indiatimes.com/mytimes/addActivityDdup/?appKey=${
      Config.APP_KEY
    }&baseEntityType=VIDEO&objectType=B&activityType=WishList&_=1498213873895&uniqueAppID=${videoId}`,
    { type: 'jsonp' },
  );
}

export function getAlreadyAddedVideos() {
  return fetch(
    'https://myt.indiatimes.com/mytimes/myActivity?activityType=WishList&openNetworkId=sso&appKey=NBTO&contID=ln_myActivity&_=1498220179473&size=10&lastSeenId=0&after=true',

    { type: 'jsonp' },
  )
    .then(() => {
      // console.log(data);
    })
    .catch(() => {
      // console.log('ERROR in loading video data', e);
    });
}

export function removeFromWatchLater(videoId, activityId) {
  if (typeof activityId === 'undefined') {
    fetch(
      `https://myt.indiatimes.com/mytimes/activities/entity?appKey=${
        Config.APP_KEY
      }&activityType=WishList&uniqueAppID=${videoId}`,
      { type: 'jsonp' },
    ).then(data => {
      if (!data.length) {
        return;
      }
      if (data[0] && data[0].id) {
        removeFromWatchLater(videoId, data[0].id);

        return fetch(
          `https://myt.indiatimes.com/mytimes/removeActivity?appKey=${
            Config.APP_KEY
          }&baseEntityType=VIDEO&objectType=B&activityType=WishList&_=1500551520199&activityId=${activityId}&uniqueAppID=${videoId}`,
          { type: 'jsonp' },
        )
          .then(data => {
            return true;
          })
          .catch(e => false);
      }
    });
  }
}
/* export function initFBSDK() {
  window.fbAsyncInit = function() {
    FB.init({
      appId: '154495568087154',
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v2.7',
    });
  };

  (function(d, s, id) {
    var js,
      fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
      return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  })(document, 'script', 'facebook-jssdk');
}
 */

/*
  https://stackoverflow.com/questions/
  123999/how-to-tell-if-a-dom-element-is-visible-in-the-current-viewport
  If element height exceeds viewport height , this works better
 */
export function isInViewport(elem) {
  if (!elem) return false;
  const rect = elem.getBoundingClientRect();
  return (
    rect.bottom >= 0 &&
    rect.right >= 0 &&
    rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
    rect.left <= (window.innerWidth || document.documentElement.clientWidth)
  );
}

export function loadJS(path, callback, attr) {
  const Attr = attr || 'defer';
  const script = document.createElement('script');
  script.setAttribute(Attr, Attr);
  if (callback) {
    if (script.readyState) {
      // IE <9
      script.onreadystatechange = () => {
        if (script.readyState === 'loaded' || script.readyState === 'complete') {
          script.onreadystatechange = null;
          callback();
        }
      };
    } else {
      // Other Browser
      script.onload = () => {
        callback();
      };
    }
  }

  script.setAttribute('src', path);
  document.body.appendChild(script);
}

/* const isInViewport = elem => {
  const distance = elem.getBoundingClientRect();
  return (
    distance.top >= 0 &&
    distance.left >= 0 &&
    distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
    distance.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
}; */

export function initEvent(eventName, fn, options = null) {
  window.addEventListener(eventName, fn);
}

export function removeEvent(eventName, fn, options = null) {
  window.removeEventListener(eventName, fn);
}

// interstitial code
export function loadInterStitial(value) {
  // interstitial close for google bot
  const googleBot = /bot|googlebot|crawler|spider|robot|crawling/i.test(navigator.userAgent);

  if (!googleBot && !getCookie('interstitial_done')) {
    if (value) {
      setCookie('interstitial_done', true);
      window.location.replace('https://navbharattimes.indiatimes.com/defaultinterstitial.cms');
    }
  }
  return false;
}

export function fbPixel(pixelId) {
  !(function(f, b, e, v, n, t, s) {
    if (f.fbq) return;
    n = f.fbq = function() {
      n.callMethod ? n.callMethod(...arguments) : n.queue.push(arguments);
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s);
  })(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', pixelId);
  fbq('track', 'PageView');
}

export function backHandler() {
  if (typeof document !== 'undefined' && document.referrer) {
    const referrer = document.referrer.match(/:\/\/(.[^/]+)/)[1];
    if (
      referrer.search(/indiatimes\.com|samayam\.com|langdev|jcmsdev|toidev|defaultinterstitial/) !==
      -1
    ) {
      return;
    }
    const stateObj = {}; // Just want to change the current history
    const goToHomeURL = `${location.origin}/?back=1`;
    // history.replaceState(stateObj, document.title, location.href + hash);
    history.pushState(stateObj, document.title, location.href);
    window.addEventListener(
      'popstate',
      () => {
        location.replace(goToHomeURL);
      },
      false,
    );
  }
}

export function formatMoney(number, decPlaces, decSep, thouSep) {
  decPlaces = isNaN((decPlaces = Math.abs(decPlaces))) ? 2 : decPlaces;
  decSep = typeof decSep === 'undefined' ? '.' : decSep;
  thouSep = typeof thouSep === 'undefined' ? ',' : thouSep;

  const sign = number < 0 ? '-' : '';
  const i = String(parseInt((number = Math.abs(Number(number) || 0).toFixed(decPlaces))));
  var j = (j = i.length) > 3 ? j % 3 : 0;

  return (
    sign +
    (j ? i.substr(0, j) + thouSep : '') +
    i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, `₹ 1${thouSep}`) +
    (decPlaces
      ? decSep +
        Math.abs(number - i)
          .toFixed(decPlaces)
          .slice(2)
      : '')
  );
}

export function objToQueryStr(obj) {
  if (obj && typeof obj === 'object' && obj.constructor === Object) {
    return Object.keys(obj)
      .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(obj[k])}`)
      .join('&');
  }
  return '';
}

export function getKeyByValue(dataObject, value) {
  for (const key in dataObject) {
    if (Object.prototype.hasOwnProperty.call(dataObject, key)) {
      if (dataObject[key] === value) return key;
    }
  }
  return '';
}

export const _getfeedCategory = category => {
  if (category == 'smartwatch' || category == 'fitnessband') {
    return category;
  }
  if (category == 'mobile-phones') {
    category = 'mobile';
  } else if (category != null && category != undefined) {
    category = category.substr(0, category.length - 1);
  }
  return category;
};
export const isCSR = () => {
  return typeof window != 'undefined';
};
export const _getStaticConfig = additionalPath => {
  // console.log(siteConfig.wapsitename + '-------------------------')
  return additionalPath && additionalPath == 'cssConfig'
    ? cssConfig
    : additionalPath
    ? footerSiteConfig
    : siteConfig;
};

export function getBuyLink({ type, affiliateData, url, price, title, amzga, tag }) {
  let buyURL = '';
  const linkCategory = !type ? 'amazon' : type;
  if (url) {
    // in case we have direct amazon URL
    buyURL = url;
  } else if (affiliateData && affiliateData.exact && affiliateData.exact[0]) {
    buyURL = affiliateData.exact[0].url;
  } else if (affiliateData && affiliateData.related) {
    if (affiliateData.related[0]) {
      const amazonData = affiliateData.related.filter(amazonItem => {
        return amazonItem.Identifier === linkCategory;
      });
      if (amazonData && amazonData[0]) {
        buyURL = amazonData[0].url;
      }
    } else {
      buyURL = affiliateData.related.items.url;
    }
  }

  let affiliateUrl = '';
  let finalURL = '';

  if (buyURL && buyURL.indexOf('timofind') !== -1) {
    finalURL = buyURL.replace('timofind', tag);
  } else {
    finalURL = buyURL.indexOf('?') === -1 ? `${buyURL}?tag=${tag}` : `${buyURL}&tag=${tag}`;
  }

  if (linkCategory === 'amazon') {
    affiliateUrl = `https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=${encodeURIComponent(
      finalURL,
    )}&price=${price}&title=${title}&amz_ga=${amzga}`;
  }
  return affiliateUrl;
}

/**
 * get config file data as per SITE
 * @param {Object} options for additional folder in common/
 * */
