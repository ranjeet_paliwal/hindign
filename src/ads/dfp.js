// import { loadJS } from '../common-utility';

/* eslint-disable operator-linebreak */
/* eslint-disable import/prefer-default-export */
/* eslint-disable camelcase */
/* eslint-disable no-lonely-if */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
/* eslint-disable no-underscore-dangle */

// const Config = require(`../../common/${process.env.SITE}`);
// const wapads = Config.Ads.DFP || {};

let adSlot_Skin_LHS = null;
let adSlot_Skin_RHS = null;
let _Ref = 'nbt_web';

if (typeof document !== 'undefined' && document.referrer) {
  const referrer = document.referrer;
  const selfurl = document.location.href;
  const UrlParams = new URL(selfurl);
  const utmSource = UrlParams.searchParams.get('utm_source');

  if (referrer.indexOf('facebook') !== -1) {
    _Ref = 'facebook_web';
  } else if (referrer.indexOf('google') !== -1) {
    _Ref = 'google_web';
  } else if (referrer.indexOf('twitter') !== -1) {
    _Ref = 'twitter_web';
  } else if (referrer.indexOf('whatsapp') !== -1) {
    _Ref = 'whatsapp_web';
  } else if (utmSource) {
    _Ref = `${utmSource}_web`;
  } else if (document.referrer) {
    _Ref = 'others_web';
  }
}

const AdConfig = {
  defSlots: [[320, 50], [468, 60], [728, 90]],
};

function defineAdSlots(wapads, config) {
  const googletag = typeof window !== 'undefined' && window.googletag ? window.googletag : {};
  const _SCN = config._SCN;
  const _SubSCN = config._SubSCN;
  const _auds = [];

  const _HDL = '';
  const _ARC1 = 'strong';
  const _Hyp1 = '';
  const _article = '';
  const _tval = param => {
    if (typeof param === 'undefined') return '';
    if (param.length > 100) return param.substr(0, 100);
    return param;
  };

  for (const key in wapads) {
    const item = wapads[key];
    const divElm = document.querySelector(`div.${item.type}`);
    const divContent = divElm ? divElm.innerHTML.trim() : '';
    const itemType = document.querySelector(`div.${item.type}`);
    if (typeof googletag !== 'undefined' && typeof googletag.defineSlot !== 'undefined') {
      if (
        item.type === 'AS_Innov1' ||
        item.type === 'AL_Innov1' ||
        item.type === 'OP_Innov1' ||
        item.type === 'OP_Inter' ||
        item.type === 'OP_Pop' ||
        item.type === 'OP_Shosh'
      ) {
        if (itemType && !divContent) {
          googletag.defineOutOfPageSlot(item.name, item.id).addService(googletag.pubads());
        }
      } else {
        if (item.type === 'Skin_LHS') {
          adSlot_Skin_LHS = googletag
            .defineSlot(item.name, item.size ? item.size : AdConfig.defSlots, item.id)
            .addService(googletag.pubads());
        } else if (item.type === 'Skin_RHS') {
          adSlot_Skin_RHS = googletag
            .defineSlot(item.name, item.size ? item.size : AdConfig.defSlots, item.id)
            .addService(googletag.pubads());
        } else if (item.type === 'ATF_300') {
          window.adSlot_ATF_300 = googletag
            .defineSlot(item.name, item.size ? item.size : AdConfig.defSlots, item.id)
            .addService(googletag.pubads());
        } else {
          try {
            if (itemType && (!divContent || divContent.indexOf('ATF_728') !== -1)) {
              googletag
                .defineSlot(item.name, item.size ? item.size : AdConfig.defSlots, item.id)
                .addService(googletag.pubads());
            }
          } catch (ex) {}
        }
      }
    }
  }

  if (typeof googletag !== 'undefined') {
    googletag
      .pubads()
      .setTargeting('sg', _auds)
      .setTargeting('HDL', _tval(_HDL))
      .setTargeting('ARC1', _tval(_ARC1))
      .setTargeting('Hyp1', _tval(_Hyp1))
      .setTargeting('_ref', _tval(_Ref))
      .setTargeting('SCN', _tval(_SCN))
      .setTargeting('SubSCN', _tval(_SubSCN))
      .setTargeting('article', _tval(_article));

    googletag.pubads().enableSingleRequest();
    googletag.pubads().collapseEmptyDivs(true);

    googletag.enableServices();
  }
}

function createAds(wapads) {
  const googletag = typeof window !== 'undefined' && window.googletag ? window.googletag : {};

  for (const key in wapads) {
    const item = wapads[key];
    const tmpScript = document.createElement('script');
    tmpScript.type = 'text/javascript';
    const divElem = document.createElement('div');
    divElem.setAttribute('id', item.id);
    divElem.appendChild(tmpScript);
    const divElm = document.querySelector(`div.${item.type}`);
    const divContent = divElm ? divElm.innerHTML.trim() : '';
    if (divElm && (!divContent || divContent.indexOf('ATF_728') !== -1)) {
      divElm.append(divElem);
    }

    googletag.cmd.push(() => {
      googletag.display(item.id); // div-gpt-ad-1387801207758-0
    });
  }
}

/* function processingGPT() {
  if (typeof document !== 'undefined') {
    const gads = document.createElement('script');
    gads.async = true;
    gads.type = 'text/javascript';
    const useSSL = 'https:' == document.location.protocol;
    gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
    const node = document.getElementsByTagName('script')[0];
    node.parentNode.insertBefore(gads, node);
  }
} */
const processAds = (wapads, config) => {
  if (typeof window !== 'undefined') {
    googletag.cmd = googletag.cmd || [];
  }
  try {
    googletag.cmd.push(defineAdSlots(wapads, config));
  } catch (ex) {}

  createAds(wapads);

  googletag.cmd.push(() => {
    let OP_Innov1 = document.querySelector('.OP_Innov1>div');
    OP_Innov1 = OP_Innov1 ? OP_Innov1.id : null;
    const googletag = typeof window !== 'undefined' && window.googletag ? window.googletag : {};
    googletag.pubads().addEventListener('slotRenderEnded', event => {
      if (OP_Innov1 && event.slot.getSlotElementId() === OP_Innov1) {
        if (!event.isEmpty) {
          googletag.destroySlots([adSlot_Skin_LHS]);
          googletag.destroySlots([adSlot_Skin_RHS]);
        }
      }
    });
  });
};

export function initGoogleAds(wapads, sectionInfo) {
  // const Config = {
  //    pageType: 'articleshow',
  //   _SCN: 'elections',
  //   _SubSCN: 'lok-sabha-elections',
  //   _LastSubSCN: 'news',
  //   _Ctnkeyword: 'PM Modi Rally in Rajasthan, PM Modi',
  //   _Tmpl_SCN: 'articleshow',
  // };
  const AdsConfig = {
    pageType: (sectionInfo && sectionInfo.pageType) || 'articleshow',
    _SCN: (sectionInfo && sectionInfo.sec1) || '',
    _SubSCN: (sectionInfo && sectionInfo.sec2) || '',
    _LastSubSCN: (sectionInfo && sectionInfo.sec3) || '',
    _Hyp1: (sectionInfo && sectionInfo.hyp1) || '',
  };

  if (typeof googletag !== 'undefined') {
    // processAds(wapads);
    setTimeout(() => {
      processAds(wapads, AdsConfig);
    }, 600);
  }
}

export function refreshAds(item) {
  // const item = wapads.ATF_728;

  /*  _slotdef = googletag.defineSlot('/7176/Navbharattimes/NBT_Bharat_NW/NBT_Bharat_NW_AS/NBT_ROS_ATF_BHT_NW_AS_728', [728,90], 'div-gpt-1542622032778-2-69629515').addService(googletag.pubads());
      googletag.cmd.push(() => {
          googletag.display('div-gpt-1542622032778-2-69629515');
         setTimeout(()=>{
            googletag.pubads().refresh([_slotdef]);
         }, 2000);
      });
  */
  try {
    if (
      typeof item === 'object' &&
      typeof googletag !== 'undefined' &&
      item &&
      item.name &&
      typeof googletag.defineSlot !== 'undefined'
    ) {
      if (item.size) {
        const adSlot = googletag
          .defineSlot(item.name, item.size, item.id)
          .addService(googletag.pubads());
        googletag.cmd.push(() => {
          googletag.pubads().refresh([adSlot]);
        });
      } else {
        const adSlot = googletag.defineSlot(item.name, item.id).addService(googletag.pubads());
        googletag.cmd.push(() => {
          googletag.pubads().refresh([adSlot]);
        });
      }
    }
  } catch (ex) {
    console.log('refreshAds defineSlot error', ex);
  }
}

// https://techpunch.co.uk/development/refresh-google-dfp-ads
