/* eslint-disable operator-linebreak */
/* eslint-disable indent */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import LazyLoad from 'react-lazyload';

import {
  removeEvent,
  initEvent,
  getCookie,
  adsPlaceholder,
  isInViewport,
} from '../../common-utility';
import ArticleShowFakeLoader from '../../components/Loaders/ArticleShowFakeLoader';
import { initGoogleAds } from '../../ads/dfp';
import fetchDataIfNeeded from '../../actions/home/news/news';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import Headline from '../../components/Home/Headlines/Headlines';
import { AnalyticsIBeat } from '../../analytics';
import NewsWidget from '../../components/Home/News/NewsWidget/NewsWidget';
import GadgetsBrands from '../../components/Home/GadgetsBrands/GadgetsBrands';
import Slider from '../../components/NewSlider/NewSlider';
import Link from '../../components/Link/Link';
import { getAds } from '../../actions/config/config';
import Thumb from '../../components/Thumb/Thumb';
import CtnAd from '../articleshow/subsections/CtnAd';
import Tabbing from '../../components/Tabbing/Tabbing';
import { updateNavData } from '../../actions/reviewlist';

const Config = require('../../../common/nbt.js');

class Index extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showElectionCube: false,
    };
  }
  componentDidMount() {
    const { dispatch, query, params, value } = this.props;
    const { headline } = value;

    Index.fetchData({ dispatch, query, params });
    // this.timerInterval = setInterval(() => {
    //   if (typeof window !== 'undefined') {
    //     window.location.href = '/';
    //   }
    // }, 600000);

    if (typeof initEvent === 'function') {
      initEvent('scroll', this.handleHomePageScroll);
    }

    dispatch(getAds('home')).then(data => {
      if (typeof initGoogleAds === 'function') {
        setTimeout(() => {
          initGoogleAds(data.payload.wapads);
        }, 1000);
      }
    });
    if (
      headline &&
      headline.ibeatconfig &&
      typeof AnalyticsIBeat === 'object' &&
      AnalyticsIBeat.initIbeat
    ) {
      AnalyticsIBeat.initIbeat(headline.ibeatconfig);
    }
    dispatch(
      updateNavData({
        sectionName: '/tech',
      }),
    );
  }

  handleHomePageScroll = () => {
    try {
      const firstLevelMenu = document.querySelector('#fixedMenu');
      if (firstLevelMenu) {
        const position = firstLevelMenu.getBoundingClientRect().y;
        if (position < -40) {
          firstLevelMenu.classList.add('fixed');
        } else {
          firstLevelMenu.classList.remove('fixed');
        }
        if (position < -700 && !getCookie('notifypopup') && getCookie('continent_gdpr') !== 'EU') {
          document.querySelector('.notifypopup').style.display = 'block';
        }
      }
    } catch (ex) {
      // console.log(ex);
    }
  };

  componentWillUnmount = () => {
    if (typeof googletag == 'object' && typeof googletag.destroySlots == 'function') {
      googletag.destroySlots();
    }

    if (this.timerInterval) {
      clearInterval(this.timerInterval);
    }
    const firstLevelMenu = document.querySelector('#fixedMenu');
    if (firstLevelMenu) {
      firstLevelMenu.classList.remove('fixed');
    }
    removeEvent('scroll', this.handleHomePageScroll);
  };

  render() {
    const { value, showLoginRegister, closeLoginRegister, isFetching, navData } = this.props;
    const { headline, pageMeta, sectionlist, sectionData, PhotosData, VideosData } = value;
    let SEOData = '';
    if (Array.isArray(value) || isFetching) {
      return <ArticleShowFakeLoader />;
    }
    let iplWidget = null;
    let electionWidget = null;
    let electionWidgetCUBE = null;
    let electionWidgetTOP = null;
    let feedConfig = null;

    const isValidData =
      (typeof sectionlist === 'object' &&
        sectionlist.section &&
        Array.isArray(sectionlist.section) &&
        sectionlist.section.length > 0) ||
      null;
    if (navData && navData.value && navData.value.topnav) {
      const topnavObj = navData.value.topnav;
      if (topnavObj && Array.isArray(topnavObj.ipliframe) && topnavObj.ipliframe.length > 0) {
        iplWidget = topnavObj.ipliframe[0];
      }
      if (
        topnavObj &&
        Array.isArray(topnavObj.electionframe) &&
        topnavObj.electionframe.length > 0
      ) {
        electionWidgetTOP = topnavObj.electionframetop[0];
      }
      if (Array.isArray(topnavObj.electionframe) && topnavObj.electionframe.length > 0) {
        electionWidget = topnavObj.electionframe[0];
      }
      if (Array.isArray(topnavObj.electionframecube) && topnavObj.electionframe.length > 0) {
        electionWidgetCUBE = topnavObj.electionframecube[0];
      }

      feedConfig = topnavObj.feedconfig;
    }

    const isValidSecData =
      (typeof sectionData === 'object' &&
        sectionData.section &&
        Array.isArray(sectionData.section) &&
        sectionData.section.length > 0) ||
      null;

    const gadgetsType =
      navData && navData.value && navData.value.navData && navData.value.navData.gadgets
        ? navData.value.navData.gadgets
        : '';

    const pageMetaCopy = (pageMeta && pageMeta.seodata) || null;
    const pageTitle = (pageMetaCopy && pageMetaCopy.title) || null;
    const pageMetaData =
      pageMetaCopy && pageMetaCopy.meta
        ? pageMetaCopy.meta.filter(element => {
            if (element) return element;
          })
        : null;
    const pageMetaLinks =
      pageMetaCopy && pageMetaCopy.links
        ? pageMetaCopy.links.filter(element => {
            if (element) return element;
          })
        : null;
    SEOData =
      typeof sectionData === 'object' &&
      sectionData.section &&
      Array.isArray(sectionData.section) &&
      sectionData.section.filter(data => {
        return data.id === '67572099';
      });
    // pageMetaData.push(null);
    return (
      <div className="contentarea clear">
        <Helmet
          title={pageTitle}
          titleTemplate="%s"
          meta={pageMetaData}
          link={pageMetaLinks}
          htmlAttributes={{ lang: 'hi' }}
        />

        <div className="leftmain">
          {headline && headline.items && Array.isArray(headline.items) ? (
            <ErrorBoundary>
              <Headline
                data={headline.items}
                position="top"
                ClassName="main_article"
                secname={Config.Locale.technews}
              />
            </ErrorBoundary>
          ) : null}

          {isValidData ? (
            <ErrorBoundary>
              {/* <LazyLoad height={100} once> */}
              <NewsWidget
                newsContent={sectionlist.section}
                type="review"
                secid="solrreview"
                secname={Config.Locale.review}
                imgrequired="all"
                count="5"
                headingRequired="yes"
                widgetClassName="full_section wdt_reviews"
              />
              {/* </LazyLoad> */}
            </ErrorBoundary>
          ) : (
            ''
          )}
        </div>

        <div className="rightnav">
          {adsPlaceholder('ATF_300 section ads_default fixedHeight')}

          {headline && headline.items && Array.isArray(headline.items) ? (
            <ErrorBoundary>
              <Headline data={headline.items} position="right" ClassName="top_other_headlines" />
            </ErrorBoundary>
          ) : null}

          <ErrorBoundary>{adsPlaceholder('BTF_300 section ads_default')}</ErrorBoundary>

          {isValidSecData ? (
            <ErrorBoundary>
              <LazyLoad height={100} once>
                <NewsWidget
                  newsContent={sectionData.section}
                  type="trending"
                  secid="68079558"
                  secname={Config.Locale.review}
                  imgrequired="none"
                  count="10"
                  widgetClassName="GN-trending"
                />
              </LazyLoad>
            </ErrorBoundary>
          ) : (
            ''
          )}

          {/* {isValidData ? (
            <ErrorBoundary>
              <LazyLoad height={100} once>
                <NewsWidget
                  newsContent={sectionlist.section}
                  type="misc"
                  secid="latestnews"
                  secname="latestnews"
                  imgrequired="none"
                  count="10"
                  headingRequired="yes"
                  widgetClassName="section"
                />
              </LazyLoad>
            </ErrorBoundary>
          ) : (
            ''
          )} */}
          <ErrorBoundary>
            <div className="section ATF_Mrec3 ads_default" />
          </ErrorBoundary>
        </div>
        <div className="full_section wdt_latest_videos">
          {typeof VideosData === 'object' && VideosData.section && VideosData.section.newsItem ? (
            <ErrorBoundary>
              {/* <LazyLoad height={100} once> */}
              <Slider
                type="videos"
                size="3"
                sliderData={VideosData.section.newsItem.items}
                width="300"
                height="168"
                SliderClass="videoslider"
                islinkable="true"
                link={
                  VideosData.section.newsItem && VideosData.section.newsItem.wu
                    ? VideosData.section.newsItem.wu
                    : ''
                }
                secname={Config.Locale.gadget.video}
                sliderWidth="930"
                headingRequired="yes"
                moreLinkRequired="yes"
                moreLinkText={Config.Locale.allvideos}
              />
              {/* </LazyLoad> */}
            </ErrorBoundary>
          ) : (
            ''
          )}
        </div>
        <div className="leftmain">
          <div className="full_section select-inline-gadgets">
            {SEOData && SEOData[0] && SEOData[0].items ? (
              <div className="seo-gadget-tags">
                {SEOData[0].items.map(value => {
                  return (
                    <span key={value.id}>
                      <Link to={value.wu}>{value.hl}</Link>
                    </span>
                  );
                })}
              </div>
            ) : (
              ''
            )}
            <ErrorBoundary>
              <LazyLoad height={100} once>
                <Tabbing seolinks={SEOData} />
              </LazyLoad>
            </ErrorBoundary>
          </div>

          {/* <ErrorBoundary>
            <div className="full_section">
              {adsPlaceholder('ATF_Mrec1 section ads_default')}
              {adsPlaceholder('ATF_Mrec2 section ads_default')}
            </div>
          </ErrorBoundary> */}
        </div>
        <div className="rightnav">
          {isValidData ? (
            <ErrorBoundary>
              <LazyLoad height={100} once>
                <NewsWidget
                  newsContent={sectionlist.section}
                  type="misc"
                  secid="ibeatmostshared"
                  secname="ibeatmostshared"
                  imgrequired="all"
                  count="5"
                  headingRequired="yes"
                  widgetClassName="section"
                />
              </LazyLoad>
            </ErrorBoundary>
          ) : (
            ''
          )}
        </div>

        <div className="full_section wdt_photo_slider">
          {typeof PhotosData === 'object' && PhotosData.section && PhotosData.section.newsItem ? (
            <ErrorBoundary>
              {/* <LazyLoad height={100} once> */}
              <Slider
                type="photogallery"
                size="3"
                sliderData={PhotosData.section.newsItem.items}
                width="300"
                height="168"
                SliderClass="photoslider"
                islinkable="true"
                link={
                  PhotosData.section.newsItem && PhotosData.section.newsItem.wu
                    ? PhotosData.section.newsItem.wu
                    : ''
                }
                secname={PhotosData.section.newsItem.secname}
                sliderWidth="930"
                headingRequired="yes"
                moreLinkRequired="yes"
                moreLinkText={Config.Locale.allphotos}
              />
              {/* </LazyLoad> */}
            </ErrorBoundary>
          ) : (
            ''
          )}
        </div>
        <div className="leftmain">
          <div className="full_section tips-tricks">
            {isValidSecData ? (
              <ErrorBoundary>
                {/* <LazyLoad height={100} once> */}
                <NewsWidget
                  newsContent={sectionData.section}
                  type="TipsTricks"
                  secid="66131052"
                  count="4"
                />
                {/* </LazyLoad> */}
              </ErrorBoundary>
            ) : (
              ''
            )}
          </div>
        </div>
        <div className="rightnav">
          {isValidData ? (
            <ErrorBoundary>
              <LazyLoad height={100} once>
                <NewsWidget
                  newsContent={sectionlist.section}
                  type="misc"
                  secid="ibeatmostread"
                  secname="ibeatmostread"
                  imgrequired="all"
                  count="7"
                  headingRequired="yes"
                  widgetClassName="section GN-mostread"
                  widgetType="mostread"
                />
              </LazyLoad>
            </ErrorBoundary>
          ) : (
            ''
          )}
          {/* <ErrorBoundary>

                <CtnAd
                  position="1"
                  height="250"
                  width="300"
                  className="colombia ads_default section"
                  slotId={Config.CTN.id_homepage_CTN}
                />

                {adsPlaceholder('section TXT_AD3 ads_default')}
                <div className="TNA_660_100_1_text section" />


            </ErrorBoundary> */}
        </div>

        <div className="full_section wdt_brands_slider">
          {gadgetsType ? (
            <ErrorBoundary>
              <LazyLoad height={100} once>
                <GadgetsBrands data={gadgetsType} />
              </LazyLoad>
            </ErrorBoundary>
          ) : (
            ''
          )}
        </div>

        <div className="ads_btf">{adsPlaceholder('BTF_728 ads_default')}</div>

        <div style={{ position: 'relative', width: '1000px' }}>
          <div style={{ position: 'absolute', left: '-150px' }}>
            {adsPlaceholder('Skin_LHS ad-skiny')}
          </div>
          <div style={{ position: 'absolute', right: '15px' }}>
            {adsPlaceholder('Skin_RHS ad-skiny')}
          </div>
        </div>
        <div id="AL_Innov1" className="AL_Innov1" />
        <div id="WSK_160_AS" className="WSK_160_AS" />
        {adsPlaceholder('OP_Pop')}
      </div>
    );
  }
}

Index.fetchData = function fetchData({ dispatch }) {
  return dispatch(fetchDataIfNeeded());
};

Index.propTypes = {
  value: PropTypes.array,
  cookie: PropTypes.any,
  dispatch: PropTypes.any,
  query: PropTypes.any,
  params: PropTypes.any,
  showLoginRegister: PropTypes.func,
  closeLoginRegister: PropTypes.func,
  navData: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    ...state.news,
    navData: state.nav,
  };
}

export default connect(mapStateToProps)(Index);
