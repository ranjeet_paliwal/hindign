const homePageMeta = {
  seodata: {
    title:
      'Hindi News: हिन्दी न्यूज़, Latest News in Hindi, Breaking Hindi News, लेटेस्ट हिंदी न्यूज़, ब्रेकिंग न्यूज़ | Navbharat Times',
    meta: [
      {
        name: 'description',
        content:
          'Hindi News: Get Breaking News in Hindi, Latest Hindi News and Hindi News Headlines. पढ़ें देश भर की ताज़ा ख़बरें, Taja Hindi Samachar,  लेटेस्ट हिंदी खबर, Navbharat Times Hindi News Website पर।',
      },
      {
        name: 'Keywords',
        content:
          'Hindi News, हिंदी न्यूज़, Latest News in Hindi, Latest Hindi News, Hindi News Headlines, हिन्दी ख़बर, Breaking News in Hindi, Breaking Hindi News, Hindi Newspaper, headlines in hindi, news headlines in hindi, Taja Samachar, Hindi Samachar',
      },
      {
        name: 'google-site-verification',
        content: 'ZgFICIedNvVZl5pV9EfAUeenwta9vBY0Za_GgmV4zuw',
      },
      {
        name: 'msvalidate.01',
        content: '1E6490AA5F9D0438A03769318F2A1088',
      },
      {
        name: 'language',
        content: 'hindi',
      },
      {
        httpEquiv: 'X-UA-Compatible',
        content: 'IE=edge',
      },
      {
        httpEquiv: 'x-ua-compatible',
        content: 'ie=edge,chrome=1',
      },
      {
        httpEquiv: 'Content-Type',
        content: 'text/html; charset=UTF-8',
      },
      {
        name: 'summary_large_image',
        value: 'noodp',
      },
      {
        name: 'apple-mobile-web-app-capable',
        content: 'yes',
      },
      {
        name: 'apple-touch-fullscreen',
        content: 'yes',
      },
      {
        name: 'msapplication-tap-highlight',
        content: 'no',
      },
      {
        charSet: 'utf-8',
      },
      {
        content: 'text/html; charset=UTF-8',
        httpEquiv: 'Content-Type',
      },
      {
        property: 'fb:app_id',
        content: '154495568087154',
      },
      {
        property: 'fb:admins',
        content: '556964827',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1.0',
      },
      {
        name: 'apple-itunes-app',
        content: 'app-id= 656093141, app-argument=https://navbharattimes.indiatimes.com',
      },
      {
        name: 'twitter:card',
        content: 'summary_large_image',
      },
      {
        name: 'twitter:title',
        content:
          'Hindi News: हिन्दी न्यूज़, Latest News in Hindi, Breaking Hindi News, लेटेस्ट हिंदी न्यूज़, ब्रेकिंग न्यूज़ | Navbharat Times',
      },
      {
        name: 'twitter:url',
        content: 'https://navbharattimes.indiatimes.com',
      },
      {
        name: 'twitter:domain',
        content: 'https://navbharattimes.indiatimes.com',
      },
      {
        name: 'twitter:creator',
        content: '@NavbharatTimes',
      },
      {
        name: 'twitter:site',
        content: '@NavbharatTimes',
      },
      {
        property: 'og:title',
        content:
          'Hindi News: हिन्दी न्यूज़, Latest News in Hindi, Breaking Hindi News, लेटेस्ट हिंदी न्यूज़, ब्रेकिंग न्यूज़ | Navbharat Times',
      },
      {
        property: 'og:description',
        content:
          'Hindi News: Get Breaking News in Hindi, Latest Hindi News and Hindi News Headlines. पढ़ें देश भर की ताज़ा ख़बरें, Taja Hindi Samachar,  लेटेस्ट हिंदी खबर, Navbharat Times Hindi News Website पर।',
      },
      {
        property: 'og:site_name',
        content: 'Navbharat Times',
      },
      {
        property: 'og:type',
        content: 'website',
      },
      {
        property: 'og:url',
        content: 'https://navbharattimes.indiatimes.com',
      },
      {
        property: 'og:locale',
        content: 'hi_IN',
      },
      {
        property: 'og:image',
        content:
          'https://navbharattimes.indiatimes.com/thumb/msid-1217647547,width-500,resizemode-4/pic.jpg?imz=430917',
      },
      {
        'http-equiv': 'Last-Modified',
        content: 'Monday, July 01, 2019  05:10:09 PM',
      },
      {
        'http-equiv': 'Content-Language',
        content: 'hi',
      },
      {
        content: '2019',
        itemprop: 'copyrightYear',
      },
      {
        itemid: 'https://navbharattimes.indiatimes.com',
        itemtype: 'https://schema.org/Organization',
        itemprop: 'copyrightHolder provider sourceOrganization',
      },
      {
        itemprop: 'name',
        content: 'Navbharat Times',
      },
      {
        content: 'https://www.facebook.com/navbharattimes',
        property: 'article:publisher',
      },
    ],
    links: [
      {
        rel: 'amphtml',
        href: 'https://navbharattimes.indiatimes.com/amp_default.cms',
      },
      {
        rel: 'shortcut icon',
        href: 'https://navbharattimes.indiatimes.com/icons/nbtfavicon.ico',
      },
      {
        rel: 'canonical',
        itemprop: 'url',
        href: 'https://navbharattimes.indiatimes.com',
      },
      {
        rel: 'manifest',
        href: '/manifest.json',
      },
    ],
  },
};

export default homePageMeta;
