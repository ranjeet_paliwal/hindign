/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';

import Slider from '../../components/NewSlider/NewSlider';
import GadgetVariants from '../../components/CommonWidgets/VariantsWidget';
import SocialToolBox from '../../components/SocialToolBox';
import SpecificationAccordion from './SpecificationAccordion';
import Thumb from '../../components/Thumb/Thumb';
import './productDetails.scss';
import '../../public/css/commonComponent.scss';
import Link from '../../components/Link/Link';
import { formatMoney, objToQueryStr, getCookie } from '../../common-utility';
import WdtAffiliatesList from '../../components/Amazon/WdtAffiliatesList';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import WdtAmazonExact from '../../components/Amazon/WdtAmazonExact';
import WdtAmazonDetail from '../../components/Amazon/WdtAmazonDetail';
import Wdt2GudAffiliatesList from '../../components/Amazon/Wdt2GudAffiliatesList';
import { adsPlaceholder, getBuyLink } from '../../common-utility';
import GadgetsRating from '../../components/CommonWidgets/GadgetRatingWidget';
import VideoWidget from '../../components/Videos/VideoWidget';
import PhotoWidget from '../../components/Photos/Photowidget';
import fetch from '../../utils/fetch/fetch';
import LatestTrendingWidget from '../../components/LatestTrendingWidget';
import ArticlesListBlock from '../articleshow/subsections/ArticlesListBlock';
import PopularComparisonWidget from './PopularComparisonWidget';

const Config = require(`../../../common/${process.env.SITE}`);

class GadgetShow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTab: 'OVERVIEW',
      commentsData: null,
      currentImageIndex: 0,
      isCommentClicked: false,
      readMoreExpanded: false,
      showReviewCard: false,
    };
  }

  componentDidMount() {
    const { data } = this.props;
    if (data && data.article) {
      const commentsParams = Object.assign({}, Config.Comments.allComments, {
        msid: data.article.msid,
      });

      fetch(`${process.env.API_ENDPOINT}/commentsdata.cms?${objToQueryStr(commentsParams)}`)
        .then(commentsData => {
          this.setState({
            commentsData,
          });
        })
        .catch(e => {
          this.setState({
            commentsData: null,
          });
        });
    }
  }

  componentDidUpdate(prevProps) {
    const { data } = this.props;
    if (!prevProps.data && data) {
      if (data && data.article) {
        const commentsParams = Object.assign({}, Config.Comments.allComments, {
          msid: data.article.msid,
        });

        fetch(`${process.env.API_ENDPOINT}/commentsdata.cms?${objToQueryStr(commentsParams)}`)
          .then(commentsData => {
            this.setState({
              commentsData,
            });
          })
          .catch(e => {
            this.setState({
              commentsData: null,
            });
          });
      }
    }
  }

  // Default values mobile and recently reviewed data

  closeIfOnOverlay = e => {
    if (e.target.id === 'comments__body') {
      return false;
    }
    this.closeCommentSlider();
    return false;
  };

  closeCommentSlider = () => {
    this.setState({
      isCommentClicked: false,
    });

    document.body.classList.remove('disable-scroll');
  };

  showComments = () => {
    this.setState({
      isCommentClicked: true,
    });
    document.body.classList.add('disable-scroll');
  };

  // eslint-disable-next-line react/sort-comp
  getReviewCard = () => {
    this.setState({
      showReviewCard: true,
    });
  };

  toggleReadMore = () => {
    const { readMoreExpanded } = this.state;
    this.setState({
      readMoreExpanded: !readMoreExpanded,
    });
  };

  scrollToSection = e => {
    const { data } = this.props;
    const gadgetData = data.gadget && data.gadget[0] && data.gadget[0];
    const targetId = e.target.id || e.target.parentNode.id || '';
    if (targetId) {
      const scrollToElement = document.getElementById(
        `${targetId}_${gadgetData ? gadgetData.uname : ''}`,
      );
      if (scrollToElement) {
        window.scrollTo({ top: scrollToElement.offsetTop });
      }
    }
  };

  closeReviewCard = () => {
    this.setState({
      showReviewCard: false,
    });
  };

  openHoveredImage = currentImageIndex => {
    if (currentImageIndex !== this.state.currentImageIndex) {
      this.setState({
        currentImageIndex,
      });
    }
  };

  socialShare = (type, data) => {
    switch (type) {
      case 'twitter':
        if (typeof window !== 'undefined') {
          window.open(
            `https://twitter.com/share?url=${`${
              data.url
            }?utm_source=twitter.com&utm_medium=social&utm_campaign=NBTWeb`}&amp;text=${encodeURIComponent(
              data.text,
            )}&via=${Config.SiteInfo.twitterShareId}`,
            'sharer',
            'toolbar=0,status=0,width=626,height=436,scrollbars=1',
          );
        }
        return false;

      case 'fb':
        const url = `${data.url}?utm_source=facebook.com&utm_medium=Facebook&utm_campaign=web`;
        const fbShareParams = { method: 'share', href: url };
        if (data && data.quote) {
          fbShareParams.quote = data.quote;
        }
        if (typeof window !== 'undefined') {
          window.FB.ui(fbShareParams, () => {});
        }
        break;

      default:
    }

    return false;
  };

  scrollToElementTop = (elemId, changeHash = false, setTimeout = false) => {
    if (changeHash) {
      window.history.replaceState({}, '', changeHash);
    }
    const elem = document.getElementById(elemId);
    if (elem) {
      if (!setTimeout) {
        elem.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
      } else {
        setTimeout(
          () => elem.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' }),
          1000,
        );
      }
    }
  };

  render() {
    const {
      data,
      category,
      authentication,
      fixedNav,
      trendingRightData,
      currentTab,
      showLoginRegister,
      latestTrendingData,
      isLastGadget,
    } = this.props;

    const {
      showReviewCard,
      currentImageIndex,
      readMoreExpanded,
      commentsData,
      isCommentClicked,
    } = this.state;
    if (!data) {
      return null;
    }

    let mainImageId;

    if (data && Array.isArray(data.gadget)) {
      if (Array.isArray(data.gadget[0].imageMsid)) {
        mainImageId = data.gadget[0].imageMsid[currentImageIndex];
      } else if (typeof data.gadget[0].imageMsid === 'string' && data.gadget[0].imageMsid) {
        mainImageId = data.gadget[0].imageMsid;
      }
    }

    const gadgetData = (data.gadget && data.gadget[0] && data.gadget[0]) || null;
    const getRegionaldata = [].concat(gadgetData && gadgetData.regional);

    const secData =
      getRegionaldata &&
      Array.isArray(getRegionaldata) &&
      getRegionaldata.filter(data => {
        return data && data.host === Config.SiteInfo.hostid;
      });
    const getregionalreview =
      secData && secData[0] && secData[0].reviewid && secData[0].reviewid.msid
        ? secData[0].reviewid.msid
        : '';
    let isReviewDisabled = false;

    if (typeof window !== 'undefined' && window.localStorage && gadgetData) {
      const reviewAndLoginIds = localStorage.getItem('reviewAndLoginIds');
      const ssoid = getCookie('ssoid');
      if (reviewAndLoginIds && Array.isArray(JSON.parse(reviewAndLoginIds)) && ssoid) {
        if (JSON.parse(reviewAndLoginIds).includes(`${getregionalreview}-${ssoid}`)) {
          isReviewDisabled = true;
        }
      }
    }

    let sliderClass = '';

    const productTitle = secData && secData[0] && secData[0].name ? secData[0].name : '';
    const h1Value = data && data.h2 ? data.h2 : productTitle;
    if (gadgetData && (parseInt(gadgetData.upcoming) || parseInt(gadgetData.rumoured))) {
      if (parseInt(gadgetData.upcoming) && parseInt(gadgetData.rumoured)) {
        sliderClass = 'rumoured';
      } else {
        sliderClass = parseInt(gadgetData.upcoming) ? 'upcoming' : 'rumoured';
      }
    }

    let sortPrice = '';
    let priceTableInfo = '';
    if (gadgetData.affiliate && gadgetData.affiliate) {
      const affiliateData = gadgetData.affiliate;
      if (affiliateData && affiliateData.exact && affiliateData.exact.items) {
        sortPrice = affiliateData.exact.items.sort_price;
      } else if (affiliateData && affiliateData.related && affiliateData.related[0]) {
        sortPrice = affiliateData.related[0].sort_price;
      }

      if (affiliateData.exact) {
        if (affiliateData.exact.items) {
          priceTableInfo = affiliateData.exact.items;
        } else {
          priceTableInfo = affiliateData.exact[0];
        }
      }
    }

    let updatedDate = '';
    if (gadgetData.affiliate && gadgetData.affiliate.related) {
      const relatedNode = gadgetData.affiliate.related;
      if (relatedNode && relatedNode.items) {
        updatedDate = relatedNode.items.updatedAt;
      } else if (relatedNode && relatedNode[0]) {
        updatedDate = relatedNode[0].updatedAt;
      }
    }

    return (
      <div style={{ overflow: 'hidden' }}>
        <div className="contentarea productdetails_body">
          <h1>{h1Value}</h1>
          <div className={`pd_top_nav ${fixedNav ? 'fixed' : ''}`}>
            <div className="nav_items">
              <ul className="tabs" onClick={this.scrollToSection}>
                <li id="overview" className={currentTab === 'overview' ? 'active' : ''}>
                  <span>{Config.Locale.overview}</span>
                </li>
                {data && data.article && parseInt(data.article.isMarkedReview) ? (
                  <li id="critic_review" className={currentTab === 'critic_review' ? 'active' : ''}>
                    <span>{Config.Locale.criticReview}</span>
                  </li>
                ) : (
                  ''
                )}

                <li id="user_review" className={currentTab === 'user_review' ? 'active' : ''}>
                  <span>{Config.Locale.userReview}</span>
                </li>
                <li id="specification" className={currentTab === 'specification' ? 'active' : ''}>
                  <span>{Config.Locale.specifications}</span>
                </li>
              </ul>
            </div>
            {gadgetData ? (
              <div className="social_items">
                <SocialToolBox
                  fb
                  comments
                  twitter
                  socialShare={this.socialShare}
                  showComments={this.showComments}
                  twitterData={{
                    url: `${process.env.WEBSITE_URL}/${
                      Config.gadgetCategories[gadgetData.category]
                    }/${gadgetData.uname}`,
                    text: (data && data.pwa_meta && data.pwa_meta.desc) || '',
                  }}
                  fbData={{
                    url: `${process.env.WEBSITE_URL}/${
                      Config.gadgetCategories[gadgetData.category]
                    }/${gadgetData.uname}`,
                    quote: (data && data.pwa_meta && data.pwa_meta.desc) || '',
                  }}
                />
              </div>
            ) : (
              ''
            )}
          </div>
          <div className="leftmain">
            <div className="page_content">
              <div
                className="product_summary"
                id={`overview_${gadgetData ? gadgetData.uname : ''}`}
              >
                {/* <Slider /> */}
                <div className="product_slider">
                  <ul className="slider-tabs">
                    {data &&
                    Array.isArray(data.gadget) &&
                    data.gadget[0] &&
                    Array.isArray(data.gadget[0].imageMsid)
                      ? data.gadget[0].imageMsid.slice(0, 4).map((imageData, index) => (
                          <li
                            className={index === currentImageIndex ? 'active' : ''}
                            key={imageData}
                            onMouseOver={() => this.openHoveredImage(index)}
                          >
                            <Thumb
                              imgId={imageData}
                              height="75"
                              alt={imageData}
                              resizeMode="4"
                              title={imageData}
                            />
                          </li>
                        ))
                      : null}
                  </ul>
                  <div className={`slider-content ${sliderClass}`}>
                    <span className="top-caption">
                      <b />
                    </span>
                    <div className="img_wrap">
                      <Thumb
                        imgId={mainImageId}
                        height="330"
                        alt={
                          data &&
                          Array.isArray(data.gadget) &&
                          data.gadget[0] &&
                          Array.isArray(data.gadget[0].imageMsid)
                            ? data.gadget[0].imageMsid[currentImageIndex]
                            : 'NBT GadgetsNow'
                        }
                        resizeMode="4"
                        title={
                          data &&
                          Array.isArray(data.gadget) &&
                          data.gadget[0] &&
                          Array.isArray(data.gadget[0].imageMsid)
                            ? data.gadget[0].imageMsid[currentImageIndex]
                            : 'NBT GadgetsNow'
                        }
                      />
                    </div>

                    {gadgetData ? (
                      <div className="tabs_wrap">
                        {/*  TODO: Add links to correct sections here */}
                        <Link
                          to={`/tech/compare-${
                            Config.gadgetCategories[gadgetData.category]
                          }?device=${gadgetData.name}&name=${gadgetData.uname}`}
                          className="active"
                        >
                          {Config.Locale.comparedotxt}
                        </Link>

                        <span
                          onClick={() =>
                            this.scrollToElementTop(`more_like_this_${gadgetData.uname}`)
                          }
                        >
                          {Config.Locale.moreLikeThis}
                        </span>
                      </div>
                    ) : (
                      ''
                    )}
                  </div>
                </div>

                <span
                  className={`summary_content ${readMoreExpanded ? '' : 'less'}`}
                  dangerouslySetInnerHTML={{ __html: data && data.story }}
                />
                {data && data.story && data.story.length > 500 ? (
                  <div className="read_more" onClick={this.toggleReadMore}>
                    <span>
                      {readMoreExpanded ? Config.Locale.readLess : Config.Locale.summaryReadMore}
                    </span>
                  </div>
                ) : (
                  ''
                )}
              </div>
              {data &&
              Array.isArray(data.gadget) &&
              data.gadget[0] &&
              (Array.isArray(data.gadget[0].variants) || data.gadget[0].variants) ? (
                <GadgetVariants
                  currentlyActive={data.gadget[0].common_uname}
                  gadgetCategory={category}
                  data={{ variants: [].concat(data.gadget[0].variants) }}
                />
              ) : null}

              {data && gadgetData && data.article && parseInt(data.article.isMarkedReview) ? (
                <CriticReview
                  data={data.article}
                  gadgetId={gadgetData.uname}
                  averageRating={gadgetData.averageRating}
                  criticRating={data.criticRating}
                />
              ) : (
                ''
              )}

              {/* {adsPlaceholder(`MID_640_${data.article.msid} ads_default mr20`)} */}

              {/* {gadgetData &&
              gadgetData.affiliate &&
              gadgetData.affiliate.exact &&
              gadgetData.affiliate.exact.items ? (
                <ErrorBoundary>
                  <WdtAmazonExact
                    type="gadgetshow"
                    data={gadgetData.affiliate.exact.items}
                    tag="nbt_web_pdp_buyatwidget-21"
                    msid={data.article.msid}
                  />
                </ErrorBoundary>
              ) : (
                ''
              )} */}

              {data && data.productid ? (
                <ErrorBoundary>
                  <WdtAmazonDetail
                    affview="3"
                    type="gadgetshow"
                    productid={data.productid}
                    noimg="0"
                    msid={data.article.msid}
                    title={Config.Locale.tech.seemore}
                    tag={Config.affiliateTags.GS_SEEALSO}
                  />
                </ErrorBoundary>
              ) : (
                ''
              )}

              {data && data.twogud ? (
                <ErrorBoundary>
                  <Wdt2GudAffiliatesList
                    twogudData={data.twogud}
                    brandName={data.twogud.brandname}
                    price={data.twogud.price}
                    productName={data.twogud.productname}
                    tag="nbt_web_pdp_2GudWidget"
                    msid={data.id}
                  />
                </ErrorBoundary>
              ) : (
                ''
              )}
              {data && data.relatedgadgets && Array.isArray(data.relatedgadgets.gadget) ? (
                <div
                  className="full_section pd_suggested_slider ui_slider"
                  id={`more_like_this_${gadgetData ? gadgetData.uname : ''}`}
                >
                  <h2>
                    <span>{Config.Locale.similarGadgets}</span>
                  </h2>
                  <Slider
                    type="suggestedLinkable"
                    size="1"
                    sliderData={data.relatedgadgets.gadget}
                    width="165"
                    height="230"
                    SliderClass="photoslider"
                    islinkable="true"
                    sliderWidth="150"
                    headingRequired="yes"
                  />
                </div>
              ) : (
                ''
              )}
              {data && data.gadget[0] && data.gadget[0].specs ? (
                <div id={`specification_${gadgetData.uname}`}>
                  <SpecificationAccordion
                    heading={productTitle}
                    data={data && data.gadget[0] && data.gadget[0].specs}
                    price={sortPrice}
                  />
                </div>
              ) : null}

              {Config.gadgetShow.popularComparison}
              {data && data.popularGadgetPair && Array.isArray(data.popularGadgetPair.compare) ? (
                <PopularComparisonWidget
                  // dataForPopularMobile={dataForPopularMobile}
                  // restdataForPopularMobile={restdataForPopularMobile}
                  title={`${Config.Locale.popular} ${data.regionalcat} ${Config.Locale.compare}`}
                  data={data.popularGadgetPair.compare}
                  classValue="pd_popular_slider"
                  size={2}
                />
              ) : (
                ''
              )}

              {priceTableInfo && data && (
                <div className="full_section price-widget">
                  <h2>
                    <span>
                      {`${data.gadget && data.gadget[0] && data.gadget[0].name} की भारत में कीमत`}
                    </span>
                  </h2>
                  <table>
                    <tr>
                      <td>STORE</td>
                      <td>PRODUCT NAME</td>
                      <td>OFFER PRICE</td>
                      {priceTableInfo.ListPrice && priceTableInfo.ListPrice.FormattedPrice && (
                        <td>ACTUAL PRICE</td>
                      )}
                    </tr>
                    <tr>
                      <td>{priceTableInfo.Identifier}</td>
                      <td>{priceTableInfo.name}</td>
                      <td>
                        {priceTableInfo.LowestNewPrice &&
                          priceTableInfo.LowestNewPrice.FormattedPrice}
                      </td>
                      <td>{priceTableInfo.ListPrice && priceTableInfo.ListPrice.FormattedPrice}</td>
                    </tr>
                  </table>
                </div>
              )}

              {data && data.relarticle && data.relarticle.items && (
                <div className="videowidget_LEFT">
                  <div className="full_section wdt_latest_videos">
                    <ErrorBoundary>
                      <Slider
                        type="newsSlider"
                        size="3"
                        sliderData={data.relarticle.items}
                        width="200"
                        height="112"
                        SliderClass="videoslider"
                        secname={data.relarticle.secname}
                        sliderWidth="270"
                        headingRequired="yes"
                        moreLinkRequired="yes"
                        moreLinkText={Config.Locale.morenews}
                        link={data.relarticle.wu}
                        islinkable="true"
                      />
                    </ErrorBoundary>
                  </div>
                </div>
              )}

              {data && data.relphoto && data.relphoto.items && (
                <div className="videowidget_LEFT">
                  <div className="full_section wdt_latest_videos">
                    <ErrorBoundary>
                      <Slider
                        type="photogalleryAS"
                        size="3"
                        sliderData={data.relphoto.items}
                        width="200"
                        height="112"
                        SliderClass="videoslider"
                        secname={data.relphoto.secname}
                        sliderWidth="270"
                        headingRequired="yes"
                        moreLinkRequired="yes"
                        moreLinkText={Config.Locale.allphotos}
                        link={data.relphoto.wu}
                        islinkable="true"
                      />
                    </ErrorBoundary>
                  </div>
                </div>
              )}

              {data && data.relvideo && data.relvideo.items && (
                <div className="videowidget_LEFT">
                  <div className="full_section wdt_latest_videos">
                    <ErrorBoundary>
                      <Slider
                        type="videosAS"
                        size="3"
                        sliderData={data.relvideo.items}
                        width="200"
                        height="112"
                        SliderClass="videoslider"
                        secname={data.relvideo.secname}
                        sliderWidth="270"
                        headingRequired="yes"
                        moreLinkRequired="yes"
                        moreLinkText={Config.Locale.allvideos}
                        link={data.relvideo.wu}
                        islinkable="true"
                      />
                    </ErrorBoundary>
                  </div>
                </div>
              )}

              {gadgetData ? (
                <div id={`user_review_${gadgetData.uname}`}>
                  <GadgetsRating
                    data={{
                      ur: gadgetData.averageRating
                        ? Number.parseFloat(gadgetData.averageRating / 2).toFixed(1)
                        : Config.Locale.befirsttoreview,
                      rating: gadgetData.ratings,
                      id: getregionalreview,
                      productCategory: gadgetData.category,
                      productName: h1Value,
                      brandName: gadgetData.brand_name,
                      imageLink: `${process.env.IMG_URL}/photo/${mainImageId || '66951362'}/${
                        gadgetData.uname
                      }.jpg`,
                      itemDescription: data && data.story,
                      isProductAvailable: sliderClass === '',
                      itemPrice: gadgetData.price,
                      sortPrice: sortPrice,
                      pid: gadgetData.pid,
                      updatedDate: updatedDate,
                      announced: gadgetData.announced,
                      canonical: data && data.pwa_meta && data.pwa_meta.canonical,
                    }}
                    isReviewDisabled={isReviewDisabled}
                    showLoginRegister={showLoginRegister}
                    loggedIn={authentication.loggedIn}
                    getreview={this.getReviewCard}
                    closereview={this.closeReviewCard}
                    showReviewCard={showReviewCard}
                  />
                </div>
              ) : (
                ''
              )}

              <ErrorBoundary>
                {commentsData ? (
                  <CommentsSection
                    data={commentsData}
                    loggedIn={authentication.loggedIn}
                    showComments={this.showComments}
                    type="reviews"
                  />
                ) : (
                  ''
                )}
              </ErrorBoundary>
            </div>
            <div
              className="colombia"
              data-section="tech"
              data-slot="323819"
              id={`div-clmb-ctn-323819-${data.article.msid}`}
              data-position={`${data.article.msid}1`}
            />
          </div>
          <div className="rightnav">
            {adsPlaceholder(`ATF_300_${data.article.msid} section ads_default`)}
            {latestTrendingData && Array.isArray(latestTrendingData.section) ? (
              <ErrorBoundary>
                <LatestTrendingWidget
                  data={latestTrendingData.section}
                  title={`${Config.Locale.others} ${data && data.regionalcat}`}
                />
              </ErrorBoundary>
            ) : (
              ''
            )}

            {trendingRightData &&
            trendingRightData.section &&
            trendingRightData.section.newsItem ? (
              <ErrorBoundary>
                <ArticlesListBlock
                  data={trendingRightData.section.newsItem}
                  widgetClassName="with-scroll"
                />
              </ErrorBoundary>
            ) : (
              ''
            )}

            {data && Array.isArray(data.recentcomp) ? (
              <ErrorBoundary>
                <RecentlyComparedWidget data={data.recentcomp} />
              </ErrorBoundary>
            ) : (
              ''
            )}
            <ErrorBoundary>
              {gadgetData ? (
                <MoreFromBrand
                  gadgetCategory={
                    Config.gadgetCategories[(gadgetData && gadgetData.category) || '']
                  }
                  title={`${data.reginalname} ${data.regionalcat}`}
                  titleLink={`${process.env.WEBSITE_URL}/${
                    Config.gadgetCategories[gadgetData && gadgetData.category]
                  }/${gadgetData.brand_name}`}
                  scrollToElementTop={this.scrollToElementTop}
                  elemID={`user_review_${gadgetData.uname}`}
                  data={
                    data && Array.isArray(data.similar) && data.similar[0] && data.similar[0].gadget
                  }
                />
              ) : (
                ''
              )}
            </ErrorBoundary>
            {adsPlaceholder(`BTF_300_${data.article.msid} section adDivs`)}
            <ErrorBoundary>
              <WdtAffiliatesList
                title=""
                category={(gadgetData && gadgetData.category) || 'mobile'}
                tag={Config.affiliateTags.GS_RHS_SPONS}
                isslider="0"
                noimg="0"
                noh2="0"
              />
            </ErrorBoundary>
            <div
              className="colombia section"
              data-section="tech"
              data-slot="323818"
              id={`div-clmb-ctn-323818~1-${data.article.msid}`}
              data-position={`${data.article.msid}0`}
            />
            {adsPlaceholder(`MREC1_${data.article.msid} section adDivs`)}
            {adsPlaceholder(`ATF_300_${data.article.msid} section adDivs`)}
          </div>

          {isCommentClicked ? (
            <div className="popup_comments">
              <div className="body_overlay" onClick={this.closeIfOnOverlay} />
              <div style={{ right: 0 }} id="comments__body" className="comments-body">
                <span className="close_icon" onClick={this.closeCommentSlider} />
                <iframe
                  src={`${process.env.API_ENDPOINT}/comments_slider_react.cms?msid=${
                    data.article.msid
                  }&type=reviews&isloggedin=${authentication.loggedIn}`}
                  height="100%"
                  width="550"
                  border="0"
                  MARGINWIDTH="0"
                  MARGINHEIGHT="0"
                  HSPACE="0"
                  VSPACE="0"
                  FRAMEBORDER="0"
                  SCROLLING="no"
                  align="center"
                  title="iplwidget"
                  ALLOWTRANSPARENCY="true"
                  id="comment-frame"
                />
              </div>
            </div>
          ) : (
            ''
          )}
        </div>
        {!isLastGadget ? (
          <div className="pdp-story-seperator" id="">
            <div className="end-ad">
              <div className="ads_btf">
                {adsPlaceholder(`BTF_728_${data.article.msid} ads_default`)}
              </div>
            </div>
            <div className="next-story">
              <span>{Config.Locale.nextGadget}</span>
            </div>
          </div>
        ) : (
          ''
        )}
        {adsPlaceholder(`AS_Innov1_${data.article.msid}`)}
      </div>
    );
  }
}

const CommentsSection = ({ data, showComments, type }) => {
  const removeSpcChar = str => {
    const removed = str.replace(/<\/?[^>]+(>|$)/g, '');
    return removed;
  };
  if (type === 'reviews') {
    return (
      <div className="pd-comments-wrapper" onClick={showComments} role="button" tabIndex={0}>
        <button className="btn-blue">{Config.ArticleShow.text_write_reviews}</button>
        {data && Array.isArray(data) && data.length > 0 ? (
          <div className="pd-bottom-comments">
            {data.slice(1).length > 0 &&
              data
                .slice(1)
                .slice(0, 2)
                .map(c => (
                  <div className="pd-comment-box" key={c.C_T}>
                    <div className="user-thumbnail">
                      <Thumb
                        className="userimg flL"
                        src={
                          (c.user_detail && c.user_detail.thumb) ||
                          `${process.env.WEBSITE_URL}${Config.Thumb.userImage}`
                        }
                      />
                    </div>
                    {'user_detail' in c && c.user_detail.FL_N ? (
                      <Link
                        to={c.A_ID ? `${Config.profile.url}/${c.A_ID}` : ''}
                        className="name"
                        target="_blank"
                      >
                        {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                      </Link>
                    ) : null}
                    <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                    <div className="footbar clearfix">
                      <span
                        data-action="comment-reply"
                        className="cpointer"
                        onClick={() => showComments()}
                        role="button"
                        tabIndex={0}
                      >
                        Reply
                      </span>
                      {/* <span
                        className="up cpointer"
                        data-action="comment-agree"
                        title="Up Vote"
                        onClick={() => showComments()}
                        role="button"
                        tabIndex={0}
                      >
                        <i className="icon-uparrow" />
                      </span>
                      <span
                        className="down cpointer"
                        data-action="comment-disagree"
                        title="Down Vote"
                        onClick={() => showComments()}
                        role="button"
                        tabIndex={0}
                      >
                        <i className="icon-downarrow" />
                      </span> */}
                    </div>
                  </div>
                ))}
          </div>
        ) : null}
        {data.length > 2 ? (
          <span className="btn-blue btm" onClick={() => showComments()}>
            {Config.Locale.readMoreReview}
          </span>
        ) : (
          ''
        )}
      </div>
    );
  }
  return (
    <div className="pd-comments-wrapper" onClick={showComments} role="button" tabIndex={0}>
      <div className="pd-bottom-comments-btns">
        <div className="cmtbtn-wrapper">
          {data && Array.isArray(data) && data.length > 0 ? (
            <span className="cmtbtn view" onClick={showComments} role="button" tabIndex={0}>
              {`${Config.ArticleShow.text_view_comment} (${data[0].cmt_c})`}
            </span>
          ) : null}
          <span
            className="cmtbtn add"
            onClick={showComments}
            style={{ width: data.length === 0 ? '100%' : '50%' }}
            role="button"
            tabIndex={0}
          >
            {type === 'reviews' ? (
              <span>{Config.ArticleShow.text_write_reviews}</span>
            ) : (
              <span>
                <span className="icon" />
                <span>{Config.ArticleShow.text_write_comment}</span>
              </span>
            )}
          </span>
        </div>
      </div>
      {data && Array.isArray(data) && data.length > 0 ? (
        <div className="pd-bottom-comments">
          {data.slice(1).length > 0 &&
            data
              .slice(1)
              .slice(0, 2)
              .map(c => (
                <div
                  className={`pd-comment-box${c && c.C_A_ID ? ' authorComment' : ''}`}
                  key={c.C_T}
                >
                  <div className="user-thumbnail">
                    <Thumb
                      className="userimg flL"
                      src={
                        (c.user_detail && c.user_detail.thumb) ||
                        `${process.env.WEBSITE_URL}${Config.Thumb.userImage}`
                      }
                    />
                  </div>
                  {'user_detail' in c && c.user_detail.FL_N ? (
                    <Link
                      to={c.A_ID ? `${Config.profile.url}/${c.A_ID}` : ''}
                      className="name"
                      target="_blank"
                    >
                      {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                    </Link>
                  ) : null}
                  <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                  <div className="footbar clearfix">
                    <span
                      data-action="comment-reply"
                      className="cpointer"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      Reply
                    </span>
                    <span
                      className="up cpointer"
                      data-action="comment-agree"
                      title="Up Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-uparrow" />
                    </span>
                    <span
                      className="down cpointer"
                      data-action="comment-disagree"
                      title="Down Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-downarrow" />
                    </span>
                  </div>
                </div>
              ))}
        </div>
      ) : null}
      {data.length > 2 ? (
        <span className="btn-blue btm" onClick={() => showComments()}>
          {Config.Locale.readMoreReview}
        </span>
      ) : (
        ''
      )}
    </div>
  );
};

CommentsSection.propTypes = {
  data: PropTypes.array,
  loggedIn: PropTypes.bool,
  showComments: PropTypes.func,
  type: PropTypes.string,
};

const RecentlyComparedWidget = ({ data }) => {
  if (!Array.isArray(data)) {
    return null;
  }

  return (
    <div className="section pd-list with-scroll">
      <h2>
        <span>{Config.Locale.recentlyCompared}</span>
      </h2>
      <ul>
        {data.map(compData => (
          <li key={compData.uname}>
            <Link
              to={`/tech/compare-${Config.gadgetCategories[compData.category]}/${compData._id}`}
            >
              {`${Config.Locale.comparedotxt} ${compData._id}`}
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
};

RecentlyComparedWidget.propTypes = {
  data: PropTypes.array,
};

const MoreFromBrand = ({ data, title, titleLink, gadgetCategory, scrollToElementTop, elemID }) => {
  if (!Array.isArray(data)) {
    return null;
  }

  return (
    <div className="section wdt_amazon_list with-scroll">
      <h2>
        <Link to={titleLink}>{title}</Link>
      </h2>
      <ul>
        {data.map(brandData => {
          const amztitle = brandData.c_model_lcuname;
          const price = brandData.price;
          const amzga = `${amztitle}_${price}`;
          const affiliateTag =
            (Config && Config.affiliateTags && Config.affiliateTags.GS_BRAND) || '';

          const brandTitle =
            brandData && brandData.regional && brandData.regional.name
              ? brandData.regional.name
              : brandData.name;
          const url =
            brandData.affiliate &&
            Array.isArray(brandData.affiliate.related) &&
            brandData.affiliate.related[0]
              ? brandData.affiliate.related[0].url
              : `/tech/${gadgetCategory}/${brandData.uname}`;
          const buyURL = getBuyLink({
            url,
            price,
            title: brandTitle,
            amzga,
            tag: affiliateTag,
          });
          return (
            <li className="gadget_unit" key={brandData.uname}>
              <span className="img_wrap">
                <Link to={`/tech/${gadgetCategory}/${brandData.uname}`}>
                  <Thumb
                    width="65"
                    title={brandTitle}
                    alt={brandTitle}
                    imgId={[].concat(brandData.imageMsid)[0]}
                    resizeMode="4"
                  />
                </Link>
              </span>
              <span className="con_wrap">
                <h4 title={brandTitle} className="text_ellipsis">
                  <Link to={`/tech/${gadgetCategory}/${brandData.uname}`}>{brandTitle}</Link>
                </h4>

                <span className="rtng">
                  <span>
                    {`${Config.Locale.criticRating}:`}
                    <b>
                      {brandData.criticRating && parseInt(brandData.criticRating) === 0
                        ? 'NA'
                        : Math.round(brandData.criticRating / 2)}
                      {brandData.criticRating && parseInt(brandData.criticRating) !== 0 ? (
                        <sup>/5</sup>
                      ) : (
                        ''
                      )}
                    </b>
                  </span>
                  <span>
                    {`${Config.Locale.user}:`}
                    {brandData.averageRating ? (
                      <b className="rtng1">
                        {Math.round(brandData.averageRating / 2)}
                        <sup>/5</sup>
                      </b>
                    ) : (
                      <Link
                        to={`/tech/${gadgetCategory}/${brandData.uname}#rate`}
                        className="sub-rating"
                      >
                        {Config.Locale.submitrating}
                      </Link>
                    )}
                  </span>
                </span>
                <span className="price_tag symbol_rupees">{formatMoney(brandData.price, 0)}</span>
              </span>

              <span className="btn_wrap absolute">
                {brandData &&
                brandData.affiliate &&
                Array.isArray(brandData.affiliate.related) &&
                brandData.affiliate.related[0] &&
                brandData.affiliate.related[0].Identifier === 'amazon' ? (
                  <Link to={buyURL} rel="nofollow noopener" target="_blank" className="buy_btn">
                    {Config.Locale.tech.buyFrom}
                  </Link>
                ) : (
                  ''
                )}
              </span>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

MoreFromBrand.propTypes = {
  data: PropTypes.array,
  title: PropTypes.string,
  titleLink: PropTypes.string,
  gadgetCategory: PropTypes.string,
};

const PopularComparison = ({ dataForPopularMobile, restdataForPopularMobile, title }) => {
  return (
    <div className="full_section pd_popular_slider ui_slider">
      <h2>
        <span>{title}</span>
      </h2>

      {dataForPopularMobile && (
        <Slider
          type="compareListPopular"
          size="1"
          sliderData={dataForPopularMobile}
          width="165"
          height="185"
          SliderClass="pd_slider"
          islinkable="true"
          sliderWidth="180"
          headingRequired="yes"
        />
      )}

      <div className="items-in-list">
        <ul>
          {restdataForPopularMobile
            ? restdataForPopularMobile.map(item => {
                return (
                  <li key={item.product_name.join(' vs ')}>
                    <span className="text_ellipsis">
                      {`${Config.Locale.comparedotxt} ${item.product_name.join(' vs ')}`}
                    </span>
                  </li>
                );
              })
            : ''}
        </ul>
      </div>
    </div>
  );
};

PopularComparison.propTypes = {
  dataForPopularMobile: PropTypes.array,
  restdataForPopularMobile: PropTypes.array,
  title: PropTypes.string,
};

const CriticReview = ({ data, criticRating, averageRating, gadgetId }) => {
  return (
    <div className="thumb_img_reviews" id={`critic_review_${gadgetId}`}>
      <h2 className="sectionHead">
        <span>{Config.Locale.criticReview}</span>
      </h2>
      <h3>{data.title}</h3>
      <div className="img_wrap">
        <Thumb imgId={data.msid} resizeMode="4" width="630" />
        <span className="image_caption">
          <span className="lft">
            <span className="head">
              <b className="icon_star one" />
              {Config.Locale.criticRating}
            </span>
            {Number.parseInt(criticRating) !== 0 ? (
              <span className="rate_txt">
                {Number.parseFloat(criticRating).toFixed(1)}
                <sub>/5</sub>
              </span>
            ) : (
              'NA'
            )}
          </span>
          <span className="cen">
            <span className="head">
              <b className="icon_star one" />
              {Config.Locale.userRating}
            </span>
            <span className="rate_txt">
              {Number.parseFloat(averageRating / 2).toFixed(1)}
              <sub>/5</sub>
            </span>
          </span>
          {data.topfeature && Array.isArray(data.topfeature['#text']) ? (
            <span className="rgt">
              <span className="head">{Config.Locale.specialfeatures}</span>
              <ul className="features_txt">
                {data.topfeature['#text'].map(topFeatData => (
                  <li key={topFeatData}>{topFeatData}</li>
                ))}
              </ul>
            </span>
          ) : (
            ''
          )}
        </span>
      </div>
      <span className="review_text">{data.artsyn}</span>
      <Link to={data.url} className="more-btn">
        {Config.Locale.readCompleteReview}
      </Link>
    </div>
  );
};

CriticReview.propTypes = {
  data: PropTypes.object,
  criticRating: PropTypes.string,
  averageRating: PropTypes.string,
};

GadgetShow.propTypes = {
  data: PropTypes.object,
};

export default GadgetShow;
