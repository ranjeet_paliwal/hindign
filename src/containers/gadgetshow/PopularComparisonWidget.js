import React from 'react';

import GadgetsCompare from '../Comparison/GadgetsCompare/GadgetsCompare';
import Link from '../../components/Link/Link';
import { getKeyByValue } from '../../common-utility';

const Config = require(`../../../common/${process.env.SITE}`);

const PopularComparisonWidget = ({ data, title, classValue, size }) => {
  const arrPopularTwoDevices = [];
  const arrPopularOtherThanTwo = [];
  let htmlPopularDevices = '';

  if (data) {
    data.forEach(item => {
      // console.log(item);
      if (
        item &&
        item.product_name &&
        item.product_name.length === size &&
        arrPopularTwoDevices.length <= size
      ) {
        arrPopularTwoDevices.push(item);
      } else {
        arrPopularOtherThanTwo.push(item);
      }
    });

    if (arrPopularOtherThanTwo.length > 0) {
      htmlPopularDevices = <GetHtml data={arrPopularOtherThanTwo} />;
    }

    return (
      <div className={`full_section ${classValue} ui_slider`}>
        <h2>
          <span>{`${title}`}</span>
        </h2>

        <div className="slider">
          <div className="slider_content">
            <ul>
              <GadgetsCompare data={arrPopularTwoDevices} />
            </ul>
          </div>
        </div>

        <div className="items-in-list">
          <ul>{htmlPopularDevices}</ul>
        </div>
      </div>
    );
  }
  return null;
};

// if (
//   deviceDataCopy &&
//   deviceDataCopy.popularGadgetPair &&
//   deviceDataCopy.popularGadgetPair.compare
// ) {
//   const arrPopular = [...deviceDataCopy.popularGadgetPair.compare];
//   console.log('arrPopular', arrPopular);

//   arrPopular.forEach(item => {
//     // console.log(item);
//     if (
//       item &&
//       item.product_name &&
//       item.product_name.length === 2 &&
//       arrPopularTwoDevices.length <= 2
//     ) {
//       arrPopularTwoDevices.push(item);
//     } else {
//       arrPopularOtherThanTwo.push(item);
//     }
//   });

//   if (arrPopularOtherThanTwo.length > 0) {
//     htmlPopularDevices = <GetHtml data={arrPopularOtherThanTwo} />;
//   }
// }

const GetHtml = ({ data }) => {
  return data && Array.isArray(data)
    ? data.slice(0, 12).map(item => {
        const seoPath =
          item && item._id && item._id.indexOf('-vs-') !== -1
            ? item._id
                .split('-vs-')
                .sort()
                .join('-vs-')
            : '';
        const deviceCategory =
          Config && Config.gadgetMapping && item.category
            ? getKeyByValue(Config.gadgetMapping, item.category)
            : '';
        const urlCmpDetail = `/tech/compare-${deviceCategory}/${seoPath}`;
        return (
        <li key={item.product_name}>
            <span className="text_ellipsis">
            <Link to={urlCmpDetail}>
                {`${Config.Locale.comparedotxt} ${item.product_name.join(' vs ')}`}
              </Link>
          </span>
          </li>
        );
      })
    : '';
};

export default PopularComparisonWidget;
