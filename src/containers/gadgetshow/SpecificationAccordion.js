import React, { Component } from 'react';
import PropTypes from 'prop-types';

const Config = require(`../../../common/${process.env.SITE}`);

class SpecificationAccordion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentlyExpanded: [0],
    };
  }

  toggleAccordion = index => {
    let { currentlyExpanded } = this.state;
    // Remove from array if exists, else, add.
    if (currentlyExpanded.includes(index)) {
      currentlyExpanded = currentlyExpanded.filter(value => value !== index);
    } else {
      currentlyExpanded = currentlyExpanded.concat(index);
    }
    this.setState({
      currentlyExpanded,
    });
  };

  render() {
    const { heading, data, price } = this.props;
    const { currentlyExpanded } = this.state;

    if (!data) {
      return null;
    }

    return (
      <div className="pd-accordion">
        <h3>{`${heading} ${Config.Locale.specifications}`}</h3>
        <div className="pd-spec-show">
          {Object.values(data).map((specData, index) => (
            <div key={specData.regional} className="gn_table_layout">
              <label
                onClick={() => this.toggleAccordion(index)}
                className={`${currentlyExpanded.includes(index) ? 'active' : ''} spec`}
              >
                {specData.regional || ''}
              </label>

              <table className="gn_table_layout">
                {price && price != '0' && (
                  <tr>
                    <td>भारत में कीमत </td>
                    <td>₹{price}</td>
                  </tr>
                )}

                {Object.values(specData).map((speckey, index) => {
                  return speckey != 'regional' && speckey.key ? (
                    <React.Fragment>
                      {speckey && speckey.key != 'price_in_india' && (
                        <tr>
                          <td>{speckey && speckey.key && speckey.key.replace('_', ' ')}</td>
                          <td>{speckey != '' ? speckey.value : '-'}</td>
                        </tr>
                      )}
                    </React.Fragment>
                  ) : null;
                })}
              </table>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

SpecificationAccordion.propTypes = {
  heading: PropTypes.string,
  data: PropTypes.object,
};

export default SpecificationAccordion;
