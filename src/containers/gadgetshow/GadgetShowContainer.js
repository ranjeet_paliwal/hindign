/* eslint-disable func-names */
/* eslint-disable indent */
/* eslint-disable react/jsx-indent */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import fetchDataIfNeeded, { updateNavData, fetchRelatedGadgetData } from '../../actions/gadgetshow';
import { analyticsGA, AnalyticsComscore, AnalyticsIBeat } from '../../analytics';

import { initGoogleAds, refreshAds } from '../../ads/dfp';
import Link from '../../components/Link/Link';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import BreadCrumbs from '../../components/BreadCrumb';
import * as TPLib from '../../analytics/TimesPoint';

import fetch from '../../utils/fetch/fetch';
import GadgetShow from './gadgetShow';
import ArticleSHowFakeLoader from '../../components/Loaders/ArticleShowFakeLoader';

const Config = require(`../../../common/${process.env.SITE}`);

class GadgetShowContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentArticle: 0,
      currentTop: 0,
      trendingRightData: null,
      currentTab: 'overview',
      currentBottom: 0,
      fixedNav: false,
    };

    this.article1Ref = React.createRef();

    this.handleScrollChange = this.handleScrollChange.bind(this);
    this.changePageUrl = this.changePageUrl.bind(this);
  }

  // getLatestTrendingData = (category = 'mobile', tag = 'recentreviewed') => {
  //   if (!this.state.data[tag]) {
  //     this.setState({ loading: true, currentTab: tag });

  //     const api1 = fetch(
  //       `${
  //         process.env.API_ENDPOINT
  //       }/web_recentlatest.cms?tag=${tag}&category=${category}&perpage=10&limit=49&hostid=307&feedtype=sjson `,
  //     );

  //     const api2 = fetch(
  //       `${
  //         process.env.API_ENDPOINT
  //       }/web_recentlatest.cms?tag=${tag}&category=${category}&perpage=10&limit=49&hostid=307&feedtype=sjson `,
  //     );

  //     Promise.all([api1, api2]).then();

  //     fetch(api)
  //       .then(response => response.json())
  //       .then(feedData => {
  //         const data = { ...this.state.data, [tag]: feedData };
  //         this.setState({
  //           data,
  //           loading: false,
  //         });
  //       });
  //   } else {
  //     this.setState({
  //       currentTab: tag,
  //     });
  //   }
  // };

  componentDidMount() {
    const { dispatch, params, query, history, data, relatedGadgetsToFetch, location } = this.props;
    const sectionName =
      data && data.articleData && data.articleData.secname ? data.articleData.secname : '';

    if ((location.hash === '#specifications' || location.hash === '#rate') && data) {
      const gadgetData =
        (data &&
          data.gadgetData &&
          data.gadgetData.gadget &&
          data.gadgetData.gadget[0] &&
          data.gadgetData.gadget[0]) ||
        null;

      const hashKey = {
        '#specifications': 'specification',
        '#rate': 'user_review',
      };

      if (window && gadgetData) {
        window.onload = () => {
          this.scrollToElementTop(`${hashKey[location.hash]}_${gadgetData.uname}`);
        };
      }
    }

    fetch(
      `${
        process.env.API_ENDPOINT
      }/pwafeeds/webgn_common.cms?feedtype=sjson&count=10&platform=web&tag=ibeatmostread`,
    ).then(data => {
      this.setState({
        trendingRightData: data,
      });
    });

    GadgetShowContainer.fetchData({ dispatch, query, params, history });

    if (data && data.gadgetData) {
      window.addEventListener('scroll', this.handleScrollChange);
      // FIXME: Latest trending widget should be moved here so call only
      // goes once.
      // this.getLatestTrendingData(category, 'recentreviewed');
    }

    // Create refs for all related gadgets before hand.
    if (relatedGadgetsToFetch && Array.isArray(relatedGadgetsToFetch)) {
      for (let i = 0; i < relatedGadgetsToFetch.length; i++) {
        this[`article${i + 2}Ref`] = React.createRef();
      }
    }

    dispatch(
      updateNavData({
        sectionName: 'gadgets',
      }),
    );

    const adsData = data && data.adsData && data.adsData.wapads ? data.adsData.wapads : '';
    if (adsData) {
      const sectionInfo = {
        pagetype: 'gadgetshow',
        sec1: data && data.gadgetData && data.gadgetData.sec1,
        sec2: data && data.gadgetData && data.gadgetData.sec2,
        sec3: data && data.gadgetData && data.gadgetData.sec3,
        hyp1: data && data.gadgetData && data.gadgetData.pwa_meta && data.gadgetData.pwa_meta.hyp1,
      };

      if (!this.AdServingRules) {
        setTimeout(() => {
          initGoogleAds(adsData, sectionInfo);
        }, 2000);
      }
    }
  }

  componentDidUpdate(prevProps) {
    const { dispatch, params, query, location, history, data, relatedGadgetsToFetch } = this.props;

    if (!prevProps.data && data) {
      window.addEventListener('scroll', this.handleScrollChange);
      const articleOneDiv = this.article1Ref.current;
      if (articleOneDiv) {
        const articleOneDim = articleOneDiv.getBoundingClientRect();
        const currentTop = articleOneDiv.offsetTop;
        const currentBottom = articleOneDiv.offsetTop + articleOneDim.height;
        this.setState({
          currentTop,
          currentBottom,
        });
      }

      if ((location.hash === '#specifications' || location.hash === '#rate') && data) {
        const gadgetData =
          (data &&
            data.gadgetData &&
            data.gadgetData.gadget &&
            data.gadgetData.gadget[0] &&
            data.gadgetData.gadget[0]) ||
          null;

        const hashKey = {
          '#specifications': 'specification',
          '#rate': 'user_review',
        };

        this.scrollToElementTop(`${hashKey[location.hash]}_${gadgetData.uname}`);
      }

      if (data && data.adsData && data.adsData.wapads) {
        const sectionInfo = {
          pagetype: 'gadgetshow',
          sec1: data && data.gadgetData && data.gadgetData.sec1,
          sec2: data && data.gadgetData && data.gadgetData.sec2,
          sec3: data && data.gadgetData && data.gadgetData.sec3,
        };
        if (document.querySelector('div.atf-topad')) {
          document.querySelector('div.atf-topad').innerHTML = '';
        }
        refreshAds(data.adsData.wapads.AS_Innov1);
        delete data.adsData.wapads.AS_Innov1;
        const AdServingRules =
          (data &&
            data.gadgetData &&
            data.gadgetData.pwa_meta &&
            data.gadgetData.pwa_meta.AdServingRules) ||
          null;

        if (!AdServingRules) {
          initGoogleAds(data.adsData.wapads, sectionInfo);
        }
      }

      if (relatedGadgetsToFetch && Array.isArray(relatedGadgetsToFetch)) {
        for (let i = 0; i < relatedGadgetsToFetch.length; i++) {
          this[`article${i + 2}Ref`] = React.createRef();
        }
      }
    } else if (prevProps.location.pathname !== location.pathname) {
      window.scrollTo({ top: 0 });
      GadgetShowContainer.fetchData({ dispatch, query, params, history });
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScrollChange);
    document.querySelector('body').classList.remove('AS');
  }

  scrollToElementTop = elemId => {
    const elem = document.getElementById(elemId);
    if (elem) {
      // window.scrollTo({ top: elem.offsetTop });
      setTimeout(
        () =>
          elem.scrollIntoView({
            behavior: 'smooth',
            block: 'end',
            inline: 'nearest',
          }),
        1000,
      );
    }
  };

  showLoaderIfNeeded = () => {
    const { data } = this.props;
    if (
      data &&
      data.articleData &&
      data.articleData.nextarticles &&
      data.articleData.nextarticles.length > 0
    ) {
      return <ArticleSHowFakeLoader />;
    }
    return null;
  };

  handleScrollChange(e) {
    const { currentArticle, currentTop, fixedNav, currentTab } = this.state;
    const {
      data,
      relatedGadgetsToFetch,
      isFetchingRelatedArticles,
      dispatch,
      params,
      relatedGadgetsData,
    } = this.props;

    // if (gadgetData && typeof document !== 'undefined') {
    //   if(document.getElementById(`overview_${gadgetData.uname}`);
    //   overViewInViewPort = isInViewport(document.getElementById(`overview_${gadgetData.uname}`));
    //   specificationInViewPort = isInViewport(
    //     document.getElementById(`specification_${gadgetData.uname}`),
    //   );
    //   userReviewInViewPort = isInViewport(
    //     document.getElementById(`user_review_${gadgetData.uname}`),
    //   );

    //   if (data && data.article && parseInt(data.article.isMarkedReview)) {
    //     criticReviewInViewPort = isInViewport(
    //       document.getElementById(`crtic_review_${gadgetData.uname}`),
    //     );
    //   }
    // }
    let currentId = '';
    if (currentArticle === 0) {
      currentId =
        (data.gadgetData &&
          data.gadgetData.gadget &&
          data.gadgetData.gadget[0] &&
          data.gadgetData.gadget[0].uname) ||
        '';
    } else if (
      relatedGadgetsData[currentArticle - 1] &&
      relatedGadgetsData[currentArticle - 1].gadget &&
      relatedGadgetsData[currentArticle - 1].gadget[0] &&
      relatedGadgetsData[currentArticle - 1].gadget[0].uname
    ) {
      currentId = relatedGadgetsData[currentArticle - 1].gadget[0].uname;
    }
    const overviewElement = document.getElementById(`overview_${currentId}`);
    let newTab = currentTab;
    if (overviewElement) {
      const inBetween = elem =>
        window.scrollY > elem.offsetTop - 150 &&
        window.scrollY <= elem.offsetTop + elem.getBoundingClientRect().height + 150;

      // Check which element is in view
      const specificationElement = document.getElementById(`specification_${currentId}`);
      const userReviewElement = document.getElementById(`user_review_${currentId}`);
      const criticReviewElement = document.getElementById(`critic_review_${currentId}`);
      if (overviewElement && currentTab !== 'overview') {
        if (inBetween(overviewElement)) {
          newTab = 'overview';
        }
      }
      if (specificationElement && currentTab !== 'specification') {
        if (inBetween(specificationElement)) {
          newTab = 'specification';
        }
      }

      if (userReviewElement && currentTab !== 'user_review') {
        if (inBetween(userReviewElement)) {
          newTab = 'user_review';
        }
      }

      if (criticReviewElement && currentTab !== 'critic_review') {
        if (inBetween(criticReviewElement)) {
          newTab = 'critic_review';
        }
      }
    }

    const currentArticleDiv = this[`article${currentArticle + 1}Ref`].current;
    const currentArticleDim = currentArticleDiv.getBoundingClientRect();
    const articleTop = currentArticleDiv.offsetTop;
    const articleBottom = currentArticleDiv.offsetTop + currentArticleDim.height;

    // Sticky Navigation on scroll
    if (window.scrollY > articleTop + 150 && !fixedNav) {
      this.setState({
        fixedNav: true,
      });
    } else if (window.scrollY < articleTop - 150 && fixedNav) {
      this.setState({
        fixedNav: false,
      });
    }

    const nextArticlesLength =
      (data &&
        data.gadgetData &&
        Array.isArray(data.gadgetData.similar) &&
        data.gadgetData.similar[0] &&
        Array.isArray(data.gadgetData.similar[0].gadget) &&
        data.gadgetData.similar[0].gadget.length) ||
      0;

    /* To fetch data when in the middle of the last article */
    if (
      window.scrollY > currentTop / 2 &&
      relatedGadgetsData.length === currentArticle &&
      !isFetchingRelatedArticles &&
      Array.isArray(relatedGadgetsToFetch) &&
      relatedGadgetsToFetch.length > 0
    ) {
      dispatch(fetchRelatedGadgetData({ id: relatedGadgetsToFetch[0], category: params.category }));
    }

    let newArticle = currentArticle;

    /*  If user has scrolled past current article downwards or upwards */
    if (window.scrollY > articleBottom || window.scrollY < articleTop) {
      /*  User moves upwards, and is not at first article */
      if (window.scrollY < articleTop && currentArticle !== 0) {
        newArticle = currentArticle - 1;
      } else if (window.scrollY > articleBottom && currentArticle !== nextArticlesLength) {
        /* User moves downwards, and is not at the last article */
        newArticle = currentArticle + 1;
      }
    }

    if (newArticle !== currentArticle) {
      const newArticleDiv = this[`article${newArticle + 1}Ref`].current;
      const newArticleDim = newArticleDiv.getBoundingClientRect();
      const newTop = newArticleDiv.offsetTop;
      const newBottom = newArticleDiv.offsetTop + newArticleDim.height;

      this.changePageUrl(newArticle);

      this.setState({
        currentArticle: newArticle,
        currentTop: newTop,
        currentBottom: newBottom,
      });
    }
    // console.log('CURRENTTAB', currentTab);
    // console.log('NEWTAB', newTab);
    if (currentTab !== newTab) {
      this.setState({
        currentTab: newTab,
      });
    }
  }

  changePageUrl(newArticle) {
    let url;
    let gadgetData;
    const { data, relatedGadgetsData, relatedAdsData } = this.props;
    const { currentArticle } = this.state;

    const currentStateArticle = currentArticle;
    // To change history only when article is being changed.
    if (newArticle !== currentStateArticle) {
      // if (TPLib && TPLib.logPoints) {
      //   TPLib.logPoints(TPLib.ConfigData.tpconfig.ARTICLE_READ, '1234');
      // }

      if (newArticle === 0) {
        gadgetData =
          (data.gadgetData && data.gadgetData.gadget && data.gadgetData.gadget[0]) || null;

        document.title =
          data.gadgetData && data.gadgetData.pwa_meta ? data.gadgetData.pwa_meta.title : '';

        url =
          gadgetData && gadgetData.uname && gadgetData.category
            ? `/tech/${Config.gadgetCategories[gadgetData.category]}/${gadgetData.uname}`
            : data.gadgetData && data.gadgetData.pwa_meta && data.gadgetData.pwa_meta.redirectUrl;

        window.history.pushState({ title: '' }, '', url.toLowerCase());
      } else {
        const articleData = relatedGadgetsData[newArticle - 1];
        const newTitle =
          articleData.pwa_meta && articleData.pwa_meta.title ? articleData.pwa_meta.title : '';
        document.title = newTitle;
        gadgetData = (articleData.gadget && articleData.gadget[0]) || null;

        url =
          gadgetData && gadgetData.uname && gadgetData.category
            ? `/tech/${Config.gadgetCategories[gadgetData.category]}/${gadgetData.uname}`
            : gadgetData && gadgetData.pwa_meta && gadgetData.pwa_meta.redirectUrl;

        window.history.pushState({ title: '' }, '', url.toLowerCase());

        const adsData =
          Array.isArray(relatedAdsData) &&
          relatedAdsData[newArticle - 1] &&
          relatedAdsData[newArticle - 1].wapads
            ? relatedAdsData[newArticle - 1].wapads
            : null;

        const rGadgetsData =
          Array.isArray(relatedGadgetsData) && relatedGadgetsData[newArticle - 1]
            ? relatedGadgetsData[newArticle - 1]
            : null;

        if (adsData && rGadgetsData) {
          const sectionInfo = {
            pagetype: 'gadgetshow',
            sec1: rGadgetsData && rGadgetsData.sec1,
            sec2: rGadgetsData && rGadgetsData.sec2,
            sec3: rGadgetsData && rGadgetsData.sec3,
            hyp1: rGadgetsData.pwa_meta && rGadgetsData.pwa_meta.hyp1,
          };

          if (!this.AdServingRules) {
            setTimeout(() => {
              initGoogleAds(adsData, sectionInfo);
            }, 1000);
          }
        }
      }

      if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
        analyticsGA.pageview();
      }
      if (
        typeof AnalyticsComscore !== 'undefined' &&
        typeof AnalyticsComscore.invokeComScore !== 'undefined'
      ) {
        AnalyticsComscore.invokeComScore();
      }
      // if (
      //   typeof AnalyticsIBeat !== 'undefined' &&
      //   typeof AnalyticsIBeat.initIbeat !== 'undefined'
      // ) {
      //   if (currentArticle === 0 && currentArticle !== -1) {
      //     AnalyticsIBeat.initIbeat(data.articleData.ibeatconfig);
      //   } else {
      //     AnalyticsIBeat.initIbeat(relatedArticlesData[currentArticle - 1].articleData.ibeatconfig);
      //   }
      // }
    }
    return true;
  }

  render() {
    const {
      isFetching,
      data,
      params,
      showLoginRegister,
      authentication,
      location,
      relatedGadgetsData,
      isFetchingRelatedArticles,
      metaData,
    } = this.props;

    const { fixedNav, currentArticle, trendingRightData, currentTab } = this.state;

    if (isFetching || !data) {
      return <ArticleSHowFakeLoader />;
    }
    //  let schemaHTML = '';
    let relatedGadgetsToFetch = '';
    let latestTrendingData = null;
    if (data) {
      relatedGadgetsToFetch = data.relatedGadgetsToFetch;
      latestTrendingData = data.latestTrendingData;
      const gadgetData =
        data &&
        data.gadgetData &&
        Array.isArray(data.gadgetData.gadget) &&
        data.gadgetData.gadget[0]
          ? data.gadgetData.gadget[0]
          : '';
      // if (gadgetData) {
      //   schemaHTML = `{
      //     "@context": "https://schema.org/",
      //     "@type": "Product",
      //     "name": "${`${gadgetData.name}`}",
      //     "image": "${`${`${process.env.IMG_URL}/photo/${
      //       Array.isArray(gadgetData.imageMsid) || gadgetData.imageMsid
      //         ? [].concat(gadgetData.imageMsid)[0]
      //         : '66951362'
      //     }/${gadgetData.uname}.jpg`}`}",
      //     "category": "${gadgetData.category}",
      //     "brand": {
      //     "@type": "Thing",
      //     "name": "${gadgetData.brand_name}"
      //     },
      //     "description": "${data.gadgetData.story}",

      //     "offers": {
      //     "@type": "Offer",
      //       "priceCurrency": "INR",
      //       "price": "${gadgetData.price}",
      //       "url": "${process.env.WEBSITE_URL}/tech/${
      //     Config.gadgetCategories[gadgetData.category]
      //   }/${gadgetData.uname}"
      //     }
      //   }`;
      // }
    }

    return (
      <div className="articles_container AS">
        <Helmet
          title={data && data.gadgetData && data.gadgetData.pwa_meta.title}
          titleTemplate="%s"
          meta={metaData && metaData.metaTags}
          link={metaData && metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          //script={[schema]}
        />
        <BreadCrumbs
          data={
            data && data.gadgetData && data.gadgetData.breadcrumb && data.gadgetData.breadcrumb.div
          }
        />

        <div ref={this.article1Ref} style={{ overflow: 'hidden' }}>
          <GadgetShow
            fixedNav={currentArticle === 0 ? fixedNav : false}
            data={data && data.gadgetData}
            hash={location.hash}
            currentTab={currentTab}
            trendingRightData={trendingRightData}
            showLoginRegister={showLoginRegister}
            category={params.category}
            authentication={authentication}
            latestTrendingData={latestTrendingData}
            isLastGadget={relatedGadgetsToFetch && relatedGadgetsToFetch.length === 0}
          />
        </div>
        {Array.isArray(relatedGadgetsData)
          ? relatedGadgetsData.map((relatedData, index) => (
              <div
                key={relatedGadgetsData.productid}
                style={{ overflow: 'hidden' }}
                ref={this[`article${index + 2}Ref`]}
              >
                <GadgetShow
                  data={relatedData}
                  fixedNav={currentArticle > 0 && currentArticle === index + 1 ? fixedNav : false}
                  category={params.category}
                  currentTab={currentTab}
                  latestTrendingData={latestTrendingData}
                  showLoginRegister={showLoginRegister}
                  trendingRightData={trendingRightData}
                  authentication={authentication}
                  isLastGadget={index === relatedGadgetsData.length - 1}
                />
              </div>
            ))
          : ''}
        {isFetchingRelatedArticles ? <ArticleSHowFakeLoader /> : ''}
      </div>
    );
  }
}

GadgetShowContainer.fetchData = function({ dispatch, query, history, params }) {
  return dispatch(fetchDataIfNeeded(params));
};

GadgetShowContainer.propTypes = {
  dispatch: PropTypes.func,
  showLoginRegister: PropTypes.func,
  closeLoginRegister: PropTypes.func,
  params: PropTypes.object,
  location: PropTypes.object,
  query: PropTypes.object,
  history: PropTypes.object,
  dockedVideo: PropTypes.object,
  data: PropTypes.object,
  isFetchingRelatedArticles: PropTypes.bool,
  relatedArticlesDataFetched: PropTypes.bool,
  isFetching: PropTypes.bool,
  relatedArticlesData: PropTypes.object,
  loggedIn: PropTypes.bool,
  isFetchingNewsData: PropTypes.bool,
  relatedGadgetsToFetch: PropTypes.array,
  newsDataFetched: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.gadgetshow,
    authentication: state.authentication,
  };
}

export default connect(mapStateToProps)(GadgetShowContainer);
