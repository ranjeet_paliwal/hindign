/* eslint-disable object-shorthand */
/* eslint-disable no-bitwise */
import React, { PureComponent } from 'react';
import './Gdpr.scss';
import { loadJS, setCookie, getCookie } from '../../common-utility';
import Link from '../../components/Link/Link';
// import fetchJsonP from 'fetch-jsonp';

// import { setCookie, getCookie } from '../../common-utility';

const Config = require(`../../../common/${process.env.SITE}`);

const shortsitename = Config.SiteInfo.shortName;
const continentInAction = 'EU';

class Gdpr extends PureComponent {
  state = {
    continent: null,
  };

  componentDidMount() {
    let continent = getCookie('continent_gdpr');

    if (!continent) {
      loadJS('https://geoapi.indiatimes.com/?cb=1', () => {
        if (typeof geoinfo !== 'undefined' && geoinfo.Continent) {
          continent = geoinfo.Continent.toUpperCase();
          setCookie('continent_gdpr', continent);
          if (geoinfo.Continent === 'EU') {
            document.body.classList.add('continent-eu');
          }
          this.setState({ continent: continent });
        }
      });
    } else if (continent === continentInAction && typeof document !== 'undefined') {
      document.body.classList.add('continent-eu');
      this.setState({ continent: continent });
    }
  }

  continueClickHandler = () => {
    this.postconsent();
  };

  postconsent = () => {
    const guID = shortsitename.toLowerCase();
    '-web-xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, item => {
      const random16 = (Math.random() * 16) | 0;
      const d = item === 'x' ? random16 : (random16 & 3) | 8;

      return d.toString(16);
    });

    setCookie('ckns_policy', guID, 365);
    setCookie('optout', 1, 365);
    document.querySelector('#gdpr-block').classList.toggle('hide');

    const ConfigGdpr = {
      consentApiurl: 'https://etservices2.indiatimes.com/nbt/consent',
      consentCookieName: 'ckns_policy',
      consentCookieAge: 365,
      consentAcceptText: '',
      consentCookieId: 5,
      productCode: `${shortsitename.toLowerCase()}-web`,
      channelcode: shortsitename.toLowerCase(),
      PRIMARY: getCookie('ckns_policy'),
    };

    const JsonConsent = {
      consent: {
        consents: [
          {
            agree: true,
            dataPoint: {
              id: ConfigGdpr.consentCookieId,
            },
            text: ConfigGdpr.consentAcceptText,
          },
        ],
        productCode: ConfigGdpr.productCode,
      },
    };

    const request = new XMLHttpRequest();
    request.open('POST', ConfigGdpr.consentApiurl, true);
    request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    request.setRequestHeader('Authorization', ConfigGdpr.channelcode);
    request.setRequestHeader('X-PRIMARY', ConfigGdpr.PRIMARY);
    request.send(JSON.stringify(JsonConsent));

    /*  $.ajax({
      type: 'POST',
      dataType: 'json',
      contentType: 'application/json; charset=utf-8',
      url: b.consentApiurl,
      headers: {
        Authorization: b.channelcode,
        'X-PRIMARY': b.PRIMARY,
      },
      data: JSON.stringify(JsonConsent),
      success: function() {},
    }); */
  };

  render() {
    const cknsPolicy = getCookie('ckns_policy');
    const { continent } = this.state;
    return continent === continentInAction && !cknsPolicy ? (
      <div className="wdt_gdpr" id="gdpr-block" style={{ display: 'block' }}>
        <div className="gdpr_content">
          <div className="gdpr_left">
            <h2>Cookies on the Navbharat Times Website</h2>
            <p>
              Navbharat Times has updated its Privacy and Cookie policy. We use cookies to ensure
              that we give you the better experience on our website. If you continue without
              changing your settings, we'll assume that you are happy to receive all cookies on the
              Navbharat Times website. However, you can change your cookie setting at any time by
              clicking on our
              <Link to={`${process.env.WEBSITE_URL}/cookiepolicy.cms`}>Cookie Policy link</Link> at
              any time. You can also see our{' '}
              <Link to={`${process.env.WEBSITE_URL}/privacypolicyeu.cms`}> Privacy Policy</Link>
            </p>
          </div>
          <div className="gdpr_right">
            <input
              type="button"
              value="Continue"
              onClick={this.continueClickHandler}
              className="btn-continue"
            />

            <Link to={`${process.env.WEBSITE_URL}/cookiepolicy.cms`}>Review Cookies</Link>
          </div>
        </div>
      </div>
    ) : null;
  }
}

export default Gdpr;
