import React from 'react';
import PropTypes from 'prop-types';
import serialize from 'serialize-javascript';

const Config = require(`../../../common/${process.env.SITE}`);

const ASSET_PATH = process.env.ASSET_PATH || '/';

function Html({ js, css, html, head, initialState, version, pagetype }) {
  const organizationSchema = {
    __html: `{
      "@context": "http://schema.org",
      "@type": "Organization",
      "location": {"@type": "Place", "address": "${Config.seoConfig.address}"},
      "description": "${Config.seoConfig.description}",
      "name": "${Config.seoConfig.name}",
      "email": "${Config.seoConfig.email}",    
      "logo":"${Config.seoConfig.logo}",
      "url": "${Config.seoConfig.url}",  
      "sameAs": [
        "${Config.seoConfig.fb}",
        "${Config.seoConfig.twitter}",
        "${Config.seoConfig.gplus}",
        "${Config.seoConfig.linkedin}",
        "${Config.seoConfig.youtube}",
        "${Config.seoConfig.instagram}",
        "${Config.seoConfig.pinterest}"
      ]
    }`,
  };

  const commonSchema = {
    __html: `{"@context": "http://schema.org","@graph":[{"@type": "Organization", "name": "${
      Config.seoConfig.name
    }", "url": "${Config.seoConfig.url}", "logo":{"@type":"ImageObject", "url": "${
      Config.seoConfig.logo
    }", "width":600, "height":60}, "sameAs": ["${Config.seoConfig.fb}","${
      Config.seoConfig.twitter
    }", "${Config.seoConfig.gplus}"]}]}`,
  };

  const searchSchema = {
    __html: `{
      "@context": "http://schema.org",
      "@type": "WebSite",
      "url": "${Config.seoConfig.url}",
      "potentialAction": {
        "@type": "SearchAction",
        "target": "${Config.seoConfig.url}/search?q={search_term_string}",
        "query-input": "required name=search_term_string"
      }
    }`,
  };

  const mainSchema = {
    __html: `{
      "@context": "http://schema.org",
              "@type": "WebSite",
            "name": "${Config.seoConfig.englishName}",
            "alternateName": "${Config.seoConfig.englishName}",
           "url": "${Config.seoConfig.url}"
    }`,
  };

  const mainSchemaTwo = {
    __html: `{
      "@context": "http://schema.org", 
      "@type": "WebSite",
       "url": "${Config.seoConfig.url}", 
       "name": "${Config.seoConfig.name}", 
       "alternateName": "${Config.seoConfig.englishName}"
    }`,
  };

  // Service worker script in case an unregister is needed.
  // const serviceWorkerScript = `
  // navigator.serviceWorker.getRegistrations().then(function(registrations) {
  //   for (let registration of registrations) {
  //     registration
  //       .unregister()
  //       .then(function() {
  //         return self.clients.matchAll();
  //       })
  //       .then(function(clients) {
  //         clients.forEach(client => {
  //           if (client.url && 'navigate' in client) {
  //             client.navigate(client.url);
  //           }
  //         });
  //       });
  //   }
  // });

  // `;

  // const serviceWorkerScript = `
  // if ('serviceWorker' in navigator ) {
  //   if (document.readyState === 'loading') {
  //     // console.log("LOADING DOM")
  //     document.addEventListener('DOMContentLoaded', function(){
  //       navigator.serviceWorker.register('/service-worker.js?v=${version}').then(function (registration) {
  //         // console.log('Service Worker registered! Scope: ' + registration.scope);
  //       }).catch(function (err) {
  //         // console.log('Service Worker registration failed: ' + err);
  //       });
  //     });
  //   }else {
  //     // console.log("WITHOUT DOM CONTENT LOADED",version)
  //     navigator.serviceWorker.register('/service-worker.js?v=${version}').then(function (registration) {
  //       // console.log('Service Worker registered! Scope: ' + registration.scope);
  //     }).catch(function (err) {
  //       // console.log('Service Worker registration failed: ' + err);
  //     });
  //   }
  // }else {
  //   // console.log("service worker not found")
  // }
  //   `;

  return (
    <html lang="hi">
      <head>
        {head.title.toComponent()}
        {head.meta.toComponent()}
        <meta content="IE=edge" httpEquiv="X-UA-Compatible" />
        <meta charSet="utf-8" />
        <meta httpEquiv="x-ua-compatible" content="ie=edge,chrome=1" />
        {/* noodp,noydir meta tags is applicable only for following pages, needs to handle these pages once we move to SCB
          Gadgets List Page
          Product Detail Page
          Comparison Landing Page 
          and - Comparison Detail
        Page */}
        <meta content="noodp,noydir" name="robots" />
        <script async="async" src="https://ads.pubmatic.com/AdServer/js/pwt/23105/1815/pwt.js" />
        <link
          rel="shortcut icon"
          href="https://navbharattimes.indiatimes.com/icons/nbtfavicon.ico"
        />
        {head.link.toComponent()}
        {head.script.toComponent()}
        <script type="application/ld+json" dangerouslySetInnerHTML={organizationSchema} />
        <script type="application/ld+json" dangerouslySetInnerHTML={commonSchema} />
        <script type="application/ld+json" dangerouslySetInnerHTML={searchSchema} />
        <script type="application/ld+json" dangerouslySetInnerHTML={mainSchema} />
        <script type="application/ld+json" dangerouslySetInnerHTML={mainSchemaTwo} />
        <style type="text/css">
          {`
            .wrap {width: 1000px; background: #fff; margin: 0 auto} 
            .contentarea{padding:15px 20px 15px; clear:both; position: relative; overflow:hidden;}
            .leftmain {width: 630px; float: left;}
            .rightnav {width: 300px; float: right;}

            .header {position: relative; min-height: 110px; overflow: hidden;}
            .header .logo {padding: 18px 0 0 0; float: left; width: 243px; text-align: center}
            .header .logo .date{font-family: Arial,Helvetica,sans-serif; font-size: 10px; color: #575757; display: block; margin: 3px 0 0 0}
            .header .add_atf {float: right; margin: 10px 20px 0 0;}
            .header .add_atf b.hide-txt{text-indent:-10000px; opacity: 0; height: 0; display: block;}

            .top_nav{display: none;}
            .bnews-placeholder{display: none;}
            .fixmenu_wrap .first-level-menu ul li.nav_right{display:none}

            .fixmenu_wrap .first-level-menu {background: #e9e9e9; height: 42px; padding: 0; position: relative; z-index: 12;}
            .fixmenu_wrap .first-level-menu ul li {padding: 0 9px 0 10px; line-height: 46px; height: 42px; display: inline-block; font-size: 15px; vertical-align: top;}
            .fixmenu_wrap .first-level-menu ul li a.nav-item {font-size: 16px; color: #000; text-decoration:none}
            .fixmenu_wrap .first-level-menu h3.home, .fixmenu_wrap .first-level-menu .go-to-nbt{display:none}
            .fixmenu_wrap .first-level-menu ul li.nav-elections, .fixmenu_wrap .first-level-menu ul li.nav-elections:hover, .fixmenu_wrap .first-level-menu ul li.nav-elections.active{background:#db6b48}
            .fixmenu_wrap .first-level-menu ul li.nav-elections.active:hover a.nav-item, .fixmenu_wrap .first-level-menu ul li.nav-elections:hover a.nav-item, .fixmenu_wrap .first-level-menu ul li.nav-elections.active a.nav-item{color:#fff}

            .breadcrumb{display: none;}
            .header .logo a.section_name{display:none}
            .articleshow_body h1{font-size:28px;}
            .articleshow_body .article_synopics{font-size:14px}
            body.AS .header .logo{font-size: 22px;}
            
            .section-404{background-color: #f2f2f2; border: 1px solid #e7e7e7; border-radius: 1px; margin: 30px 20px;}
            .section-404 img{display: inline-block; vertical-align: top; padding: 15px 18px 15px 10px; border:0; outline: none;}
            .section-404 .errortext{border-left: 1px solid #ddd; padding: 20px 0 0 20px; display:inline-block; vertical-align: top; min-height: 115px; width: 750px}

            .header .logo .section_name {display: block; color: #ea9926; font-size: 18px; font-weight:bold;}
            .header .logo .section_name span {font-size: 14px; display: block;}
            .react-tabs [role=tab]  { border-radius: 0px } 
            .react-tabs [role=tabpanel]  { border: 4px }            
            `}
        </style>
        {css.map(j => (
          <link key={j} href={`${ASSET_PATH}${j}`} media="all" rel="stylesheet" type="text/css" />
        ))}
        <script async="async" src="//js-sec.indexww.com/ht/p/191952-71956427846521.js" />
        <script src="https://jssocdn.indiatimes.com/crosswalk/jsso_crosswalk_0.2.5.min.js" async />
        <script async="async" src="https://securepubads.g.doubleclick.net/tag/js/gpt.js" />
      </head>

      <body>
        <div
          id="root"
          className="container"
          dangerouslySetInnerHTML={{
            __html: html,
          }}
        />
        <div id="fb-root" />
        {process.env.NODE_ENV === 'production' ? (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.process = ${serialize({
                env: {
                  BROWSER: 'true',
                  API_ENDPOINT: process.env.API_ENDPOINT,
                  API_BASEPOINT: process.env.API_BASEPOINT,
                  IMG_URL: process.env.IMG_URL,
                  WEBSITE_URL: process.env.WEBSITE_URL,
                  SITE: process.env.SITE,
                  NODE_ENV: process.env.NODE_ENV,
                  ASSET_PATH: process.env.ASSET_PATH,
                  BASE_URL: process.env.BASE_URL,
                },
              })}`,
            }}
          />
        ) : (
          <script
            dangerouslySetInnerHTML={{
              __html: `window.process = ${serialize({
                env: {
                  BROWSER: 'true',
                  REDUX_LOGGER: process.env.REDUX_LOGGER,
                  API_ENDPOINT: process.env.API_ENDPOINT,
                  API_BASEPOINT: process.env.API_BASEPOINT,
                  IMG_URL: process.env.IMG_URL,
                  WEBSITE_URL: process.env.WEBSITE_URL,
                  SITE: process.env.SITE,
                  VERSION: version,
                  PAGETYPE: pagetype,
                  NODE_ENV: process.env.NODE_ENV,
                  BASE_URL: process.env.BASE_URL,
                },
              })}`,
            }}
          />
        )}
        <script
          dangerouslySetInnerHTML={{
            __html: `window.INITIAL_STATE = ${serialize(initialState)}`,
          }}
        />
        {/* <script
          dangerouslySetInnerHTML={{
            __html: serviceWorkerScript,
          }}
        /> */}
        <script type="text/javascript" src="//imasdk.googleapis.com/js/sdkloader/ima3.js" defer />
        <script
          type="text/javascript"
          src="//videoplayer.indiatimes.com/v2/includes/ima3.js"
          defer
        />
        {/* <script type="text/javascript" src="//videoplayer.indiatimes.com/v2.2.6.8/sdk.js" defer /> */}
        <script type="text/javascript" src="//videoplayer.indiatimes.com/v2.4/sdk.js" defer />
        <script src="//static.clmbtech.com/ad/commons/js/colombia_v2.js" defer />
        <script src="https://navbharattimes.indiatimes.com/sct.cms" />
        {js.map(j => (
          <script data-val={j} key={j} src={`${ASSET_PATH}${j}`} />
        ))}

        <script
          dangerouslySetInnerHTML={{
            __html: `function removeElectionsCube() {
              document.querySelector('#cubeFrm').remove();
            }`,
          }}
        />
      </body>
    </html>
  );
}

Html.propTypes = {
  js: PropTypes.array.isRequired,
  css: PropTypes.array.isRequired,
  html: PropTypes.string,
  head: PropTypes.object.isRequired,
  initialState: PropTypes.object.isRequired,
  version: PropTypes.string,
  pagetype: PropTypes.string,
};

export default Html;
