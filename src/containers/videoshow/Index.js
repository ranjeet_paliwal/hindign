/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/prop-types */
/* eslint-disable react/no-unused-state */
/* eslint-disable operator-linebreak */
/* eslint-disable indent */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import {
  fetchDataIfNeeded,
  updateVideoShowData,
  updateNavData,
} from '../../actions/videoshow/index';
import BreadCrumbs from '../../components/BreadCrumb';
import Link from '../../components/Link/Link';
import {
  getDifferenceInHours,
  adsPlaceholder,
  addToWatchLater as addVideoToWatchLater,
  removeFromWatchLater,
  isCSR,
} from '../../common-utility';
import { analyticsGA, AnalyticsComscore, AnalyticsIBeat } from '../../analytics';
import { initGoogleAds } from '../../ads/dfp';

import Thumb from '../../components/Thumb/Thumb';
import CtnAd from '../articleshow/subsections/CtnAd';
import '../../public/css/commonComponent.scss';
import '../videolist/Videolist.scss';
import './Videoshow.scss';
import Videocommon from '../articleshow/subsections/Videocommon';
import ArticleListFakeLoader from '../../components/Loaders/ArticleListFakeLoader';

const Config = require(`../../../common/${process.env.SITE}.js`);

class VideoShowContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      embedOptionsShown: false,
      embedPopupShown: false,
      watchLaterVideos: {},
    };

    this.AdServingRules =
      (props &&
        props.data &&
        props.data.videoData &&
        props.data.videoData.pwa_meta &&
        props.data.videoData.pwa_meta.AdServingRules) ||
      null;
  }

  componentDidMount() {
    const { data, dispatch, params, query } = this.props;
    const sectionName = 'video';
    VideoShowContainer.fetchData({ dispatch, query, params });

    if (!this.AdServingRules) {
      this.renderAds(data);
    }

    setTimeout(() => {
      if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
        if (data && data.videoData && data.videoData.crby) {
          window.ga('set', 'dimension4', data.videoData.crby);
        }
        analyticsGA.pageview();
      }
      if (
        typeof AnalyticsComscore !== 'undefined' &&
        typeof AnalyticsComscore.invokeComScore !== 'undefined'
      ) {
        AnalyticsComscore.invokeComScore();
      }
    }, 0);

    if (data && data.videoData && data.videoData.pwa_meta && data.videoData.pwa_meta.ibeat) {
      const ibeatvalObj = data.videoData.pwa_meta.ibeat;
      const ibeatObj = Object.assign(Config.ibeatConfig, ibeatvalObj);
      AnalyticsIBeat.initIbeat(ibeatObj);
    }

    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  componentDidUpdate(prevProps) {
    const { data, location, query, params, dispatch, isLoadingNextVideo } = this.props;
    const sectionName = 'video';
    if (prevProps.location.pathname !== location.pathname) {
      dispatch(fetchDataIfNeeded({ query, params }));
    }

    if ((!prevProps.data && data) || (prevProps.isLoadingNextVideo && !isLoadingNextVideo)) {
      this.renderAds(data);
    } else if (data && data.videoData && data.videoData.pwa_meta && data.videoData.pwa_meta.ibeat) {
      const ibeatvalObj = data.videoData.pwa_meta.ibeat;
      const ibeatObj = Object.assign(Config.ibeatConfig, ibeatvalObj);
      AnalyticsIBeat.initIbeat(ibeatObj);
    }

    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  renderAds = data => {
    const adsData = (data && data.adsData && data.adsData.wapads) || '';
    if (adsData) {
      const sectionInfo = {
        pagetype: 'videoshow',
        sec1: data && data.videoData && data.videoData.sec1,
        sec2: data && data.videoData && data.videoData.sec2,
        sec3: data && data.videoData && data.videoData.sec3,
        hyp1: data && data.videoData && data.videoData.pwa_meta && data.videoData.pwa_meta.hyp1,
      };
      setTimeout(() => {
        initGoogleAds(adsData, sectionInfo);
      }, 0);
    }
  };

  hideEmbedPopup = () => {
    this.setState({
      embedPopupShown: false,
    });
  };

  showEmbedPopup = () => {
    this.setState({
      embedPopupShown: true,
    });
  };

  toggleEmbedOptions = () => {
    const { embedOptionsShown } = this.state;
    this.setState({ embedOptionsShown: !embedOptionsShown });
  };

  toggleWatchLater = videoId => {
    const { watchLaterVideos } = this.state;
    const { authentication, showLoginRegister } = this.props;
    const { loggedIn } = authentication;
    if (!loggedIn) {
      showLoginRegister();
      return;
    }
    if (watchLaterVideos[videoId]) {
      // Remove from watch later if it exists
      removeFromWatchLater(videoId);
      this.setState(prevState => ({
        watchLaterVideos: {
          ...prevState.watchLaterVideos,
          [videoId]: !prevState.watchLaterVideos[videoId],
        },
      }));
    } else {
      // Add to watch later
      addVideoToWatchLater(videoId)
        .then(() => {
          this.setState(prevState => ({
            watchLaterVideos: {
              ...prevState.watchLaterVideos,
              [videoId]: true,
            },
          }));
        })
        .catch(() => {});
    }
  };

  socialShare = type => {
    const { data } = this.props;

    const videoDetail =
      (data && data.videoData && data.videoData.items && data.videoData.items[0]) || null;

    const twitterData = {
      url: videoDetail ? videoDetail.wu : '',
      text: videoDetail && videoDetail.hl ? videoDetail.hl : '',
      via: Config.twitterHandle,
    };

    const fbData = {
      url: (videoDetail && videoDetail.wu) || '',
      quote: (videoDetail && videoDetail.hl) || '',
    };

    switch (type) {
      case 'TWITTER':
        if (typeof window !== 'undefined') {
          window.open(
            `https://twitter.com/share?url=${`${twitterData.url}`}&amp;text=${encodeURIComponent(
              twitterData.text,
            )}&via=${twitterData.via}`,
            'sharer',
            'toolbar=0,status=0,width=626,height=436,scrollbars=1',
          );
        }
        return false;

      case 'FB':
        const url = `${fbData.url}?utm_source=facebook.com&utm_medium=Facebook&utm_campaign=web`;
        const fbShareParams = { method: 'share', href: url };
        if (fbData && fbData.quote) {
          fbShareParams.quote = fbData.quote;
        }
        if (typeof window !== 'undefined') {
          window.FB.ui(fbShareParams, () => {});
        }
        break;

      case 'LINKEDIN':
        window.open(
          `https://www.linkedin.com/shareArticle?mini=true&url=${encodeURIComponent(videoDetail.wu)}
           
            &title= ${encodeURIComponent(
              (data.videoData && data.videoData.pwa_meta && data.videoData.pwa_meta.title) || '',
            )} 
            &summery=${encodeURIComponent(videoDetail.hl)}`,
          'sharer',
          'toolbar=0,status=0,width=626,height=436,scrollbars=1',
        );

      default:
    }

    return false;
  };

  changeVideoShowUrl = data => {
    const { seopath, msid } = data;
    const { dispatch } = this.props;
    try {
      if (seopath && msid) {
        dispatch(updateVideoShowData(msid));
        const url = `/${seopath}/videoshow/${msid}.cms`;
        window.history.pushState({ title: '' }, '', url);
      }
    } catch (e) {
      // console.log(e);
    }
  };

  render() {
    const { data, dispatch } = this.props;
    const pwaMeta = (data && data.videoData && data.videoData.pwa_meta) || null;
    const schema = {
      type: 'application/ld+json',
      innerHTML: data && data.videoData && data.videoData.Newsarticle,
    };

    const { embedOptionsShown, embedPopupShown, watchLaterVideos } = this.state;
    const { authentication } = this.props;
    const { loggedIn } = authentication;

    if (!data) {
      return <ArticleListFakeLoader />;
    }
    if (data && !data.videoData) {
      // If Error occurs on SSR return null so page can be redirected to 404.
      if (isCSR()) {
        this.props.router.push('/404');
      }
      return null;
    }

    const sectionId =
      data && data.videoData && data.videoData.sectioninfo && data.videoData.sectioninfo.msid
        ? data.videoData.sectioninfo.msid
        : null;

    const videoDetail =
      (data && data.videoData && data.videoData.items && data.videoData.items[0]) || null;
    const videoId = videoDetail ? videoDetail.id : '';
    const videoEid = videoDetail ? videoDetail.eid : '';
    // console.log('Videodetail', videoDetail.eid);
    const aurJaaneKeys =
      data && data.videoData && data.videoData.pwa_meta && data.videoData.pwa_meta.topicskey
        ? data.videoData.pwa_meta.topicskey.split(',')
        : null;
    const checkSkipad =
      data &&
      data.videoData &&
      data.videoData.pwa_meta &&
      data.videoData.pwa_meta.AdServingRules === 'Turnoff';
    const { isLoadingNextVideo } = this.props;

    const embedIframe = `<iframe src="//tvid.in/${videoEid}/lang?autoplay=false" style="height: 100%; width: 100%; max-height: 100%; max-width: 100%; visibility: visible;" border="0" frameborder="0" seamless="" scrolling="no" allowfullscreen="true" mozallowfullscreen="true" allowtransparency="true"></iframe>`;

    return (
      <div>
        <Helmet
          title={(pwaMeta && pwaMeta.title) || ''}
          titleTemplate="%s"
          meta={data && data.metaData && data.metaData.metaTags}
          link={data && data.metaData && data.metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          script={[schema]}
        />

        <BreadCrumbs
          data={
            data && data.videoData && data.videoData.breadcrumb && data.videoData.breadcrumb.div
              ? data.videoData.breadcrumb.div
              : null
          }
        />
        <div className={`videoshow_body ${isLoadingNextVideo ? 'loader_overlay' : ''}`}>
          <div className="leftmain">
            <div style={{ width: '660px', height: '430px', position: 'relative' }}>
              <div style={{ display: 'none' }} className="vidhidecontent">
                <p>This Content has been blocked in your country on Copyright grounds</p>
              </div>
              {/*
               slikeid={(isDocked && dockedVideo.videoData.eid) || ''}
                height="299"
                width="400"
                imageid={
                  (isDocked && dockedVideo.videoData.imageid) || Config.Locale.SiteInfo.defaultimg
                }
                imgsize={(isDocked && dockedVideo.videoData.imgsize) || ''}
                vidtitle={(isDocked && dockedVideo.videoData.hl) || ''}
                adsection="default"
                isDocked
                dispatch={dispatch}
                isPlaying={isDocked && dockedVideo.isPlaying}
                seolocation={isDocked && dockedVideo.videoData.seolocation}
                pagetype="articleshow"
                articleID={isDocked && dockedVideo.articleID}


              */}

              <div>
                <Videocommon
                  slikeid={videoDetail ? videoDetail.eid : null}
                  height="430"
                  width="660"
                  imageid={videoDetail ? videoDetail.imageid : null}
                  dispatch={dispatch}
                  imgsize="123"
                  changeVideoShowUrl={this.changeVideoShowUrl}
                  // imgsrc={domNode.attribs.thumburl}
                  vidtitle={videoDetail ? videoDetail.title : null}
                  isDockable
                  sectionId={sectionId}
                  adsection="default"
                  pagetype="videoshow"
                  articleID={videoId}
                  seolocation={videoDetail ? videoDetail.seolocation : ''}
                  autoplay
                  skipAd={checkSkipad}
                />
              </div>
            </div>
            <ul className="videosharing">
              <li
                title="share on facebook"
                onClick={() => this.socialShare('FB')}
                className="vid_fb"
              >
                Facebook
              </li>
              <li
                title="share on twitter"
                onClick={() => this.socialShare('TWITTER')}
                className="vid_twt"
              >
                Twitter
              </li>
              <li title="Embed" onClick={this.showEmbedPopup} className="vid_embbed">
                Embed
              </li>
            </ul>

            <div className="publishedData ">
              <span
                title="Add to Watch Later"
                className={watchLaterVideos[videoId] ? 'active watchlater' : 'watchlater'}
                onClick={() => this.toggleWatchLater(videoId)}
              />

              <span className="views">Views: {(videoDetail && videoDetail.vw) || 0} | </span>
              <span className="newsdate">
                {videoDetail ? getDifferenceInHours(videoDetail.dl, Date.now()) : ''}
              </span>
            </div>
            <div id="ss_ads" style={{ lineHeight: '0px', position: 'relative' }} />

            <div style={{ display: 'none' }} className="videoData" />
            <div className="vdtext">
              <h1 className="videotitle">{videoDetail ? videoDetail.hl : null}</h1>
              <span
                className="article_datetime"
                id="datef"
                style={{ width: '100%', display: 'block' }}
              >
                {videoDetail ? videoDetail.lu : ''}
              </span>
              <h3 className="videodescription">{videoDetail ? videoDetail.Story : null}</h3>
            </div>
            <div style={{ display: embedPopupShown ? 'block' : 'none' }} className="overlay" />
            <div style={{ display: embedPopupShown ? 'block' : 'none' }} className="emmbedUrl">
              <div className="emmbedhead">
                <span className="txt">Press CTRL+C to copy</span>
                <span className="close" onClick={this.hideEmbedPopup}>
                  X
                </span>
              </div>
              <div id="emebecodeval">{embedIframe}</div>
            </div>
          </div>
          <div className="rightnav">
            {adsPlaceholder('ATF_300')}

            {
              <TopVideos
                data={
                  data && data.videoData && data.videoData.topvideo && data.videoData.topvideo[0]
                    ? data.videoData.topvideo[0]
                    : null
                }
                loggedIn={loggedIn}
                watchLaterVideos={watchLaterVideos}
                toggleWatchLater={this.toggleWatchLater}
              />
            }
          </div>
          <div className="keyinfo">
            {aurJaaneKeys ? (
              <React.Fragment>
                <strong>{Config.Locale.articleshow.knowmore}</strong>
                {aurJaaneKeys.map((topicKey, index) => (
                  <Link
                    key={topicKey}
                    target="_blank"
                    to={`${process.env.BASE_URL}/topics/${topicKey}`}
                  >
                    {topicKey}
                    {/* {index < aurJaaneKeys.length - 1 ? <small> | </small> : null} */}
                  </Link>
                ))}
              </React.Fragment>
            ) : null}
          </div>
          <div className="web_title">
            <strong>Web Title </strong>
            {`${
              data &&
              data.videoData &&
              data.videoData.pwa_meta &&
              data.videoData.pwa_meta.alternatetitle
                ? data.videoData.pwa_meta.alternatetitle
                : null
            }`}

            <span id="webname" />
            <span>
              (
              <a
                target="_blank"
                href="//navbharattimes.indiatimes.com"
                style={{ fontFamily: 'arial' }}
              >
                Hindi News
              </a>
              from Navbharat Times , TIL Network)
            </span>
          </div>
          {adsPlaceholder('ROS_TextLink')}

          <MostViewedVideos
            data={
              data && data.videoData && data.videoData.mostviewed && data.videoData.mostviewed[0]
                ? data.videoData.mostviewed[0]
                : null
            }
            loggedIn={loggedIn}
            watchLaterVideos={watchLaterVideos}
            toggleWatchLater={this.toggleWatchLater}
            AdServingRules={this.AdServingRules}
          />
        </div>
      </div>
    );
  }
}

const MostViewedVideos = ({ data, toggleWatchLater, watchLaterVideos, AdServingRules }) => {
  return (
    <div className="vdlist">
      {data && data.secname ? (
        <h2>
          {data.secname} <span> {Config.Locale.video} </span>
        </h2>
      ) : null}
      <ul>
        {data && data.items && Array.isArray(data.items)
          ? data.items.map((item, index) => {
              return item && item.type === 'ctn' && !AdServingRules ? (
                <li className="section">
                  <CtnAd position={index} className="colombia" slotId={Config.CTN.videolistCtn} />
                </li>
              ) : (
                <li key={item.id} className="inline-slide">
                  <small className="duration">{item.du}</small>
                  <Thumb
                    width="174"
                    height="134"
                    imgId={item.imageid}
                    title={item.hl}
                    alt={item.hl}
                    islinkable="true"
                    link={item.wu || ''}
                    resizeMode="4"
                    className="img"
                  />

                  <div className="vidtxt">
                    <Link className="slidelink text_ellipsis" to={item.wu || ''}>
                      {item.hl}
                    </Link>
                  </div>
                  <span className="newsdate">{getDifferenceInHours(item.lu, Date.now())}</span>
                  <span
                    title="Add to Watch Later"
                    className={watchLaterVideos[item.imageid] ? 'active watchlater' : 'watchlater'}
                    onClick={() => toggleWatchLater(item.imageid)}
                    role="button"
                    tabIndex={0}
                  />
                </li>
              );
            })
          : null}
      </ul>
    </div>
  );
};

const TopVideos = ({ data }) => {
  return (
    <div className="section">
      <h2>
        {data ? data.secname : ''} {Config.Locale.video}
      </h2>
      <div className="topvid">
        <ul>
          {data && data.items && Array.isArray(data.items)
            ? data.items.map((data, index) => {
                return (
                  data &&
                  data.type !== 'ctn' && (
                    <li key={data.id} className="inline-slide">
                      <Thumb
                        width="100"
                        height="70"
                        imgId={data.imageid}
                        title={data.hl}
                        alt={data.hl}
                        islinkable="true"
                        link={data.wu || ''}
                        resizeMode="4"
                        className="img"
                      />
                      <small className="duration">{data.du}</small>
                      <div className="vidtxt">
                        <Link className="slidelink text_ellipsis" to={data.wu || ''}>
                          {data.hl}
                        </Link>
                        <span className="newsdate">
                          {getDifferenceInHours(data.lu, Date.now())}
                        </span>
                      </div>
                    </li>
                  )
                );
              })
            : null}
        </ul>
      </div>
    </div>
  );
};

MostViewedVideos.propTypes = {
  data: PropTypes.object,
  toggleWatchLater: PropTypes.func,
};

VideoShowContainer.propTypes = {
  data: PropTypes.object,
  dispatch: PropTypes.func,
  isLoadingNextVideo: PropTypes.bool,
  authentication: PropTypes.object,
};

VideoShowContainer.fetchData = function fetchData({ dispatch, query, params }) {
  return dispatch(fetchDataIfNeeded({ query, params }));
};

function mapStateToProps(state) {
  return {
    ...state.videoshow,
    authentication: state.authentication,
  };
}

export default connect(mapStateToProps)(VideoShowContainer);
