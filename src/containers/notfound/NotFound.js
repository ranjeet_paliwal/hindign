import React, { PureComponent } from 'react';
import Link from '../../components/Link/Link';
import './404.scss';
import Thumb from '../../components/Thumb/Thumb';
import '../../public/css/commonComponent.scss';

const Config = require('../../../common/nbt.js');

class NotFound extends PureComponent {
  constructor() {
    super();
    this.state = {
      MostReadData: null,
    };
  }

  componentDidMount() {
    const self = this;
    fetch(
      `${
        process.env.API_ENDPOINT
      }/webgn_combine.cms?type=article&tag=ibeatmostread&days=2&count=20&feedtype=sjson`,
    )
      .then(response => {
        return response.json();
      })
      .then(resultdata => {
        self.setState({
          MostReadData: resultdata.section.newsItem,
        });
      })
      .catch({});
  }

  render() {
    const { MostReadData } = this.state;
    return (
      <div className="warp404">
        <div className="notfoundWrap">
          <Thumb
            title="404 Error Found"
            alt="404 Error Found"
            src="https://static.langimg.com/photo/20874751.cms"
          />
          <div className="errortext">
            <h1>{Config.Locale.nofound.notfoundtext}</h1>
            {/* It may have expired, or there could be a typo. Maybe you can find what you need on our
        homepage. */}
            {Config.Locale.nofound.notfounddec}
            {/* <Link className="return-btn" to="/">
              Please click here to return
            </Link> */}
          </div>
        </div>
        <div className="mostpopular">{Config.Locale.nofound.mostpopular}</div>
        <ul className="list">
          {MostReadData && MostReadData.items
            ? MostReadData.items.map(items => {
                return (
                  <li>
                    <Link to={items.wu}>{items.hl}</Link>
                  </li>
                );
              })
            : null}
        </ul>
      </div>
    );
  }
}

export default NotFound;
