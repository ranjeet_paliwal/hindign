import React, { Component } from 'react';
import { connect } from 'react-redux';
import Parser from 'html-react-parser';
import {
  fetchGadgetListDataIfNeeded,
  fetchNextGadgetListDataIfNeeded,
} from '../../actions/gadgetlist/gadgetlist';

import './gadgetList.scss';
import '../../public/css/commonComponent.scss';

import GadgetFilterTool from '../../components/GadgetFilterTool';
import GadgetWidget from '../../components/CommonWidgets/GadgetWidget';
import BreadCrumbs from '../../components/BreadCrumb';
import { fetchFilterListDataIfNeeded } from '../../actions/gadgetfiltertool/gadgetfiltertool';
import Link from '../../components/Link/Link';
import { analyticsGA } from '../../analytics';
import { initGoogleAds } from '../../ads/dfp';
import { updateNavData } from '../../actions/gadgetshow';
import LatestGadgetsTable from '../../components/CommonWidgets/LatestGadgetTable';
// eslint-disable-next-line import/named
import { PageMeta } from '../../components/PageMeta/PageMeta';

const Config = require('../../../common/nbt');

class Gadgetlist extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curpg: 1,
      category: '',
    };
    this.config = {
      isFetchingNext: false,
    };
  }

  componentDidMount() {
    const { dispatch, params, router, gadgetAdsData } = this.props;
    const { query } = this.props.location;
    this.setState({
      category: params.category,
    });
    const _this = this;
    Gadgetlist.fetchData({ dispatch, query, params, router }).then(data => {
      // prefill fiter
      this.prefillfilter();
    });
    dispatch(
      updateNavData({
        sectionName: 'gadgets',
      }),
    );
    this.renderAds(gadgetAdsData);
  }

  capitalize = s => {
    if (typeof s !== 'string') return '';
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  componentWillReceiveProps(newProps, newState) {
    // debugger;
    const _this = this;
    const { dispatch, query, params, router, gadgetAdsData } = newProps;
    const dataType = document.querySelector('.tabs-square li.active')
      ? document.querySelector('.tabs-square li.active').id
      : null;
    if (
      newProps.params.filters !== this.props.params.filters ||
      newProps.params.brand !== this.props.params.brand ||
      newProps.params.category !== this.props.params.category ||
      router.location.pathname != this.props.location.pathname
      // newProps.location.pathname.indexOf('upcoming') > -1
    ) {
      const brandSelector = document.querySelectorAll('.filtercontent-brand input:checked');
      let brands = [];
      if (brandSelector) {
        brandSelector.forEach(item => brands.push(_this.capitalize(item.value)));
      }

      if (!params.brand) {
        params.brand = brands.join('|');
      }
      Gadgetlist.fetchData({ dispatch, query, params, router, dataType }).then(data => {
        // set first tab active no active tab in filter found
        // this.filterTabActive();
        // set section and subsection in window
        const { pwa_meta } = this.props.item && this.props.item[0] ? this.props.item[0] : {};
        if (pwa_meta && typeof pwa_meta === 'object') {
          // this.scrollBind();
          // this.animate_btn_App();
          // Ads_module.setSectionDetail(pwa_meta);
          // set hyp1 variable
          // pwa_meta ? setHyp1Data(pwa_meta) : '';
          // fire ibeat
          // pwa_meta.ibeat ? setIbeatConfigurations(pwa_meta.ibeat) : '';
          // handleReadMore();
        }
      });
    }
  }

  prefillfilter() {
    // debugger;
    const { params, router } = this.props;
    // let checkBox = document.getElementsByTagName('input');
    const checkBox = document.querySelectorAll('input');
    for (let b = 0; b < checkBox.length; b++) {
      if (checkBox[b].type == 'radio' && params.filters && params.filters.indexOf('sort=') > -1) {
        // set radio button value
        const filterlist = params.filters.split('&');
        for (let i = 0; i < filterlist.length; i++) {
          const getIndex = filterlist[i].indexOf('sort=') + 5;
          const getvalue = filterlist[i].substr(getIndex);
          if (getvalue == checkBox[b].value) checkBox[b].click();
        }
      } else if (
        checkBox[b].type == 'radio' &&
        (!params.filters || params.filters.indexOf('sort=') < 0) &&
        checkBox[b].value == 'popular'
      ) {
        checkBox[b].click();
      }
      if (
        checkBox[b].type == 'checkbox' &&
        checkBox[b].getAttribute('datavalues') == 'upcoming' &&
        router &&
        router.location &&
        router.location.pathname.indexOf('upcoming') > -1
      ) {
        // set Upcoming checkbox button value
        checkBox[b].click();
      } else if (
        checkBox[b].type == 'checkbox' &&
        params.brand &&
        checkBox[b].getAttribute('datavalues') == 'brand' &&
        checkBox[b].value == params.brand
      ) {
        // set brand checkbox button value
        checkBox[b].click();
      } else if (checkBox[b].type == 'checkbox' && params.filters) {
        // set all filters checkbox button value
        // set radio button value
        const filterlist = params.filters.split('&');
        for (let i = 0; i < filterlist.length; i++) {
          const getIndex = filterlist[i].indexOf('=') + 1;
          const getfiltecriteria = filterlist[i].substr(0, getIndex - 1);
          const getvalue = filterlist[i].substr(getIndex);
          if (
            getfiltecriteria == checkBox[b].getAttribute('datavalues') &&
            getvalue.indexOf(checkBox[b].value) > -1
          )
            checkBox[b].click();
        }
      }
    }
  }

  setCategoryfromParent(obj) {
    // debugger;
    const { dispatch, params, query, router } = this.props;
    const category = obj.currentTarget.id;
    this.props.router.push(`/tech/${category}`);
    // this.restForm();
    const categoryoverride = category;
    if (category) this.setState({ category });
    const tabparams = { category };
    dispatch(fetchFilterListDataIfNeeded(tabparams, query, router, categoryoverride));

    dispatch(fetchGadgetListDataIfNeeded(tabparams, query, router, categoryoverride));
  }

  readMore(event) {
    const { dispatch, query, params, router, value, location } = this.props;

    const curpg = this.state.curpg + 1;
    this.setState({ curpg });

    const _this = this;
    if (_this.config.isFetchingNext == false) {
      _this.config.isFetchingNext = !_this.config.isFetchingNext;
      Gadgetlist.fetchNextListData({ dispatch, query, params, router, curpg }).then(data => {
        const _data = data ? data.payload : {};
        _this.config.isFetchingNext = !_this.config.isFetchingNext;
        analyticsGA.event('Web GS', 'GadgetsList', 'morepage');
        if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
          analyticsGA.pageview(window.location.origin + location.pathname);
        }
        // AnalyticsGA.event({ category: "Wap_GS", action: "GadgetsList", label: "morepage" });
        // AnalyticsGA.pageview(location.origin + _this.generateListingLink());
        // Ads_module.refreshAds(['fbn']);//refresh fbn ads when next list added
        // fire ibeat for next
        // data && _data.pwa_meta && _data.pwa_meta.ibeat ? setIbeatConfigurations(_data.pwa_meta.ibeat) : '';
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const { params, gadgetAdsData } = this.props;
    if (prevProps.params.category !== params.category) {
      this.restForm();
    }
    setTimeout(() => {
      this.renderAds(gadgetAdsData);
    }, 3000);
  }

  restForm(obj) {
    // clear check box

    const checkBox = document.getElementsByTagName('input');
    if (checkBox && checkBox.length > 0) {
      for (let b = 0; b < checkBox.length; b++) {
        if (checkBox[b].type == 'radio') {
          checkBox[b].checked = false;
        }
        if (checkBox[b].type == 'checkbox') {
          checkBox[b].checked = false;
        }
      }
    }
    if (document.getElementById('filterapplied'))
      document.getElementById('filterapplied').innerHTML = '';
  }

  renderAds = data => {
    const adsData = (data && data.wapads) || '';
    setTimeout(() => {
      initGoogleAds(adsData);
    }, 30);
  };

  render() {
    const _this = this;

    const pagetype = 'gadgetlist';
    const category = Config.Locale.tech.category;

    const brandTitle =
      this.props.item &&
      this.props.item[0] &&
      this.props.item[0].gadgets &&
      this.props.item[0].gadgets[0] &&
      this.props.item[0].gadgets[0].regional &&
      this.props.item[0].gadgets[0].regional instanceof Array &&
      this.props.item[0].gadgets[0].regional.filter(gname => {
        return gname && gname.host === Config.SiteInfo.hostid;
      });
    const { isFetching, params, router, item, gadgetLatestData } = this.props;
    const brandName = params.brand ? brandTitle && brandTitle[0].brand_name : '';
    const heading = item && item[0] && item[0].brandheader;
    // let category = 'mobile-phones';
    const gadgetList = this.props.item && this.props.item.length > 0 ? this.props.item : null;
    const pagination =
      this.props.item && this.props.item[0] && this.props.item[0].pg ? this.props.item[0].pg : null;
    const navigationfilter =
      this.props.item &&
      this.props.item.length &&
      this.props.item[0].navigationfilter &&
      this.props.item[0].navigationfilter.length > 0
        ? this.props.item[0].navigationfilter
        : null;
    const pwaMeta = (gadgetList && gadgetList instanceof Array && gadgetList[0].pwa_meta) || null;
    const gadgetsArray = gadgetList && gadgetList instanceof Array && gadgetList[0];
    const gadgetsCount = gadgetList && gadgetList instanceof Array && gadgetList[0].gadgets.length;
    const secData = category.filter(data => {
      return data.keyword === params.category;
    });

    return (
      <React.Fragment>
        <BreadCrumbs
          data={
            gadgetList &&
            gadgetList instanceof Array &&
            gadgetList[0].breadcrumb &&
            gadgetList[0].breadcrumb.div
          }
        />
        <div className="contentarea gadgetlist_body">
          <div className="gl_top_bar">
            {pwaMeta ? PageMeta(pwaMeta) : null}
            <div className="select-inline-gadgets">
              <ul className="scroll">
                {category.map((item, index) => {
                  return (
                    <li
                      id={item.keyword}
                      onClick={_this.setCategoryfromParent.bind(this)}
                      className={`gdt-${item.keyword} ${
                        item.keyword == _this.state.category ? 'active' : ''
                      }`}
                    >
                      <b />
                      <label>{item.keylabel}</label>
                    </li>
                  );
                })}
              </ul>
            </div>
            {gadgetList &&
            gadgetList instanceof Array &&
            gadgetList[0].seo &&
            gadgetList[0].seo.atfcontent
              ? Parser(gadgetList[0].seo.atfcontent)
              : ''}
            <ul className="gl_suggested_keywords">
              {navigationfilter && navigationfilter.length > 0
                ? navigationfilter.map((item, index) => {
                    // set active class on filter
                    let className = '';
                    const itemname = item.name;
                    if (this.props.params && this.props.params.filters) {
                      const filterlist = this.props.params.filters.split('&');
                      for (let i = 0; i < filterlist.length; i++) {
                        let filtervalue = filterlist[i]
                          .substring(filterlist[i].lastIndexOf('=') + 1)
                          .toLowerCase();
                        if ((className = itemname.toLowerCase().indexOf(`${filtervalue}-`) > -1)) {
                          className = 'active';
                          break;
                        }
                      }
                    }

                    return (
                      <li className={className}>
                        <Link to={item.link}>{item.title}</Link>
                      </li>
                    );
                  })
                : null}
            </ul>
          </div>

          <div className="gl_filters">
            <GadgetFilterTool
              category={this.state.category}
              params={this.props.params}
              isFetching={isFetching}
              router={router}
              pagetype={pagetype}
              dispatch={this.props.dispatch}
            />
          </div>

          <div className="gl_list_mobiles">
            <h1 className="sectionHead">
              <span> {heading} </span>
            </h1>
            <GadgetWidget
              _category={this.state.category}
              params={this.props.params}
              isFetching={isFetching}
              router={router}
              pagetype={pagetype}
              gadgetList={gadgetList}
              dispatch={this.props.dispatch}
              navigationfilter={navigationfilter}
            />
            {gadgetsCount > 19 ? (
              <div>
                {pagination &&
                pagination.cp &&
                pagination.tp &&
                parseInt(pagination.cp) < parseInt(pagination.tp) ? (
                  <a onClick={this.readMore.bind(this)} className="more-btn">
                    {Config.Locale.tech.loadmore}
                  </a>
                ) : null}
              </div>
            ) : (
              ''
            )}
            {/* <div>
              {pagination &&
              pagination.cp &&
              pagination.tp &&
              parseInt(pagination.cp) < parseInt(pagination.tp) ? (
                <a onClick={this.readMore.bind(this)} className="more-btn">
                  {Config.Locale.tech.loadmore}
                </a>
              ) : null}
            </div> */}
          </div>
        </div>
        <div
          className="seo-btf-content"
          dangerouslySetInnerHTML={{
            __html:
              gadgetList && gadgetList[0] && gadgetList[0].seo && gadgetList[0].seo.btfcontent,
          }}
        />
        <LatestGadgetsTable data={gadgetsArray} params={params} />
        <div id="AL_Innov1" className="AL_Innov1" />
        <div id="WSK_160_AS" className="WSK_160_AS" />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state.gadgetlist,
  };
}

Gadgetlist.fetchData = ({ dispatch, params, query, router }) => {
  return dispatch(fetchGadgetListDataIfNeeded(params, query, router));
};
Gadgetlist.fetchNextListData = ({ dispatch, params, query, router, curpg }) => {
  return dispatch(fetchNextGadgetListDataIfNeeded(params, query, router, curpg));
};

export default connect(mapStateToProps)(Gadgetlist);
// export default Gadgetlist;
