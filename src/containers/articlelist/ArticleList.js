/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import Helmet from 'react-helmet';
import LazyLoad from 'react-lazyload';

import { fetchDataIfNeeded, updateNavData, updateListData } from '../../actions/articlelist';
import { adsPlaceholder } from '../../common-utility';
import { analyticsGA, AnalyticsComscore, AnalyticsIBeat } from '../../analytics';
import ArticleListFakeLoader from '../../components/Loaders/ArticleListFakeLoader';
import { initGoogleAds } from '../../ads/dfp';
import ArticlelistLeft from './articlelistLeft';
import ArticlelistRight from './articlelistRight';
import BreadCrumbs from '../../components/BreadCrumb';
import VideoWidget from '../../components/Videos/VideoWidget';
import PhotoWidget from '../../components/Photos/Photowidget';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';

import './Articlelist.scss';
import '../../public/css/commonComponent.scss';
import Link from '../../components/Link/Link';
import Thumb from '../../components/Thumb/Thumb';
import CtnAd from '../../components/CtnAd/CtnAd';

// import { logPoints, ConfigData } from '../../analytics/TimesPoint';

const Config = require(`../../../common/${process.env.SITE}`);

class ArticleList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activepg: '',
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    const { dispatch, params, query, data } = this.props;
    const sectionName =
      data && data.articleData && data.articleData.secnameseo ? data.articleData.secnameseo : '';
    ArticleList.fetchData({ dispatch, query, params });

    this.renderAds(data);

    // TPLib.logPoints(TPLib.ConfigData.tpconfig.ARTICLE_READ, '1234');

    // document.querySelector('body').classList.add('AL');

    if (data && data.articleData && data.articleData.pwa_meta && data.articleData.pwa_meta.ibeat) {
      AnalyticsIBeat.initIbeat(data.articleData.pwa_meta.ibeat);
    }

    const script = document.createElement('script');
    script.src = `https://d1uck549nef0ok.cloudfront.net/youplus.view.min.js?v=${Date.now().toString()}`;
    script.async = true;
    document.body.appendChild(script);

    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  componentDidUpdate() {
    const { dispatch, params, query, data } = this.props;

    const sectionName =
      data && data.articleData && data.articleData.secnameseo ? data.articleData.secnameseo : '';
    ArticleList.fetchData({ dispatch, query, params });
    this.renderAds(data);
    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  showLoaderIfNeeded = () => {
    const { data } = this.props;
    if (data && data.articleData) {
      return <ArticleListFakeLoader />;
    }
    return null;
  };

  UpdateNav = () => {
    const articleId = 'news';
    const { dispatch } = this.props;
    dispatch(
      updateNavData({
        articleId,
      }),
    );
  };

  handleClick(event) {
    const { dispatch, data } = this.props;
    dispatch(
      updateListData({
        articleId: (data && data.articleData && data.articleData.id) || '',
        pgno: Number(event.target.id),
      }),
    );
    document.documentElement.scrollTop = 0;
    const pageUrl =
      typeof document === 'object'
        ? `${document.location.protocol}//${document.location.host}${document.location.pathname}`
        : '';
    const curID = `${pageUrl}?curpg=${event.target.id}`;
    this.setState({
      activepg: Number(event.target.id),
    });

    window.history.replaceState({}, pageUrl, curID);
  }

  renderAds = data => {
    const adsData = (data && data.adsData && data.adsData.wapads) || '';
    if (adsData) {
      const sectionInfo = {
        pagetype: 'articlelist',
        sec1: data && data.articleData && data.articleData.sec1,
        sec2: data && data.articleData && data.articleData.sec2,
        sec3: data && data.articleData && data.articleData.sec3,
        hyp1:
          data && data.articleData && data.articleData.pwa_meta && data.articleData.pwa_meta.hyp1,
      };

      setTimeout(() => {
        initGoogleAds(adsData, sectionInfo);
      }, 1000);
    }
  };

  componentWillUnmount() {
    if (typeof googletag == 'object' && typeof googletag.destroySlots == 'function') {
      googletag.destroySlots();
    }
  }

  render() {
    const { isFetching, data } = this.props;
    const { activepg } = this.state;
    let renderPageNumbers = '';

    // Logic for displaying page numbers
    if (data && data.articleData && data.articleData.pg && data.articleData.pg.tp) {
      const activePage = Number(data.articleData.pg.cp);
      const tpFound = parseInt(data.articleData.pg.tp);
      const totalpage = tpFound > 15 ? 15 : tpFound;
      const pageNumbers = [];
      for (let i = 1; i <= totalpage; i++) {
        pageNumbers.push(i);
      }

      renderPageNumbers = pageNumbers.map(pageNo => {
        const activeClass = pageNo === activepg || pageNo === activePage ? 'active' : '';

        return (
          <a className={activeClass} key={pageNo} id={pageNo} onClick={this.handleClick}>
            {pageNo}
          </a>
        );
      });
    }

    const schema = {
      type: 'application/ld+json',
      innerHTML: data && data.articleData && data.articleData.Newsarticle,
    };

    const pwaMeta = (data && data.articleData && data.articleData.pwa_meta) || null;

    return (
      <div className="articles_container AL">
        <Helmet
          title={(pwaMeta && pwaMeta.title) || ''}
          titleTemplate="%s"
          meta={data && data.metaData && data.metaData.metaTags}
          link={data && data.metaData && data.metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          script={[schema]}
        />

        {isFetching && !data ? (
          <ArticleListFakeLoader />
        ) : (
          <div style={{ overflow: 'hidden' }}>
            {adsPlaceholder('ATF_AS_STRIPPD')}
            {adsPlaceholder('PPD')}
            <BreadCrumbs
              data={
                data &&
                data.articleData &&
                data.articleData.breadcrumb &&
                data.articleData.breadcrumb.div
              }
            />
            {/* <CtnAd
              articleId={data && data.articleData && data.articleData.id}
              height="40"
              width="1000"
              className="colombia"
              slotId={Config.CTN.stripped_pbd_article_show}
            /> */}

            <div className="contentarea AL">
              <div className="leftmain">
                {/* <div onClick={this.UpdateNav.bind(this, 'News')}>Update Nav</div> */}
                <div>
                  {data && data.updatedNav && data.updatedNav.articleId
                    ? data.updatedNav.articleId
                    : ''}
                </div>
                <ArticlelistLeft data={data && data.articleData} />
                {data && data.articleData && !data.articleData.sections ? (
                  <div className="printpage">{renderPageNumbers}</div>
                ) : null}
              </div>

              <ArticlelistRight
                data={data && data.articleRhsData}
                articleId={data && data.articleData && data.articleData.id}
              />
              <ErrorBoundary>
                <LazyLoad height={100} once>
                  <VideoWidget />
                </LazyLoad>
              </ErrorBoundary>
              <ErrorBoundary>
                <LazyLoad height={100} once>
                  <PhotoWidget />
                </LazyLoad>
              </ErrorBoundary>
            </div>
          </div>
        )}

        <div id="AL_Innov1" className="AL_Innov1" />
        <div id="WSK_160_AS" className="WSK_160_AS" />
      </div>
    );
  }
}

ArticleList.fetchData = function fetchData({ dispatch, query, params }) {
  return dispatch(fetchDataIfNeeded({ query, params }));
};

ArticleList.propTypes = {
  dispatch: PropTypes.func,
  params: PropTypes.object,
  query: PropTypes.object,
  history: PropTypes.object,
  data: PropTypes.object,
  isFetching: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.articlelist,
    ...state.authentication,
    newsDataFetched: state.news.didFetch,
    isFetchingNewsData: state.news.isFetching,
  };
}

export default connect(mapStateToProps)(ArticleList);
