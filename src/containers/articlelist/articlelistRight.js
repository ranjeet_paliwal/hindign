import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import NewsWidget from '../../components/Home/News/NewsWidget/NewsWidget';
import { adsPlaceholder } from '../../common-utility';
import WdtAffiliatesList from '../../components/Amazon/WdtAffiliatesList';

const ArticlelistRight = ({ data, articleId }) => {
  const isValidData =
    (data && Array.isArray(data.section) && data.section && data.section.length > 0) || null;
  return (
    <div className="rightnav">
      {adsPlaceholder('ATF_300 section')}

      <ErrorBoundary>
        <WdtAffiliatesList
          title=""
          category="mobile"
          tag="nbt_web_articlelist_sponsoredwidgetrhs-21"
          isslider="0"
          noimg="0"
          noh2="0"
        />
      </ErrorBoundary>

      {isValidData ? (
        <ErrorBoundary>
          <LazyLoad height={100} once>
            <NewsWidget
              newsContent={data.section}
              type="misc"
              secid="trending"
              secname="trending"
              imgrequired="none"
              count="20"
              headingRequired="yes"
              headingLinkable={false}
              widgetClassName="section trending-topics"
            />
          </LazyLoad>
        </ErrorBoundary>
      ) : (
        ''
      )}

      {isValidData ? (
        <ErrorBoundary>
          <LazyLoad height={100} once>
            <NewsWidget
              newsContent={data.section}
              type="misc"
              secid="ibeatmostread"
              secname="ibeatmostread"
              imgrequired="all"
              count="20"
              headingRequired="yes"
              headingLinkable={false}
              widgetClassName="section with-scroll"
            />
          </LazyLoad>
        </ErrorBoundary>
      ) : (
        ''
      )}

      <div
        className="colombia section"
        data-section="tech"
        data-slot="323818"
        id={`div-clmb-ctn-323818~1-${articleId}`}
        data-position={articleId}
      />

      {adsPlaceholder('section adDivs BTF_300')}
    </div>
  );
};

ArticlelistRight.propTypes = {
  data: PropTypes.object,
  articleId: PropTypes.string,
};

export default ArticlelistRight;
