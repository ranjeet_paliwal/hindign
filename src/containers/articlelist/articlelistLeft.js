import React from 'react';
import propTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Thumb from '../../components/Thumb/Thumb';
import Link from '../../components/Link/Link';
import CtnAd from '../../components/CtnAd/CtnAd';
import SocialToolBox from '../../components/SocialToolBox';

const Config = require(`../../../common/${process.env.SITE}`);

const ArticlelistLeft = React.memo(props => {
  const { data } = props;
  const type = (data && data.tn) || '';
  let CTNHtml = '';
  let newsHtml = '';

  const socialShare = (type, data) => {
    switch (type) {
      case 'twitter':
        if (typeof window !== 'undefined') {
          window.open(
            `https://twitter.com/share?url=${`${data.url}`}&amp;text=${encodeURIComponent(
              data.text,
            )}&via=${data.via}`,
            'sharer',
            'toolbar=0,status=0,width=626,height=436,scrollbars=1',
          );
        }
        return false;

      case 'fb': {
        const url = `${data.url}?utm_source=facebook.com&utm_medium=Facebook&utm_campaign=web`;
        const fbShareParams = { method: 'share', href: url };
        if (data && data.quote) {
          fbShareParams.quote = data.quote;
        }
        if (typeof window !== 'undefined') {
          window.FB.ui(fbShareParams, () => {});
        }
        break;
      }

      default:
    }

    return false;
  };

  switch (type) {
    default:
      return (
        <div className="full_section list_subsections">
          {data && data.pwa_meta ? (
            <h1 className="sectionHead">
              <span>{data.secname}</span>
            </h1>
          ) : (
            ''
          )}
          <ul>
            {data && data.items && Array.isArray(data.items) && data.items.length > 0
              ? data.items.map((items, index) => {
                  const random = Math.round(Math.random() * 10000);
                  if (index === 0) {
                    newsHtml = (
                      <li className="lead-post">
                        {/* <LazyLoad height={100} once> */}
                        <div className="img_conatiner">
                          <Thumb
                            width="630"
                            height="472"
                            imgId={items.imageid}
                            title={items.hl}
                            alt={items.hl}
                            islinkable="true"
                            link={items.wu}
                            resizeMode="75"
                          />
                          <SocialToolBox
                            fb
                            twitter
                            printUrl={
                              items && items.wu
                                ? items.wu.replace('articleshow', 'articleshowprint')
                                : null
                            }
                            twitterData={{
                              url: items ? items.wu : '',
                              text: items && items.hl ? items.hl : '',
                              via: Config.twitterHandle,
                            }}
                            fbData={{
                              url: (items && items.wu) || '',
                              quote: (items && items.hl) || '',
                            }}
                            print
                            articleId={items && items.id}
                            socialShare={socialShare}
                          />
                        </div>

                        <div className="txt_container">
                          <Link to={items.wu}>{items.hl}</Link>
                        </div>
                        {/* </LazyLoad> */}
                      </li>
                    );
                  } else if (items && items.tn && items.type === 'ctn') {
                    CTNHtml = (
                      <li>
                        <div className="ctnad">
                          <CtnAd
                            articleId={items.id}
                            position={random}
                            height="225"
                            width="300"
                            className="colombia"
                            slotId={Config.CTN.id_articlelist}
                          />
                        </div>
                      </li>
                    );
                  } else {
                    CTNHtml = (
                      <li>
                        {/* <LazyLoad> */}
                        <div className="img_container">
                          <Link to={items.wu} title={items.hl}>
                            <Thumb
                              width="300"
                              height="225"
                              imgId={items.imageid}
                              title={items.hl}
                              alt={items.hl}
                              islinkable="true"
                              link={items.wu}
                              resizeMode="4"
                            />
                          </Link>
                          <SocialToolBox
                            fb
                            twitter
                            printUrl={
                              items && items.wu
                                ? items.wu.replace('articleshow', 'articleshowprint')
                                : null
                            }
                            twitterData={{
                              url: items ? items.wu : '',
                              text: items && items.hl ? items.hl : '',
                              via: Config.twitterHandle,
                            }}
                            fbData={{
                              url: (items && items.wu) || '',
                              quote: (items && items.hl) || '',
                            }}
                            print
                            articleId={items && items.id}
                            socialShare={socialShare}
                          />
                        </div>
                        <div className="txt_container">
                          <Link to={items.wu}>{items.hl}</Link>
                        </div>
                        {/* </LazyLoad> */}
                      </li>
                    );
                  }
                  return index === 0 ? newsHtml : CTNHtml;
                })
              : null}
          </ul>
        </div>
      );
  }
});

ArticlelistLeft.displayName = 'ArticlelistLeft';

ArticlelistLeft.propTypes = {
  data: propTypes.array,
  type: propTypes.string,
};

export default ArticlelistLeft;
