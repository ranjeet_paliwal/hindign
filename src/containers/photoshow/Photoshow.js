/* eslint-disable react/no-unused-state */
/* eslint-disable operator-linebreak */
/* eslint-disable indent */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

import { fetchDataIfNeeded } from '../../actions/photoshow/index';

import Link from '../../components/Link/Link';

import Thumb from '../../components/Thumb/Thumb';
import '../../public/css/commonComponent.scss';
import './PhotoShow.scss';
import ArticleListFakeLoader from '../../components/Loaders/ArticleListFakeLoader';
import PhotogalleryLeft from '../../components/Photoshow/PhotogalleryLeft';
import CtnAd from '../articleshow/subsections/CtnAd';

const Config = require(`../../../common/${process.env.SITE}.js`);

class PhotoShow extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      embedOptionsShown: false,
      embedPopupShown: false,
      showComments: false,
      watchLaterVideos: {},
    };
  }

  componentDidMount() {}

  componentDidUpdate(prevProps) {}

  closeComments = () => {
    this.setState({
      showComments: false,
    });

    document.body.classList.remove('disable-scroll');
  };

  showComments = () => {
    this.setState({
      showComments: true,
    });
    document.body.classList.add('disable-scroll');
  };

  closeIfOnOverlay = e => {
    if (e.target.id === 'comments__body') {
      return false;
    }
    this.closeComments();
    return false;
  };

  socialShare = (type, data) => {
    switch (type) {
      case 'twitter':
        if (typeof window !== 'undefined') {
          window.open(
            `https://twitter.com/share?url=${data.url}&amp;text=${encodeURIComponent(
              data.text,
            )}&via=NavbharatTimes`,
            'sharer',
            'toolbar=0,status=0,width=626,height=436,scrollbars=1',
          );
        }
        return false;

      case 'fb':
        const url = `${data.url}?utm_source=facebook.com&utm_medium=Facebook&utm_campaign=web`;
        const fbShareParams = { method: 'share', href: url };
        if (data && data.quote) {
          fbShareParams.quote = data.quote;
        }
        if (typeof window !== 'undefined') {
          window.FB.ui(fbShareParams, () => {});
        }
        break;

      default:
    }

    return false;
  };

  render() {
    const { photoshowData, authentication, isFirstPhotoArticle, pIndex } = this.props;
    const { showComments } = this.state;
    if (!photoshowData) {
      return <ArticleListFakeLoader />;
    }

    return (
      <React.Fragment>
        {isFirstPhotoArticle ? (
          <h1>{photoshowData && photoshowData.it && photoshowData.it.hl}</h1>
        ) : (
          <div className="nextGallery">
            <h3>
              {photoshowData && photoshowData.it && photoshowData.it.hl}
              <b className="ar" />
            </h3>
            <span className="artext">अगली गेलरी</span>
          </div>
        )}
        {isFirstPhotoArticle ? (
          <div className="gallery_caption">
            <strong className="ft-12">Web Title:</strong>
            <h3 id="webname" className="ft-12">
              {photoshowData && photoshowData.pwa_meta && photoshowData.pwa_meta.alternatetitle}
            </h3>
            <p>
              (
              <Link to={process.env.WEBSITE_URL} target="_blank">
                {'Hindi News '}
              </Link>
              {'from Navbharat Times , TIL Network)'}
            </p>
          </div>
        ) : null}

        {/* next story h1 will replace by below code end */}
        <div id="galleries">
          <PhotogalleryLeft
            isFirstPhotoArticle={isFirstPhotoArticle}
            photoCount={
              photoshowData && photoshowData.pg && photoshowData.pg.cnt ? photoshowData.pg.cnt : ''
            }
            shiftBy={(photoshowData && photoshowData.shiftBy) || 0}
            firstpicUrl={photoshowData && photoshowData.firstpicUrl}
            pIndex={pIndex}
            data={photoshowData ? photoshowData.items : null}
            showComments={this.showComments}
            socialShare={this.socialShare}
          />
        </div>
        {showComments ? (
          <div className="popup_comments">
            <div className="body_overlay" onClick={this.closeIfOnOverlay} />
            <div style={{ right: 0 }} id="comments__body" className="comments-body">
              <span className="close_icon" onClick={this.closeComments} />
              <iframe
                src={`https://navbharattimes.indiatimes.com/comments_slider_react.cms?msid=${
                  photoshowData.it.id
                }&isloggedin=${authentication.loggedIn}`}
                height="100%"
                width="550"
                border="0"
                MARGINWIDTH="0"
                MARGINHEIGHT="0"
                HSPACE="0"
                VSPACE="0"
                FRAMEBORDER="0"
                SCROLLING="no"
                align="center"
                title="iplwidget"
                ALLOWTRANSPARENCY="true"
                id="comment-frame"
              />
            </div>
          </div>
        ) : (
          ''
        )}
      </React.Fragment>
    );
  }
}

export default PhotoShow;
