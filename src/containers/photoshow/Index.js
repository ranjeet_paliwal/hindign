/* eslint-disable func-names */
/* eslint-disable indent */
/* eslint-disable react/jsx-indent */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import Helmet from 'react-helmet';

import Photoshow from './Photoshow';
import { analyticsGA, AnalyticsComscore, AnalyticsIBeat } from '../../analytics';
import { fetchDataIfNeeded, updatePhotoShowData, updateNavData } from '../../actions/photoshow';

import ArticleSHowFakeLoader from '../../components/Loaders/ArticleShowFakeLoader';

import Link from '../../components/Link/Link';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import BreadCrumbs from '../../components/BreadCrumb';

import fetch from '../../utils/fetch/fetch';
import { adsPlaceholder } from '../../common-utility';
import { initGoogleAds, refreshAds } from '../../ads/dfp';
import ClientSliderContainer from '../../components/NewSlider/ClientSliderContainer';
import Slider from '../../components/NewSlider/NewSlider';
import CtnAd from '../articleshow/subsections/CtnAd';

const Config = require(`../../../common/${process.env.SITE}.js`);

class PhotoShowContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentArticle: 0,
      stickyRight: false,
      currentTop: 0,
      currentBottom: 0,
    };

    this.photoArticle1Ref = React.createRef();

    this.handleScrollChange = this.handleScrollChange.bind(this);
    this.changePageUrl = this.changePageUrl.bind(this);
  }

  componentDidMount() {
    const { data, dispatch, params, query, history } = this.props;
    const sectionName = 'photo';
    PhotoShowContainer.fetchData({ dispatch, query, history, params });

    if (
      data &&
      data.photoshowData &&
      data.photoshowData.pwa_meta &&
      data.photoshowData.pwa_meta.ibeat
    ) {
      window.addEventListener('scroll', this.handleScrollChange);
      this.photoArticle2Ref = React.createRef();

      /* const ibeatvalObj = data.photoshowData.pwa_meta && data.photoshowData.pwa_meta.ibeat;
      let ibeatObj = {};

      ibeatObj = Object.assign(Config.ibeatConfig, {
        ...ibeatvalObj,
        url: data.photoshowData && data.photoshowData.it && data.photoshowData.it.wu,
      });
      */
      this.renderAds(data);
      // this.trackGa(ibeatObj, true);

      dispatch(
        updateNavData({
          sectionName,
        }),
      );
      const { id } = params;
      let picId = '';
      let picDiv = null;
      if (id.includes('msid-') && id.includes('picid-')) {
        const matchedId = id.match(/picid-[0-9]+/);
        if (matchedId && matchedId[0]) {
          picId = matchedId[0].split('picid-')[1];
          picDiv = document.getElementById(`photo-block-${picId}`);
        }
      }
      let top = 0;

      if (picId && picDiv) {
        top = picDiv.offsetTop;
      }
      window.scrollTo({
        top,
      });
    }
  }

  componentDidUpdate(prevProps) {
    const { dispatch, params, query, location, history, data, isFetchingNextdata } = this.props;
    const sectionName = 'photo';
    if (!prevProps.data && data) {
      window.addEventListener('scroll', this.handleScrollChange);

      const { combinedPhotoArray } = data;
      const firstPhotoData =
        Array.isArray(combinedPhotoArray) && combinedPhotoArray.length > 0
          ? combinedPhotoArray[0]
          : null;
      const articleOneDiv = this.photoArticle1Ref.current;
      const firstPhotoDiv = document.getElementById(
        `photo-block-${firstPhotoData && firstPhotoData.id}`,
      );

      /* const ibeatvalObj = data.photoshowData.pwa_meta.ibeat;
      let ibeatObj = {};

      if (
        data &&
        data.photoshowData &&
        data.photoshowData.pwa_meta &&
        data.photoshowData.pwa_meta.ibeat
      ) {
        ibeatObj = Object.assign(Config.ibeatConfig, {
          ...ibeatvalObj,
          url: data.photoshowData && data.photoshowData.it && data.photoshowData.it.wu,
        });
      } */

      dispatch(
        updateNavData({
          sectionName,
        }),
      );

      this.renderAds(data);
      // this.trackGa(ibeatObj, true);

      const { id } = params;
      let picId = '';
      let picDiv = null;
      if (id.includes('msid-') && id.includes('picid-')) {
        const matchedId = id.match(/picid-[0-9]+/);
        if (matchedId && matchedId[0]) {
          picId = matchedId[0].split('picid-')[1];
          picDiv = document.getElementById(`photo-block-${picId}`);
        }
      }
      let top = 0;

      if (picId && picDiv) {
        top = picDiv.offsetTop;
      }
      window.scrollTo({
        top,
      });

      this.photoArticle2Ref = React.createRef();
      if (firstPhotoDiv) {
        const firstPhotoDivDim = firstPhotoDiv.getBoundingClientRect();
        const currentTop = firstPhotoDiv.offsetTop;
        const currentBottom = articleOneDiv.offsetTop + firstPhotoDivDim.height;

        this.setState({
          currentTop,
          currentBottom,
          currentArticle: 0,
        });
      }
    } else if (prevProps.location.pathname !== location.pathname) {
      window.scrollTo({ top: 0 });
      PhotoShowContainer.fetchData({ dispatch, query, params, history });
    } else if (prevProps.isFetchingNextdata && !isFetchingNextdata) {
      // Create a new ref to store next photo gallery div.
      const { nextPhotoArticlesData } = data;

      const nextPhotoArticlesLength = Array.isArray(nextPhotoArticlesData)
        ? nextPhotoArticlesData.length
        : 0;
      this[`photoArticle${nextPhotoArticlesLength + 2}Ref`] = React.createRef();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScrollChange);
    document.querySelector('body').classList.remove('PS');
  }

  renderAds = data => {
    const adsData = (data && data.adsData && data.adsData.wapads) || '';
    if (adsData) {
      // initGoogleAds(adsData);
      const sectionInfo = {
        pagetype: 'photoshow',
        sec1:
          data && data.photoshowData && data.photoshowData.pwa_meta.subsectitle1
            ? data.photoshowData.pwa_meta.subsectitle1
            : '',
        sec2:
          data && data.photoshowData && data.photoshowData.pwa_meta.subsectitle2
            ? data.photoshowData.pwa_meta.subsectitle2
            : '',
        sec3:
          data && data.photoshowData && data.photoshowData.pwa_meta.subsectitle3
            ? data.photoshowData.pwa_meta.subsectitle3
            : '',
        hyp1:
          data &&
          data.photoshowData &&
          data.photoshowData.pwa_meta &&
          data.photoshowData.pwa_meta.hyp1,
      };
      setTimeout(() => {
        initGoogleAds(adsData, sectionInfo);
      }, 0);
    }
  };

  trackGa = (ibeatObj, onlyIbeat) => {
    // FIXME: Already written in routeUpdate
    setTimeout(() => {
      if (
        !onlyIbeat &&
        typeof analyticsGA !== 'undefined' &&
        typeof analyticsGA.pageview !== 'undefined'
      ) {
        if (ibeatObj && ibeatObj.crby) {
          window.ga('set', 'dimension4', ibeatObj.crby);
        }

        analyticsGA.pageview();
      }
      if (
        !onlyIbeat &&
        typeof AnalyticsComscore !== 'undefined' &&
        typeof AnalyticsComscore.invokeComScore !== 'undefined'
      ) {
        AnalyticsComscore.invokeComScore();
      }

      // if (AnalyticsIBeat) {
      //   AnalyticsIBeat.initIbeat(ibeatObj);
      // }
    }, 0);
  };

  showLoaderIfNeeded = () => {
    const { data } = this.props;
    if (
      data &&
      data.articleData &&
      data.articleData.nextarticles &&
      data.articleData.nextarticles.length > 0
    ) {
      return <ArticleSHowFakeLoader />;
    }
    return null;
  };

  handleScrollChange(e) {
    const { dispatch, data, isFetchingNextdata } = this.props;
    const { currentBottom, currentTop, currentArticle, stickyRight } = this.state;

    if (data) {
      const { photoshowData, combinedPhotoArray } = data;

      const currentScrollPosition = window.scrollY;
      let newArticle = currentArticle;
      let newTop = currentTop;
      let newBottom = currentBottom;
      let photoDiv = null;

      const scrolledDownwards = currentScrollPosition > currentBottom;
      const scrolledUpwards = currentScrollPosition < currentTop;

      if (scrolledDownwards || scrolledUpwards) {
        /*
      Code for changing url when moving to different photos.
    */

        const isFirstArticle = currentArticle === 0;
        const isLastArticle = currentArticle === combinedPhotoArray.length - 1;
        /*
      Check if user is not between first or
      last photo , to make calculations easier
      if scrolled down photo index + 1
      if scrolled up photo index - 1
    */
        if (isFirstArticle || isLastArticle) {
          if (isFirstArticle && scrolledDownwards) {
            newArticle = currentArticle + 1;
          } else if (isLastArticle && scrolledUpwards) {
            newArticle = currentArticle - 1;
          }
        } else {
          newArticle = scrolledDownwards ? currentArticle + 1 : currentArticle - 1;
        }
      }

      if (newArticle !== currentArticle) {
        const id = combinedPhotoArray[newArticle] ? combinedPhotoArray[newArticle].id : '';
        photoDiv = document.getElementById(`photo-block-${id}`);

        if (photoDiv) {
          const photoDivDim = photoDiv.getBoundingClientRect();
          newTop = photoDiv.offsetTop;
          newBottom = photoDiv.offsetTop + photoDivDim.height;
          this.changePageUrl(newArticle);

          this.setState({
            currentTop: newTop,
            currentBottom: newBottom,
            currentArticle: newArticle,
          });
        }
      }

      /*
       Code for adding sticky class to right nav while scrolling.
    */

      if (window.scrollY > 100 && !stickyRight) {
        this.setState({
          stickyRight: true,
        });
      } else if (window.scrollY <= 100 && stickyRight) {
        this.setState({
          stickyRight: false,
        });
      }

      /*
      Code for fetching next article if scrolled to middle of
      current article
    */
      const { nextPhotoArticlesData } = data;
      const lastPhotoArticle = Array.isArray(nextPhotoArticlesData)
        ? nextPhotoArticlesData[nextPhotoArticlesData.length - 1]
        : null;
      const nextArticleId =
        photoshowData && photoshowData.pwa_meta && photoshowData.pwa_meta.nextGal
          ? photoshowData.pwa_meta.nextGal.msid
          : null;

      let lastArticleNode = null;
      let lastArticleHeight = null;
      let lastArticleTop = null;
      if (lastPhotoArticle) {
        lastArticleNode = this[`photoArticle${nextPhotoArticlesData.length + 1}Ref`]
          ? this[`photoArticle${nextPhotoArticlesData.length + 1}Ref`].current
          : null;
        if (lastArticleNode) {
          lastArticleHeight = lastArticleNode.getBoundingClientRect().height;
          lastArticleTop = lastArticleNode.offsetTop;
        }
      }
      const firstPhotoShowDiv = this.photoArticle1Ref ? this.photoArticle1Ref.current : null;

      if (
        firstPhotoShowDiv &&
        nextArticleId &&
        Array.isArray(nextPhotoArticlesData) &&
        nextPhotoArticlesData.length === 0 &&
        !isFetchingNextdata
      ) {
        const firstPhotoDivHeight = firstPhotoShowDiv.getBoundingClientRect().height;
        const firstPhotoDivTop = firstPhotoShowDiv.offsetTop;

        if (window.scrollY >= (firstPhotoDivTop + firstPhotoDivHeight) / 2) {
          // Add code for fetching next article( after first article)
          if (nextArticleId) {
            dispatch(updatePhotoShowData(nextArticleId));
          }
        }
      } else if (
        !isFetchingNextdata &&
        lastPhotoArticle &&
        lastArticleNode &&
        lastArticleHeight &&
        lastArticleTop &&
        window.scrollY >= (lastArticleTop + lastArticleHeight) / 2
      ) {
        const nextToLastArticleId =
          lastPhotoArticle.pwa_meta &&
          lastPhotoArticle.pwa_meta.nextGal &&
          lastPhotoArticle.pwa_meta.nextGal.msid
            ? lastPhotoArticle.pwa_meta.nextGal.msid
            : null;
        if (nextToLastArticleId) {
          dispatch(updatePhotoShowData(nextToLastArticleId));
        }
      }
    }
  }

  changePageUrl(articleIndex) {
    let title = '';
    let newUrl = '';
    const { data } = this.props;
    const { combinedPhotoArray } = data;

    if ((articleIndex + 1) % 3 === 0) {
      if (window && window.adSlot_ATF_300) {
        googletag.pubads().refresh([window.adSlot_ATF_300]);
      }
    }

    const currentPhotoData =
      combinedPhotoArray &&
      Array.isArray(combinedPhotoArray) &&
      articleIndex <= combinedPhotoArray.length - 1
        ? combinedPhotoArray[articleIndex]
        : null;

    if (currentPhotoData) {
      newUrl = currentPhotoData.wu ? currentPhotoData.wu.replace(process.env.WEBSITE_URL, '') : '';
      title = currentPhotoData.hl || '';
    }

    if (newUrl && title) {
      /* const ibeatvalObj = currentPhotoData.ibeat;
      let ibeatObj = {};

      if (currentPhotoData && currentPhotoData.ibeat) {
        ibeatObj = Object.assign(Config.ibeatConfig, {
          ...ibeatvalObj,
          url: currentPhotoData.wu,
        });
      }

      this.trackGa(ibeatObj, false);
      */
      window.history.pushState({ title }, '', `/tech${newUrl}`);
      document.title = title;
    }
  }

  render() {
    const { data, authentication } = this.props;

    if (!data) {
      return <ArticleSHowFakeLoader />;
    }
    const { stickyRight } = this.state;

    const { nextPhotoArticlesData, photoshowData } = data;

    return (
      <div className="articles_container PS">
        <BreadCrumbs
          data={
            photoshowData && photoshowData.breadcrumb && photoshowData.breadcrumb.div
              ? photoshowData.breadcrumb.div
              : null
          }
        />
        <div className="contentarea photoshow_body">
          <div className="leftmain">
            <Helmet
              title={photoshowData && photoshowData.pwa_meta && photoshowData.pwa_meta.title}
              titleTemplate="%s"
              meta={data && data.metaData && data.metaData.metaTags}
              link={data && data.metaData && data.metaData.metaLinks}
              htmlAttributes={{ lang: 'hi' }}
              // script={[schema]}
            />

            <ErrorBoundary>
              <div className="single-story-conrainer" ref={this.photoArticle1Ref}>
                <Photoshow
                  isFirstPhotoArticle
                  photoshowData={photoshowData || null}
                  authentication={authentication}
                />
              </div>
            </ErrorBoundary>

            {nextPhotoArticlesData && Array.isArray(nextPhotoArticlesData)
              ? nextPhotoArticlesData.map((pData, index) => (
                  <ErrorBoundary key={pData && pData.it && pData.it.id}>
                    <div
                      className="single-story-conrainer"
                      ref={this[`photoArticle${index + 2}Ref`]}
                    >
                      <Photoshow
                        isFirstPhotoArticle={false}
                        photoshowData={pData}
                        pIndex={index}
                        authentication={authentication}
                      />
                    </div>
                  </ErrorBoundary>
                ))
              : null}
            {/* ) : null} */}
          </div>
          <div className="rightnav">
            <div className={`${stickyRight ? 'photoshow_right_fixed' : ''}`}>
              {adsPlaceholder('ATF_300 section ads_default')}

              <ErrorBoundary>
                {data && data.photoshowRightData && Array.isArray(data.photoshowRightData.items) ? (
                  <div className="photoshow-slider section">
                    <Slider
                      type="photoshowright_slider"
                      size="6"
                      sliderData={data.photoshowRightData.items}
                      width="90"
                      height="69"
                      SliderClass=""
                      islinkable
                      secname={data.photoshowRightData.hl || ''}
                      link={data.photoshowRightData.wu || ''}
                      sliderWidth="300"
                      moreLinkRequired="no"
                    />
                  </div>
                ) : null}
              </ErrorBoundary>
              <CtnAd
                height="40"
                position="0"
                width="1000"
                className="colombia"
                slotId={Config.CTN.id_photoshow}
              />
            </div>
          </div>
          {this.showLoaderIfNeeded()}
        </div>
        <div id="AS_Innov1" className="AS_Innov1" />
        <div id="WSK_160_AS" className="WSK_160_AS" />
      </div>
    );
  }
}

PhotoShowContainer.fetchData = function({ dispatch, query, history, params }) {
  const { id } = params;
  let newId = id;
  if (id.includes('msid-') && id.includes('picid-')) {
    const matchedId = id.match(/msid-[0-9]+/);
    if (matchedId && matchedId[0]) {
      newId = matchedId[0].split('msid-')[1];
    }
  }

  const newParams = { ...params, id: newId };
  return dispatch(fetchDataIfNeeded({ query, params: newParams, history }));
};

PhotoShowContainer.propTypes = {
  data: PropTypes.object,
  authentication: PropTypes.object,
  showComments: PropTypes.func,
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ...state.photoshow,
    authentication: state.authentication,
  };
}

export default connect(mapStateToProps)(PhotoShowContainer);
