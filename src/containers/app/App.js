/* eslint-disable func-names */
/* eslint-disable implicit-arrow-linebreak */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import { getCookie, loadJS, loadInterStitial, fbPixel, isInViewport } from '../../common-utility';
import { fetchDataIfNeeded } from '../../actions/nav/nav';
import { storeUserDetails } from '../../actions/authentication';
import Header from '../../components/Header/Header';
import Footer from '../../components/Footer/Footer';
import Logobar from '../../components/Header/Logobar/Logobar';
import Masthead from '../../components/Header/Masthead';
import { analyticsGA, AnalyticsComscore } from '../../analytics';
// import BreakingNews from '../../components/BreakingNews/BreakingNews';
import Gdpr from '../Gdpr/Gdpr';
import LoginRegister from '../../components/LoginRegister/LoginRegister';
import { OneTapIntialize, saveUserData } from '../../onetapsignin/onetap';
import { oneTapSignIn } from '../../../common/nbt';
import GlobalTopNav from '../../components/Header/GlobalTopNav';
import BreadCrumbs from '../../components/BreadCrumb';
import { initTimesPoint, initPostLoginAction } from '../../analytics/TimesPoint';
//  DON'T REMOVE THIS - this is merging this into final css loader module in webpack
import '../../public/css/main.css';
import Videocommon from '../articleshow/subsections/Videocommon';
import Cube from '../../components/Cube/Cube';

import CtnAd from '../../components/CtnAd/CtnAd';

const Config = require(`../../../common/${process.env.SITE}`);

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLoginRegister: false,
      showElectionCube: false,
    };
    this.showLoginRegister = this.showLoginRegister.bind(this);
    this.closeLoginRegister = this.closeLoginRegister.bind(this);
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      window.addEventListener('beforeunload', () => {
        window.scrollTo(0, 0);
      });
    }

    const { value } = this.props;
    const { dispatch, params, location } = this.props;
    // if (location.pathname !== '/' && location.pathname !== '/default.cms') {
    const { id } = params;
    if (typeof id === 'string') {
      const msid = id.slice(0, id.indexOf('.'));
      // dispatch(fetchHeaderData(msid));
    }
    // }

    if (typeof window !== 'undefined' && !window.JSSO_INSTANCE) {
      try {
        window.JSSO_INSTANCE = new JssoCrosswalk('nbt', 'WEB');
      } catch (ex) {
        // continue regardless of error
      }

      if (
        typeof getCookie === 'function' &&
        typeof JSSO_INSTANCE !== 'undefined' &&
        getCookie('ssoid')
      ) {
        JSSO_INSTANCE.getUserDetails(r => {
          if (r.code === 200 && r.status === 'SUCCESS') {
            dispatch(storeUserDetails(r));
          } else {
            saveUserData(dispatch); // saves user data to redux store
          }
        });
      } else if (typeof OneTapIntialize === 'function') {
        OneTapIntialize(oneTapSignIn, dispatch);
      }
    }

    if (typeof window !== 'undefined' && !window.FB) {
      loadJS('https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2', () => {
        FB.init({
          appId: '154495568087154',
          autoLogAppEvents: true,
          xfbml: true,
          version: 'v2.10',
          oauth: true,
          status: true,
          cookie: true,
        });
      });
    }
    //  initTimesPoint();
    setTimeout(() => {
      loadJS(Config.TimesPoint.jsProd, () => {
        if (typeof initTimesPoint === 'function') {
          initTimesPoint();
        }
      });
    }, 2000);

    setTimeout(() => {
      if (document.getElementById('tpwidget-prelogin')) {
        const loginDiv = document.getElementById('tpwidget-prelogin');
        loginDiv.onclick = this.showLoginRegister;
      }
    }, 3000);

    const frequency =
      (value &&
        value.topnav &&
        value.topnav.interstitial &&
        value.topnav.interstitial.item &&
        value.topnav.interstitial.item.frequency) ||
      null;
    const intValue =
      (value &&
        value.topnav &&
        value.topnav.interstitial &&
        value.topnav.interstitial.item &&
        value.topnav.interstitial.item.value) ||
      null;

    if (
      typeof loadInterStitial === 'function' &&
      (location.pathname === '/' || location.pathname === '/default.cms')
    ) {
      loadInterStitial(intValue, frequency);
    }

    loadJS('https://browser.sentry-cdn.com/4.6.4/bundle.min.js', () => {
      if (typeof window !== 'undefined' && window.Sentry && window.Sentry.init) {
        window.Sentry.init({ dsn: 'https://838e342d27874768bcd257b3f621d893@sentry.io/1401675' });
      }
    });
    setTimeout(() => {
      if (typeof window !== 'undefined' && typeof window.ga === 'undefined' && analyticsGA) {
        analyticsGA.initialize();
      }

      if (typeof window !== 'undefined' && typeof COMSCORE === 'undefined' && AnalyticsComscore) {
        AnalyticsComscore.initialize();
      }
      // if (typeof getCookie === 'function' && getCookie('continent_gdpr') !== 'EU') {
      //   loadJS(Config.Analytics.mouseFlow);
      // }
      if (typeof fbPixel === 'function') {
        fbPixel(Config.Analytics.fbPixel.trackingId);
      }
      if (typeof loadJS === 'function') {
        loadJS(Config.Analytics.dmpPixel);
        loadJS(Config.Analytics.dmpPixel2);
      }
    }, 1000);
  }

  componentDidUpdate(prevProps) {
    const { loggedIn, userData, dockedVideo, location } = this.props;
    if (!prevProps.loggedIn && loggedIn) {
      setTimeout(() => {
        if (typeof initPostLoginAction === 'function') {
          initPostLoginAction(loggedIn, userData);
        }
      }, 3000);
    }
    if (
      prevProps.dockedVideo &&
      !prevProps.dockedVideo.isDocked &&
      dockedVideo &&
      dockedVideo.isDocked
    ) {
      document.querySelector('.enable_dock').classList.add('stickycls');
    }

    if (
      dockedVideo &&
      dockedVideo.isDocked &&
      prevProps.location.pathname != location.pathname &&
      !document.querySelector('.stickycls').classList.contains('IsDockGafired')
    ) {
      document.querySelector('.stickycls').classList.add('IsDockGafired');
      if (typeof analyticsGA === 'object') {
        analyticsGA.event('PIP Dock', 'home', `${dockedVideo.videoData.seolocation}`);
      }
    }

    // adding disable_video class while PIP is playing and route change from default to articleshow
    if (
      dockedVideo &&
      dockedVideo.videoData &&
      dockedVideo.videoData.eid &&
      dockedVideo.articleID &&
      dockedVideo.isDocked &&
      document.getElementById(`DocId_${dockedVideo.videoData.eid}_${dockedVideo.articleID}`)
    ) {
      const leadvideoID = document.getElementById(
        `DocId_${dockedVideo.videoData.eid}_${dockedVideo.articleID}`,
      );
      leadvideoID.classList.add('disable_video');
    }
  }

  handleHomePageScroll = () => {
    try {
      const firstLevelMenu = document.querySelector('#fixedMenu');
      if (firstLevelMenu) {
        const position = firstLevelMenu.getBoundingClientRect().y;
        if (position < -40) {
          firstLevelMenu.classList.add('fixed');
        } else {
          firstLevelMenu.classList.remove('fixed');
        }
        if (position < -700 && !getCookie('notifypopup') && getCookie('continent_gdpr') !== 'EU') {
          document.querySelector('.notifypopup').style.display = 'block';
        }
      }
    } catch (ex) {
      // continue regardless of error
    }
  };

  handleHomePageScroll11 = () => {
    try {
      const { showElectionCube } = this.state;
      let displayElectionCube = false;
      // If top widget is not in viewport, and cube is shown
      const electionWidget = document.querySelector('#electionTopWidget');

      if (electionWidget && !isInViewport(electionWidget)) {
        displayElectionCube = true;
      }
      // If state needs to be changed
      if (showElectionCube !== displayElectionCube) {
        this.setState({
          showElectionCube: displayElectionCube,
        });
      }
    } catch (ex) {
      // continue regardless of error
    }
  };

  adIndex = pathName => {
    let indexName = 'home';
    if (pathName.indexOf('articleshow') !== -1) {
      indexName = 'articleshow';
    }
    return indexName;
  };

  closeLoginRegister() {
    this.setState({
      showLoginRegister: false,
    });
  }

  showLoginRegister() {
    loadJS(Config.SSO.JsCaptcha);
    this.setState({
      showLoginRegister: true,
    });
  }

  render() {
    const { children, value, params, location, dockedVideo, dispatch } = this.props;
    const isDocked = (dockedVideo && dockedVideo.videoData) || '';
    const { showLoginRegister, showElectionCube } = this.state;
    let isNotHome = false;
    let electionWidgetCUBE = '';
    if (value && value.topnav) {
      const topnavObj = value.topnav;

      if (Array.isArray(topnavObj.electionframecube) && topnavObj.electionframe.length > 0) {
        electionWidgetCUBE = topnavObj.electionframecube[0];
      }
    }
    // console.log('fetchdata appjs', this.props);
    let msid = '';
    if (location.pathname !== '/' && location.pathname !== '/tech') {
      isNotHome = true;
      const { id } = params;
      if (typeof id === 'string') {
        msid = id.slice(0, id.indexOf('.'));
      }
    }

    const { topnav, nav, navHoverData } = value;
    const mastHead = typeof topnav !== 'undefined' && topnav.masthead ? topnav.masthead : null;
    const dateTime = typeof topnav !== 'undefined' && topnav.datetime ? topnav.datetime : {};
    // console.log('fetchdata appjs nav', nav);
    // console.log('fetchdata appjs navHoverData', navHoverData);
    const childrenWithProps = React.Children.map(children, child =>
      React.cloneElement(child, {
        showLoginRegister: this.showLoginRegister,
        closeLoginRegister: this.closeLoginRegister,
        handleHomePageScroll: this.handleHomePageScroll,
      }),
    );
    const cubeConfig = (topnav && topnav.feedconfig && topnav.feedconfig.cube) || '';

    return (
      <div>
        <Helmet
          defaultTitle="NBT"
          titleTemplate="%s"
          meta={[{ name: 'description', content: 'NBT' }]}
          htmlAttributes={{ lang: 'hi' }}
        />
        <ErrorBoundary>
          <Gdpr />
        </ErrorBoundary>

        <div style={{ display: 'table', width: '1000px', margin: '0 auto', 'font-size': '0px' }}>
          <div className="ELECTION_TOPBAND" />
        </div>

        <div className="top_nav">
          <div className="top_nav_content">
            <ErrorBoundary>
              <GlobalTopNav
                data={topnav}
                isNotHome={isNotHome}
                type="topnav"
                showLoginRegister={this.showLoginRegister}
              />
            </ErrorBoundary>
            <ErrorBoundary>
              {showLoginRegister ? (
                <LoginRegister
                  isShown={showLoginRegister}
                  closeLoginRegister={this.closeLoginRegister}
                />
              ) : null}
            </ErrorBoundary>
          </div>
        </div>
        <div className="wrap">
          <header className="header">
            <ErrorBoundary>{mastHead && <Masthead data={mastHead} />}</ErrorBoundary>
            <ErrorBoundary>
              <Logobar
                isNotHome={isNotHome}
                width="116"
                height=""
                alt=""
                dateTimeData={dateTime}
                loc={location}
              />
            </ErrorBoundary>
          </header>
          <div className="fixmenu_wrap">
            <div id="fixedMenu">
              {/* <ErrorBoundary>
                <Header data={nav} location={location} navHoverData={navHoverData} />
              </ErrorBoundary> */}

              <ErrorBoundary>
                <Header
                  NavData={value && value.navData}
                  location={location}
                  router={this.props.router}
                  navHoverData={navHoverData}
                />
              </ErrorBoundary>
            </div>
          </div>
          {isNotHome ? (
            <BreadCrumbs />
          ) : (
            ''
            // <ErrorBoundary>
            //   <div style={{ opacity: 0, height: '0px' }} id="tileyeDiv" />
            //   <div className="adsdivlyr" id="adsdivLyr" />
            // </ErrorBoundary>
          )}

          <ErrorBoundary>
            <div id="parentContainer">{childrenWithProps}</div>
          </ErrorBoundary>
          {/* <div className={isDocked && dockedVideo.isDocked ? 'showcls' : 'hidecls'}>
            <ErrorBoundary>
              <Videocommon
                slikeid={(isDocked && dockedVideo.videoData.eid) || ''}
                height="299"
                width="400"
                imageid={(isDocked && dockedVideo.videoData.imageid) || Config.SiteInfo.defaultimg}
                imgsize={(isDocked && dockedVideo.videoData.imgsize) || ''}
                vidtitle={(isDocked && dockedVideo.videoData.hl) || ''}
                adsection="default"
                isDocked
                dispatch={dispatch}
                isPlaying={isDocked && dockedVideo.isPlaying}
                seolocation={isDocked && dockedVideo.videoData.seolocation}
                pagetype="articleshow"
                articleID={isDocked && dockedVideo.articleID}
              />
            </ErrorBoundary>
          </div> */}
          <ErrorBoundary>
            <Cube location={location} cubeConfig={cubeConfig} status={false} />
          </ErrorBoundary>
          {electionWidgetCUBE &&
          electionWidgetCUBE.cubestatus === 'true' &&
          (showElectionCube || isNotHome) ? (
            <div id="cubeFrm" style={electionWidgetCUBE.styles}>
              <iframe
                width={electionWidgetCUBE.width}
                height={electionWidgetCUBE.height}
                src={`${electionWidgetCUBE.url_result}`}
                title="cube"
                marginWidth="0"
                marginHeight="0"
                hspace="0"
                vspace="0"
                frameBorder="0"
                scrolling="no"
              />
            </div>
          ) : (
            ''
          )}
          <ErrorBoundary>
            <Footer data={value && value.footer} />
          </ErrorBoundary>
          {isNotHome && <div className="NETWORK_PTG" />}
          <div className="GEO_TRACKERS" />
          <div className="GEO_TRACKERS_STATE" />
        </div>
        <div id="fb-root" />
      </div>
    );
  }
}

// eslint-disable-next-line func-names
App.fetchData = function({ dispatch, params, query, route }) {
  return dispatch(fetchDataIfNeeded(params, query, route));
};

App.propTypes = {
  value: PropTypes.object,
  children: PropTypes.node,
  dispatch: PropTypes.any,
  params: PropTypes.any,
  location: PropTypes.string,
  loggedIn: PropTypes.bool,
  dockedVideo: PropTypes.object,
  userData: PropTypes.object,
};

function mapStateToProps(state) {
  return {
    ...state.nav,
    Ads: state.config,
    ...state.authentication,
    video: state.video,
    dockedVideo: state.dockedVideo,
  };
}

export default connect(mapStateToProps)(App);
