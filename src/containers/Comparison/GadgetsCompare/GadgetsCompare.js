import React from 'react';
import { getKeyByValue } from '../../../common-utility';
import Link from '../../../components/Link/Link';
import Thumb from '../../../components/Thumb/Thumb';

const siteConfig = require(`../../../../common/${process.env.SITE}`);

const GadgetsCompare = ({ data }) => {
  return data
    ? data.map(item => {
        let toolTip = '';
        const seoPath =
          item && item._id && item._id.indexOf('-vs-') !== -1
            ? item._id
                .split('-vs-')
                .sort()
                .join('-vs-')
            : '';

        if (item && item.product_name) {
          toolTip = `${siteConfig.Locale.comparedotxt} ${item.product_name.join(' vs ')}`;
        }

        const deviceCategory =
          siteConfig && siteConfig.gadgetMapping && item.category
            ? getKeyByValue(siteConfig.gadgetMapping, item.category)
            : '';
        return (
        <li key={item._id}>
            <Link to={`/tech/compare-${deviceCategory}/${seoPath}`} title={toolTip}>
            <div className="slide">
                <span className="prod_img">
                <Thumb imgId={item.imageMsid[0]} width="200" resizeMode="4" title="no" />
              </span>
                <span className="title text_ellipsis">{item.product_name[0]}</span>
                <span className="vs">VS</span>
              </div>
            <div className="slide">
                <span className="prod_img">
                <Thumb imgId={item.imageMsid[1]} width="200" resizeMode="4" title="no" />
              </span>
                <span className="title text_ellipsis">{item.product_name[1]}</span>
              </div>
          </Link>
          </li>
        );
      })
    : '';
};

export default GadgetsCompare;
