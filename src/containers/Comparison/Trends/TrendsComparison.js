/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-restricted-syntax */
import React, { PureComponent } from 'react';
import LazyLoad from 'react-lazyload';
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import { browserHistory } from 'react-router';
import Helmet from 'react-helmet';
import Link from '../../../components/Link/Link';
// import Thumb from '../../../components/Thumb/Thumb';

import {
  fetchDataIfNeeded,
  updateGadgetData,
  updateNavData,
} from '../../../actions/comparision/Trends';
import { initGoogleAds } from '../../../ads/dfp';

import VideoWidget from '../../../components/Videos/VideoWidget';
import PhotoWidget from '../../../components/Photos/Photowidget';
import BreadCrumbs from '../../../components/BreadCrumb';
import { getKeyByValue } from '../../../common-utility';

import '../compare_gadgets.scss';
import '../../../public/css/commonComponent.scss';

const siteConfig = require(`../../../../common/${process.env.SITE}`);
const gadgetMapping = (siteConfig && siteConfig.gadgetMapping) || '';
const deviceText = (siteConfig && siteConfig.Locale && siteConfig.Locale.gadget) || {};

class TrendsComparison extends PureComponent {
  constructor(props) {
    super(props);
    //let pathname = props.pathname.split('/');
    //pathname = pathname.slice(-1).toString();
    //const urlValue = pathname || '';
  }

  componentDidMount() {
    const { dispatch, query, params, data, location } = this.props;
    let pathname = location.pathname.split('/');
    pathname = pathname.slice(-1).toString();

    //console.log('props', pathArr);

    const adsData = (data && data.listData && data.listData.ads && data.listData.ads.wapads) || '';
    TrendsComparison.fetchData({ dispatch, query, params, pathname });
    if (adsData) {
      initGoogleAds(adsData);
    }
    dispatch(
      updateNavData({
        sectionName: 'compare',
      }),
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const { dispatch, query, params, location } = this.props;
    //let pathname = location.pathname.split('/');
    //pathname = pathname.slice(-1);
    if (prevProps.location.pathname !== location.pathname) {
      const { data } = this.props;
      let pathname = location.pathname.split('/');
      pathname = pathname.slice(-1).toString();

      const adsData =
        (data && data.listData && data.listData.ads && data.listData.ads.wapads) || '';

      // console.log('componentDidMount', this.props);

      TrendsComparison.fetchData({ dispatch, query, params, pathname });

      if (adsData) {
        initGoogleAds(adsData);
      }
    }
  }

  render() {
    // console.log('Render', this.props);
    const { data, location } = this.props;
    let pathname = location.pathname.split('/');
    pathname = pathname.slice(-1).toString();
    const catName = pathname === 'popular-comparisons' ? deviceText.popular : deviceText.latest;
    const deviceDataCopy =
      data && data.listData && data.listData.compareData ? { ...data.listData.compareData } : '';
    const gadgetName =
      (deviceDataCopy && deviceDataCopy.compare && deviceDataCopy.compare[0].category) || '';

    const metaData = (data && data.metaData) || '';
    const seoSchema = '';

    const arrLatestTwoDevices = [];

    if (deviceDataCopy && deviceDataCopy.compare) {
      const arrLatest = [...deviceDataCopy.compare];

      arrLatest.forEach(item => {
        if (item && item.product_name && item.product_name.length > 1) {
          arrLatestTwoDevices.push(item);
        }
      });
    }

    return (
      <React.Fragment>
        {deviceDataCopy && deviceDataCopy.breadcrumb && deviceDataCopy.breadcrumb.div && (
          <BreadCrumbs data={deviceDataCopy.breadcrumb.div} />
        )}
        <Helmet
          title={(metaData && metaData.title) || ''}
          titleTemplate="%s"
          meta={metaData && metaData.metaTags}
          link={metaData && metaData.metaLinks}
          htmlAttributes={{ lang: siteConfig.SiteInfo.langLang }}
          script={[seoSchema]}
        />

        <div className="container-comparewidget">
          <div className="full_section wdt_popular_slider ui_slider">
            {data && data.listData && data.listData.compareData ? (
              <h2>
                <span>{`${catName} ${deviceText[gadgetName]} ${deviceText.compare}`}</span>
              </h2>
            ) : (
              ''
            )}

            <div className="items-in-list">
              <ul>
                <GetHtml data={arrLatestTwoDevices} />
              </ul>
            </div>
          </div>

          <LazyLoad height={100} once>
            <VideoWidget />
          </LazyLoad>
          <LazyLoad height={100} once>
            <PhotoWidget />
          </LazyLoad>

          <div className="AL_Innov1" />
        </div>
      </React.Fragment>
    );
  }
}

const GetHtml = ({ data }) => {
  return data && Array.isArray(data)
    ? data.map(item => {
        const seoPath =
          item && item._id && item._id.indexOf('-vs-') !== -1
            ? item._id
                .split('-vs-')
                .sort()
                .join('-vs-')
            : '';
        const deviceCategory =
          siteConfig && siteConfig.gadgetMapping && item.category
            ? getKeyByValue(siteConfig.gadgetMapping, item.category)
            : '';
        const urlCmpDetail = `/tech/compare-${deviceCategory}/${seoPath}`;
        return (
          <li key={item.product_name}>
            <span className="text_ellipsis">
              <Link to={urlCmpDetail}>
                {`${siteConfig.Locale.comparedotxt} ${item.product_name.join(' vs ')}`}
              </Link>
            </span>
          </li>
        );
      })
    : '';
};

TrendsComparison.fetchData = function fetchData({ dispatch, query, params, pathname }) {
  return dispatch(fetchDataIfNeeded({ query, params, pathname }));
};

TrendsComparison.propTypes = {
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ...state.trendsComparision,
  };
}

export default connect(mapStateToProps)(TrendsComparison);
