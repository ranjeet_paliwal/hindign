/* eslint-disable jsx-a11y/interactive-supports-focus */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-restricted-syntax */

// this.setState(prevState => ({ checked: !prevState.checked }));

import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import PropTypes from 'prop-types';
import Link from '../../../components/Link/Link';
import { initGoogleAds } from '../../../ads/dfp';
import { getBuyLink, initEvent } from '../../../common-utility';

import {
  fetchDataIfNeeded,
  addGadget,
  removeGadget,
  updateNavData,
} from '../../../actions/comparision/Detail';
import FakeLoader from '../../../components/Loaders/ArticleShowFakeLoader';
import BreadCrumbs from '../../../components/BreadCrumb';
import ErrorBoundary from '../../../components/lib/errorboundery/ErrorBoundary';
import Slider from '../../../components/NewSlider/NewSlider';
import WdtAffiliatesList from '../../../components/Amazon/WdtAffiliatesList';
import Thumb from '../../../components/Thumb/Thumb';
import GadgetsCompare from '../GadgetsCompare/GadgetsCompare';

import '../../../public/css/commonComponent.scss';
import '../compare_gadgets.scss';

const siteConfig = require(`../../../../common/${process.env.SITE}`);
// const gadgetMapping = (siteConfig && siteConfig.gadgetMapping) || '';
const deviceText = (siteConfig && siteConfig.Locale && siteConfig.Locale.gadget) || {};

class ComparisonDetails extends PureComponent {
  constructor(props) {
    super(props);
    // console.log('constructor', props);
    const { routeParams } = props;
    let searchQueryArr = '';
    if (routeParams && routeParams.qstring) {
      const qstring = routeParams.qstring;
      if (qstring && qstring.indexOf('-vs-') !== -1) {
        searchQueryArr = qstring.split('-vs-');
      }
    }

    this.state = {
      deviceInfo: {
        device1: {
          searchResult: '',
          userInput: '',
          userInputHtml: '',
        },
        device2: {
          searchResult: '',
          userInput: '',
          userInputHtml: '',
        },
      },
      searchQueryArr,
      differentSpecsObject: '',
      statusMsg: '',
    };
  }

  componentDidMount() {
    const { dispatch, query, params, data } = this.props;
    const adsData = (data && data.ads && data.ads.wapads) || '';
    if (typeof initEvent === 'function') {
      initEvent('scroll', this.initPageScroll);
    }

    const gadgetsInfo =
      (data &&
        data.compareData &&
        data.compareData.techgadget &&
        data.compareData.techgadget.gadget) ||
      '';

    const differentSpecsObject = {};

    const gadgetsInfoCopy2 = [...gadgetsInfo];
    const firstObject = gadgetsInfoCopy2[0];
    const gadgetsInfoCopy2rest = gadgetsInfoCopy2.slice(1);
    if (firstObject && firstObject.specs) {
      // Take the specifications in first gadget
      // ( consider as common in every subsequent object)
      gadgetsInfoCopy2rest.forEach(gadgetData => {
        Object.keys(firstObject.specs).forEach(specKey => {
          const firstObjectspecsData = firstObject.specs[specKey];
          Object.keys(firstObjectspecsData).forEach(firstObjectItem => {
            const specsData = gadgetData.specs;
            if (specsData[specKey]) {
              if (firstObjectItem === 'regional') {
                return false;
              }
              if (specsData[specKey][firstObjectItem]) {
                if (
                  firstObjectspecsData[firstObjectItem].value !==
                  specsData[specKey][firstObjectItem].value
                ) {
                  if (!differentSpecsObject[specKey]) {
                    differentSpecsObject[specKey] = { [firstObjectItem]: {} }; //
                  } else {
                    differentSpecsObject[specKey][firstObjectItem] = {}; //
                  }
                }
              } else if (!differentSpecsObject[specKey]) {
                differentSpecsObject[specKey] = { [firstObjectItem]: {} }; //
              } else {
                differentSpecsObject[specKey][firstObjectItem] = {}; //
              }
            } else {
              differentSpecsObject[specKey] = {}; // specs key NA
            }
          });
        });
      });
    }

    // console.log('differentSpecsObject', differentSpecsObject);

    this.setState({
      differentSpecsObject,
    });

    if (adsData) {
      initGoogleAds(adsData);
    }

    ComparisonDetails.fetchData({ dispatch, query, params });
    dispatch(
      updateNavData({
        sectionName: 'compare',
      }),
    );
  }

  componentDidUpdate(prevProps) {
    const { location, dispatch, query, params } = this.props;

    if (prevProps.location !== location) {
      ComparisonDetails.fetchData({ dispatch, query, params });
      window.scrollTo({ top: 0 });
    }
  }

  initPageScroll = () => {
    try {
      const fixedMenu = document.querySelector('#fixedMenu');
      const productView = document.querySelector('.wdt_compare-gadgets');
      if (fixedMenu) {
        const position = fixedMenu.getBoundingClientRect().y;
        if (position < -200) {
          productView.classList.add('fixed');
        } else {
          productView.classList.remove('fixed');
        }
      }
    } catch (ex) {
      // continue regardless of error
    }
  };

  onFocusHandler = () => {
    const { deviceInfo } = this.state;
    const deviceInfoCopy = { ...deviceInfo };
    for (const key in deviceInfoCopy) {
      if ({}.hasOwnProperty.call(deviceInfoCopy, key)) {
        const device = deviceInfoCopy[key];
        deviceInfoCopy[key] = {
          searchResult: '',
          userInput: '',
          userInputHtml: device.userInputHtml,
        };
      }
    }
    this.setState({ deviceInfo: deviceInfoCopy });
  };

  onKeyDownHandler = event => {
    const deviceName = event.currentTarget.name;
    if (event.keyCode !== 40 && event.keyCode !== 38 && event.keyCode !== 13) {
      return;
    }
    const liItem = document.querySelector(`#ulinput_${deviceName} .active`);
    const inputSelected = document.querySelector(`#ulinput_${deviceName}`);

    if (event.keyCode === 40) {
      // Arrow Down Key Pressed
      if (!liItem.nextSibling) {
        const firstLi = inputSelected.firstChild;
        liItem.classList.remove('active');
        firstLi.classList.add('active');
      } else {
        liItem.nextSibling.classList.add('active');
        liItem.classList.remove('active');
      }
    } else if (event.keyCode === 38) {
      // Arrow Up Key Pressed
      if (!liItem.previousSibling) {
        const lastLi = inputSelected.lastChild;
        liItem.classList.remove('active');
        lastLi.classList.add('active');
      } else {
        liItem.previousSibling.classList.add('active');
        liItem.classList.remove('active');
      }
    } else if (event.keyCode === 13) {
      // ENTER key is pressed, prevent the form from being submitted,
      event.preventDefault();
      liItem.click();
    }
  };

  onChangeEventHandler = (category, evt) => {
    const { state } = this;
    const userInput = evt.currentTarget.value;
    const deviceName = evt.target.name;

    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };
    updatedDeviceInfo[deviceName] = {
      userInput,
    };
    this.setState({ deviceInfo: updatedDeviceInfo });

    this.searchDevice(userInput, category, deviceName);
  };

  addDeviceHandler = (deviceClicked, deviceName, gadgetCategory) => {
    const { dispatch, data } = this.props;
    const { deviceInfo } = this.state;
    const deviceInfoCopy = { ...deviceInfo };
    const searchQueryArr = [];
    if (data.compareData && data.compareData.techgadget && data.compareData.techgadget.gadget) {
      const gadgets = data.compareData.techgadget.gadget;
      gadgets.forEach(item => {
        searchQueryArr.push(item.productname);
      });
    }

    const searchQueryArrCopy = [...searchQueryArr];

    if (searchQueryArrCopy.indexOf(deviceClicked) !== -1) {
      // if user selects same device again, alert a message
      deviceInfoCopy[deviceName] = {
        userInput: '',
        userInputHtml: '',
        searchResult: deviceInfoCopy[deviceName].searchResult,
      };

      return this.setState({
        deviceInfo: deviceInfoCopy,
        statusMsg: 'ये डिवाइस अपने पहले से ही जुड़ी हुई है.',
      });
    }

    searchQueryArrCopy.push(deviceClicked);

    const finalSearchQuery = searchQueryArrCopy.join(',');

    deviceInfoCopy[deviceName] = {
      userInput: '',
      userInputHtml: deviceClicked,
    };
    searchQueryArrCopy.sort();
    const seoPath = searchQueryArrCopy.join('-vs-');
    if (typeof window !== 'undefined') {
      window.history.pushState({}, '', `/tech/compare-mobile-phones/${seoPath}`);
    }

    this.setState({ deviceInfo: deviceInfoCopy, searchQueryArr: searchQueryArrCopy });
    // console.log('clicked', qstring, finalSearchQuery);
    dispatch(addGadget({ finalSearchQuery, gadgetCategory }));

    // const qstring = (routeParams && routeParams.qstring) || '';
    // if (qstring && qstring.indexOf('-vs-') !== -1) {
    //   const deviceClicked = (clickedData && clickedData.seoname) || '';
    //   const qstringArr = qstring.split('-vs-');
    //   qstringArr.push(deviceClicked);
    //   const finalSearchQuery = qstringArr.join(',');
    //   // console.log('clicked', qstring, finalSearchQuery);
    //   dispatch(addGadget({ finalSearchQuery, gadgetCategory }));
    // }
  };

  removeDeviceHandler = (deviceClicked, gadgetCategory) => {
    const { dispatch, data } = this.props;

    // const { searchQueryArr } = this.state;

    const searchQueryArr = [];
    if (data.compareData && data.compareData.techgadget && data.compareData.techgadget.gadget) {
      const gadgets = data.compareData.techgadget.gadget;
      gadgets.forEach(item => {
        searchQueryArr.push(item.productname);
      });
    }

    const searchQueryArrCopy = [...searchQueryArr];

    // const qstring = searchQuery;
    const index = searchQueryArrCopy.indexOf(deviceClicked);
    if (index > -1) {
      searchQueryArrCopy.splice(index, 1);
      const finalSearchQuery = searchQueryArrCopy.join(',');
      // this.setState(prevState => ({
      //   deviceInfo: prevState.deviceInfo,
      //   searchQueryArr: searchQueryArrCopy,
      // }));

      searchQueryArrCopy.sort();
      const seoPath = searchQueryArrCopy.join('-vs-');
      if (typeof window !== 'undefined') {
        window.history.pushState({}, '', `/tech/compare-mobile-phones/${seoPath}`);
      }

      dispatch(removeGadget({ finalSearchQuery, gadgetCategory })).then(() => {
        this.setState(prevState => ({
          deviceInfo: prevState.deviceInfo,
          searchQueryArr: searchQueryArrCopy,
        }));
      });
    }
  };

  labelHandler = event => {
    const element = document.getElementById(event.target.id);
    const classArr = element.classList;
    if (element.classList.contains('active')) {
      classArr.remove('active');
    } else {
      classArr.add('active');
    }
  };

  scrollToElement = event => {
    const elemId = event.target.value;
    //console.log('elemId', elemId);
    const elmnt = document.getElementById(elemId);
    if (elmnt) {
      elmnt.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
      });
    } else {
      //console.log('Scroll Element Not found');
    }
  };

  showDifferences = evt => {
    // alert('sss');
    // alert(evt.target.id);
    const checkBox = document.getElementById(evt.target.id);
    // Get the output text
    if (checkBox.checked === true) {
      this.toggleElement('same', 'none'); // Shows
    } else {
      this.toggleElement('same', ''); // Shows
    }
  };

  toggleElement = (className, displayState) => {
    const elements = document.getElementsByClassName(className);
    for (let i = 0; i < elements.length; i++) {
      elements[i].style.display = displayState;
    }
  };

  searchDevice(value, category, deviceName) {
    const { state } = this;
    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };

    fetch(
      `${
        process.env.API_ENDPOINT
      }/autosuggestion.cms?type=brand&tag=product_cat&category=${category}&q=${value}`,
    )
      .then(response => {
        // Examine the text in the response
        response.json().then(data => {
          const devices =
            data &&
            data.map(item => {
              return { Product_name: item.Product_name, seoname: item.seoname };
            });

          updatedDeviceInfo[deviceName] = {
            searchResult: devices.filter(item => item !== ''),
            userInput: value,
            userInputHtml: '',
          };

          this.setState(prevState => ({
            deviceInfo: updatedDeviceInfo,
            searchQueryArr: prevState.searchQueryArr,
            statusMsg: '',
          }));
        });
      })
      .catch(() => {
        // console.log('Fetch Error :-S', err);
      });
  }

  render() {
    const { data } = this.props;

    const { deviceInfo, searchQueryArr, differentSpecsObject, statusMsg } = this.state;

    const metaData = (data && data.metaData) || '';
    const seoSchema = '';

    const gadgetsInfo =
      (data &&
        data.compareData &&
        data.compareData.techgadget &&
        data.compareData.techgadget.gadget) ||
      '';
    const gadgetCategory = (gadgetsInfo && gadgetsInfo[0] && gadgetsInfo[0].category) || '';
    const gadgetSeo = siteConfig.gadgetCategories[gadgetCategory];
    const popularGadgets =
      (data &&
        data.compareData &&
        data.compareData.techgadget &&
        data.compareData.techgadget.popularGadgetPair &&
        data.compareData.techgadget.popularGadgetPair.compare) ||
      '';

    // console.log('state', this.state);

    // const criticRating111 = gadgetsInfo.map(item => (
    //   <td key={item.criticRating}>{item.criticRating}</td>
    // ));

    const pageTitle = [];

    const indexForLabel = {};

    // Code for geetting diff devices

    // Code for getting  Label Index, as diffrent devices may have diffrent attributes
    const gadgetsInfoCopy = [...gadgetsInfo];

    for (const key in gadgetsInfoCopy) {
      if ({}.hasOwnProperty.call(gadgetsInfoCopy, key)) {
        const item = gadgetsInfoCopy[key];

        const specsData = gadgetsInfoCopy[key] && gadgetsInfoCopy[key].specs;
        const getregionalname =
          item &&
          item.regional &&
          Array.isArray(item.regional) &&
          item.regional.filter(data => {
            return data && data.host === siteConfig.SiteInfo.hostid;
          });
        pageTitle.push(getregionalname && getregionalname[0] && getregionalname[0].name);
        for (const speckey in specsData) {
          if ({}.hasOwnProperty.call(specsData, speckey)) {
            if (speckey && specsData[speckey]) {
              const specsItem = specsData[speckey];

              if (!indexForLabel[speckey]) {
                indexForLabel[speckey] = {
                  size: 0,
                  indexwithMaxCount: 0,
                };
              }
              if (specsItem && Object.keys(specsItem).length > indexForLabel[speckey].size) {
                indexForLabel[speckey].size = Object.keys(specsItem).length;
                indexForLabel[speckey].indexwithMaxCount = key;
                indexForLabel[speckey].regional = specsItem.regional;
              }
            }
          }
        }
      }
    }

    const indexForLabelArr = Object.keys(indexForLabel).map(key => {
      return { key, val: indexForLabel[key] };
    });

    // console.log('diffInfo', differentSpecsObject);

    // console.log('indexForLabel', indexForLabel, indexForLabelArr);

    // console.log('designConfig', designConfig, deviceWidth);

    return gadgetsInfo ? (
      <React.Fragment>
        {data &&
          data.compareData &&
          data.compareData.techgadget &&
          data.compareData.techgadget.breadcrumb &&
          data.compareData.techgadget.breadcrumb.div && (
            <BreadCrumbs data={data.compareData.techgadget.breadcrumb.div} />
          )}
        <Helmet
          title={(metaData && metaData.title) || ''}
          titleTemplate="%s"
          meta={metaData && metaData.metaTags}
          link={metaData && metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          script={[seoSchema]}
        />

        <div className="container-comparewidget">
          <h1>{`${siteConfig.Locale.comparedotxt} ${pageTitle.join(' vs ')}`}</h1>
          <div className="wdt_compare-gadgets details wdt_amazon_list horizontal">
            <DeviceSearched
              data={gadgetsInfo}
              gadgetCategory={gadgetCategory}
              deviceInfo={deviceInfo}
              changed={this.onChangeEventHandler}
              keydown={this.onKeyDownHandler}
              focused={this.onFocusHandler}
              deviceAdded={this.addDeviceHandler}
              deviceRemoved={this.removeDeviceHandler}
              statusMsg={statusMsg}
            />
            <div className="compare_criteria">
              <CompareCriteria data={indexForLabelArr} scrollto={this.scrollToElement} />
              {/* <label>
                <input type="checkbox" onClick={this.showDifferences} id="showdiff" />
                केवल अंतर देखें
              </label> */}

              <div className="only_difference">
                <input type="checkbox" onClick={this.showDifferences} id="showdiff" />
                <label htmlFor="showdiff">केवल अंतर देखें</label>
              </div>

              {statusMsg && <span className="info-camparsion">{statusMsg}</span>}
            </div>
          </div>
          <div className="detailed-spec-show">
            <div className="gn_table_layout">
              {indexForLabelArr &&
                indexForLabelArr.map(item => {
                  // console.log(
                  //   'keyExsist',
                  //   item.key,
                  //   differentSpecsObject[item.key],
                  //   differentSpecsObject,
                  // );
                  let keyExsist = true;
                  // keyExsist should be only used for adding same/differ class
                  if (item.key && differentSpecsObject[item.key]) {
                    const diffKey = differentSpecsObject[item.key];
                    if (Object.keys(diffKey).length === 0 && diffKey.constructor === Object) {
                      keyExsist = false;
                    }
                  }

                  // const keyExsist = !(
                  //   (item.key && differentSpecsObject[item.key] == 'NA') ||
                  //   !differentSpecsObject[item.key]
                  // );
                  const className = !keyExsist ? 'differ' : '';
                  return (
                    <div key={item.key} className={className}>
                      <label className="spec active" id={item.key} onClick={this.labelHandler}>
                        {item.val.regional}
                      </label>
                      <GetLabelData
                        item={item.key}
                        data={item.val}
                        gadgetsInfo={gadgetsInfo}
                        diffObj={differentSpecsObject}
                        keyExsist={keyExsist}
                      />
                    </div>
                  );
                })}
            </div>

            {/* <div className="gn_table_layout">
              <label className="spec active" id="price" onClick={this.labelHandler}>
                कीमत
              </label>
              <table>
                <tr>
                  {gadgetsInfoCopy.map(gadgetData => {
                    return (
                      <td key={gadgetData.price}>
                        {gadgetData.price && gadgetData.price !== '0.0' ? (
                          <span className="symbol_rupees">{gadgetData.price}</span>
                        ) : (
                          'N.A.'
                        )}
                      </td>
                    );
                  })}
                </tr>
              </table>
            </div> */}
          </div>
          <ErrorBoundary>
            <WdtAffiliatesList
              title=""
              category={gadgetCategory}
              tag={
                (siteConfig &&
                  siteConfig.affiliateTags &&
                  siteConfig.affiliateTags.CD &&
                  `${siteConfig.affiliateTags.CD}`) ||
                ''
              }
              isslider="1"
              noimg="0"
              noh2="0"
            />
          </ErrorBoundary>
          <ErrorBoundary>
            {popularGadgets && popularGadgets.length > 0 && (
              <div className="full_section wdt_popular_slider ui_slider">
                <h2>
                  <span>
                    {`${deviceText.popular} ${deviceText[gadgetCategory]} ${deviceText.compare}`}
                  </span>
                </h2>
                <Link
                  className="more-btn"
                  to={`${process.env.WEBSITE_URL}/compare-${gadgetSeo}/popular-comparisons`}
                  target="_blank"
                >
                  {siteConfig.Locale.seemore}
                </Link>

                {/* <Slider
                  type="compareListPopular"
                  size="1"
                  sliderData={popularGadgets}
                  width="165"
                  height="185"
                  SliderClass="photoslider"
                  islinkable="true"
                  sliderWidth="100"
                  headingRequired="yes"
                /> */}

                <div className="slider">
                  <div className="slider_content">
                    <ul>
                      <GadgetsCompare data={popularGadgets.slice(0, 3)} />
                    </ul>
                  </div>
                </div>
              </div>
            )}
          </ErrorBoundary>

          <div className="AS_Innov1" />
        </div>
      </React.Fragment>
    ) : (
      <FakeLoader />
    );
  }
}

const CompareCriteria = ({ data, scrollto }) => {
  const options =
    data &&
    data.map(item => {
      return (
        <option key={item.key} value={item.key}>
          {item.val && item.val.regional}
        </option>
      );
    });
  return (
    <select onChange={scrollto}>
      <option value="">कंपरीजन क्राइटेरिया</option>
      {options}
    </select>
  );
};

const GetLabelData = ({ item, data, gadgetsInfo, diffObj, keyExsist }) => {
  // console.log('GetLabelData', gadgetsInfo);

  const categoryData = gadgetsInfo[data.indexwithMaxCount].specs[item];
  const categoryDataArr = [];
  for (const key in categoryData) {
    if (Object.prototype.hasOwnProperty.call(categoryData, key)) {
      const item1 = categoryData[key];
      // console.log('item1', item1, key);
      if (item1.key && item1.value) {
        categoryDataArr.push({
          name: key,
          regional: item1.key,
          value: item1.value,
        });
      }
    }
  }
  // console.log('GetLabelData', item, data, categoryDataArr);

  return (
    <table>
      {categoryDataArr &&
        categoryDataArr.map((item2, index) => {
          // console.log('item2', item2, item, diffObj);
          let getClass = 'same';

          const keyName = (item2 && item2.name) || '';
          if (!keyExsist) {
            getClass = 'differ';
          }
          if (diffObj && keyName && diffObj[item]) {
            if (diffObj[item][keyName]) {
              getClass = 'differ';
            }
          }

          return (
            <React.Fragment key={item2.key}>
              <tr className={getClass}>
                <th colSpan="4">
                  {item2.regional && item2.regional.includes('_')
                    ? item2.regional.replace(/_/gi, ' ')
                    : item2.regional}
                </th>
              </tr>

              <tr className={getClass}>
                <GetTdData
                  item={item}
                  name={item2.name}
                  itemData={item2}
                  gadgetsInfo={gadgetsInfo}
                />
              </tr>
            </React.Fragment>
          );
        })}
    </table>
  );
};

const GetTdData = ({ item, name, gadgetsInfo }) => {
  let gadgetsInfoCopy = [...gadgetsInfo];
  // To show empty tds
  const slotsToFill = gadgetsInfoCopy.length < 4 ? 4 - gadgetsInfoCopy.length : 0;
  const emptyCells = Array(slotsToFill).fill('EMPTY');
  // Concat original array with 'EMPTY'
  gadgetsInfoCopy = gadgetsInfoCopy.concat(emptyCells);
  let starWidth = 0;
  return gadgetsInfoCopy
    ? gadgetsInfoCopy.map(data => {
        if (data === 'EMPTY') {
          return <td />;
        }

        // delete data.specs[item].regional;
        if (data.specs[item]) {
          const feature = data.specs[item];
          // console.log('feature1111', feature, 'ss', name, feature[name]);
          if (feature && feature[name]) {
            if (name && name === 'quick_charging') {
              return (
                <td>
                  <span className={`charging-${feature[name].value.toLowerCase()}`}>
                    {feature[name].value}
                  </span>
                </td>
              );
            }
            if (name && (name === 'userrating' || name === 'criticRating')) {
              starWidth = (feature[name].value / 5) * 100;
              return (
                <td>
                  {feature[name].value !== 'NA' ? (
                    <React.Fragment>
                      <span className="stars-in-value">
                        <b>{feature[name].value}</b>
                        /5
                      </span>
                      <span className="stars-in-rating">
                        <span className="empty-stars" />
                        <span
                          className="filled-stars critic"
                          style={{
                            width: `${starWidth}%`,
                          }}
                        />
                      </span>
                    </React.Fragment>
                  ) : name === 'userrating' ? (
                    <Link
                      className="rt-submit"
                      to={`${process.env.WEBSITE_URL}/${
                        siteConfig.gadgetCategories[data.category]
                      }/${data.productname}#rate`}
                    >
                      {siteConfig.Locale.submitrating}
                    </Link>
                  ) : (
                    'NA'
                  )}
                </td>
              );
            }
            return <td>{feature[name].value}</td>;
          }
          return <td>NA</td>;

          // return feature && feature[name] ? (
          //   <td key={feature[name].value}>
          //     {name && name == 'quick_charging' && feature[name].value == 'yes' ? (
          //       <span className="charging-yes">{feature[name].value}</span>
          //     ) : (
          //       <span className="charging-no">{feature[name].value}</span>
          //     )}
          //     {feature[name].value}
          //   </td>
          // ) : (
          //   <td>NA</td>
          // );
        }

        return <td key={data}>NA</td>;
      })
    : '';
};

const DeviceSearched = ({
  data,
  gadgetCategory,
  deviceInfo,
  changed,
  keydown,
  focused,
  deviceAdded,
  deviceRemoved,
  statusMsg,
}) => {
  // const { device1, device2 } = deviceInfo;
  // const inputCount = data && Array.isArray(searchQueryArr) ? 4 - searchQueryArr.length : '';
  // const deviceCount = (data && Array.isArray(searchQueryArr) && searchQueryArr.length) || '';
  const inputCount = data && Array.isArray(data) ? 4 - data.length : '';
  const deviceCount = (data && Array.isArray(data) && data.length) || '';
  const inputCountArr = [];
  for (let i = 1; i <= inputCount; i++) {
    inputCountArr.push(i);
  }

  const deviceHTML =
    data && Array.isArray(data)
      ? data.map((item, index) => {
          const getregionalDevice =
            item &&
            item.regional &&
            Array.isArray(item.regional) &&
            item.regional.filter(data => {
              return data.host === siteConfig.SiteInfo.hostid;
            });
          const amzga = `${item.productname}_${item.price}`;
          const tag = (siteConfig && siteConfig.affiliateTags && siteConfig.affiliateTags.SM) || '';
          const price = item.price;
          const title = item.productname;
          const linkToItem = `${siteConfig.seoConfig.techPrefix}/${
            siteConfig.gadgetCategories[item.category]
          }/${item.productname}`;
          const finalBuyUrl = getBuyLink({
            affiliateData: item.affiliate,
            price,
            title,
            amzga,
            tag,
          });

          const imagePath =
            item.imageMsid && Array.isArray(item.imageMsid) ? item.imageMsid[0] : item.imageMsid;

          return (
            <li key={item.productname}>
              {deviceCount && deviceCount > 2 && (
                <b
                  onClick={deviceRemoved.bind(this, item.productname, gadgetCategory)}
                  role="button"
                  className="close_icon"
                />
              )}
              <Link to={linkToItem}>
                <span className="img_wrap">
                  <Thumb
                    width="300"
                    imgId={imagePath}
                    alt={item.productname}
                    title={item.productname}
                    resizeMode="4"
                  />
                </span>
                <span className="con_wrap">
                  <h4 className="text_ellipsis">
                    {getregionalDevice && getregionalDevice[0] && getregionalDevice[0].name}
                  </h4>
                  {item.price && <span className="price_tag">&#8377; {item.price}</span>}
                </span>
              </Link>

              {finalBuyUrl && (
                <span className="btn_wrap">
                  <Link to={finalBuyUrl} target="_blank" className="buy_btn">
                    {siteConfig.Locale.tech.buyFrom}
                  </Link>
                </span>
              )}
            </li>
          );
        })
      : '';

  // for (const key in deviceInfo) {
  //   const deviceItem = deviceInfo[key];
  // }

  return (
    <React.Fragment>
      <ul>
        {deviceHTML}

        {inputCountArr.length > 0 &&
          inputCountArr.map(itemc => {
            return (
              <ManageDevice
                key={itemc}
                deviceName={`device${itemc}`}
                gadgetCategory={gadgetCategory}
                deviceInfo={deviceInfo}
                changed={changed}
                keydown={keydown}
                focused={focused}
                deviceAdded={deviceAdded}
                statusMsg={statusMsg}
              />
            );
          })}
      </ul>
    </React.Fragment>
  );
};

const ManageDevice = ({
  deviceName,
  deviceInfo,
  gadgetCategory,
  changed,
  keydown,
  focused,
  deviceAdded,
  statusMsg,
}) => {
  const device = deviceInfo[deviceName];
  return (
    <li>
      <div className="input_field">
        <span className="search_icon" />
        <input
          type="text"
          name={deviceName}
          value={(device && device.userInput) || ''}
          onChange={changed.bind(this, gadgetCategory)}
          onKeyDown={keydown}
          onFocus={focused}
          placeholder={siteConfig.Locale.tech.addDevice}
          autoComplete="off"
        />

        {/* <InputSearched
          device={device}
          name={deviceName}
          changed={changed}
          blured={blured}
          onKeyDown={keydown}
          category={gadgetCategory}
        /> */}
        {device &&
          !statusMsg &&
          !device.userInputHtml &&
          device.searchResult &&
          device.searchResult.length > 0 && (
            <div className="auto-suggest">
              <ul id={`ulinput_${deviceName}`}>
                {device.searchResult.map((item, index) => (
                  <li
                    key={item.Product_name}
                    data-item={item.Product_name}
                    onClick={deviceAdded.bind(this, item.seoname, deviceName, gadgetCategory)}
                    className={index === 0 ? 'active' : ''}
                  >
                    <span>{item.Product_name}</span>
                  </li>
                ))}
              </ul>
            </div>
          )}
      </div>
    </li>
  );
};

// const InputSearched = ({ device, changed, blured, category, name }) => {
//   return (
//     <input
//       type="text"
//       name={name}
//       value={(device && device.userInput) || ''}
//       onChange={changed.bind(this, category)}
//       //  onBlur={blured.bind(this, name)}
//       placeholder={siteConfig.Locale.tech.addDevice}
//       autoComplete="off"
//     />
//   );
// };

ComparisonDetails.fetchData = function fetchData({ dispatch, query, params }) {
  return dispatch(fetchDataIfNeeded({ query, params }));
};

ComparisonDetails.propTypes = {
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ...state.compareDetail,
  };
}

export default connect(mapStateToProps)(ComparisonDetails);
