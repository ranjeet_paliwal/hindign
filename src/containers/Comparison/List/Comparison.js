/* eslint-disable no-underscore-dangle */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-restricted-syntax */
import React, { PureComponent } from 'react';
import LazyLoad from 'react-lazyload';
import { connect } from 'react-redux';
import PropTypes, { array } from 'prop-types';
import { browserHistory } from 'react-router';
import Helmet from 'react-helmet';
import Link from '../../../components/Link/Link';
// import Thumb from '../../../components/Thumb/Thumb';

import {
  fetchDataIfNeeded,
  updateGadgetData,
  updateNavData,
} from '../../../actions/comparision/List';
import { initGoogleAds } from '../../../ads/dfp';
import Slider from '../../../components/NewSlider/NewSlider';
import GadgetsCompare from '../GadgetsCompare/GadgetsCompare';

import VideoWidget from '../../../components/Videos/VideoWidget';
import PhotoWidget from '../../../components/Photos/Photowidget';
import BreadCrumbs from '../../../components/BreadCrumb';
import { getKeyByValue } from '../../../common-utility';

// import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';

// import Thumb from '../../components/Thumb/Thumb';

import '../compare_gadgets.scss';
import '../../../public/css/commonComponent.scss';

const siteConfig = require(`../../../../common/${process.env.SITE}`);
const gadgetMapping = (siteConfig && siteConfig.gadgetMapping) || '';
const deviceText = (siteConfig && siteConfig.Locale && siteConfig.Locale.gadget) || {};

class Comparison extends PureComponent {
  constructor(props) {
    super(props);
    // const activeDevice = (props.routeParams && props.routeParams.device) || '';
    const activeDevice =
      props.routeParams && props.routeParams.device
        ? props.routeParams.device
        : props.location.query.category;
    const deviceToSearch = activeDevice
      ? siteConfig.gadgetMapping && siteConfig.gadgetMapping[activeDevice]
      : 'mobile';
    this.state = {
      deviceInfo: {
        device1: {
          isVisible: true,
          searchResult: '',
          userInput: '',
          userInputHtml: '',
          seoName: '',
        },
        device2: {
          isVisible: true,
          searchResult: '',
          userInput: '',
          userInputHtml: '',
          seoName: '',
        },
        device3: {
          isVisible: true,
          searchResult: '',
          userInput: '',
          userInputHtml: '',
          seoName: '',
        },
        device4: {
          isVisible: true,
          searchResult: '',
          userInput: '',
          userInputHtml: '',
          seoName: '',
        },
      },
      deviceData: '',
      suggestedDevices: '',
      IsdeviceAdded: false,
      activeGadget: deviceToSearch,
      searchCategory: '',
      searchSeoName: '',
      // itemAddedToCompare: 0,
    };
  }

  componentDidMount() {
    const { dispatch, query, params, data, location } = this.props;
    const { deviceInfo } = this.state;
    const deviceInfoUpdated = { ...deviceInfo };

    console.log('props', location.query.category);

    if (location && location.query && location.query.device) {
      const deviceLanded = location.query.device;
      const Name = location.query.name;
      deviceInfoUpdated.device1.userInputHtml = deviceLanded;
      deviceInfoUpdated.device1.seoName = Name;
      this.setState({
        deviceInfo: deviceInfoUpdated,
      });
    }

    const adsData = (data && data.listData && data.listData.ads && data.listData.ads.wapads) || '';
    Comparison.fetchData({ dispatch, query, params });
    if (adsData) {
      initGoogleAds(adsData);
    }
    dispatch(
      updateNavData({
        sectionName: 'compare',
      }),
    );

    // document.addEventListener('click', event => {
    //   if (!isDeviceClicked) {
    //     const divsToHide = document.getElementsByClassName('auto-suggest');
    //     if (divsToHide) {
    //       for (let i = 0; i < divsToHide.length; i++) {
    //         divsToHide[i].style.display = 'none';
    //       }
    //     }
    //   }
    // });
  }

  componentDidUpdate(prevProps, prevState) {
    const { location, routeParams } = this.props;
    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };
    console.log('state', this.state);
    if (prevProps.location.pathname !== location.pathname) {
      const { data } = this.props;

      const adsData =
        (data && data.listData && data.listData.ads && data.listData.ads.wapads) || '';

      // console.log('componentDidMount', this.props);
      const { dispatch, query, params } = this.props;

      Comparison.fetchData({ dispatch, query, params });

      if (adsData) {
        initGoogleAds(adsData);
      }

      for (const key in updatedDeviceInfo) {
        if (Object.prototype.hasOwnProperty.call(updatedDeviceInfo, key)) {
          updatedDeviceInfo[key].searchResult = '';
          updatedDeviceInfo[key].userInput = '';
          updatedDeviceInfo[key].userInputHtml = '';
        }
      }

      if (routeParams && routeParams.device && gadgetMapping) {
        this.setState({
          deviceInfo: updatedDeviceInfo,
          activeGadget: gadgetMapping[routeParams.device],
        });
      }
    }
  }

  onFocusHandler = () => {
    const { deviceInfo } = this.state;
    const deviceInfoCopy = { ...deviceInfo };
    for (const key in deviceInfoCopy) {
      if ({}.hasOwnProperty.call(deviceInfoCopy, key)) {
        const device = deviceInfoCopy[key];
        deviceInfoCopy[key] = {
          userInput: '',
          userInputHtml: device.userInputHtml,
          seoName: device.seoName,
          searchResult: '',
        };
      }
    }
    this.setState({ deviceInfo: deviceInfoCopy });
  };

  keyDownHandler = event => {
    const deviceName = event.currentTarget.name;
    if (event.keyCode !== 40 && event.keyCode !== 38 && event.keyCode !== 13) {
      return;
    }
    const liItem = document.querySelector(`#ulinput_${deviceName} .active`);
    const inputSelected = document.querySelector(`#ulinput_${deviceName}`);

    if (event.keyCode === 40) {
      // Arrow Down Key Pressed
      if (!liItem.nextSibling) {
        const firstLi = inputSelected.firstChild;
        liItem.classList.remove('active');
        firstLi.classList.add('active');
      } else {
        liItem.nextSibling.classList.add('active');
        liItem.classList.remove('active');
      }
    } else if (event.keyCode === 38) {
      // Arrow Up Key Pressed
      if (!liItem.previousSibling) {
        const lastLi = inputSelected.lastChild;
        liItem.classList.remove('active');
        lastLi.classList.add('active');
      } else {
        liItem.previousSibling.classList.add('active');
        liItem.classList.remove('active');
      }
    } else if (event.keyCode === 13) {
      // ENTER key is pressed, prevent the form from being submitted,
      event.preventDefault();
      liItem.click();
    }
  };

  removeDevice = (deviceInfo, deviceName) => {
    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };
    updatedDeviceInfo[deviceName] = {
      userInput: '',
      userInputHtml: '',
      seoName: '',
      searchResult: '',
    };

    this.setState({
      deviceInfo: updatedDeviceInfo,
    });
  };

  clickEventHandler = (deviceInfo, deviceName) => {
    // const {
    //   routeParams: { device },
    // } = this.props;
    const activeGadget = this.state.activeGadget;
    // const activeGadget = (siteConfig.gadgetMapping && siteConfig.gadgetMapping[device]) || '';

    this.updateSuggestedData(activeGadget, deviceInfo, deviceName);
  };

  handleChange = evt => {
    const { state } = this;
    const { activeGadget } = state;
    const userInput = evt.currentTarget.value;
    const device = evt.currentTarget.name;
    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };

    updatedDeviceInfo[device].userInput = userInput;
    this.setState({
      deviceInfo: updatedDeviceInfo,
      IsdeviceAdded: false,
    });
    // console.log('handleChange', this.state);
    this.searchDevice(userInput, device, activeGadget);
  };

  addFromSuggestions = deviceInfo => {
    const { activeGadget, state } = this;
    let device;

    for (const key in state.deviceInfo) {
      if (!state.deviceInfo[key].userInputHtml) {
        device = key;
        break;
      }
    }

    this.updateSuggestedData(state.activeGadget, deviceInfo, device);
  };

  submitEventHandler = event => {
    event.preventDefault();
    const { deviceInfo, activeGadget } = this.state;

    const seoItem = [];
    if (deviceInfo) {
      for (const key in deviceInfo) {
        if (Object.prototype.hasOwnProperty.call(deviceInfo, key)) {
          if (deviceInfo[key].seoName) {
            seoItem.push(deviceInfo[key].seoName);
            // prepareSeoURL += deviceInfo[key].seoName;
          }
        }
      }

      if (seoItem.length >= 2) {
        seoItem.sort();
        const seoName = this.getKeyByValue(activeGadget);
        const seoPath = `/tech/compare-${seoName}/${seoItem.join('-vs-')}`;
        browserHistory.push(seoPath && seoPath.toLowerCase());
      }
    }

    // console.log('submitEventHandler', this.state);
  };

  switchDeviceHandler = deviceType => {
    // alert(deviceType);
    const { deviceData } = this.state;
    const { dispatch } = this.props;

    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };

    const seoName = this.getKeyByValue(deviceType);
    if (typeof window !== 'undefined') {
      window.history.pushState({}, '', `/tech/compare-${seoName}`);
    }

    for (const key in updatedDeviceInfo) {
      if (Object.prototype.hasOwnProperty.call(updatedDeviceInfo, key)) {
        updatedDeviceInfo[key].searchResult = '';
        updatedDeviceInfo[key].userInput = '';
        updatedDeviceInfo[key].userInputHtml = '';
      }
    }

    this.setState({
      deviceInfo: updatedDeviceInfo,
      activeGadget: deviceType,
      deviceData,
      suggestedDevices: '',
    });

    dispatch(updateGadgetData(deviceType));
  };

  getKeyByValue = value => {
    const gadgets = siteConfig.gadgetMapping;
    for (const key in gadgets) {
      if (Object.prototype.hasOwnProperty.call(gadgets, key)) {
        if (gadgets[key] === value) return key;
      }
    }
    return '';
  };

  updateSuggestedData = (activeGadget, deviceInfo, device) => {
    // const { suggestedDevices, deviceInfo } = this.state;
    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };

    for (const key in updatedDeviceInfo) {
      if (updatedDeviceInfo[key].userInputHtml === deviceInfo.Product_name) {
        updatedDeviceInfo[device].userInput = '';
        return this.setState({
          deviceInfo: updatedDeviceInfo,
          IsdeviceAdded: true,
        });
      }
    }

    updatedDeviceInfo[device] = {
      userInput: updatedDeviceInfo[device].userInput,
      isVisible: updatedDeviceInfo[device].isVisible,
      userInputHtml: deviceInfo.Product_name,
      seoName: deviceInfo.seoname,
      searchResult: updatedDeviceInfo[device].searchResult,
    };

    if (
      !this.state.suggestedDevices ||
      this.state.searchCategory !== activeGadget ||
      this.state.searchSeoName !== deviceInfo.seoname
    ) {
      const apiSuggested = `${
        process.env.API_ENDPOINT
      }/wdt_gadgetreleted.cms?feedtype=sjson&type=brand&category=${activeGadget}&productid=${
        deviceInfo.seoname
      }`;
      fetch(apiSuggested)
        .then(promise => promise.json())
        .then(response => {
          this.setState({
            deviceInfo: updatedDeviceInfo,
            suggestedDevices: response,
            IsdeviceAdded: false,
            searchCategory: activeGadget,
            searchSeoName: deviceInfo.seoname,
          });
        });
    } else {
      this.setState({
        deviceInfo: updatedDeviceInfo,
        IsdeviceAdded: false,
      });
    }
  };

  searchDevice(value, device, gadget) {
    const { state } = this;
    const { deviceData } = state;
    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };

    fetch(
      `${
        process.env.API_ENDPOINT
      }/autosuggestion.cms?type=brand&tag=product_cat&category=${gadget}&q=${value}`,
    )
      .then(response => {
        // Examine the text in the response
        response.json().then(data => {
          // console.log(data);
          const devices =
            data &&
            data.map(item => {
              return { Product_name: item.Product_name, seoname: item.seoname };
            });

          updatedDeviceInfo[device] = {
            userInput: updatedDeviceInfo[device].userInput,
            isVisible: updatedDeviceInfo[device].isVisible,
            userInputHtml: updatedDeviceInfo[device].userInputHtml,
            searchResult: devices.filter(item => item !== ''),
          };
          this.setState({
            deviceInfo: updatedDeviceInfo,
            deviceData,
          });
          // return deviceData.filter(item => item != '');
        });
      })
      .catch(() => {
        // console.log('Fetch Error :-S', err);
      });
  }

  render() {
    // console.log('Render', this.props);
    const { data } = this.props;

    const { deviceInfo, activeGadget, IsdeviceAdded, suggestedDevices } = this.state;
    const { device1, device2, device3, device4 } = deviceInfo;
    let count = 0;
    const deviceInfoCopy = { ...deviceInfo };

    const gadgetSeo = siteConfig.gadgetCategories[activeGadget];
    const prepareDeviceURL = `${process.env.WEBSITE_URL}/compare-${gadgetSeo}`;

    if (deviceInfo) {
      for (const key in deviceInfo) {
        if (Object.prototype.hasOwnProperty.call(deviceInfo, key)) {
          if (deviceInfo[key].userInputHtml) {
            count++;
          }
          if (count === 2) {
            break;
          }
        }
      }
    }

    const dataForPopularGadget = '';
    const restdataForPopularGadget = '';
    let htmlPopularDevices = '';

    const dataForLatestGadget = '';
    const restdataForLatestGadget = '';
    let htmlLatestDevices = '';

    const deviceDataCopy =
      data && data.listData && data.listData.compareData ? { ...data.listData.compareData } : '';

    // const finalizeDeviceData = (Array.isArray(deviceDataCopy) && deviceDataCopy) || '';
    const metaData = (data && data.metaData) || '';
    const seoSchema = '';
    // let deviceCountPopular = 0;
    // let deviceCountLatest = 0;
    // let sliderWidthLatest = 0;
    // let sliderWidthPopular = 0;

    const arrPopularTwoDevices = [];
    const arrPopularOtherThanTwo = [];
    const arrLatestTwoDevices = [];
    const arrLatestOtherThanTwo = [];

    if (
      deviceDataCopy &&
      deviceDataCopy.popularGadgetPair &&
      deviceDataCopy.popularGadgetPair.compare
    ) {
      const arrPopular = [...deviceDataCopy.popularGadgetPair.compare];
      console.log('arrPopular', arrPopular);

      arrPopular.forEach(item => {
        // console.log(item);
        if (
          item &&
          item.product_name &&
          item.product_name.length === 2 &&
          arrPopularTwoDevices.length <= 2
        ) {
          arrPopularTwoDevices.push(item);
        } else {
          arrPopularOtherThanTwo.push(item);
        }
      });

      if (arrPopularOtherThanTwo.length > 0) {
        htmlPopularDevices = <GetHtml data={arrPopularOtherThanTwo} />;
      }
    }

    if (deviceDataCopy && deviceDataCopy.recentcomp && deviceDataCopy.recentcomp.compare) {
      const arrLatest = [...deviceDataCopy.recentcomp.compare];

      arrLatest.forEach(item => {
        // console.log(item);
        if (
          item &&
          item.product_name &&
          item.product_name.length === 2 &&
          arrLatestTwoDevices.length <= 2
        ) {
          arrLatestTwoDevices.push(item);
        } else {
          arrLatestOtherThanTwo.push(item);
        }
      });

      if (arrLatestOtherThanTwo.length > 0) {
        htmlLatestDevices = <GetHtml data={arrLatestOtherThanTwo} />;
      }
    }

    const activeGadgetName = activeGadget === 'mobile' ? 'mobilePhones' : activeGadget;

    return (
      <React.Fragment>
        {deviceDataCopy && deviceDataCopy.breadcrumb && deviceDataCopy.breadcrumb.div && (
          <BreadCrumbs data={deviceDataCopy.breadcrumb.div} />
        )}
        <Helmet
          title={(metaData && metaData.title) || ''}
          titleTemplate="%s"
          meta={metaData && metaData.metaTags}
          link={metaData && metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          script={[seoSchema]}
        />

        <div className="container-comparewidget">
          <div className="wdt_compare-gadgets">
            <DeviceWidget clicked={this.switchDeviceHandler} activeGadget={activeGadget} />
            <h3>{`${deviceText[activeGadgetName]} ${deviceText.doCompare}`}</h3>
            <div className="input_gadgets">
              <form onSubmit={this.submitEventHandler}>
                <div className="row-fields">
                  {Object.keys(deviceInfoCopy).map(deviceName => {
                    return (
                      <ManageDevice
                        key={deviceName}
                        device={deviceInfoCopy[deviceName]}
                        deviceName={deviceName}
                        changed={this.handleChange}
                        focused={this.onFocusHandler}
                        keydown={this.keyDownHandler}
                        clicked={this.removeDevice}
                        self={this}
                        IsdeviceAdded={IsdeviceAdded}
                      />
                    );
                  })}
                  {/* <ManageDevice
                    device={device1}
                    deviceName="device1"
                    changed={this.handleChange}
                    blured={this.handleBlur}
                    keydown={this.keyDownHandler}
                    clicked={this.removeDevice}
                    self={this}
                    IsdeviceAdded={IsdeviceAdded}
                  />
                  <ManageDevice
                    device={device2}
                    deviceName="device2"
                    changed={this.handleChange}
                    blured={this.handleBlur}
                    keydown={this.keyDownHandler}
                    clicked={this.removeDevice}
                    self={this}
                    IsdeviceAdded={IsdeviceAdded}
                  />
                  <ManageDevice
                    device={device3}
                    deviceName="device3"
                    changed={this.handleChange}
                    blured={this.handleBlur}
                    keydown={this.keyDownHandler}
                    clicked={this.removeDevice}
                    self={this}
                    IsdeviceAdded={IsdeviceAdded}
                  />
                  <ManageDevice
                    device={device4}
                    deviceName="device4"
                    changed={this.handleChange}
                    blured={this.handleBlur}
                    keydown={this.keyDownHandler}
                    clicked={this.removeDevice}
                    self={this}
                    IsdeviceAdded={IsdeviceAdded}
                  /> */}
                </div>
                <input type="submit" name="compare" className="btn-blue" value="कंपेयर करें" />
                {count < 2 ? (
                  <span className="info-camparsion">
                    {siteConfig.Locale.minimumComparisonError}
                  </span>
                ) : (
                  ''
                )}
                {IsdeviceAdded ? (
                  <span className="info-camparsion">
                    {siteConfig.Locale.deviceAlreadySelectedError}
                  </span>
                ) : (
                  ''
                )}
              </form>
            </div>
          </div>
          {suggestedDevices && suggestedDevices.gadget && (
            <div className="full_section wdt_suggested_slider ui_slider">
              <h2>
                <span>
                  {`${deviceText.suggested} ${deviceText[activeGadget]}
              ${deviceText.compare}`}
                </span>
              </h2>
              <Slider
                type="suggested"
                size="5"
                sliderData={suggestedDevices.gadget}
                width="165"
                height="230"
                SliderClass="photoslider"
                islinkable="true"
                sliderWidth="920"
                headingRequired="yes"
                clickedEvent={this.addFromSuggestions}
              />
            </div>
          )}

          <div className="full_section wdt_popular_slider ui_slider">
            <h2>
              <span>
                {`${deviceText.popular} ${deviceText[activeGadget]} ${deviceText.compare}`}
              </span>
            </h2>
            <span className="more-btn">
              <Link to={`${prepareDeviceURL}/popular-comparisons`}>
                {siteConfig.Locale.seemore}
              </Link>
            </span>

            <div className="slider">
              <div className="slider_content">
                <ul>
                  <GadgetsCompare data={arrPopularTwoDevices} />
                </ul>
              </div>
            </div>

            <div className="items-in-list">
              <ul>{htmlPopularDevices}</ul>
            </div>
          </div>

          <div className="full_section wdt_lstcomparsion_slider ui_slider">
            <h2>
              <span>
                {`${deviceText.latest} ${deviceText[activeGadget]}
            ${deviceText.compare}`}
              </span>
            </h2>
            <span className="more-btn">
              <Link to={`${prepareDeviceURL}/latest-comparisons`}>{siteConfig.Locale.seemore}</Link>
            </span>

            <div className="slider">
              <div className="slider_content">
                <ul>
                  <GadgetsCompare data={arrLatestTwoDevices} />
                </ul>
              </div>
            </div>

            <div className="items-in-list">
              <ul>{htmlLatestDevices}</ul>
            </div>
          </div>

          <LazyLoad height={100} once>
            <VideoWidget />
          </LazyLoad>
          <LazyLoad height={100} once>
            <PhotoWidget />
          </LazyLoad>

          <div className="AL_Innov1" />
        </div>
      </React.Fragment>
    );
  }
}

const ManageDevice = ({ device, deviceName, changed, focused, keydown, self, IsdeviceAdded }) => {
  return device && !device.userInputHtml ? (
    <div className="input_field">
      <input
        type="text"
        placeholder={siteConfig.Locale.tech.addDevice}
        name={deviceName}
        className={deviceName}
        value={device.userInput}
        onChange={changed}
        onKeyDown={keydown}
        onFocus={focused}
        id={`input_${deviceName}`}
        autoComplete="off"
      />
      {!IsdeviceAdded &&
        device.searchResult &&
        Array.isArray(device.searchResult) &&
        device.searchResult.length > 0 && (
          <div className="auto-suggest">
            <ul id={`ulinput_${deviceName}`}>
              {device.searchResult.map((item, index) => (
                <li
                  key={item.Product_name}
                  data-item={item.Product_name}
                  onClick={self.clickEventHandler.bind(this, item, deviceName)}
                  className={index == 0 ? 'active' : ''}
                >
                  <span>{item.Product_name}</span>
                </li>
              ))}
            </ul>
          </div>
        )}
    </div>
  ) : (
    <div className="input_field">
      <input type="text" value={device.userInputHtml} />
      <span className="close_icon" onClick={self.removeDevice.bind(this, device, deviceName)} />
    </div>
  );
};

const DeviceWidget = ({ clicked, activeGadget }) => {
  const Gadgets = siteConfig.gadgetsList;
  const deviceHtml =
    Gadgets &&
    Gadgets.map(item => {
      return (
        <li
          key={item.name}
          className={`gdt-${item.name}${item.name == activeGadget ? ' active' : ''}`}
          onClick={clicked.bind(this, item.name)}
        >
          <b />
          <label> {item.regName}</label>
        </li>
      );
    });

  return (
    <div className="select-inline-gadgets">
      <ul className="scroll">{deviceHtml}</ul>
    </div>
  );
};

const GetHtml = ({ data }) => {
  return data && Array.isArray(data)
    ? data.map(item => {
        const seoPath =
          item && item._id && item._id.indexOf('-vs-') !== -1
            ? item._id
                .split('-vs-')
                .sort()
                .join('-vs-')
            : '';
        const deviceCategory =
          siteConfig && siteConfig.gadgetMapping && item.category
            ? getKeyByValue(siteConfig.gadgetMapping, item.category)
            : '';
        const urlCmpDetail = `/tech/compare-${deviceCategory}/${seoPath}`;
        return (
          <li key={item.product_name}>
            <span className="text_ellipsis">
              <Link to={urlCmpDetail}>
                {`${siteConfig.Locale.comparedotxt} ${item.product_name.join(' vs ')}`}
              </Link>
            </span>
          </li>
        );
      })
    : '';
};

Comparison.fetchData = function fetchData({ dispatch, query, params }) {
  return dispatch(fetchDataIfNeeded({ query, params }));
};

Comparison.propTypes = {
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ...state.compareList,
  };
}

export default connect(mapStateToProps)(Comparison);
