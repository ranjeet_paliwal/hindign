/* eslint-disable react/prop-types */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable no-restricted-syntax */
import React, { PureComponent } from 'react';
import LazyLoad from 'react-lazyload';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { browserHistory } from 'react-router';
import Helmet from 'react-helmet';

import { fetchDataIfNeeded, updateGadgetData } from '../../../actions/comparision/List';
import { initGoogleAds } from '../../../ads/dfp';
import Slider from '../../../components/NewSlider/NewSlider';
import VideoWidget from '../../../components/Videos/VideoWidget';
import PhotoWidget from '../../../components/Photos/Photowidget';
import BreadCrumbs from '../../../components/BreadCrumb';

// import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';

// import Thumb from '../../components/Thumb/Thumb';

import '../compare_gadgets.scss';
import '../../../public/css/commonComponent.scss';

const siteConfig = require(`../../../../common/${process.env.SITE}`);

const deviceText = {
  mobile: 'मोबाइल',
  mobilePhones: 'मोबाइल फोन्स',
  laptop: 'लैपटॉप्स',
  tablet: 'टैबलेट्स',
  camera: 'कैमरों',
  smartwatch: 'स्मार्ट वॉच',
  fitnessband: 'फिटनेस बैंड',
  compare: 'की तुलना',
  doCompare: 'की तुलना करें',
  suggested: 'सुझाए गए',
  latest: 'लेटेस्ट',
  popular: 'पॉप्युलर',
  photo: 'फोटो',
  video: 'विडियो',
};

class Comparison extends PureComponent {
  constructor(props) {
    super(props);
    console.log('propss', props);
    const activeDevice = (props.routeParams && props.routeParams.device) || '';
    const deviceToSearch = activeDevice
      ? siteConfig.gadgetMapping && siteConfig.gadgetMapping[activeDevice]
      : 'mobile';
    this.state = {
      deviceInfo: {
        device1: {
          isVisible: true,
          searchResult: '',
          userInput: '',
          userInputHtml: '',
          seoName: '',
        },
        device2: {
          isVisible: true,
          searchResult: '',
          userInput: '',
          userInputHtml: '',
          seoName: '',
        },
        device3: {
          isVisible: true,
          searchResult: '',
          userInput: '',
          userInputHtml: '',
          seoName: '',
        },
        device4: {
          isVisible: true,
          searchResult: '',
          userInput: '',
          userInputHtml: '',
          seoName: '',
        },
      },
      deviceData: '',
      suggestedDevices: '',
      IsdeviceAdded: false,
      activeGadget: deviceToSearch,
      // itemAddedToCompare: 0,
    };
  }

  componentDidMount() {
    // const { activeGadget } = this.state;

    const { data } = this.props;
    const adsData = (data && data.listData && data.listData.ads && data.listData.ads.wapads) || '';

    // console.log('componentDidMount', this.props);
    const {
      dispatch,
      query,
      params,
      routeParams: { device },
    } = this.props;
    if (device) {
      const activeGadget = (siteConfig.gadgetMapping && siteConfig.gadgetMapping[device]) || '';

      this.devicesPopularNLatest(activeGadget);
    }
    Comparison.fetchData({ dispatch, query, params });

    if (adsData) {
      initGoogleAds({ ATF_728: adsData.ATF_728 });
    }
  }

  componentDidUpdate(prevProps) {
    const {
      routeParams: { device },
    } = this.props;

    if (device && device !== prevProps.routeParams.device) {
      const activeGadget = (siteConfig.gadgetMapping && siteConfig.gadgetMapping[device]) || '';

      this.devicesPopularNLatest(activeGadget);
    }
  }

  handleBlur = event => {};

  removeDevice = (deviceInfo, deviceName) => {
    console.log('removeDevice', deviceName);
    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };
    updatedDeviceInfo[deviceName] = {
      userInput: '',
      userInputHtml: '',
      seoName: '',
      searchResult: '',
    };

    this.setState({
      deviceInfo: updatedDeviceInfo,
    });
  };

  clickEventHandler = (deviceInfo, deviceName) => {
    // const { activeGadget } = this.state;

    const {
      routeParams: { device },
    } = this.props;

    const activeGadget = (siteConfig.gadgetMapping && siteConfig.gadgetMapping[device]) || '';

    this.updateSuggestedData(activeGadget, deviceInfo, deviceName);
  };

  handleChange = evt => {
    const { state } = this;
    const { activeGadget } = state;
    const userInput = evt.currentTarget.value;
    const device = evt.currentTarget.name;
    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };

    updatedDeviceInfo[device].userInput = userInput;
    this.setState({
      deviceInfo: updatedDeviceInfo,
      IsdeviceAdded: false,
    });
    // console.log('handleChange', this.state);
    this.searchDevice(userInput, device, activeGadget);
  };

  addFromSuggestions = deviceInfo => {
    const { activeGadget, state } = this;
    let device;
    for (const key in state.deviceInfo) {
      if (!state.deviceInfo[key].userInputHtml) {
        device = key;
        break;
      }
    }
    this.updateSuggestedData(activeGadget, deviceInfo, device);
  };

  submitEventHandler = event => {
    event.preventDefault();
    const { deviceInfo, deviceData } = this.state;
    console.log('sss', deviceInfo);

    const seoItem = [];
    if (deviceInfo) {
      for (const key in deviceInfo) {
        if (Object.prototype.hasOwnProperty.call(deviceInfo, key)) {
          if (deviceInfo[key].seoName) {
            seoItem.push(deviceInfo[key].seoName);
            // prepareSeoURL += deviceInfo[key].seoName;
          }
        }
      }

      if (seoItem.length >= 2) {
        seoItem.sort();
        const seoPath = `/tech/compare-mobile-phones/${seoItem.join('-vs-')}`;
        browserHistory.push(seoPath);
      }
    }

    // console.log('submitEventHandler', this.state);
  };

  devicesPopularNLatest = category => {
    const apiComparision = `${
      process.env.API_ENDPOINT
    }/pwagn_comparisonlist.cms?pagetype=comparelist&category=${category}&feedtype=sjson`;
    fetch(apiComparision)
      .then(promise => promise.json())
      .then(response => {
        this.setState({ deviceData: response });
      });
  };

  switchDeviceHandler = deviceType => {
    // alert(deviceType);
    const { deviceData } = this.state;
    const { dispatch, data } = this.props;

    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };

    console.log('sssss', this.state);
    const seoName = this.getKeyByValue(siteConfig.gadgetMapping, deviceType);
    if (typeof window !== 'undefined') {
      window.history.pushState({}, '', `/compare-${seoName}`);
    }

    for (const key in updatedDeviceInfo) {
      if (Object.prototype.hasOwnProperty.call(updatedDeviceInfo, key)) {
        updatedDeviceInfo[key].searchResult = '';
        updatedDeviceInfo[key].userInput = '';
        updatedDeviceInfo[key].userInputHtml = '';
      }
    }

    this.setState({
      deviceInfo: updatedDeviceInfo,
      activeGadget: deviceType,
      deviceData,
      searchResult: false,
      suggestedDevices: '',
    });

    dispatch(updateGadgetData(deviceType));

    // this.devicesPopularNLatest(deviceType);
  };

  getKeyByValue = (object, value) => {
    for (const key in object) {
      if (Object.prototype.hasOwnProperty.call(object, key)) {
        if (object[key] === value) return key;
      }
    }
    return '';
  };

  updateSuggestedData = (activeGadget, deviceInfo, device) => {
    // const { suggestedDevices, deviceInfo } = this.state;
    const updatedDeviceInfo = {
      ...this.state.deviceInfo,
    };

    for (const key in updatedDeviceInfo) {
      if (updatedDeviceInfo[key].userInputHtml === deviceInfo.Product_name) {
        return this.setState({
          deviceInfo: updatedDeviceInfo,
          IsdeviceAdded: true,
        });
      }
    }

    updatedDeviceInfo[device] = {
      userInput: updatedDeviceInfo[device].userInput,
      isVisible: updatedDeviceInfo[device].isVisible,
      userInputHtml: deviceInfo.Product_name,
      seoName: deviceInfo.seoname,
      searchResult: updatedDeviceInfo[device].searchResult,
    };

    if (!this.state.suggestedDevices) {
      const apiSuggested = `${
        process.env.API_ENDPOINT
      }/wdt_gadgetreleted.cms?feedtype=sjson&type=brand&category=${activeGadget}&productid=${
        deviceInfo.seoname
      }`;
      fetch(apiSuggested)
        .then(promise => promise.json())
        .then(response => {
          this.setState({
            deviceInfo: updatedDeviceInfo,
            suggestedDevices: response,
            IsdeviceAdded: false,
          });
        });
    } else {
      this.setState({
        deviceInfo: updatedDeviceInfo,
        IsdeviceAdded: false,
      });
    }
  };

  searchDevice(value, device, gadget) {
    const { state } = this;
    const { deviceData } = state;
    const updatedDeviceInfo = {
      ...state.deviceInfo,
    };

    fetch(
      `${
        process.env.API_ENDPOINT
      }/autosuggestion.cms?type=brand&tag=product_cat&category=${gadget}&q=${value}`,
    )
      .then(response => {
        // Examine the text in the response
        response.json().then(data => {
          // console.log(data);
          const devices =
            data &&
            data.map(item => {
              return { Product_name: item.Product_name, seoname: item.seoname };
            });

          updatedDeviceInfo[device] = {
            userInput: updatedDeviceInfo[device].userInput,
            isVisible: updatedDeviceInfo[device].isVisible,
            userInputHtml: updatedDeviceInfo[device].userInputHtml,
            searchResult: devices.filter(item => item !== ''),
          };
          this.setState({
            deviceInfo: updatedDeviceInfo,
            deviceData,
          });
          // return deviceData.filter(item => item != '');
        });
      })
      .catch(() => {
        // console.log('Fetch Error :-S', err);
      });
  }

  render() {
    // console.log('Render', this.props);
    const { data } = this.props;

    const { deviceInfo, activeGadget, deviceData, IsdeviceAdded, suggestedDevices } = this.state;
    const { device1, device2, device3, device4 } = deviceInfo;
    let count = 0;

    if (deviceInfo) {
      for (const key in deviceInfo) {
        if (Object.prototype.hasOwnProperty.call(deviceInfo, key)) {
          if (deviceInfo[key].userInputHtml) {
            count++;
          }
          if (count === 2) {
            break;
          }
        }
      }
    }
    console.log('state inside render ', data);

    let dataForPopularMobile = '';
    let restdataForPopularMobile = '';

    let dataForLatestMobile = '';
    let restdataForLatestMobile = '';

    const deviceDataCopy =
      data && data.listData && data.listData.compareData ? { ...data.listData.compareData } : '';

    // const finalizeDeviceData = (Array.isArray(deviceDataCopy) && deviceDataCopy) || '';
    const metaData = (data && data.metaData) || '';
    const seoSchema = '';

    if (
      deviceDataCopy &&
      deviceDataCopy.popularGadgetPair &&
      deviceDataCopy.popularGadgetPair.compare
    ) {
      const arrPopular = [...deviceDataCopy.popularGadgetPair.compare];
      if (Array.isArray(arrPopular) && arrPopular.length > 0) {
        dataForPopularMobile = arrPopular.splice(0, 3); // after making slice arrPopular will continue rest items
        if (arrPopular.length > 0) {
          restdataForPopularMobile = [...arrPopular];
        }
      }
    }

    if (deviceDataCopy && deviceDataCopy.recentcomp && deviceDataCopy.recentcomp.compare) {
      const arrLatest = [...deviceDataCopy.recentcomp.compare];
      if (Array.isArray(arrLatest) && arrLatest.length > 0) {
        dataForLatestMobile = arrLatest.splice(0, 3); // after making slice arrPopular will continue rest items
        if (arrLatest.length > 0) {
          restdataForLatestMobile = [...arrLatest];
        }
      }
    }

    // console.log('xxxx', dataForPopularMobile, restdataForPopularMobile);

    // const headingDoCompare = `${activeGadget}${deviceText.doCompare}`;
    // const headingCompare = `${activeGadget}${deviceText.compare}`;

    const activeGadgetName = activeGadget === 'mobile' ? 'mobilePhones' : activeGadget;

    return (
      <React.Fragment>
        {deviceDataCopy &&
          deviceDataCopy.compareData &&
          deviceDataCopy.compareData.breadcrumb &&
          deviceDataCopy.compareData.breadcrumb.div && (
            <BreadCrumbs data={deviceDataCopy.compareData.breadcrumb.div} />
          )}
        <Helmet
          title={(metaData && metaData.title) || ''}
          titleTemplate="%s"
          meta={metaData && metaData.metaTags}
          link={metaData && metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          script={[seoSchema]}
        />

        <div className="container-comparewidget">
          <div className="wdt_compare-gadgets">
            <DeviceWidget clicked={this.switchDeviceHandler} activeGadget={activeGadget} />
            <h3>{`${deviceText[activeGadgetName]} ${deviceText.doCompare}`}</h3>
            <div className="input_gadgets">
              <form onSubmit={this.submitEventHandler}>
                <div className="row-fields">
                  <ManageDevice
                    device={device1}
                    deviceName="device1"
                    changed={this.handleChange}
                    blured={this.handleBlur}
                    clicked={this.removeDevice}
                    self={this}
                    IsdeviceAdded={IsdeviceAdded}
                  />
                  <ManageDevice
                    device={device2}
                    deviceName="device2"
                    changed={this.handleChange}
                    blured={this.handleBlur}
                    clicked={this.removeDevice}
                    self={this}
                    IsdeviceAdded={IsdeviceAdded}
                  />
                  <ManageDevice
                    device={device3}
                    deviceName="device3"
                    changed={this.handleChange}
                    blured={this.handleBlur}
                    clicked={this.removeDevice}
                    self={this}
                    IsdeviceAdded={IsdeviceAdded}
                  />
                  <ManageDevice
                    device={device4}
                    deviceName="device4"
                    changed={this.handleChange}
                    blured={this.handleBlur}
                    clicked={this.removeDevice}
                    self={this}
                    IsdeviceAdded={IsdeviceAdded}
                  />
                </div>
                <input type="submit" name="compare" className="btn-blue" value="कंपेयर करें" />
                {count < 2 ? (
                  <span className="info-camparsion">
                    तुलना के लिए कम से कम आवश्यकता होती है दो डिवाइस
                  </span>
                ) : (
                  ''
                )}
                {IsdeviceAdded ? <span className="info-camparsion">Already There</span> : ''}
              </form>
            </div>
          </div>
          {suggestedDevices && suggestedDevices.gadget && (
            <div className="full_section wdt_suggested_slider ui_slider">
              <h2>
                <span>
                  {`${deviceText.suggested} ${deviceText[activeGadget]}
              ${deviceText.compare}`}
                </span>
              </h2>
              <Slider
                type="suggested"
                size="5"
                sliderData={suggestedDevices.gadget}
                width="165"
                height="230"
                SliderClass="photoslider"
                islinkable="true"
                sliderWidth="920"
                headingRequired="yes"
                clickedEvent={this.addFromSuggestions}
              />
            </div>
          )}

          <div className="full_section wdt_popular_slider ui_slider">
            <h2>
              <span>
                {`${deviceText.popular} ${deviceText[activeGadget]} ${deviceText.compare}`}
              </span>
            </h2>

            {dataForPopularMobile && (
              <Slider
                type="compareListPopular"
                size="1"
                sliderData={dataForPopularMobile}
                width="165"
                height="185"
                SliderClass="photoslider"
                islinkable="true"
                sliderWidth="100"
                headingRequired="yes"
              />
            )}

            <div className="items-in-list">
              <ul>
                {restdataForPopularMobile
                  ? restdataForPopularMobile.map(item => {
                      return (
                        <li>
                          <span className="text_ellipsis">{item.product_name.join(' vs ')}</span>
                        </li>
                      );
                    })
                  : ''}
              </ul>
            </div>
          </div>

          <div className="full_section wdt_lstcomparsion_slider ui_slider">
            <h2>
              <span>
                {`${deviceText.latest} ${deviceText[activeGadget]}
            ${deviceText.compare}`}
              </span>
            </h2>

            {dataForLatestMobile && (
              <Slider
                type="compareListLatest"
                size="1"
                sliderData={dataForLatestMobile}
                width="165"
                height="185"
                SliderClass="photoslider"
                islinkable="true"
                sliderWidth="100"
                headingRequired="yes"
              />
            )}
            <div className="items-in-list">
              <ul>
                {restdataForLatestMobile
                  ? restdataForLatestMobile.map(item => {
                      return (
                        <li>
                          <span className="text_ellipsis">{item.product_name.join(' vs ')}</span>
                        </li>
                      );
                    })
                  : ''}
              </ul>
            </div>
          </div>

          <LazyLoad height={100} once>
            <VideoWidget />
          </LazyLoad>
          <LazyLoad height={100} once>
            <PhotoWidget />
          </LazyLoad>
        </div>
      </React.Fragment>
    );
  }
}

const ManageDevice = ({ device, deviceName, changed, blured, self, IsdeviceAdded }) => {
  return device && !device.userInputHtml ? (
    <div className="input_field">
      <input
        type="text"
        name={deviceName}
        className={deviceName}
        value={device.userInput}
        onChange={changed}
        onBlur={blured}
      />
      {!IsdeviceAdded && (
        <div className="auto-suggest">
          <ul>
            {device.searchResult &&
              device.searchResult.map(item => (
                <li
                  key={item.Product_name}
                  data-item={item.Product_name}
                  onClick={self.clickEventHandler.bind(this, item, deviceName)}
                >
                  <span>{item.Product_name}</span>
                </li>
              ))}
          </ul>
        </div>
      )}
    </div>
  ) : (
    <div className="input_field">
      <input type="text" value={device.userInputHtml} />
      <span className="close_icon" onClick={self.removeDevice.bind(this, device, deviceName)} />
    </div>
  );
};

const DeviceWidget = ({ clicked, activeGadget }) => {
  console.log('activeGadget', activeGadget);
  // gdt-mobile active
  const Gadgets = siteConfig.gadgetsList;
  const deviceHtml =
    Gadgets &&
    Gadgets.map(item => {
      return (
        <li
          key={item.name}
          className={`gdt-${item.name}${item.name == activeGadget ? ' active' : ''}`}
          onClick={clicked.bind(this, item.name)}
        >
          <b />
          <label> {item.regName}</label>
        </li>
      );
    });

  return (
    <div className="select-inline-gadgets">
      <ul className="scroll">{deviceHtml}</ul>
    </div>
  );
};

Comparison.fetchData = function fetchData({ dispatch, query, params }) {
  return dispatch(fetchDataIfNeeded({ query, params }));
};

Comparison.propTypes = {
  dispatch: PropTypes.func,
};

function mapStateToProps(state) {
  return {
    ...state.compareList,
  };
}

export default connect(mapStateToProps)(Comparison);
