/* eslint-disable react/jsx-key */
/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';

import { fetchDataIfNeeded, updateListData, updateNavData } from '../../actions/photolist';
import { adsPlaceholder } from '../../common-utility';
import { analyticsGA, AnalyticsComscore, AnalyticsIBeat } from '../../analytics';
import ArticleListFakeLoader from '../../components/Loaders/ArticleListFakeLoader';
import { initGoogleAds } from '../../ads/dfp';
import BreadCrumbs from '../../components/BreadCrumb';
import './Photolist.scss';
import '../../public/css/commonComponent.scss';
import Link from '../../components/Link/Link';
import Thumb from '../../components/Thumb/Thumb';
import CtnAd from '../articleshow/subsections/CtnAd';

const Config = require(`../../../common/${process.env.SITE}`);

class PhotoList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activepg: '',
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    const { dispatch, params, query, data } = this.props;
    PhotoList.fetchData({ dispatch, query, params });
    const sectionName = 'photo';
    this.renderAds(data);

    /* if (
      data &&
      data.photolistData &&
      data.photolistData.pwa_meta &&
      data.photolistData.pwa_meta.ibeat
    ) {
      const ibeatvalObj = data.photolistData.pwa_meta && data.photolistData.pwa_meta.ibeat;
      const ibeatObj = Object.assign(Config.ibeatConfig, ibeatvalObj);
      AnalyticsIBeat.initIbeat(ibeatObj);
    } */
    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  componentWillUnmount() {
    if (typeof googletag == 'object' && typeof googletag.destroySlots == 'function') {
      googletag.destroySlots();
    }
  }

  componentDidUpdate(prevProps) {
    const { dispatch, params, query, data, location } = this.props;
    const sectionName = 'photo';
    if (
      prevProps.location.pathname !== location.pathname ||
      (prevProps.location.search && !location.search)
    ) {
      let isForcedUpdate = false;
      if (prevProps.location.search && !location.search) {
        isForcedUpdate = true;
      }
      PhotoList.fetchData({ dispatch, query, params, isForcedUpdate });
      this.setState({
        activepg: '',
      });
    }
    this.renderAds(data);

    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  showLoaderIfNeeded = () => {
    const { data } = this.props;
    if (data && data.photolistData) {
      return <ArticleListFakeLoader />;
    }
    return null;
  };

  handleClick(event) {
    const { dispatch, data, router } = this.props;
    dispatch(
      updateListData({
        id:
          (data &&
            data.photolistData &&
            data.photolistData.sectioninfo &&
            data.photolistData.sectioninfo.id) ||
          '',
        pgno: Number(event.target.id),
      }),
    );
    document.documentElement.scrollTop = 0;
    const pageUrl =
      typeof document === 'object'
        ? `${document.location.protocol}//${document.location.host}${document.location.pathname}`
        : '';
    const curID = `${pageUrl}?curpg=${event.target.id}`;
    this.setState(
      {
        activepg: Number(event.target.id),
      },
      () => {
        router.replace(curID);
      },
    );

    // window.history.replaceState({}, pageUrl, curID);
  }

  renderAds = data => {
    const adsData = (data && data.adsData && data.adsData.wapads) || '';
    if (adsData) {
      // initGoogleAds(adsData);
      const sectionInfo = {
        pagetype: 'photolist',
        sec1: data && data.photolistData && data.photolistData.sec1,
        sec2: data && data.photolistData && data.photolistData.sec2,
        sec3: data && data.photolistData && data.photolistData.sec3,
        hyp1:
          data &&
          data.photolistData &&
          data.photolistData.pwa_meta &&
          data.photolistData.pwa_meta.hyp1,
      };
      setTimeout(() => {
        initGoogleAds(adsData, sectionInfo);

        // if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
        //   analyticsGA.pageview();
        // }
        // if (
        //   typeof AnalyticsComscore !== 'undefined' &&
        //   typeof AnalyticsComscore.invokeComScore !== 'undefined'
        // ) {
        //   AnalyticsComscore.invokeComScore();
        // }
      }, 500);
    }
  };

  render() {
    const { isFetching, data } = this.props;
    const { activepg } = this.state;
    let renderPageNumbers = '';

    // Logic for displaying page numbers
    if (data && data.photolistData && data.photolistData.pg && data.photolistData.pg.tp) {
      const activePage = Number(data.photolistData.pg.cp);
      const tpFound = parseInt(data.photolistData.pg.tp);
      const totalpage = tpFound > 15 ? 15 : tpFound;
      const pageNumbers = [];
      for (let i = 1; i <= totalpage; i++) {
        pageNumbers.push(i);
      }

      renderPageNumbers = pageNumbers.map(pageNo => {
        const activeClass = pageNo === activepg || pageNo === activePage ? 'active' : '';

        return (
          <a className={activeClass} key={pageNo} id={pageNo} onClick={this.handleClick}>
            {pageNo}
          </a>
        );
      });
    }

    const schema = {
      type: 'application/ld+json',
      innerHTML: data && data.photolistData && data.photolistData.Newsarticle,
    };
    const pwaMeta = (data && data.photolistData && data.photolistData.pwa_meta) || null;
    const photoListData = (data && data.photolistData) || null;
    // console.log('VLDATA', photoListData.sections);

    return (
      <div className="articles_container">
        <Helmet
          title={(pwaMeta && pwaMeta.title) || ''}
          titleTemplate="%s"
          meta={data && data.metaData && data.metaData.metaTags}
          link={data && data.metaData && data.metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          script={[schema]}
        />

        {isFetching && !data ? (
          <ArticleListFakeLoader />
        ) : (
          <div style={{ overflow: 'hidden' }}>
            {adsPlaceholder('ATF_AS_STRIPPD')}
            {adsPlaceholder('PPD')}
            <BreadCrumbs
              data={photoListData && photoListData.breadcrumb && photoListData.breadcrumb.div}
            />
            <CtnAd
              articleId={photoListData && photoListData.id}
              height="40"
              width="1000"
              className="colombia"
              slotId={Config.CTN.stripped_pbd_article_show}
            />
            <div className="contentarea PL">
              <div className="leftmain">
                <div className="photoData">
                  <h1 className="sectionHead">
                    <span>
                      {photoListData &&
                        photoListData.sectioninfo &&
                        photoListData.sectioninfo.secname}
                    </span>
                  </h1>
                  <ul>
                    {photoListData &&
                      photoListData.items &&
                      photoListData.items.map(data => {
                        return (
                          <li key={data.id}>
                            <Thumb
                              width="144"
                              height="108"
                              imgId={data.imageid}
                              title={data.hl}
                              alt={data.hl}
                              islinkable="true"
                              link={data.wu}
                              resizeMode="4"
                            />
                            <p>
                              <Link to={data.wu}>{data.hl}</Link>
                            </p>
                          </li>
                        );
                      })}
                  </ul>
                  <div className="printpage">{renderPageNumbers}</div>
                </div>
              </div>

              <div className="rightnav">
                {adsPlaceholder('ATF_300 section ads_default')}

                {photoListData &&
                photoListData.mostviewed &&
                photoListData.mostviewed.items &&
                Array.isArray(photoListData.mostviewed.items) ? (
                  <div className="section all_image_view">
                    <h2>
                      <span>{photoListData.mostviewed.heading}</span>
                    </h2>
                    <div className="rt-mostpopular">
                      <ul>
                        {photoListData.mostviewed.items.map(items => {
                          return (
                            <li key={items.id}>
                              <span className="img_container">
                                <Thumb
                                  width="100"
                                  height="75"
                                  imgId={items.imageid}
                                  title={items.hl}
                                  alt={items.hl}
                                  islinkable="true"
                                  link={items.wu}
                                  resizeMode="4"
                                />
                              </span>
                              <span className="txt_container">
                                <Link className="dec" to={items.wu}>
                                  {items.hl}
                                </Link>
                              </span>
                            </li>
                          );
                        })}
                      </ul>
                    </div>
                  </div>
                ) : null}
                {adsPlaceholder('BTF_300 section ads_default')}
              </div>

              {photoListData &&
              photoListData.section &&
              photoListData.section.items &&
              Array.isArray(photoListData.section.items) &&
              !photoListData.sections ? (
                <div className="bottomsec">
                  <h2>{data.photolistData.section.heading}</h2>
                  <div className="btbox">
                    <ul>
                      {photoListData.section.items.map(items => {
                        return (
                          <li key={items.id}>
                            <Thumb
                              width="230"
                              height="155"
                              imgId={items.imageid}
                              title={items.hl}
                              alt={items.hl}
                              islinkable="true"
                              link={items.wu}
                              resizeMode="4"
                            />
                            <p>
                              <Link to={items.wu}>{items.hl}</Link>
                            </p>
                          </li>
                        );
                      })}
                    </ul>
                  </div>
                </div>
              ) : null}
            </div>
          </div>
        )}

        <div id="AL_Innov1" className="AL_Innov1" />
        <div id="WSK_160_AS" className="WSK_160_AS" />
      </div>
    );
  }
}

PhotoList.fetchData = function fetchData({ dispatch, query, params, isForcedUpdate }) {
  return dispatch(fetchDataIfNeeded({ query, params, isForcedUpdate }));
};

PhotoList.propTypes = {
  dispatch: PropTypes.func,
  isForcedUpdate: PropTypes.func,
  showLoginRegister: PropTypes.func,
  authentication: PropTypes.object,
  params: PropTypes.object,
  query: PropTypes.object,
  history: PropTypes.object,
  data: PropTypes.object,
  isFetching: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.photolist,
    authentication: state.authentication,
    newsDataFetched: state.news.didFetch,
    isFetchingNewsData: state.news.isFetching,
  };
}

export default connect(mapStateToProps)(PhotoList);
