import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import { CookiesProvider } from 'react-cookie';

import routes from '../../routes';
import { backHandler } from '../../common-utility';

import { analyticsGA, AnalyticsComscore } from '../../analytics';

function onRouteUpdate() {
  const { pathname } = this.state.location;
  if (typeof window !== 'undefined') {
    window.scrollTo(0, 0);
  }
  setTimeout(() => {
    if (
      typeof analyticsGA !== 'undefined' &&
      typeof analyticsGA.pageview !== 'undefined' &&
      !pathname.includes('/articleshow/')
    ) {
      analyticsGA.pageview();
    }

    if (
      typeof AnalyticsComscore !== 'undefined' &&
      typeof AnalyticsComscore.invokeComScore !== 'undefined'
    ) {
      AnalyticsComscore.invokeComScore();
    }

    if (typeof backHandler === 'function') {
      backHandler();
    }
  }, 3000);

  // let { pathname, query } = this.state.location;
  // let { params } = this.state;
  // adsHandler(pathname, params, query);
  // update landing page flag
  // isLandingPage = false;
}

function Root({ store, history }) {
  return (
    <CookiesProvider>
      <Provider store={store}>
        <Router history={history} routes={routes} onUpdate={onRouteUpdate} />
      </Provider>
    </CookiesProvider>
  );
}

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default Root;
