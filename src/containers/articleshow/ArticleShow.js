/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { PureComponent } from 'react';
import LazyLoad from 'react-lazyload';

import './ArticleShow.scss';
import '../../public/css/commonComponent.scss';
import ArticleLeftSection from './ArticleLeftSection';
import ArticleRightSection from './ArticleRightSection';
import Slider from '../../components/NewSlider/NewSlider';

const Config = require(`../../../common/${process.env.SITE}`);

class ArticleShow extends PureComponent {
  state = {
    isCommentClicked: false,
    isNewsLetterClicked: false,
    isMailClicked: false,
    rhsData: '',
  };

  componentDidMount() {
    const { data } = this.props;
    if (data && data.articleData && data.articleData.secid) {
      const sid = data.articleData.secname === 'reviews' ? '66130905' : data.articleData.secid;
      this.getRhsData(sid);
    }
  }

  componentWillUnmount() {
    if (typeof googletag == 'object' && typeof googletag.destroySlots == 'function') {
      googletag.destroySlots();
    }
  }

  getRhsData(secId) {
    fetch(
      `${
        process.env.API_ENDPOINT
      }/webgn_common.cms?feedtype=sjson&msid=${secId}&count=10&platform=web&tag=trending,ibeatmostread,sectiondata`,
    )
      .then(response => response.json())
      .then(data => {
        this.setState({ rhsData: data });
      })
      .catch({});
  }

  showMailBox = () => {
    this.setState({
      isMailClicked: true,
    });
  };

  closeMailBox = () => {
    this.setState({
      isMailClicked: false,
    });
  };

  closeIfOnOverlay = e => {
    if (e.target.id === 'comments__body') {
      return false;
    }
    this.closeCommentSlider();
    return false;
  };

  showNewsLetterBox = () => {
    this.setState({
      isNewsLetterClicked: true,
    });
    document.body.classList.add('disable-scroll');
  };

  closeNewsLetterBox = () => {
    this.setState({
      isNewsLetterClicked: false,
    });

    document.body.classList.remove('disable-scroll');
  };

  closeCommentSlider = () => {
    this.setState({
      isCommentClicked: false,
    });

    document.body.classList.remove('disable-scroll');
  };

  showComments = () => {
    this.setState({
      isCommentClicked: true,
    });
    document.body.classList.add('disable-scroll');
  };

  render() {
    const {
      data,
      loggedIn,
      showSeperator,
      showLoginRegister,
      closeLoginRegister,
      isCommentVerified,
      isFirst,
      dispatch,
      dockAndPlayVideo,
      articleScrollId,
    } = this.props;
    const { isCommentClicked, isNewsLetterClicked, isMailClicked, rhsData } = this.state;
    // const isLoogedIn = !!getCookie('ssoid');
    const isreview =
      data && data.articleData && data.articleData.secname === 'reviews'
        ? 'reviews'
        : 'articleshow';
    return (
      <React.Fragment>
        {data && data.articleData && showSeperator ? (
          <div
            className="story-seperator"
            id={`story-seperator-${data && data.articleData && data.articleData.id}`}
          >
            <div className="end-ad">
              <div className={`ATF_728_${data && data.articleData && data.articleData.id}`} />
            </div>
            <div className="next-story">
              <span>{Config.Locale.nextStory}</span>
            </div>
          </div>
        ) : null}
        {isCommentVerified ? (
          <div className="email_verification">{Config.Locale.emailVerification} </div>
        ) : null}
        <div className="contentarea articleshow_body" id={`article_show_body${articleScrollId}`}>
          {data && data.articleData ? (
            <ArticleLeftSection
              dispatch={dispatch}
              articleScrollId={articleScrollId}
              data={data && data.articleData}
              showLoginRegister={showLoginRegister}
              dockAndPlayVideo={dockAndPlayVideo}
              commentsData={data && data.commentsData}
              topCommentsData={data && data.topCommentsData}
              showComments={this.showComments}
              showNewsLetterBox={this.showNewsLetterBox}
              showMailBox={this.showMailBox}
              loggedIn={loggedIn}
              showLoginRegister={showLoginRegister}
              closeLoginRegister={closeLoginRegister}
            />
          ) : null}

          <ArticleRightSection
            data={rhsData || ''}
            articleData={data && data.articleData}
            articleId={data && data.articleData && data.articleData.id}
            isFirst={isFirst}
          />
        </div>

        {isCommentClicked ? (
          <div className="popup_comments">
            <div className="body_overlay" onClick={this.closeIfOnOverlay} />
            <div style={{ right: 0 }} id="comments__body" className="comments-body">
              <span className="close_icon" onClick={this.closeCommentSlider} />
              <iframe
                src={`${process.env.API_ENDPOINT}/comments_slider_react.cms?msid=${
                  data.articleData.id
                }&type=${isreview}&isloggedin=${loggedIn}`}
                height="100%"
                width="550"
                border="0"
                MARGINWIDTH="0"
                MARGINHEIGHT="0"
                HSPACE="0"
                VSPACE="0"
                FRAMEBORDER="0"
                SCROLLING="no"
                align="center"
                title="iplwidget"
                ALLOWTRANSPARENCY="true"
                id="comment-frame"
              />
            </div>
          </div>
        ) : (
          ''
        )}
        {/* {isNewsLetterClicked ? (
          <div className="popup_newsletter">
            <div className="body_overlay" id="TB_overlay" />
            <div className="overlay_content">
              <span onClick={this.closeNewsLetterBox} className="close_icon">
                Close
              </span>
              <iframe
                src={`${process.env.BASE_URL}/subscriptionform.cms?type=as`}
                height="250"
                width="760"
                border="0"
                MARGINWIDTH="0"
                MARGINHEIGHT="0"
                HSPACE="0"
                VSPACE="0"
                FRAMEBORDER="0"
                SCROLLING="no"
                align="center"
                title="iplwidget"
                ALLOWTRANSPARENCY="true"
                id="comment-frame"
              />
            </div>
          </div>
        ) : (
          ''
        )} */}

        {isMailClicked ? (
          <div className="popup_sendViaEmail">
            <div className="body_overlay" id="TB_overlay" />
            <div className="overlay_content">
              <span onClick={this.closeMailBox} className="close_icon">
                Close
              </span>
              <iframe
                src={`${process.env.BASE_URL}/pwafeeds/articleshow_articlemail/${
                  data.articleData.id
                }.cms`}
                height="400"
                width="450"
                border="0"
                MARGINWIDTH="0"
                MARGINHEIGHT="0"
                HSPACE="0"
                VSPACE="0"
                FRAMEBORDER="0"
                SCROLLING="no"
                align="center"
                title="iplwidget"
                ALLOWTRANSPARENCY="true"
                id="comment-frame"
              />
            </div>
          </div>
        ) : (
          ''
        )}
      </React.Fragment>
    );
  }
}

export default ArticleShow;
