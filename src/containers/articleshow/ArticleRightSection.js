/* eslint-disable indent */
/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import LazyLoad from 'react-lazyload';
import VideoSection from './subsections/VideoSection';
import ArticlesListBlock from './subsections/ArticlesListBlock';
import { adsPlaceholder } from '../../common-utility';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import NewsWidget from '../../components/Home/News/NewsWidget/NewsWidget';
import WdtAffiliatesList from '../../components/Amazon/WdtAffiliatesList';

const Config = require(`../../../common/${process.env.SITE}`);

const ArticleRightSection = ({ data, articleData, articleId, isFirst }) => {
  const ibeatData =
    data &&
    data.section.filter(data => {
      return data.id === 'ibeatmostread';
    });
  const trendingData =
    data &&
    data.section.filter(data => {
      return data.id === 'trending';
    });
  const secData =
    data &&
    data.section.filter(data => {
      return data.id === '66130905';
    });

  // console.log('news content 2222', secData, trendingData, ibeatData);

  const random1 = Math.floor(Math.random() * 10000);
  // console.log('==random1==', random1);

  return (
    <div className="rightnav">
      {adsPlaceholder(`ATF_300_${articleId} section ads_default`)}

      {trendingData ? (
        <ErrorBoundary>
          <LazyLoad height={100} once>
            <NewsWidget
              newsContent={trendingData}
              type="trending"
              secid="trending"
              secname="Trending"
              imgrequired="none"
              count="10"
              widgetClassName="GN-trending"
            />
          </LazyLoad>
        </ErrorBoundary>
      ) : (
        ''
      )}

      {secData ? (
        <ErrorBoundary>
          <LazyLoad height={100} once>
            <ArticlesListBlock data={secData[0]} widgetClassName="with-scroll" />
          </LazyLoad>
        </ErrorBoundary>
      ) : (
        ''
      )}

      {adsPlaceholder(`section adDivs BTF_300_${articleId}`)}

      <ErrorBoundary>
        <LazyLoad height={100} once>
          <WdtAffiliatesList
            title=""
            category="mobile"
            tag={
              articleData &&
              articleData.pwa_meta &&
              articleData.pwa_meta.subpagetype === 'techreview'
                ? 'nbt_web_reviewshow_sponsoredwidgetrhs-21'
                : 'nbt_web_articleshow_sponsoredwidgetrhs-21'
            }
            isslider="0"
            noimg="0"
            noh2="0"
          />
        </LazyLoad>
      </ErrorBoundary>

      <div
        className="colombia section"
        data-section="tech"
        data-slot="323818"
        id={`div-clmb-ctn-323818~1-${random1}`}
        data-position={random1}
      />

      {ibeatData ? (
        <ErrorBoundary>
          <LazyLoad height={100} once>
            <ArticlesListBlock data={ibeatData[0]} widgetClassName="with-scroll" />
          </LazyLoad>
        </ErrorBoundary>
      ) : (
        ''
      )}

      {adsPlaceholder(`section adDivs MREC1_${articleId}`)}
    </div>
  );
};

ArticleRightSection.propTypes = {
  data: PropTypes.object,
  articleId: PropTypes.string,
};

export default ArticleRightSection;
