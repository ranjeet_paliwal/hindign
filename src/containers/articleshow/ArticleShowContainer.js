/* eslint-disable func-names */
/* eslint-disable indent */
/* eslint-disable react/jsx-indent */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import ArticleShow from './ArticleShow';
import fetchDataIfNeeded, {
  fetchRelatedArticlesData,
  updateNavData,
} from '../../actions/articleshow';
import { truncateStr, adsPlaceholder, isInViewport, isCSR } from '../../common-utility';
import { analyticsGA, AnalyticsComscore, AnalyticsIBeat } from '../../analytics';

import ArticleSHowFakeLoader from '../../components/Loaders/ArticleShowFakeLoader';
import { initGoogleAds, refreshAds } from '../../ads/dfp';
import Link from '../../components/Link/Link';
import CtnAd from './subsections/CtnAd';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import BreadCrumbs from '../../components/BreadCrumb';
import * as TPLib from '../../analytics/TimesPoint';

import fetch from '../../utils/fetch/fetch';
import { playDockedVideo } from '../../actions/video';

const Config = require(`../../../common/${process.env.SITE}`);
let articleLastViewd = '';
let articleInView = '';
let psArticleLastViewd = '';
let psArticleInView = '';
let psArticleInViewUrl = '';
let psFirstContainer = '';

class ArticleShowContainer extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      currentArticle: 0,
      currentTop: 0,
      currentBottom: 0,
      isCommentVerified: false,
    };

    this.article1Ref = React.createRef();

    this.handleScrollChange = this.handleScrollChange.bind(this);
    this.scrollToArticle = this.scrollToArticle.bind(this);
    this.changePageUrl = this.changePageUrl.bind(this);

    this.AdServingRules =
      (props &&
        props.data &&
        props.data.articleData &&
        props.data.articleData.pwa_meta &&
        props.data.articleData.pwa_meta.AdServingRules) ||
      null;
  }

  componentDidMount() {
    const { dispatch, params, query, history, data } = this.props;
    const sectionName =
      data && data.articleData && data.articleData.secname ? data.articleData.secname : '';

    ArticleShowContainer.fetchData({ dispatch, query, params, history });
    window.addEventListener('scroll', this.handleScrollChange);
    this.callVerifyComments();
    const adsData = data && data.adsData && data.adsData.wapads ? data.adsData.wapads : '';
    if (adsData) {
      const sectionInfo = {
        pagetype: 'articleshow',
        sec1: data && data.articleData && data.articleData.sec1,
        sec2: data && data.articleData && data.articleData.sec2,
        sec3: data && data.articleData && data.articleData.sec3,
        hyp1:
          data && data.articleData && data.articleData.pwa_meta && data.articleData.pwa_meta.hyp1,
      };

      if (!this.AdServingRules) {
        setTimeout(() => {
          initGoogleAds(adsData, sectionInfo);
        }, 2000);
      }
    }

    if (data && data.articleData && Array.isArray(data.articleData.nextarticles)) {
      for (let i = 0; i < data.articleData.nextarticles.length; i++) {
        this[`article${i + 2}Ref`] = React.createRef();
      }
    }

    TPLib.logPoints(TPLib.ConfigData.tpconfig.ARTICLE_READ, '1234');

    document.querySelector('body').classList.add('AS');
    if (data && data.articleData && data.articleData.ibeatconfig) {
      AnalyticsIBeat.initIbeat(data.articleData.ibeatconfig);
    }
    localStorage.removeItem('Setvideo');
    // console.log('articledata', sectionName);
    dispatch(
      updateNavData({
        sectionName,
      }),
    );

    if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
      setTimeout(() => {
        if (
          data &&
          data.articleData &&
          data.articleData.pwa_meta &&
          data.articleData.pwa_meta.editorname
        ) {
          window.ga('set', 'dimension4', data.articleData.pwa_meta.editorname);
        }
        analyticsGA.pageview();
      }, 2000);
    }
  }

  componentDidUpdate(prevProps) {
    const { dispatch, params, query, location, history, data } = this.props;
    const sectionName =
      data && data.articleData && data.articleData.secname ? data.articleData.secname : '';
    if (!prevProps.data && data) {
      this.callVerifyComments();
      window.addEventListener('scroll', this.handleScrollChange);
      const articleOneDiv = this.article1Ref.current;
      if (articleOneDiv) {
        const articleOneDim = articleOneDiv.getBoundingClientRect();
        const currentTop = articleOneDiv.offsetTop;
        const currentBottom = articleOneDiv.offsetTop + articleOneDim.height;
        this.setState({
          currentTop,
          currentBottom,
        });
      }
      if (data && data.adsData && data.adsData.wapads) {
        const sectionInfo = {
          pagetype: 'articleshow',
          sec1: data && data.articleData && data.articleData.sec1,
          sec2: data && data.articleData && data.articleData.sec2,
          sec3: data && data.articleData && data.articleData.sec3,
        };
        if (document.querySelector('div.atf-topad')) {
          document.querySelector('div.atf-topad').innerHTML = '';
        }
        refreshAds(data.adsData.wapads.AS_Innov1);
        delete data.adsData.wapads.AS_Innov1;
        const AdServingRules =
          (data &&
            data.articleData &&
            data.articleData.pwa_meta &&
            data.articleData.pwa_meta.AdServingRules) ||
          null;

        if (!AdServingRules) {
          initGoogleAds(data.adsData.wapads, sectionInfo);
        }
      }

      if (data.articleData && Array.isArray(data.articleData.nextarticles)) {
        for (let i = 0; i < data.articleData.nextarticles.length; i++) {
          this[`article${i + 2}Ref`] = React.createRef();
        }
      }

      if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
        setTimeout(() => {
          if (
            data &&
            data.articleData &&
            data.articleData.pwa_meta &&
            data.articleData.pwa_meta.editorname
          ) {
            window.ga('set', 'dimension4', data.articleData.pwa_meta.editorname);
          }
          analyticsGA.pageview();
        }, 2000);
      }
    } else if (prevProps.location.pathname !== location.pathname) {
      window.scrollTo({ top: 0 });
      ArticleShowContainer.fetchData({ dispatch, query, params, history });
    }
    // console.log('articledata didupdate', sectionName);
    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScrollChange);
    document.querySelector('body').classList.remove('AS');
  }

  dockAndPlayVideo = videoData => {
    const { dispatch } = this.props;
    dispatch(playDockedVideo(videoData));
  };

  showLoaderIfNeeded = () => {
    const { data } = this.props;
    if (
      data &&
      data.articleData &&
      data.articleData.nextarticles &&
      data.articleData.nextarticles.length > 0
    ) {
      return <ArticleSHowFakeLoader />;
    }
    return null;
  };

  callVerifyComments = () => {
    const { location, data } = this.props;
    const { query } = location;

    if ('messageid' in query && 'r' in query && data && data.articleData && data.articleData.id) {
      const commentId = query.messageid;
      const articleId = data.articleData.id;
      const r = query.r;

      // Comment verification api post call.
      fetch(
        `${process.env.WEBSITE_URL}/cmtverified.cms?msid=${articleId}&cmtid=${commentId}&r=${r}`,
        { method: 'POST' },
      )
        .then(res => {
          return res;
        })
        .catch(e => e);

      this.setState({
        isCommentVerified: true,
      });
    }
  };

  scrollToArticle(index) {
    const currentArticle = index;

    const currentArticleNode =
      index == 0
        ? this[`article${index + 1}Ref`].current
        : document.getElementById(`article_show_body${index + 1}`);
    let currentTop = '';
    let currentBottom = '';
    if (currentArticleNode) {
      currentTop = (currentArticleNode && currentArticleNode.offsetTop) || '';
      currentBottom = currentTop + currentArticleNode.getBoundingClientRect().height;
    }

    // So not to triggle our scroll event

    this.changePageUrl(currentArticle);
    // Add it back again.

    this.setState(
      {
        currentArticle,
        currentTop,
        currentBottom,
      },
      () => {
        // Scroll to top of first article otherwise article top + 200
        const top = currentTop;

        window.scrollTo({
          top,
        });
      },
    );
  }

  handleScrollChange() {
    const {
      dispatch,
      isFetchingRelatedArticles,
      relatedArticlesDataFetched,
      isFetching,
      data,
      relatedArticlesData,
    } = this.props;

    if (!isFetching && data) {
      const { currentBottom, currentTop, currentArticle } = this.state;
      let newArticle = currentArticle;
      let newTop = currentTop;
      let newBottom = currentBottom;
      let hasMoreArticles = false;

      // Check if more articles are present in scroll for current article
      if (
        data &&
        data.articleData &&
        data.articleData.nextarticles &&
        data.articleData.nextarticles.length > 0
      ) {
        hasMoreArticles = true;
      }

      const nextArticlesLength = data.articleData.nextarticles.length;
      // Loading function  photofeature.
      const psElements = document.getElementsByClassName('psstory-seperator');

      for (let i = 0; i < psElements.length; i++) {
        if (isInViewport(document.getElementById(psElements[i].id))) {
          psArticleInView = psElements[i].id.split('photo_')[1];
          psArticleInViewUrl = psElements[i] && psElements[i].getAttribute('sub_url');
          psFirstContainer = psElements[i] && psElements[i].getAttribute('first_url');
          break;
        }
      }
      if (psArticleLastViewd !== psArticleInView) {
        psArticleLastViewd = psArticleInView;
        const isFirst = document
          .getElementById(`photo_${psArticleInView}`)
          .getAttribute('first_url');

        //console.log('photostory', psArticleLastViewd);
        if (isFirst) {
          window.history.pushState({ title: '' }, '', window.location.pathname);
          return;
        }
        window.history.pushState({ title: '' }, '', psArticleInViewUrl);

        if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
          if (data && data.articleData && data.articleData.crBy) {
            window.ga('set', 'dimension4', data.articleData.crBy);
          }
          analyticsGA.pageview(window.location.href);
        }
        if (
          typeof AnalyticsComscore !== 'undefined' &&
          typeof AnalyticsComscore.invokeComScore !== 'undefined'
        ) {
          AnalyticsComscore.invokeComScore();
        }
        if (
          typeof AnalyticsIBeat !== 'undefined' &&
          typeof AnalyticsIBeat.initIbeat !== 'undefined'
        ) {
          AnalyticsIBeat.initIbeat(data.articleData.pwa_meta.ibeat);
        }
      } //else if (psArticleLastViewd !== psArticleInView && psFirstContainer) {
      //window.history.pushState({ title: '' }, '', window.location.pathname);
      //}
      // Loading ads of other articles.
      const elements = document.getElementsByClassName('story-seperator');
      for (let i = 0, len = elements.length; i < len; i++) {
        if (isInViewport(document.getElementById(elements[i].id))) {
          articleInView = elements[i].id.split('story-seperator-')[1];
          break;
        }
      }
      if (articleLastViewd !== articleInView) {
        articleLastViewd = articleInView;
        const result = relatedArticlesData.filter(
          item => item.adsData.articleId === articleLastViewd,
        );
        if (result && result[0]) {
          const relatedData = result[0];
          const adsData = result.length > 0 ? relatedData.adsData.wapads : '';
          if (adsData) {
            // delete adsData.AS_Innov1;

            const sectionInfo = {
              pagetype: 'articleshow',
              sec1: relatedData && relatedData.articleData && relatedData.articleData.sec1,
              sec2: relatedData && relatedData.articleData && relatedData.articleData.sec2,
              sec3: relatedData && relatedData.articleData && relatedData.articleData.sec3,
              hyp1:
                relatedData &&
                relatedData.articleData &&
                relatedData.articleData.pwa_meta &&
                relatedData.articleData.pwa_meta.hyp1,
            };

            delete adsData.PPD;
            delete adsData.WSK_160_AS;
            const AdServingRules =
              (result.length > 0 &&
                relatedData &&
                relatedData.articleData &&
                relatedData.articleData.pwa_meta &&
                relatedData.articleData.pwa_meta.AdServingRules) ||
              null;

            if (!AdServingRules) {
              initGoogleAds(adsData, sectionInfo);
            }
          }
        }
      }

      if (!isFetchingRelatedArticles && !relatedArticlesDataFetched && hasMoreArticles) {
        if (window.scrollY >= 1000) {
          // Fetch rest of the articles data

          let ids = null;
          if (data && data.articleData && Array.isArray(data.articleData.nextarticles)) {
            ids = data.articleData.nextarticles
              .filter(nextArtData => nextArtData)
              .map(art => art.id);
          }

          if (ids) {
            dispatch(fetchRelatedArticlesData(ids));
          }
        }
      } else if (
        this.article1Ref &&
        this.article1Ref.current &&
        (relatedArticlesDataFetched || !hasMoreArticles) &&
        (window.scrollY >= currentBottom || window.scrollY <= currentTop) &&
        window.scrollY >= this.article1Ref.current.offsetTop &&
        window.scrollY <=
          this[`article${nextArticlesLength + 1}Ref`].current.offsetTop +
            this[`article${nextArticlesLength + 1}Ref`].current.getBoundingClientRect().height
      ) {
        /* Current scroll position */
        const currentScroll = window.scrollY;

        // Helper function to check if article is mid scroll
        const isMidScroll = (top, btm, cur) => {
          return cur >= top - 200 && cur <= btm;
        };

        /* current article data needed to be saved in state */

        // Check all the refs of all articles created
        for (let i = 0; i < nextArticlesLength + 1; i++) {
          let currentDiv;
          let currentDivDim;
          if (this[`article${i + 1}Ref`] && this[`article${i + 1}Ref`].current) {
            currentDiv = this[`article${i + 1}Ref`].current;
            currentDivDim = currentDiv.getBoundingClientRect();

            if (
              isMidScroll(
                currentDiv.offsetTop,
                currentDiv.offsetTop + currentDivDim.height,
                currentScroll,
              )
            ) {
              newArticle = i;
              newTop = currentDiv.offsetTop;
              newBottom = currentDiv.offsetTop + currentDivDim.height;
              break;
            }
          }
        }
      }

      if (currentArticle !== newArticle) {
        this.changePageUrl(newArticle);
        this.setState({
          currentArticle: newArticle,
          currentTop: newTop,
          currentBottom: newBottom,
        });
      }
    }
  }

  changePageUrl(currentArticle) {
    let url;
    const { data, relatedArticlesData } = this.props;

    const currentStateArticle = this.state.currentArticle;
    // To change history only when article is being changed.
    if (currentArticle !== currentStateArticle) {
      if (TPLib && TPLib.logPoints) {
        TPLib.logPoints(TPLib.ConfigData.tpconfig.ARTICLE_READ, '1234');
      }

      if (currentArticle === 0) {
        document.title =
          data.articleData && data.articleData.pwa_meta ? data.articleData.pwa_meta.title : '';
        url = data.articleData.wu.replace('https://navbharattimes.indiatimes.com', '');
        window.history.pushState({ title: '' }, '', url);
      } else {
        const nextArticle = data.articleData.nextarticles[currentArticle - 1];
        const articleData = relatedArticlesData[currentArticle - 1].articleData;
        const newTitle =
          articleData.pwa_meta && articleData.pwa_meta.title ? articleData.pwa_meta.title : '';
        document.title = newTitle;
        const seolocation = nextArticle.seolocation;
        const id = nextArticle.id;
        window.history.pushState({ title: '' }, '', `/${seolocation}/articleshow/${id}.cms`);
      }

      // if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
      //   analyticsGA.pageview();
      // }

      if (
        typeof AnalyticsComscore !== 'undefined' &&
        typeof AnalyticsComscore.invokeComScore !== 'undefined'
      ) {
        AnalyticsComscore.invokeComScore();
      }
      if (
        typeof AnalyticsIBeat !== 'undefined' &&
        typeof AnalyticsIBeat.initIbeat !== 'undefined'
      ) {
        if (currentArticle === 0 && currentArticle !== -1) {
          AnalyticsIBeat.initIbeat(data.articleData.ibeatconfig);
        } else {
          AnalyticsIBeat.initIbeat(relatedArticlesData[currentArticle - 1].articleData.ibeatconfig);
        }
      }
    }
    return true;
  }

  render() {
    const {
      isFetching,
      data,
      relatedArticlesDataFetched,
      relatedArticlesData,
      loggedIn,
      dispatch,
      showLoginRegister,
      closeLoginRegister,
      location,
    } = this.props;
    const { currentArticle, isCommentVerified } = this.state;
    let showBrandWire = false;
    const { query } = location;
    if ('brandwire' in query) {
      showBrandWire = true;
    }

    const schema = {
      type: 'application/ld+json',
      innerHTML: data && data.articleData && data.articleData.Newsarticle,
    };
    if (!data && !isFetching) {
      // If Error occurs on SSR return null so page can be redirected to 404.
      //if (isCSR()) {
      //this.props.router.push('/404');
      //}
      return null;
    }

    return (
      <div className="articles_container AS">
        <Helmet
          title={data && data.articleData && data.articleData.pwa_meta.title}
          titleTemplate="%s"
          meta={data && data.metaData && data.metaData.metaTags}
          link={data && data.metaData && data.metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          script={[schema]}
        />

        {isFetching && !data ? (
          <ArticleSHowFakeLoader />
        ) : (
          <div style={{ overflow: 'hidden' }} ref={this.article1Ref}>
            <BreadCrumbs data={data && data.articleData && data.articleData.div} />
            {adsPlaceholder(`ATF_AS_STRIPPD_${data && data.articleData && data.articleData.id}`)}
            {adsPlaceholder(`PPD_${data && data.articleData && data.articleData.id}`)}
            {/* <ErrorBoundary>
              <CtnAd
                articleId={data && data.articleData && data.articleData.id}
                height="40"
                width="1000"
                className="colombia"
                slotId={Config.CTN.stripped_pbd_article_show}
              />
            </ErrorBoundary> */}
            {showBrandWire &&
            data &&
            data.articleData &&
            data.articleData.brandwire &&
            data.articleData.brandwire.lnk ? (
              <div className="backToBrandwire">
                <Link to={data.articleData.brandwire.lnk} rel="nofollow">
                  {data.articleData.brandwire.txt}
                </Link>
              </div>
            ) : null}
            <ArticleShow
              data={data}
              articleScrollId={1}
              viralAddaData={data && data.viralAddaData}
              dockAndPlayVideo={this.dockAndPlayVideo}
              dispatch={dispatch}
              loggedIn={loggedIn}
              isCommentVerified={isCommentVerified}
              showSeperator={false}
              showLoginRegister={showLoginRegister}
              closeLoginRegister={closeLoginRegister}
              isFirst
            />
          </div>
        )}

        {relatedArticlesData && Array.isArray(relatedArticlesData)
          ? relatedArticlesData.map((article, i) => (
              <React.Fragment key={`article_${article.articleData.id}`}>
                <div style={{ overflow: 'hidden' }} ref={this[`article${i + 2}Ref`]}>
                  <ArticleShow
                    data={article}
                    showLoginRegister={showLoginRegister}
                    viralAddaData={data ? data.viralAddaData : null}
                    showBreadCrumbs={false}
                    dispatch={dispatch}
                    dockAndPlayVideo={this.dockAndPlayVideo}
                    loggedIn={loggedIn}
                    articleScrollId={i + 2}
                    showSeperator={i <= relatedArticlesData.length - 1}
                  />
                </div>
              </React.Fragment>
            ))
          : this.showLoaderIfNeeded()}
        {data && relatedArticlesDataFetched ? (
          <ErrorBoundary>
            <RelatedArticlesLinkContainer
              data={data.articleData.nextarticles}
              currentArticle={currentArticle}
              scrollToArticle={this.scrollToArticle}
              firstItem={{ id: data.articleData.id, hl: data.articleData.hl }}
            />
          </ErrorBoundary>
        ) : null}

        <div id="AS_Innov1">{adsPlaceholder('AS_Innov1')}</div>
        <div id="OP_Innov1">{adsPlaceholder('OP_Innov1')}</div>
        <div
          id="WSK_160_AS"
          className={`WSK_160_AS_${data && data.articleData && data.articleData.id}`}
        />
        {data && data.articleData && data.articleData.readmorearticle ? (
          <div className="clickherefor6th">
            <Link
              to={data && data.articleData && data.articleData.readmorearticle.wu}
              target="_blank"
            >
              {Config.ArticleShow.read_more_article}
            </Link>
          </div>
        ) : null}
      </div>
    );
  }
}

const RelatedArticlesLinkContainer = ({ data, firstItem, currentArticle, scrollToArticle }) => {
  return data && Array.isArray(data) ? (
    <div className="outerTabContainer">
      <ul>
        <li
          key={firstItem.id}
          onClick={() => scrollToArticle(0)}
          className={currentArticle === 0 ? 'active' : ''}
          title={truncateStr(firstItem.hl, 60)}
        >
          <span>{truncateStr(firstItem.hl, 60)}</span>
        </li>
        {data.map((ra, i) => (
          <li
            className={currentArticle === i + 1 ? 'active' : ''}
            onClick={() => scrollToArticle(i + 1)}
            key={ra.id}
            title={truncateStr(firstItem.hl, 60)}
          >
            <span> {truncateStr(ra.hl, 60)}</span>
          </li>
        ))}
      </ul>
    </div>
  ) : null;
};

RelatedArticlesLinkContainer.propTypes = {
  data: PropTypes.array,
  firstItem: PropTypes.object,
  currentArticle: PropTypes.string,
  scrollToArticle: PropTypes.func,
};

ArticleShowContainer.fetchData = function({ dispatch, query, history, params }) {
  return dispatch(fetchDataIfNeeded(params));
};

ArticleShowContainer.propTypes = {
  dispatch: PropTypes.func,
  showLoginRegister: PropTypes.func,
  closeLoginRegister: PropTypes.func,
  params: PropTypes.object,
  location: PropTypes.object,
  query: PropTypes.object,
  history: PropTypes.object,
  dockedVideo: PropTypes.object,
  data: PropTypes.object,
  isFetchingRelatedArticles: PropTypes.bool,
  relatedArticlesDataFetched: PropTypes.bool,
  isFetching: PropTypes.bool,
  relatedArticlesData: PropTypes.object,
  loggedIn: PropTypes.bool,
  isFetchingNewsData: PropTypes.bool,
  newsDataFetched: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.articleshow,
    ...state.authentication,
    newsDataFetched: state.news.didFetch,
    isFetchingNewsData: state.news.isFetching,
    dockedVideo: state.dockedVideo,
  };
}

export default connect(mapStateToProps)(ArticleShowContainer);
