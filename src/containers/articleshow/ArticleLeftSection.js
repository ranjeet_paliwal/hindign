/* eslint-disable indent */
/* eslint-disable react/prop-types */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import React, { Component } from 'react';
import PropTypes, { string, object } from 'prop-types';
import ReactHtmlParser from 'html-react-parser';
import domToReact from 'html-react-parser/lib/dom-to-react';
import LazyLoad from 'react-lazyload';

import Thumb from '../../components/Thumb/Thumb';
import SocialToolBox from '../../components/SocialToolBox';
import Link from '../../components/Link/Link';
import EmbeddedTweet from '../../components/SocialEmbeds/Twitter/EmbeddedTweet';
import Poll from '../../components/Home/Poll/Poll';
import Videocommon from './subsections/Videocommon';
import { loadJS, truncateStr, setCookie, getCookie, adsPlaceholder } from '../../common-utility';
import EmbeddedInstagram from '../../components/SocialEmbeds/Instagram/EmbeddedInstagram';
import Slider from '../../components/NewSlider/NewSlider';
import NicArticleContainer from './subsections/NicArticleContainer';
import TopComment from './subsections/TopComment';
import CtnAd from './subsections/CtnAd';
import ErrorBoundary from '../../components/lib/errorboundery/ErrorBoundary';
import AmazonWidget from '../../components/AmazonWidget/AmazonWidget';
import GadetsPostreview from './subsections/GadgetPostreview';
import WdtAmazonDetail from '../../components/Amazon/WdtAmazonDetail';
import WdtAffiliatesList from '../../components/Amazon/WdtAffiliatesList';
import Wdt2GudAffiliatesList from '../../components/Amazon/Wdt2GudAffiliatesList';
import VideoWidget from '../../components/Videos/VideoWidget';
import GadgetsRating from '../../components/CommonWidgets/GadgetRatingWidget';

const Config = require(`../../../common/${process.env.SITE}`);

class ArticleLeftSection extends Component {
  state = {
    shownicData: false,
    nicid: null,
    showReviewCard: false,
    imageExpanded: false,
  };

  // eslint-disable-next-line react/sort-comp
  getReviewCard = () => {
    this.setState({
      showReviewCard: true,
    });
  };

  closeReviewCard = () => {
    this.setState({
      showReviewCard: false,
    });
  };

  componentDidMount() {
    const { data } = this.props;
  }

  playVideoDocked = vidData => {
    const { data, dockAndPlayVideo } = this.props;
    if (vidData && typeof dockAndPlayVideo === 'function') {
      dockAndPlayVideo({ videoData: vidData, articleID: data.id });
    }

    const isDisablevideoClass = document.getElementsByClassName('disable_video');
    const els = document.querySelectorAll('.doc_video');
    if (isDisablevideoClass && isDisablevideoClass.length > 0) {
      for (let i = 0; i < els.length; i++) {
        if (els[i] && els[i].classList) {
          els[i].classList.remove('disable_video');
        }
      }
    }

    if (vidData && vidData.eid && data && data.id) {
      const dockedVid = document.getElementById(`DocId_${vidData.eid}_${data.id}`);
      const dockedVidinner = document.getElementById(`DocId_${vidData.eid}_${data.id}_inner`);
      if (dockedVid) {
        dockedVid.classList.add('disable_video');
      }
      if (dockedVidinner) {
        dockedVidinner.classList.add('disable_video');
      }
    }
    const isDisableminitvClass = document.getElementsByClassName('streaming_box');
    if (isDisableminitvClass && isDisableminitvClass.length > 0) {
      document.querySelector('.streaming_box').classList.add('hidecls');
    }
    // disable notificaton popups while PIP is playing
    if (getCookie('tpstoryPopup')) {
      setCookie('tpstoryPopup', 'close', 1, '.indiatimes.com');
    }
  };

  toggleImgExpansion = ThumbClass => {
    if (ThumbClass === 'thumb_img') {
      this.setState({
        imageExpanded: !this.state.imageExpanded,
      });
    }
  };

  renderArticleMainImage = () => {
    const { data } = this.props;

    if (data && data.brandwire) {
      return null;
    }

    if (
      data &&
      data.id &&
      data.lead &&
      data.lead.vdo &&
      Array.isArray(data.lead.vdo) &&
      data.lead.vdo.length > 0 &&
      data.lead.vdo[0]
    ) {
      return (
        <div
          id={`DocId_${data.lead.vdo[0].eid}_${data.id}`}
          className="doc_video posrel_mini"
          role="button"
          tabIndex={0}
        >
          <Videocommon
            slikeid={data.lead.vdo[0].eid}
            height="400"
            width="630"
            imageid={data.lead.vdo[0].imageid}
            imgsize="123"
            vidtitle={data.lead.vdo[0].hl}
            adsection="default"
            pagetype="articleshow"
            articleID={data.lead.vdo[0].id}
          />
        </div>
      );
    }

    // Case if picture exists.

    return (
      <ErrorBoundary>
        <ArticleThumbNail
          data={data && data.image && Array.isArray(data.image) && data.image[0]}
          pagetype={data && data.subsec1 && data.subsec1 === '66063640' ? 'reviews' : ''}
          cr={data && data.cr ? data.cr : ''}
          toggleImgExpansion={this.toggleImgExpansion}
          imageExpanded={this.state.imageExpanded}
        />
      </ErrorBoundary>
    );
  };

  getnicdata = id => {
    this.setState({
      shownicData: true,
      nicid: id,
    });
    document.querySelector('.AS').classList.add('disable-scroll');
  };

  closenic = () => {
    this.setState({
      shownicData: false,
    });
    document.querySelector('.AS').classList.remove('disable-scroll');
  };

  socialShare = (type, data) => {
    switch (type) {
      case 'twitter':
        if (typeof window !== 'undefined') {
          window.open(
            `https://twitter.com/share?url=${`${
              data.url
            }?utm_source=twitter.com&utm_medium=social&utm_campaign=NBTWeb`}&amp;text=${encodeURIComponent(
              data.text,
            )}&via=${Config.SiteInfo.twitterShareId}`,
            'sharer',
            'toolbar=0,status=0,width=626,height=436,scrollbars=1',
          );
        }
        return false;

      case 'fb':
        const url = `${data.url}?utm_source=facebook.com&utm_medium=Facebook&utm_campaign=web`;
        const fbShareParams = { method: 'share', href: url };
        if (data && data.quote) {
          fbShareParams.quote = data.quote;
        }
        if (typeof window !== 'undefined') {
          window.FB.ui(fbShareParams, () => {});
        }
        break;

      default:
    }

    return false;
  };

  renderArticleHtml() {
    const {
      data,
      topCommentsData,
      loggedIn,
      showComments,
      showLoginRegister,
      closeLoginRegister,
      dispatch,
    } = this.props;

    // console.log('domnode story', data);
    if (data && data.Story) {
      return ReactHtmlParser(data.Story, {
        replace: domNode => {
          if (!domNode.attribs) return null;
          const { name } = domNode;
          const { data } = this.props;
          // if(domNode.attribs.class)
          switch (name) {
            // In case of brandwire, last section ( brandwire info data)
            case 'em':
              return <div className="brandwire_info">{domToReact(domNode.children)}</div>;

            case 'facebook':
              return <div className="embed_elements">{domToReact(domNode.children)}</div>;
            case 'img':
              // eslint-disable-next-line consistent-return
              return (
                <div className="image">
                  <Thumb src={domNode.attribs && domNode.attribs.src && `${domNode.attribs.src}`} />
                  {domNode.attribs.title ? (
                    <div className="imagecaption">{domNode.attribs.title}</div>
                  ) : null}
                </div>
              );
            case 'video':
              if (domNode.attribs.type === 'youtube') {
                // eslint-disable-next-line consistent-return
                return (
                  <div
                    className="embedded_videos posrel_mini"
                    id={`DocId_${domNode.attribs.id}_inner`}
                  >
                    <Videocommon
                      slikeid={domNode.attribs.id}
                      height="400"
                      width="630"
                      imageid={domNode.attribs.id}
                      imgsize="123"
                      imgsrc={domNode.attribs.thumburl}
                      vidtitle={domNode.attribs.hl}
                      adsection="default"
                      pagetype="articleshow"
                      articleID="11111"
                    />
                  </div>
                );
                // eslint-disable-next-line no-else-return
              } else {
                const filtervid =
                  data && data.vdo && Array.isArray(data.vdo)
                    ? data.vdo.filter(value => domNode.attribs.id === value.id)
                    : null;
                return Array.isArray(filtervid) && filtervid[0] ? (
                  <div
                    className=" embedded_videos posrel_mini"
                    id={`DocId_${filtervid[0].eid}_${data && data.id}_inner`}
                  >
                    <Videocommon
                      slikeid={filtervid[0].eid}
                      height="400"
                      width="630"
                      imageid={filtervid[0].imageid}
                      imgsize="123"
                      vidtitle={domNode.attribs.hl}
                      adsection="default"
                      pagetype="articleshow"
                      articleID={filtervid[0].id}
                    />
                    <span className="image_caption">{filtervid[0].hl}</span>
                  </div>
                ) : (
                  ''
                );
              }

            case 'slideshow':
              if (data && data.slideshows && Array.isArray(data.slideshows)) {
                const filterslide =
                  data &&
                  data.slideshows &&
                  data.slideshows.filter(value => domNode.attribs.id === value.slideid);
                return (
                  <div>
                    {filterslide.map(value => (
                      <EmbedSlideshow data={value && value.slides} key={value} />
                    ))}
                  </div>
                );
              }
              return null;

            case 'relatedarticle':
              return <RelatedArticles data={data && data.embedarticle} />;

            case 'lead':
              break;
            case 'nic':
              return <NicEmbed data={domNode.children[0]} getnicdata={this.getnicdata} />;

            case 'table':
              return <TableCard data={domNode.attribs.data} />;

            case 'a':
              // eslint-disable-next-line no-case-declarations
              let Catname;
              let Amazondata;
              let amazonPhrase;

              if (domNode && domNode.attribs && domNode.attribs.id === 'amazonwidget') {
                Amazondata =
                  domNode && domNode.children && domNode.children[0] && domNode.children[0].data
                    ? domNode.children[0].data
                    : '';
                if (Amazondata.indexOf('_') > -1) {
                  Catname = Amazondata.split('_')[0];
                  amazonPhrase = Amazondata.split('_')[1];
                } else {
                  // eslint-disable-next-line no-case-declarations
                  Catname =
                    domNode && domNode.children && domNode.children[0] && domNode.children[0].data
                      ? domNode.children[0].data
                      : '';
                  amazonPhrase = data && data.search_phrase ? data.search_phrase : '';
                }
                return typeof window !== 'undefined' ? (
                  // eslint-disable-next-line react/no-danger
                  <div dangerouslySetInnerHTML={{ __html: '' }} />
                ) : (
                  <AmazonWidget category={Catname} phrase={amazonPhrase} />
                );
              }
              // eslint-disable-next-line no-cond-assign
              if (domNode.attribs.id === 'topic') {
                const topicname =
                  domNode && domNode.children && domNode.children[0] && domNode.children[0].data
                    ? domNode.children[0].data
                    : '';
                const topicLink = domNode.attribs.href.includes(
                  'https://navbharattimes.indiatimes.com',
                )
                  ? ''
                  : 'https://navbharattimes.indiatimes.com';
                return (
                  <a
                    id={`${domNode.attribs.id}`}
                    href={`${topicLink}${domNode.attribs.href}`}
                    className="linkclass"
                  >
                    {topicname}
                  </a>
                );
              }
              if (
                domNode.attribs.id === 'tilCustomLink' &&
                domNode.attribs.href.includes('https://www.amazon.in/')
              ) {
                const amazoncustomlink =
                  domNode.children && domNode.children[0] && domNode.children[0].data
                    ? domNode.children[0].data
                    : '';
                const amazonUrl = domNode.attribs.href;
                const amazoninterURL = encodeURIComponent(amazonUrl);
                return (
                  <a
                    className="linkclass"
                    target="_blank"
                    rel="nofollow noindex"
                    href={`https://navbharattimes.indiatimes.com/affiliate_amazon.cms?url=${amazoninterURL}`}
                  >
                    {amazoncustomlink}
                  </a>
                );
              }
              const Linktext =
                domNode.children && domNode.children[0] && domNode.children[0].data
                  ? domNode.children[0].data
                  : '';
              const hyperLink =
                domNode.children && domNode.children[0] && domNode.children[0].data
                  ? domNode.children[0].parent.attribs.href
                  : '';
              return (
                <Link className="linkclass" to={hyperLink}>
                  {Linktext}
                </Link>
              );

              break;

            case 'quote':
              return (
                <QuoteBlock
                  quote={domNode.attribs && domNode.attribs.text}
                  author={domNode.attribs && domNode.attribs.author}
                />
              );
            case 'embed':
              if (domNode.attribs.id === 'topComments') {
                return (
                  <TopComment
                    data={topCommentsData}
                    loggedIn={loggedIn}
                    showComments={showComments}
                  />
                );
              }
              break;

            case 'i':
              return <br />;

            case 'instagram':
              return <EmbeddedInstagram>{domToReact(domNode.children)}</EmbeddedInstagram>;

            case 'twitter':
              return <EmbeddedTweet tweetId={domNode.attribs && domNode.attribs.id} />;

            case 'poll':
              return (
                <Poll
                  pollid={domNode.attribs.id}
                  showLoginRegister={showLoginRegister}
                  closeLoginRegister={closeLoginRegister}
                />
              );

            case 'ad':
              return (
                <CtnAd
                  articleId={data ? data.id : ''}
                  position={data ? data.id : ''}
                  height="360"
                  width="630"
                  className="BlyBlk colombia"
                  slotId={Config.CTN.id_in_content_article_show}
                  style={{ float: 'left', minHeight: '2px', width: '100%' }}
                />
              );

            default:
              return '';
          }
        },
      });
    }
    return null;
  }

  render() {
    const {
      data,
      commentsData,
      showLoginRegister,
      showComments,
      showMailBox,
      loggedIn,
      articleScrollId,
    } = this.props;
    const { shownicData, nicid, showReviewCard } = this.state;
    // console.log('showReviewCard', showReviewCard);
    // console.log('====articleshow data==', data);
    const twitterData = {
      url: data ? data.wu : '',
      text: data && data.hl ? data.hl : '',
      via: Config.twitterHandle,
    };

    const fbData = {
      url: (data && data.wu) || '',
      quote: (data && data.hl) || '',
    };

    //const authorData = data && data.auid ? `${data.bl}, ` : '';

    const authorData =
      data && data.auid
        ? `${data.bl} | `
        : data && data.pwa_meta && data.pwa_meta.editornameseo && data.pwa_meta.editornameseo !== ''
        ? `Edited By ${data.pwa_meta.editornameseo} | `
        : '';

    // <Link
    //   to={`${process.env.WEBSITE_URL}/nbtreporter/author-${data.aufullname.replace(' ', '-')}-${
    //     data.auid
    //   }.cms`}
    //   className="author-byline"
    // >
    //   {data.bl}
    // </Link>
    //  : (
    //   ''
    // );
    const gadVarients = data && data.gadgetinfo && data.gadgetinfo.name ? data.gadgetinfo.name : '';
    const gadVarientsseo =
      data && data.gadgetinfo && data.gadgetinfo.nameseo ? data.gadgetinfo.nameseo : '';
    const gadgetCategory =
      data && data.gadgetinfo && data.gadgetinfo.categoryseo ? data.gadgetinfo.categoryseo : '';
    const isReviewData = !!(data && data.secname === 'reviews');

    const random1 = Math.floor(Math.random() * 10000);
    const random2 = Math.floor(Math.random() * 10000);
    // console.log("==random1==",random1);
    // console.log("==random2==",random2);

    return (
      <div className="leftmain">
        {shownicData ? (
          <ErrorBoundary>
            <NicArticleContainer id={nicid} closenic={this.closenic} />
          </ErrorBoundary>
        ) : null}
        <h1>
          {(data && data.hl) || ''}
          {data && data.disclaimer_image ? (
            <ErrorBoundary>
              <DisclaimerIcon id={data.disclaimer_image} />
            </ErrorBoundary>
          ) : (
            ''
          )}
        </h1>
        <div className="article_brandwire">{(data && data.brandwire && data.brandstory) || ''}</div>
        <div className="wdt_lft_rgt">
          <div className="article_datetime">
            {authorData}
            {`${data && data.ag ? `${data.ag}` : ''} `}
            {data.subsec1 === '54234500' ? (
              <React.Fragment>
                {data.cruthImage ? <Thumb src={data.crauthImage} height="50" width="50" /> : null}
                <span className="name_author">{data.dl} IST</span>
              </React.Fragment>
            ) : (
              <time dateTime={(data && data.luseo) || ''}>
                Updated {`${data && data.lu ? `${data.lu} IST` : ''} `}
              </time>
            )}
          </div>
          <ErrorBoundary>
            <SocialToolBox
              fb
              comments
              twitter
              email
              showMailBox={showMailBox}
              twitterData={twitterData}
              fbData={fbData}
              showComments={showComments}
              socialShare={this.socialShare}
            />
          </ErrorBoundary>
        </div>
        <ErrorBoundary>
          {data && data.syn ? <h2 className="article_synopics">{data && data.syn}</h2> : null}
          {data && data.disclaimer ? (
            <div className="cont_disclaimer">
              <span>{Config.Locale.articleshow.disclaimer}: </span>
              {data.disclaimer}
            </div>
          ) : (
            ''
          )}
        </ErrorBoundary>
        <div className="article_txt" itemProp="articleBody">
          <section
            id={data && data.substory ? `photo_${data.id}` : ''}
            first_url={data && data.substory ? data.wu : ''}
            className={data && data.substory ? 'psstory-seperator' : ''}
          >
            {data && data.substory && data.substory instanceof Array ? (
              <Thumb
                width="630"
                height="472"
                imgId={data.image[0].id}
                islinkable={false}
                alt={data.image[0].cap}
                title={data.image[0].cap}
                resizeMode="true"
              />
            ) : (
              this.renderArticleMainImage()
            )}
            {/* {data && data.lead && data.lead.vdo && data.relatedvideos ? (
            <ErrorBoundary>
              <RelatedVideos data={data && data.relatedvideos} count="4" />
            </ErrorBoundary>
          ) : null} */}
            {data &&
              data.embedhighlights &&
              data.embedhighlights.artsummary &&
              data.embedhighlights.artsummary.ul && (
                <ErrorBoundary>
                  <ArticleHighlights
                    data={data.embedhighlights.artsummary.ul}
                    articleHasVideo={Boolean(data && data.lead && data.lead.vdo)}
                    hgltclass={
                      data && data.lead && data.lead.vdo ? 'highlights pos_3rd' : 'highlights'
                    }
                  />
                </ErrorBoundary>
              )}

            {this.renderArticleHtml()}
          </section>
          {data.substory &&
            data.substory instanceof Array &&
            data.substory.map((_item, index) => {
              return (
                <section
                  id={`photo_${_item.msid}`}
                  key={'section' + index}
                  sub_url={_item.sub_url}
                  className="psstory-seperator"
                >
                  <h2>{_item.h2}</h2>

                  {this.renderArticleHtml()}
                </section>
              );
            })}

          {data && data.gadgetinfo && data.gadgetinfo.variants && data.gadgetinfo.variants ? (
            <GadgetsVarients
              data={data}
              gadVarients={gadVarients}
              gadgetCategory={gadgetCategory}
            />
          ) : (
            ''
          )}

          {data && data.pwa_meta.amazonkey ? (
            <ErrorBoundary>
              <WdtAmazonDetail
                affview="3"
                type="articleshow"
                productid={data.pwa_meta.amazonkey}
                tag={
                  data.pwa_meta.subpagetype == 'techreview'
                    ? Config.affiliateTags.RS
                    : Config.affiliateTags.AS
                }
                noimg="0"
                msid={data.id}
              />
            </ErrorBoundary>
          ) : (
            <ErrorBoundary>
              <WdtAffiliatesList
                title="यहां से खरीदें"
                category="mobile"
                tag={Config.affiliateTags.AS}
                isslider="1"
                noimg="0"
                noh2="0"
              />
            </ErrorBoundary>
          )}

          {data && data.twogud ? (
            <ErrorBoundary>
              <Wdt2GudAffiliatesList
                twogudData={data.twogud}
                brandName={data.twogud.brandname}
                price={data.twogud.price}
                productName={data.twogud.productname}
                tag={
                  data.pwa_meta.subpagetype == 'techreview'
                    ? 'nbt_web_reviewshow_2GudWidget'
                    : 'nbt_web_articleshow_2GudWidget'
                }
                msid={data.id}
              />
            </ErrorBoundary>
          ) : (
            ''
          )}

          {data && data.gadgetinfo && data.gadgetinfo.specs && data.gadgetinfo.specs.keyFeatures ? (
            <GadgetsKeyfeatures
              data={data}
              gadVarients={gadVarients}
              gadVarientsseo={gadVarientsseo}
              gadgetCategory={gadgetCategory}
            />
          ) : (
            ''
          )}

          <div className="videowidget_LEFT">
            <VideoWidget type="AS" />
          </div>

          {data && data.superhitwidget && Array.isArray(data.superhitwidget.newsItem.items) ? (
            <LazyLoad once height="100px">
              <ErrorBoundary>
                <div className="full_section pd_suggested_slider ui_slider related-news">
                  <h2>{Config.Locale.trendingTechNews}</h2>
                  <Slider
                    type="relatedNews"
                    size="3"
                    // Filter current article from slider data.
                    sliderData={data.superhitwidget.newsItem.items.filter(
                      superHitData => superHitData.id !== data.id,
                    )}
                    utmParameters={{
                      utm_source: 'techmostreadwidget',
                      utm_medium: 'referral',
                    }}
                    SliderClass="photoslider"
                    islinkable="true"
                    sliderWidth="300"
                    headingRequired="yes"
                  />
                </div>
              </ErrorBoundary>
            </LazyLoad>
          ) : (
            ''
          )}

          {isReviewData && data && data.gadgetinfo && data.gadgetinfo.ratings ? (
            <GadgetsRating
              data={{ rating: data.gadgetinfo.ratings, id: data.id, ur: data.ur }}
              getreview={this.getReviewCard}
              showLoginRegister={showLoginRegister}
              loggedIn={loggedIn}
              closereview={this.closeReviewCard}
              showReviewCard={showReviewCard}
            />
          ) : (
            ''
          )}

          <ErrorBoundary>
            {data &&
            data.subsec1 &&
            data.subsec1 === '66063640' &&
            isReviewData &&
            data &&
            data.gadgetinfo &&
            data.gadgetinfo.ratings ? (
              <CommentsSection
                data={commentsData}
                loggedIn={loggedIn}
                showComments={showComments}
                type="reviews"
              />
            ) : (
              ''
            )}
          </ErrorBoundary>
          {data && data.nextart ? (
            <div className="jump-to-nextstory">
              <h2>{Config.Locale.nextStory}</h2>
              <div className="next-txt">
                <Thumb
                  width="57"
                  imgId={data && data.nextart && data.nextart[0].id}
                  islinkable="true"
                  link={`/${data.nextart[0].seolocation}/articleshow/${data.nextart[0].id}.cms`}
                  alt={data && data.image && data.image[0].cap}
                  title={data && data.image && data.image[0].cap}
                  resizeMode="4"
                />
                <Link
                  to={`/${data.nextart[0].seolocation}/articleshow/${data.nextart[0].id}.cms`}
                  className="text_ellipsis"
                >
                  {data.nextart[0].hl}
                </Link>
              </div>
            </div>
          ) : (
            ''
          )}
          {data && data.lead && data.lead.vdo && data.image && data.image[0] && data.image[0].id ? (
            <ErrorBoundary>
              <div>
                <Thumb
                  width="630"
                  border="400"
                  imgId={data.image[0].id}
                  islinkable={false}
                  alt={data && data.image && data.image[0].cap}
                  title={data && data.image && data.image[0].cap}
                  resizeMode="4"
                />
                <span className="desc">
                  <p>
                    {data && data.image && data.image[0] && data.image[0].cap
                      ? data.image[0].cap
                      : ''}
                  </p>
                </span>
              </div>
            </ErrorBoundary>
          ) : null}
        </div>
        {adsPlaceholder(`ROS_TextLink_${data.id}`)}
        {data && data.secid === '3041664' && (
          <div className="frm">
            <iframe
              src="https://navbharattimes.indiatimes.com/wdt_amazon_slider.cms"
              id=""
              frameBorder="0"
              scrolling="no"
              width="630"
              height="305"
              title="amazon Frame"
            />
          </div>
        )}
        {data && data.hl && data.override && (
          <div className="bottom_videos">
            <strong>In Videos: </strong>
            <Link to={data.override}>{data.hl}</Link>
          </div>
        )}
        <FacebookLike data={data && data.fbliner} />
        <WebTitle title={data && data.webtitle} />
        <ArticleTopicLinks
          data={data && data.keywords}
          topicskeylink={data && data.pwa_meta && data.pwa_meta.topicskeylink}
        />
        <ErrorBoundary>
          <SeoDescription data={data && data.seodescription} />
        </ErrorBoundary>
        {data && data.id ? (
          <React.Fragment>
            {/* <CtnAd
              articleId={`${data.id}0${random1}`}
              position={`${data.id}0${random1}`}
              height="683"
              width="630"
              className="colombia"
              slotId={Config.CTN.id_article_show}
            /> */}
            <CtnAd
              articleId={`${random1}`}
              position={`${random1}`}
              height="683"
              width="630"
              className="colombia"
              slotId={Config.CTN.id_article_show}
            />
          </React.Fragment>
        ) : null}
        {data &&
        data.relatedvideos &&
        Array.isArray(data.relatedvideos) &&
        data.relatedvideos.length > 0 ? (
          <div className="wdt_popular_videos">
            <ErrorBoundary>
              <Slider
                type="relatedvdoslider"
                size="3"
                sliderData={data.relatedvideos}
                width="190"
                height="107"
                SliderClass="popular_videos"
                islinkable="true"
                link={Config.Locale.articleshow.relatedvideoLink}
                sliderWidth="590"
                secname={Config.Locale.articleshow.relatedvideos}
              />
            </ErrorBoundary>
          </div>
        ) : null}
        {adsPlaceholder(`Inuxu_${data.id}`)}

        <ErrorBoundary>
          {data &&
          data.subsec1 &&
          data.subsec1 !== '66063640' &&
          !(isReviewData && data && data.gadgetinfo && data.gadgetinfo.ratings) ? (
            <CommentsSection
              data={commentsData}
              loggedIn={loggedIn}
              showComments={showComments}
              type="articleshow"
            />
          ) : (
            ''
          )}
        </ErrorBoundary>
      </div>
    );
  }
}

const GadgetsVarients = ({ data, gadVarients, gadgetCategory }) => {
  return (
    <div className="full_section wdt_varients">
      <h2>
        <span>{Config.Locale.tech.othervariants}</span>
      </h2>
      <ul>
        <li className="active">
          <span>{gadVarients}</span>
        </li>
        {data.gadgetinfo.variants.map(value => {
          return (
            // eslint-disable-next-line react/jsx-key
            <li>
              <Link to={`${process.env.WEBSITE_URL}/${gadgetCategory}/${value.variantseo}`}>
                {value.variants}
              </Link>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

const GadgetsKeyfeatures = ({ data, gadVarients, gadVarientsseo, gadgetCategory }) => {
  const keyFeaturesObj = data.gadgetinfo.specs.keyFeatures;
  const categoryName = gadgetCategory === 'mobile' ? 'mobile-phones' : gadgetCategory;

  return Object.keys(keyFeaturesObj) ? (
    <div className="wdt_summary">
      <h2>
        <Link to={`/tech/${categoryName}/${gadVarientsseo}`}>{gadVarients}</Link>
      </h2>
      <table>
        <tr>
          <th colSpan="2">{Config.Locale.summary}</th>
        </tr>
        {Object.values(keyFeaturesObj).map((keyFeaturesObj, index) => {
          return keyFeaturesObj != 'regional' ? (
            <React.Fragment>
              <tr>
                <td>
                  {keyFeaturesObj && keyFeaturesObj.key && keyFeaturesObj.key.replace('_', ' ')}
                </td>
                <td>{keyFeaturesObj != '' ? keyFeaturesObj.value : '-'}</td>
              </tr>
            </React.Fragment>
          ) : null;
        })}
      </table>
      <div className="read_more">
        <Link to={`/tech/${categoryName}/${gadVarientsseo}#specification_${gadVarientsseo}`}>
          {Config.Locale.seefullspecification}
        </Link>
      </div>
    </div>
  ) : (
    ''
  );
};

// const GadgetsRating = ({ data, getreview, closereview, showReviewCard }) => {
//   let max = 0;
//   const defaultVal = [5, 4, 3, 2, 1];
//   const rateArr = {};
//   // eslint-disable-next-line no-unused-expressions
//   data && data.gadgetinfo && data.gadgetinfo.rating && Array.isArray(data.gadgetinfo.rating)
//     ? data.gadgetinfo.rating.map(item => {
//         // debugger;
//         const gadRate = Math.round(parseInt(item.rating) / 2);
//         rateArr[gadRate] = item.count;
//         max = parseInt(item.count) > max ? parseInt(item.count) : max;
//       })
//     : '';
//   return defaultVal ? (
//     <div className="full_section wdt_review_rate">
//       <h2>
//         <span>{Config.Locale.userreviewandrating}</span>
//       </h2>
//       <div className="rating-values">
//         <div className="user_review">
//           {' '}
//           {data && data.ur ? (
//             <React.Fragment>
//               <span>
//                 {data.ur}
//                 <sub>/5</sub>
//               </span>
//               {/* <b>{Config.Locale.averageuserrating}</b> */}
//             </React.Fragment>
//           ) : (
//             ''
//           )}
//         </div>
//         <div className="user_star">
//           <ul>
//             {defaultVal.map(item => {
//               const gadgetWidth =
//                 typeof rateArr[item] !== 'undefined'
//                   ? `${parseInt((rateArr[item] / max) * 100)}%`
//                   : '0%';
//               return (
//                 // eslint-disable-next-line react/jsx-key
//                 <li>
//                   <span className="star_txt">{item} &#9733;</span>
//                   <span className="star_bg">
//                     <span
//                       className={`star${item}`}
//                       style={{ width: gadgetWidth }}
//                       data-attr={typeof rateArr[item] !== 'undefined' ? rateArr[item] : '0'}
//                     />
//                   </span>
//                 </li>
//               );
//             })}
//           </ul>
//         </div>
//       </div>
//       <button onClick={getreview} showReviewCard={showReviewCard} className="btn-blue">
//         {Config.Locale.ratedevice}
//       </button>
//       {showReviewCard ? (
//         <div className="rating_popup">
//           <div className="container">
//             <header>
//               <h3>{Config.Locale.reviewdevice}</h3>
//               <span onClick={closereview} className="close_icon" />
//             </header>
//             <GadetsPostreview reviewID={data && data.id ? data.id : ''} />
//           </div>
//         </div>
//       ) : (
//         ''
//       )}
//     </div>
//   ) : (
//     ''
//   );
// };

const SeoDescription = ({ data }) => {
  if (data) {
    if (typeof data === 'string') {
      return <div className="seo_description"> {ReactHtmlParser(data)} </div>;
    }
    if (typeof data === 'object' && Array.isArray(data['#text'])) {
      return (
        <div className="keyinfo no-pipe no-border">
          {data['#text'].map((txt, i) => (
            <React.Fragment key={`seo_desc_${txt}`}>
              {txt}
              {i >= data.a.length ? null : (
                <Link to={data.a[i]['@href']}>{data.a[i]['#text']}</Link>
              )}
            </React.Fragment>
          ))}
        </div>
      );
    }
  }
  return null;
};

SeoDescription.propTypes = {
  data: PropTypes.oneOf(string, object),
};

const FacebookLike = ({ data }) => (
  <div className="bottom_fb_link">
    <div className="txt">
      {data && Array.isArray(data) && data.length > 0
        ? data.map(item => {
            if ('wu' in item) {
              return <Link to={item.wu}>{`${item.hl} `}</Link>;
            }
            return `${item.hl} `;
          })
        : null}
    </div>
    <div
      className="fb-like"
      data-href="https://www.facebook.com/NBTTechnology/"
      data-layout="button_count"
      data-action="like"
      data-size="small"
      data-show-faces="false"
      data-share="false"
    />
  </div>
);

FacebookLike.propTypes = {
  data: PropTypes.array,
};

const RelatedVideos = ({ data, count }) => {
  return data && Array.isArray(data) ? (
    <React.Fragment>
      <div className="related_videos">
        <h3>{Config.Locale.articleshow.relatedvideos}</h3>
        <ul>
          {data.map((value, index) => {
            if (index <= count - 1) {
              return (
                <li key={value.id}>
                  <Thumb
                    width="85"
                    border="52"
                    imgId={value.imageid}
                    alt={value.hl}
                    title={value.hl}
                    islinkable="true"
                    link={value.wu}
                    isvdo="true"
                    resizeMode="4"
                  />
                  <Link to={value.wu} className="rlvdtxt">
                    {truncateStr(value.hl, 35)}
                  </Link>
                </li>
              );
            }
          })}
        </ul>
      </div>
      <div className="clear" />
    </React.Fragment>
  ) : null;
};

RelatedVideos.propTypes = {
  data: PropTypes.array,
};

const DisclaimerIcon = ({ id }) => {
  const height = id === '53824389' ? '50' : '28';
  const width = id === '53824389' ? '16' : '28';
  return (
    <span className="fakenews">
      <Thumb width={width} height={height} imgId={id} islinkable={false} resizeMode="4" />
    </span>
  );
};

DisclaimerIcon.propTypes = {
  id: PropTypes.string,
};

const WebTitle = ({ title }) => (
  <div className="web_title" data-articlettl={title}>
    <strong>Web Title</strong>&nbsp;
    {`${title}`}
    <span>
      (
      <Link target="_blank" style={{ fontFamily: 'arial' }} to={process.env.BASE_URL}>
        {`News in ${Config.SiteInfo.lang}`}
      </Link>
      <small />
      <small>&nbsp;from {Config.seoConfig.englishName} , TIL Network)</small>
    </span>
  </div>
);

WebTitle.propTypes = {
  title: PropTypes.string,
};

const RelatedArticles = ({ data }) => {
  return data ? (
    <div className="relatedBoxBot">
      <h3>संबंधित खबरें</h3>
      <ul className="list">
        {Array.isArray(data) &&
          data.map(ra => (
            <li key={ra.msid}>
              <Link to={`${process.env.WEBSITE_URL}/${ra.seolocation}/articleshow/${ra.msid}.cms`}>
                <h4>{ra.arttitle}</h4>
              </Link>
            </li>
          ))}
      </ul>
    </div>
  ) : null;
};

RelatedArticles.propTypes = {
  data: PropTypes.array,
};

const EmbedSlideshow = ({ data }) => {
  return data ? (
    <div className="imgVidSlider">
      {data[0].hl ? <h2> {data[0].hl}</h2> : null}
      <Slider
        type="embedslider"
        size="1"
        sliderData={data}
        width="630"
        height="472"
        SliderClass="embedslider"
        islinkable={false}
        link={data.wu}
        sliderWidth="630"
      />
    </div>
  ) : null;
};

EmbedSlideshow.propTypes = {
  data: PropTypes.array,
};

const NicEmbed = ({ data, getnicdata }) => {
  return data ? (
    <span onClick={() => getnicdata(data.attribs.id)} className="nic_icon_txt">
      {data && data.children && data.children[0] ? data.children[0].data : ''}
    </span>
  ) : null;
};

NicEmbed.propTypes = {
  data: PropTypes.array,
};

const TableCard = ({ data }) => {
  let TabledataArr = [];
  if (data) {
    TabledataArr = data.split('-$|$-');
  }
  return Array.isArray(TabledataArr) && TabledataArr.length > 0 ? (
    <table border="1">
      <tbody>
        {TabledataArr.map((value, index) => {
          const gettds = value.split('-|$|-');
          return (
            <tr key={index}>
              {Array.isArray(gettds) &&
                gettds.length > 0 &&
                gettds.map((value, index) => {
                  const href = (value && value.split('|').length > 0 && value.split('|')[1]) || '';
                  const tdValue =
                    (value && value.split('|').length > 0 && value.split('|')[0]) || '';
                  const getTableData =
                    value && value.includes('|') ? <Link to={href}>{tdValue}</Link> : value;
                  return <td key={index}>{getTableData}</td>;
                })}
            </tr>
          );
        })}
      </tbody>
    </table>
  ) : null;
};

TableCard.propTypes = {
  data: PropTypes.array,
};

const QuoteBlock = ({ quote, author }) => (
  <div className="quotediv">
    <span className="start-quote">
      <b />
    </span>
    <div className="quote_txt">{`"${quote}"`}</div>
    <div className="quote_author">{`-${author}`}</div>
    <span className="end-quote">
      <b />
    </span>
  </div>
);

QuoteBlock.propTypes = {
  quote: PropTypes.string,
  author: PropTypes.string,
};

const ArticleThumbNail = ({ data, pagetype, cr, toggleImgExpansion, imageExpanded }) => {
  const Imgwidth = pagetype === 'reviews' ? 630 : 400;
  const ThumbClass = pagetype === 'reviews' ? 'thumb_img_reviews' : 'thumb_img';
  return data ? (
    <div
      className={`${
        ThumbClass === 'thumb_img' && imageExpanded ? `${ThumbClass} expanded` : ThumbClass
      } `}
      onClick={() => toggleImgExpansion(ThumbClass)}
    >
      <Thumb
        width={Imgwidth}
        border="0"
        imgId={data.id}
        alt={data.cap}
        title={data.cap}
        imgSize={data && data.imgsize ? data.imgsize : ''}
        resizeMode="4"
      />
      {pagetype == 'reviews' ? (
        <div className="image_caption">
          <span className="lft">
            <span className="head">
              <b className="icon_star one" />
              {Config.Locale.criticsrating}
            </span>
            <span className="rate_txt">
              {cr !== '' ? (
                <span>
                  {cr}
                  <sub>/5</sub>
                </span>
              ) : (
                'NA'
              )}
            </span>
          </span>
          {data && data.cap ? (
            <span className="rgt">
              <span className="head">{Config.Locale.specialfeatures}</span>
              <span className="features_txt">
                <ul>
                  {data && data.topfeature
                    ? data.topfeature.map(value => {
                        return <li>{value}</li>;
                      })
                    : ''}
                </ul>
              </span>
            </span>
          ) : (
            ''
          )}
        </div>
      ) : (
        <span className="image_caption">{data.cap}</span>
      )}
    </div>
  ) : null;
};

ArticleThumbNail.propTypes = {
  data: PropTypes.object,
};

const ArticleHighlights = ({ data, hgltclass, articleHasVideo }) => {
  return data && Array.isArray(data) ? (
    <React.Fragment>
      <div className={hgltclass}>
        <span className="txt">{Config.Locale.articleshow.highlights}</span>
        <ul>
          {data.map(value => (
            <li key={value}>{value}</li>
          ))}
        </ul>
      </div>
      {/* {articleHasVideo ? null : <div className="clear" />} */}
    </React.Fragment>
  ) : null;
};

ArticleHighlights.propTypes = {
  data: PropTypes.array,
  hgltclass: PropTypes.string,
  articleHasVideo: PropTypes.bool,
};

const ArticleTopicLinks = ({ data, topicskeylink }) => {
  return data && Array.isArray(data) && data.length > 0 ? (
    <div className="keyinfo">
      <strong>{`${Config.Locale.articleshow.knowmore}:`}</strong>
      {data.map(link => {
        if (topicskeylink) {
          return (
            <Link key={link.hl} to={link.wu} target="_blank">
              {link.hl}
            </Link>
          );
        }
        return <span key={link.hl}>{link.hl}</span>;
      })}
    </div>
  ) : null;
};

ArticleTopicLinks.propTypes = {
  data: PropTypes.array,
  topicskeylink: PropTypes.string,
};

const CommentsSection = ({ data, showComments, type }) => {
  const removeSpcChar = str => {
    const removed = str.replace(/<\/?[^>]+(>|$)/g, '');
    return removed;
  };
  if (type === 'reviews') {
    return (
      <div className="comments-wrapper" onClick={showComments} role="button" tabIndex={0}>
        <button className="btn-blue">{Config.ArticleShow.text_write_reviews}</button>
        {data && Array.isArray(data) && data.length > 0 ? (
          <div className="bottom-comments">
            {data.slice(1, 3).length > 0 &&
              data.slice(1).map(c => (
                <div className="comment-box" key={c.C_T}>
                  <div className="user-thumbnail">
                    <Thumb
                      className="userimg flL"
                      src={
                        (c.user_detail && c.user_detail.thumb) ||
                        `${process.env.WEBSITE_URL}${Config.Thumb.userImage}`
                      }
                    />
                  </div>
                  {'user_detail' in c && c.user_detail.FL_N ? (
                    <Link
                      to={c.A_ID ? `${Config.profile.url}/${c.A_ID}` : ''}
                      className="name"
                      target="_blank"
                    >
                      {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                    </Link>
                  ) : null}
                  <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                  <div className="footbar clearfix">
                    <span
                      data-action="comment-reply"
                      className="cpointer"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      Reply
                    </span>
                    {/* <span
                      className="up cpointer"
                      data-action="comment-agree"
                      title="Up Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-uparrow" />
                    </span>
                    <span
                      className="down cpointer"
                      data-action="comment-disagree"
                      title="Down Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-downarrow" />
                    </span> */}
                  </div>
                </div>
              ))}
            <div onClick={showComments} role="button" tabIndex={0} className="read-more">
              {Config.Locale.readMoreReview}
            </div>
          </div>
        ) : null}
      </div>
    );
  }
  return (
    <div className="comments-wrapper" onClick={showComments} role="button" tabIndex={0}>
      <div className="bottom-comments-btns">
        <div className="cmtbtn-wrapper">
          {data && Array.isArray(data) && data.length > 0 ? (
            <span className="cmtbtn view" onClick={showComments} role="button" tabIndex={0}>
              {`${Config.ArticleShow.text_view_comment} (${data[0].cmt_c})`}
              <b />
            </span>
          ) : null}
          <span
            className="cmtbtn add"
            onClick={showComments}
            style={{ width: data.length === 0 ? '50%' : '35%' }}
            role="button"
            tabIndex={0}
          >
            {type === 'reviews' ? (
              <span>{Config.ArticleShow.text_write_reviews}</span>
            ) : (
              <span>
                {Config.ArticleShow.text_write_comment}
                <b />
              </span>
            )}
          </span>
        </div>
      </div>
      {data && Array.isArray(data) && data.length > 0 ? (
        <div className="bottom-comments">
          {data.slice(1).length > 0 &&
            data.slice(1, 3).map(c => (
              <div className={`comment-box${c && c.C_A_ID ? ' authorComment' : ''}`} key={c.C_T}>
                <div className="user-thumbnail">
                  <Thumb
                    className="userimg flL"
                    src={
                      (c.user_detail && c.user_detail.thumb) ||
                      `${process.env.WEBSITE_URL}${Config.Thumb.userImage}`
                    }
                  />
                </div>
                {'user_detail' in c && c.user_detail.FL_N ? (
                  <Link
                    to={c.A_ID ? `${Config.profile.url}/${c.A_ID}` : ''}
                    className="name"
                    target="_blank"
                  >
                    {c.user_detail && c.user_detail.FL_N ? c.user_detail.FL_N : c.A_D_N}
                  </Link>
                ) : null}
                <p className="short_comment">{removeSpcChar(c.C_T)}</p>
                <div className="footbar clearfix">
                  <span
                    data-action="comment-reply"
                    className="cpointer"
                    onClick={() => showComments()}
                    role="button"
                    tabIndex={0}
                  >
                    Reply
                  </span>
                  {/* <span
                      className="up cpointer"
                      data-action="comment-agree"
                      title="Up Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-uparrow" />
                    </span>
                    <span
                      className="down cpointer"
                      data-action="comment-disagree"
                      title="Down Vote"
                      onClick={() => showComments()}
                      role="button"
                      tabIndex={0}
                    >
                      <i className="icon-downarrow" />
                    </span> */}
                </div>
              </div>
            ))}
          <div onClick={showComments} role="button" tabIndex={0} className="read-more">
            {Config.Locale.readmMorecomment}
          </div>
        </div>
      ) : null}
    </div>
  );
};

CommentsSection.propTypes = {
  data: PropTypes.array,
  loggedIn: PropTypes.bool,
  showComments: PropTypes.func,
};

ArticleLeftSection.propTypes = {
  data: PropTypes.array,
  commentsData: PropTypes.array,
  topCommentsData: PropTypes.array,
  showComments: PropTypes.bool,
  showNewsLetterBox: PropTypes.bool,
  showMailBox: PropTypes.bool,
  loggedIn: PropTypes.bool,
  showLoginRegister: PropTypes.func,
  closeLoginRegister: PropTypes.func,
  dockAndPlayVideo: PropTypes.func,
};

export default ArticleLeftSection;
