import React from 'react';
import PropTypes from 'prop-types';

import Link from '../../../components/Link/Link';
import Thumb from '../../../components/Thumb/Thumb';
import { truncateStr } from '../../../common-utility';

const VideoSection = ({ data }) => {
  return data ? (
    <div className="section video_list_as">
      <h2>
        <Link target="_blank" to={data.wu}>
          {data.secname}
        </Link>
      </h2>
      <ul>
        {data &&
          data.items &&
          Array.isArray(data.items) &&
          data.items.length > 0 &&
          data.items.map(vidData => {
            return vidData && typeof vidData === 'object' && vidData.constructor === Object ? (
              <li key={vidData.id}>
                <Link target="_blank" to={vidData.wu} className="video_icon_thumb">
                  <Thumb
                    imgId={vidData.imageid}
                    width={142}
                    height={104}
                    imgSize={vidData.imgsize}
                    alt={vidData.hl}
                    title={vidData.hl}
                  />
                </Link>

                {typeof truncateStr === 'function' && (
                  <Link target="_blank" to={vidData.wu} className="txt_container">
                    {truncateStr(vidData.hl, 40)}
                  </Link>
                )}
              </li>
            ) : null;
          })}
      </ul>
    </div>
  ) : null;
};

VideoSection.propTypes = {
  data: PropTypes.object,
};

export default VideoSection;
