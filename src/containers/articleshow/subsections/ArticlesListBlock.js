/* eslint-disable operator-linebreak */
import React from 'react';
import PropTypes from 'prop-types';
import Link from '../../../components/Link/Link';
import Thumb from '../../../components/Thumb/Thumb';

const ArticlesListBlock = React.memo(props => {
  const { data, widgetClassName } = props;
  const widgetClass = widgetClassName || '';

  return data && data.items && Array.isArray(data.items) && data.items.length > 0 ? (
    <div
      className={`section all_image_view ${
        data.id === 'trending' ? 'trending-topics' : ''
      } ${widgetClass}`}
    >
      <h2>
        <span>{data.secname}</span>
      </h2>
      <div className="wdt_content">
        <ul>
          {data.items.map(item => (
            <li key={item.id}>
              <span className="img_container">
                <Thumb
                  imgId={item.imageid}
                  imgSize={item.imgsize}
                  width="115"
                  height="85"
                  islinkable="true"
                  link={item.wu}
                  alt={item.hl}
                  title={item.hl}
                />
              </span>
              <span className="txt_container">
                <Link to={item.wu}>{item.hl}</Link>
              </span>
            </li>
          ))}
        </ul>
      </div>
    </div>
  ) : null;
});

ArticlesListBlock.propTypes = {
  data: PropTypes.object,
};

ArticlesListBlock.displayName = 'ArticlesListBlocks';

export default ArticlesListBlock;
