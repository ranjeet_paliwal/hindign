/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable consistent-return */
/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ReactHtmlParser from 'html-react-parser';
import Thumb from '../../../components/Thumb/Thumb';

const Config = require(`../../../../common/${process.env.SITE}`);

class NicArticleContainer extends PureComponent {
  constructor(props) {
    const { id } = props;
    super(props);
    this.renderArticleHtml = this.renderArticleHtml.bind(this);
    this.state = {
      nicartid: id,
      items: null,
      loading: true,
    };
  }

  componentDidMount() {
    const { nicartid } = this.state;
    fetch(
      `${
        process.env.WEBSITE_URL
      }/pwafeeds/webgn_articleshow/${nicartid}.cms?feedtype=sjson&version=v9`,
    )
      .then(response => response.json())
      .then(result => {
        this.setState({
          items: result,
          loading: false,
        });
      })
      .catch(() => {});
  }

  renderArticleHtml() {
    const { items } = this.state;
    if (items) {
      return ReactHtmlParser(items.Story, {
        replace: domNode => {
          if (!domNode.attribs) return;
          const { name } = domNode;
          switch (name) {
            case 'img':
              // eslint-disable-next-line consistent-return
              return (
                <div className="image">
                  <Thumb
                    src={
                      domNode.attribs &&
                      domNode.attribs.src &&
                      `${process.env.IMG_URL}/${domNode.attribs.src}`
                    }
                  />
                  {domNode.attribs.title ? (
                    <div className="imagecaption">{domNode.attribs.title}</div>
                  ) : null}
                </div>
              );

            case 'i':
              return <br />;

            default:
              return '';
          }
        },
      });
    }
    return null;
  }

  render() {
    const { closenic } = this.props;
    const { items, loading } = this.state;
    return (
      <div className="enable_NIC_overlay">
        <div className="body_overlay" />
        <div style={{ right: 0 }} className="nic-body">
          {loading ? (
            <span className="nic-loader">
              <img src={`${process.env.IMG_PATH}/photo/42715392.cms`} alt="" />
            </span>
          ) : (
            ''
          )}
          <span
            className="close_icon"
            onClick={event => closenic(event)}
            role="button"
            tabIndex={0}
          />
          <h5 className="nic_head">संदर्भ</h5>
          <h1>{items && items.hl ? items.hl : ''}</h1>
          <h2 className="artsyn">{items && items.syn ? items.syn : ''}</h2>
          <div className="article_datetime">
            {(items && items.ag) || ''}
            <time dateTime="2018-07-02T08:30:24+05:30">{items && items.dl}</time>
          </div>
          <div className="image">
            {items && items.image[0] ? (
              <Thumb
                imgId={items.image[0].id}
                imgSize={items.image[0].id}
                width="400"
                height="300"
                alt={items.image[0].cap}
                title={items.image[0].cap}
              />
            ) : (
              ''
            )}
          </div>
          <React.Fragment>
            {items && items.embedhighlights && items.embedhighlights.artsummary ? (
              <ArticleHighlights
                data={
                  items.embedhighlights &&
                  items.embedhighlights.artsummary &&
                  items.embedhighlights.artsummary.ul
                }
              />
            ) : (
              ''
            )}
          </React.Fragment>
          {this.renderArticleHtml()}
        </div>
      </div>
    );
  }
}
const ArticleHighlights = ({ data }) => {
  return data && Array.isArray(data) ? (
    <React.Fragment>
      <div className="highlights">
        {Config.Locale.articleshow.highlights}
        <ul>
          {data.map(item => (
            <li key={item}>{item}</li>
          ))}
        </ul>
      </div>
    </React.Fragment>
  ) : null;
};

ArticleHighlights.propTypes = {
  data: PropTypes.array,
};

export default NicArticleContainer;
