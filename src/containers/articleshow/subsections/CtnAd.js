import React from 'react';
import PropTypes from 'prop-types';

const CtnAd = ({ articleId, position, slotId, style, className, height, width }) => {
  const plachlderId = articleId
    ? `div-clmb-ctn-${slotId}-1-${articleId}`
    : `div-clmb-ctn-${slotId}-1-${position}`;
  return (
    <div
      className={className}
      data-adheight={height}
      data-adwidth={width}
      data-section="tech"
      data-slot={slotId}
      data-position={position || 1}
      style={style}
      id={plachlderId}
    />
  );
};

CtnAd.propTypes = {
  articleId: PropTypes.string,
  slotId: PropTypes.string,
  style: PropTypes.object,
  className: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
  position: PropTypes.number,
};

export default CtnAd;
