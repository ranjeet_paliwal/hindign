import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

const Config = require(`../../../../common/${process.env.SITE}`);

class TopComment extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      commentShown: false,
    };
  }

  toggleHideComment = () => {
    const { commentShown } = this.state;
    this.setState({
      commentShown: !commentShown,
    });
  };

  render() {
    const { commentShown } = this.state;
    const { data, showComments } = this.props;
    if (!data) {
      return null;
    }
    if (Array.isArray(data) && data.length > 0) {
      let name;
      let commentText;
      let heading;
      if (data[0] && data[0].authorsComment && data[0].authorsComment.length > 0) {
        heading = Config.ArticleShow.text_author_comment;
        if (data[0].authorsComment) {
          const authCmt = data[0];

          if (authCmt.authorsComment[0].C_A_ID) {
            commentText = authCmt.authorsComment[0].C_T;
            name = authCmt.authorsComment[0].A_D_N;
          } else if (typeof authCmt.authorsComment[0].CHILD === 'object') {
            for (let g = 0; g < 2; g++) {
              if (authCmt.authorsComment[0].CHILD[g].C_A_ID) {
                commentText = authCmt.authorsComment[0].CHILD[g].C_T;
                name = authCmt.authorsComment[0].CHILD[g].A_D_N;
                break;
              }
            }
          }
        }
      } else if (typeof data[1] === 'object') {
        const cmtData = data[1];
        name = data[1].A_D_N;
        // LENGTH OF NULL ISSUE POSSIBLE (LNULL)
        commentText = data[1].C_T;
        if (cmtData.AC_A_C > 0) {
          heading = Config.ArticleShow.text_topcomment;
        } else {
          heading = Config.ArticleShow.text_latestcomment;
        }
      }

      const cmtLen = 180;
      return (
        <div className="wdt_top_comments">
          <div className="cmt_head">{heading}</div>
          <div className={`cmt_txt ${commentText.length > cmtLen ? 'topcomment_container' : ''}`}>
            {commentText.length > 180 ? (
              <React.Fragment>
                {commentShown ? commentText : `${commentText.substr(0, cmtLen)}...`}
                <small onClick={this.toggleHideComment}>{commentShown ? '-' : '+'}</small>
              </React.Fragment>
            ) : (
              commentText
            )}
          </div>
          <div className="author_name">{name}</div>
          <div className="btn-allcomment">
            <a href="javascript:void(0)" onClick={() => showComments()}>
              {Config.ArticleShow.text_see_all_comments}
            </a>
          </div>
          <div className="btn-wrcomment">
            <a href="javascript:void(0)" onClick={() => showComments()}>
              {Config.ArticleShow.text_write_comment}
            </a>
          </div>
        </div>
      );
    }
    return null;
  }
}

TopComment.propTypes = {
  data: PropTypes.object,
  showComments: PropTypes.func,
};

export default TopComment;
