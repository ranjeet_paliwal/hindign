/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable react/prop-types */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Thumb from '../../../components/Thumb/Thumb';
import { analyticsGA, AnalyticsComscore } from '../../../analytics';
import { getCookie, deleteCookie } from '../../../common-utility';
import { stopDockedVideo } from '../../../actions/video';
import '../../../components/MiniTV/MiniTV.scss';

class Videocommon extends PureComponent {
  constructor(props) {
    super(props);
    this.Playvideo = this.Playvideo.bind(this);
  }

  componentDidMount() {
    const { slikeid, autoplay } = this.props;
    if (autoplay) {
      this.Playvideo(slikeid);
    }
  }

  componentDidUpdate(prevProps) {
    const { slikeid, isPlaying } = this.props;
    if ((!prevProps.isPlaying && isPlaying) || prevProps.slikeid !== slikeid) {
      // eslint-disable-next-line react/no-did-update-set-state
      // this.setState({ slikeid }, () => this.Playvideo(slikeid));
      this.Playvideo(slikeid);
    }
  }

  Disabledock = sid => {
    const { dispatch, seolocation, articleID } = this.props;
    dispatch(stopDockedVideo());
    const placeHolderId = `DocId_${sid}_${articleID}`;
    const placeHolderIdInner = `DocId_${sid}_${articleID}_inner`;
    // document.getElementById(sid).classList.remove('stickycls');
    document.getElementById(sid).classList.remove('IsDockGafired');

    if (document.getElementById(placeHolderId) !== null) {
      document.getElementById(placeHolderId).classList.remove('disable_video');
    }
    if (document.getElementById(placeHolderIdInner) !== null) {
      document.getElementById(placeHolderIdInner).classList.remove('disable_video');
    }
    analyticsGA.event('PIP Close', 'articleshow', `${seolocation}`);
    // enable notificaton popups while PIP stop playing
    if (getCookie('tpstoryPopup') !== undefined) {
      deleteCookie('tpstoryPopup', '.indiatimes.com');
    }

    if (
      typeof window !== 'undefined' &&
      window.player &&
      typeof window.player.destroy === 'function'
    ) {
      window.player.destroy();
    }
  };

  Playvideo = vid => {
    const {
      vidtitle,
      adsection,
      seolocation,
      pagetype,
      sectionId,
      skipAd,
      autoplaynextvideo,
    } = this.props;
    const self = this;
    const checkVideoType = vid.includes('YT') ? '_youtube' : '';
    const evtAction = `user-initiated/${seolocation}${checkVideoType}`;
    const evtLabel = `${pagetype}/flash/ADAPTIVE`;
    window.SPL = window.SPL || {};
    window.SPL.config = {
      sdk: {
        apikey: 'nbtweba5ec97054033e061',
      },
    };
    const jsWebUrl =
      typeof window !== 'undefined' && window.location
        ? `${window.location.protocol}//${window.location.host}`
        : '';
    // eslint-disable-next-line prefer-const
    let config = {
      page: {
        origin: jsWebUrl,
      },
      player: {
        adSection: adsection,
        isGAPlugin: false,
        isComScorePlugin: false,
        skipAd,
        autoPlay: true,
        startFromSec: -1,
        showQuality: true,
        showNextButton: true,
        fallbackMute: true,
        nextVideoUrl: `${process.env.BASE_URL}/videpostroll_v5_slike/${sectionId ||
          '4901865'}.cms?feedtype=json&callback=cached`,
      },

      controls: {
        controlsType: 'custom',
        showFullScreen: true,
        showAutoPlayNext: true,
        showShare: true,
        volume: 80,
        mute: false,
        autoPlayNext: autoplaynextvideo !== false,
        showNextButton: true,
        showPrevButton: true,
        showDimIcon: false,
        showWatermark: true,
        showQuality: true,
        showVolumeSeekBar: true,
        preview: true,
        embedPlaylist: true,
        eneblePIP: false,
        showPipIcon: false,
      },
      video: {
        id: vid,
        image: '',
        title: vidtitle,
      },
      events: {
        onPlayerEvent(evtname) {
          switch (evtname) {
            case 'onInit':
              analyticsGA.event('VIDEOAFTERSLOAD', evtAction, evtLabel);
              break;
            case 'onReady':
              analyticsGA.event('VIDEOREQUEST', evtAction, evtLabel);
              break;
            case 'onPlayStart':
              setTimeout(() => {
                AnalyticsComscore.invokeComScore();
                analyticsGA.event('VIDEOVIEW', evtAction, evtLabel);
              }, 2000);
              break;
            case 'onComplete':
              analyticsGA.event('VIDEOCOMPLETE', evtAction, evtLabel);
              break;
            default:
          }
        },
        onEndScreenReplay() {},
        onEndScreenPlay(data) {
          analyticsGA.event('NEWNEXTVIDEOREQUEST', `user-initiated${data.seopath}`, evtLabel);
          if (self.props.pagetype === 'videoshow') {
            self.props.changeVideoShowUrl(data);
          }
        },
        onEndScreenAutoPlay(data) {
          self.setState({
            vidtitle: data.title,
            seolocation: data.seopath,
          });
          analyticsGA.event('NEWNEXTVIDEOREQUEST', `user-initiated${data.seopath}`, evtLabel);
          if (self.props.pagetype === 'videoshow') {
            self.props.changeVideoShowUrl(data);
          }
        },
        onAdEvent(evtname) {
          switch (evtname.state) {
            case 'adPlayed':
              analyticsGA.event('ADVIEW', evtAction, evtLabel);
              break;
            case 'complete':
              analyticsGA.event('ADCOMPLETE', evtAction, evtLabel);
              break;
            default:
          }
        },
      },
    };

    function retry() {
      const retryInterval = 100;
      if (typeof window.S !== 'undefined' && typeof window.S.load === 'function') {
        window.S.load(`playerContainer${vid}`, config, inst => {
          window.player = inst;
        });
      } else {
        setTimeout(() => {
          retry();
        }, retryInterval);
      }
    }
    retry();
  };

  render() {
    // const { slikeid } = this.state;
    const { imageid, imgsize, height, width, imgsrc, slikeid } = this.props;

    return (
      <div className="thumb_video" id={slikeid}>
        <div className="posrel_mini" style={{ height: `${height}px`, width: `${width}px` }}>
          {/* <span
            className="close_icon"
            id={`closebtn_${slikeid}`}
            onClick={() => this.Disabledock(slikeid)}
            role="button"
            tabIndex={0}
          /> */}

          <div id={`outerPlayer${slikeid}`} className="outerPlayer">
            <div id={`playerContainer${slikeid}`} className="playerContainer">
              <Thumb
                imgId={imageid}
                width={width}
                height={height}
                alt={imageid}
                imgSize={imgsize}
                src={imgsrc}
              />

              <div
                className="videoicons"
                onClick={() => this.Playvideo(slikeid)}
                role="button"
                tabIndex={0}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Videocommon.propTypes = {
  imageid: PropTypes.string,
  imgsize: PropTypes.string,
  height: PropTypes.string,
  width: PropTypes.string,
  slikeid: PropTypes.string,
  seolocation: PropTypes.string,
  vidtitle: PropTypes.string,
  adsection: PropTypes.string,
};

export default Videocommon;
