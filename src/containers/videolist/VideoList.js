/* eslint-disable indent */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import LazyLoad from 'react-lazyload';

import { fetchDataIfNeeded, updateListData, updateNavData } from '../../actions/videolist';
import {
  adsPlaceholder,
  getDifferenceInHours,
  addToWatchLater as addVideoToWatchLater,
  removeFromWatchLater,
} from '../../common-utility';
import { analyticsGA, AnalyticsComscore, AnalyticsIBeat } from '../../analytics';
import ArticleListFakeLoader from '../../components/Loaders/ArticleListFakeLoader';
import { initGoogleAds } from '../../ads/dfp';
import BreadCrumbs from '../../components/BreadCrumb';
import './Videolist.scss';
import '../../public/css/commonComponent.scss';
import Link from '../../components/Link/Link';
import Thumb from '../../components/Thumb/Thumb';
import CtnAd from '../articleshow/subsections/CtnAd';

const Config = require(`../../../common/${process.env.SITE}`);

class VideoList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      activepg: '',
      watchLaterVideos: {},
    };
    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    const { dispatch, params, query, data } = this.props;
    const sectionName = 'video';
    VideoList.fetchData({ dispatch, query, params, isForcedUpdate: true });
    this.renderAds(data);

    if (
      data &&
      data.videolistData &&
      data.videolistData.pwa_meta &&
      data.videolistData.pwa_meta.ibeat
    ) {
      const ibeatvalObj = data.videolistData.pwa_meta.ibeat;
      const ibeatObj = Object.assign(Config.ibeatConfig, ibeatvalObj);
      AnalyticsIBeat.initIbeat(ibeatObj);
    }

    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  componentDidUpdate(prevProps) {
    const { dispatch, params, query, data, location } = this.props;
    const sectionName = 'video';
    if (
      prevProps.location.pathname !== location.pathname ||
      (prevProps.location.search && !location.search)
    ) {
      let isForcedUpdate = false;
      if (prevProps.location.search && !location.search) {
        isForcedUpdate = true;
      }
      VideoList.fetchData({ dispatch, query, params, isForcedUpdate });
      this.setState({
        activepg: '',
      });
    }
    this.renderAds(data);
    dispatch(
      updateNavData({
        sectionName,
      }),
    );
  }

  toggleWatchLater = videoId => {
    const { watchLaterVideos } = this.state;
    const { authentication, showLoginRegister } = this.props;
    const { loggedIn } = authentication;
    if (!loggedIn) {
      showLoginRegister();
      return;
    }
    if (watchLaterVideos[videoId]) {
      // Remove from watch later if it exists
      removeFromWatchLater(videoId);
      this.setState(prevState => ({
        watchLaterVideos: {
          ...prevState.watchLaterVideos,
          [videoId]: !prevState.watchLaterVideos[videoId],
        },
      }));
    } else {
      // Add to watch later
      addVideoToWatchLater(videoId)
        .then(data => {
          this.setState(prevState => ({
            watchLaterVideos: {
              ...prevState.watchLaterVideos,
              [videoId]: true,
            },
          }));
        })
        .catch(e => {});
    }
  };

  showLoaderIfNeeded = () => {
    const { data } = this.props;
    if (data && data.videolistData) {
      return <ArticleListFakeLoader />;
    }
    return null;
  };

  handleClick(event) {
    const { dispatch, data, router } = this.props;
    dispatch(
      updateListData({
        id:
          (data &&
            data.videolistData &&
            data.videolistData.sectioninfo &&
            data.videolistData.sectioninfo.id) ||
          '',
        pgno: Number(event.target.id),
      }),
    );
    document.documentElement.scrollTop = 0;
    const pageUrl =
      typeof document === 'object'
        ? `${document.location.protocol}//${document.location.host}${document.location.pathname}`
        : '';
    const curID = `${pageUrl}?curpg=${event.target.id}`;
    this.setState(
      {
        activepg: Number(event.target.id),
      },
      () => {
        router.replace(curID);
      },
    );

    // window.history.replaceState({}, pageUrl, curID);
  }

  renderAds = data => {
    const adsData = (data && data.adsData && data.adsData.wapads) || '';
    if (adsData) {
      // initGoogleAds(adsData);
      const sectionInfo = {
        pagetype: 'videolist',
        sec1: data && data.videolistData && data.videolistData.sec1,
        sec2: data && data.videolistData && data.videolistData.sec2,
        sec3: data && data.videolistData && data.videolistData.sec3,
        hyp1:
          data &&
          data.videolistData &&
          data.videolistData.pwa_meta &&
          data.videolistData.pwa_meta.hyp1,
      };
      setTimeout(() => {
        initGoogleAds(adsData, sectionInfo);

        if (typeof analyticsGA !== 'undefined' && typeof analyticsGA.pageview !== 'undefined') {
          analyticsGA.pageview();
        }
        if (
          typeof AnalyticsComscore !== 'undefined' &&
          typeof AnalyticsComscore.invokeComScore !== 'undefined'
        ) {
          AnalyticsComscore.invokeComScore();
        }
      }, 0);
    }
  };

  render() {
    const { isFetching, data } = this.props;
    const { activepg, watchLaterVideos } = this.state;
    let renderPageNumbers = '';

    // Logic for displaying page numbers
    if (data && data.videolistData && data.videolistData.pg && data.videolistData.pg.tp) {
      const activePage = Number(data.videolistData.pg.cp);
      const tpFound = parseInt(data.videolistData.pg.tp);
      const totalpage = tpFound > 15 ? 15 : tpFound;
      const pageNumbers = [];
      for (let i = 1; i <= totalpage; i++) {
        pageNumbers.push(i);
      }

      renderPageNumbers = pageNumbers.map(pageNo => {
        const activeClass = pageNo === activepg || pageNo === activePage ? 'active' : '';

        return (
          <a className={activeClass} key={pageNo} id={pageNo} onClick={this.handleClick}>
            {pageNo}
          </a>
        );
      });
    }

    const schema = {
      type: 'application/ld+json',
      innerHTML: data && data.videolistData && data.videolistData.Newsarticle,
    };
    const pwaMeta = (data && data.videolistData && data.videolistData.pwa_meta) || null;
    const videoListData = (data && data.videolistData) || null;
    // console.log('VLDATA', videoListData.sections);

    return (
      <div className="articles_container">
        <Helmet
          title={(pwaMeta && pwaMeta.title) || ''}
          titleTemplate="%s"
          meta={data && data.metaData && data.metaData.metaTags}
          link={data && data.metaData && data.metaData.metaLinks}
          htmlAttributes={{ lang: 'hi' }}
          script={[schema]}
        />

        {isFetching && !data ? (
          <ArticleListFakeLoader />
        ) : (
          <div style={{ overflow: 'hidden' }}>
            {adsPlaceholder('ATF_AS_STRIPPD')}
            {adsPlaceholder('PPD')}
            <BreadCrumbs
              data={videoListData && videoListData.breadcrumb && videoListData.breadcrumb.div}
            />

            {/* <CtnAd
              articleId={videoListData && videoListData.id}
              height="40"
              width="1000"
              className="colombia"
              slotId={Config.CTN.stripped_pbd_article_show}
            /> */}

            <div className="contentarea VL">
              <div className="vdlist">
                <h1>
                  {videoListData && videoListData.sectioninfo && videoListData.sectioninfo.secname}
                </h1>
                <ul>
                  {videoListData && videoListData.items && Array.isArray(videoListData.items)
                    ? videoListData.items.map((data, index) => {
                        const random = Math.round(Math.random() * 10000);
                        // console.log('random', random);

                        return data && data.type === 'ctn' ? (
                          <li className="section">
                            <CtnAd
                              position={random}
                              className="colombia"
                              slotId={Config.CTN.videolistCtn}
                            />
                          </li>
                        ) : (
                          <li key={data.id} className="inline-slide">
                            <small className="duration">{data.du}</small>
                            <Thumb
                              width="174"
                              height="134"
                              imgId={data.imageid}
                              title={data.hl}
                              alt={data.hl}
                              islinkable="true"
                              link={data.wu || ''}
                              resizeMode="4"
                              className="img"
                            />
                            <div className="vidtxt">
                              <Link className="slidelink text_ellipsis" to={data.wu || ''}>
                                {data.hl}
                              </Link>
                            </div>

                            <span className="newsdate">
                              {getDifferenceInHours(data.lu, Date.now())}
                            </span>
                            <span
                              title="Add to watch later"
                              className={
                                this.state.watchLaterVideos[data.id]
                                  ? 'active watchlater'
                                  : 'watchlater'
                              }
                              onClick={() => this.toggleWatchLater(data.id)}
                            />
                          </li>
                        );
                      })
                    : null}
                </ul>
                <div className="pgno">{renderPageNumbers}</div>
              </div>
            </div>
          </div>
        )}

        <div id="AL_Innov1" className="AL_Innov1" />
        <div id="WSK_160_AS" className="WSK_160_AS" />
      </div>
    );
  }
}

VideoList.fetchData = function fetchData({ dispatch, query, params, isForcedUpdate }) {
  return dispatch(fetchDataIfNeeded({ query, params, isForcedUpdate }));
};

VideoList.propTypes = {
  dispatch: PropTypes.func,
  isForcedUpdate: PropTypes.func,
  showLoginRegister: PropTypes.func,
  authentication: PropTypes.object,
  params: PropTypes.object,
  query: PropTypes.object,
  history: PropTypes.object,
  data: PropTypes.object,
  isFetching: PropTypes.bool,
};

function mapStateToProps(state) {
  return {
    ...state.videolist,
    authentication: state.authentication,
    newsDataFetched: state.news.didFetch,
    isFetchingNewsData: state.news.isFetching,
  };
}

export default connect(mapStateToProps)(VideoList);
