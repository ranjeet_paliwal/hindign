import React from 'react';
import { IndexRoute, Route, Redirect } from 'react-router';

import App from './containers/app/App';
import IndexLoadable from './containers/index/IndexLoadable';
import NotFound from './containers/notfound/NotFound';
import SsoRedirect from './components/LoginRegister/SsoRedirect';
import ComparisonLoadable from './containers/Comparison/List/ComparisonLoadable';
import TrendsComparisonLoadable from './containers/Comparison/Trends/TrendsComparisonLoadable';
import ComparisonDetailsLoadable from './containers/Comparison/Detail/ComparisonDetailsLoadable';
import ArticleshowLoadable from './containers/articleshow/articleshowLoadable';
import ArticlelistLoadable from './containers/articlelist/articlelistLoadable';
import ReviewlistLoadable from './containers/reviewlist/reviewlistLoadable';
import videoListLoadable from './containers/videolist/VideoListLoadable';
import VideoShowLoadable from './containers/videoshow/VideoShowLoadable';
import photoListLoadable from './containers/photolist/PhotoListLoadable';
import PhotoShowLoadable from './containers/photoshow/PhotoShowLoadable';
import GadgetslistLodable from './containers/Gadgetlist/GadgetslistLodable';
import StatusCheck from './components/StatusCheck/StatusCheck';
import gadgetshowLoadable from './containers/gadgetshow/gadgetshowLoadable';

export default (
  <React.Fragment>
    <Route path="/status" component={StatusCheck} />
    <Route path="/" component={App}>
      <IndexRoute component={IndexLoadable} />
      <Route path="/default.cms" component={IndexLoadable} />

      <Route path="/tech" component={IndexLoadable} />
      <Route path="/tech/reviews.cms" component={ReviewlistLoadable} />
      <Redirect from="/tech/tech/reviews.cms" to="/tech/reviews.cms" />
      <Redirect from="/tech/tecs/reviews.cms" to="/tech/reviews.cms" />
      <Route path="/tech/login" component={SsoRedirect} />
      <Route path="/tech/login.cms" component={SsoRedirect} />
      <Route path="/login.cms" component={SsoRedirect} />
      <Route path="/login" component={SsoRedirect} />
      <Route
        path="/tech/compare-:device/popular-comparisons"
        component={TrendsComparisonLoadable}
      />
      <Route path="/tech/compare-:device/latest-comparisons" component={TrendsComparisonLoadable} />
      <Route path="/tech/compare-:device" component={ComparisonLoadable} />
      <Route path="/tech/compare-:device/:qstring" component={ComparisonDetailsLoadable} />
      <Route path="/(*/)tech/compare.cms" component={ComparisonLoadable} />
      <Route path="/**/compare.cms" component={ComparisonLoadable} />
      <Route path="/compare.cms" component={ComparisonLoadable} />
      <Route path="/tech/compare-:device/latest-comparisons" component={TrendsComparisonLoadable} />

      {/* compare-mobile-phones/Xiaomi-Redmi-Note-6-Pro-vs-Xiaomi-Redmi-Note-8-Pro */}
      <Route path="/navbharattimes/nbthomepage_rprod.htm" component={IndexLoadable} />
      <Route path="/navbharattimes/nbthomepage.htm" component={IndexLoadable} />
      <Route path="/navbharattimesmum/nbthomepage.htm" component={IndexLoadable} />

      <Route path="*/articleshow/:id.cms" component={ArticleshowLoadable} />
      <Route path="*/articlelist/:articleId.cms" component={ArticlelistLoadable} />

      <Route pageType="VS" path="/*videoshow/:id.cms" component={VideoShowLoadable} />
      <Route pageType="VL" path="/*videolist/:id.cms" component={videoListLoadable} />
      <Redirect from="/photolist/:id.cms" to="*/photoarticlelist/:id.cms" />

      <Route pageType="PS" path="/*photoshow/:id.cms" component={PhotoShowLoadable} />
      <Route pageType="PL" path="*/photoarticlelist/:id.cms" component={photoListLoadable} />
      <Route path="/tech/(upcoming-):category" component={GadgetslistLodable} />
      <Route path="/tech/(upcoming-):category/filters/:filters" component={GadgetslistLodable} />
      <Route
        path="/tech/:category/(i-smart)(hi-tech)(sony-ericsson)(t-mobile)"
        component={GadgetslistLodable}
      />

      <Route path="/tech/:category/*-*" component={gadgetshowLoadable} />
      <Route path="/tech/(upcoming-):category/:brand" component={GadgetslistLodable} />

      <Route path="*" component={NotFound} />
    </Route>
  </React.Fragment>
);

export { NotFound as NotFoundComponent };
